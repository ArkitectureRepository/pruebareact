Pod::Spec.new do |s|
  s.name             = 'PruebaReact'
  s.version          = '1.0'
  s.summary          = 'Something like summary'

  s.description      = <<-DESC
Something biggest for the description.
                       DESC

  s.homepage         = 'https://gitlab.com/ArkitectureRepository/pruebareact.git'
  #s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.authors          = 'Alvaro Royo'
  s.source           = { :git => 'https://gitlab.com/ArkitectureRepository/pruebareact.git' }

  s.ios.deployment_target = '9.0'
  s.swift_version = '5.0'
  s.static_framework = true

  #s.xcconfig = { 'SWIFT_OBJC_BRIDGING_HEADER' => 'PruebaReact-Bridging-Header.h' }
  s.source_files = '*.{swift,h,m}'
  s.resource_bundle = { 'ReactBundle' => 'main.jsbundle' }

  s.dependency 'FBLazyVector'
  s.dependency 'FBReactNativeSpec'
  s.dependency 'RCTRequired'
  s.dependency 'RCTTypeSafety'
  s.dependency 'React'
  s.dependency 'React-Core'
  s.dependency 'React-CoreModules'
  s.dependency 'React-Core/DevSupport'
  s.dependency 'React-RCTActionSheet'
  s.dependency 'React-RCTAnimation'
  s.dependency 'React-RCTBlob'
  s.dependency 'React-RCTImage'
  s.dependency 'React-RCTLinking'
  s.dependency 'React-RCTNetwork'
  s.dependency 'React-RCTSettings'
  s.dependency 'React-RCTText'
  s.dependency 'React-RCTVibration'
  s.dependency 'React-Core/RCTWebSocket'

  s.dependency 'React-cxxreact'
  s.dependency 'React-jsi'
  s.dependency 'React-jsiexecutor'
  s.dependency 'React-jsinspector'
  s.dependency 'ReactCommon/callinvoker'
  s.dependency 'ReactCommon/turbomodule/core'
  s.dependency 'Yoga'

  s.dependency 'DoubleConversion'
  s.dependency 'glog'
  s.dependency 'Folly'

  s.dependency 'GlobileAppEngine'
  s.dependency 'BranchLocator'
end
