//
//  ShowLocationVC.swift
//  PruebaReact
//
//  Created by Alvaro Royo on 08/04/2020.
//

import UIKit
import BranchLocator

class ShowLocationVC: UIViewController {
  
  let cancelBtn = UIButton()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.view.backgroundColor = .white
    
    self.view.addSubview(cancelBtn)
    cancelBtn.setTitle("Cancel", for: .normal)
    cancelBtn.setTitleColor(.systemBlue, for: .normal)
    cancelBtn.addTarget(self, action: #selector(cancel), for: .touchUpInside)
    cancelBtn.translatesAutoresizingMaskIntoConstraints = false
    if #available(iOS 11.0, *) {
      cancelBtn.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 15).isActive = true
    } else {
      cancelBtn.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 15).isActive = true
    }
    cancelBtn.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 15).isActive = true
    
    let container = UIView()
    self.view.addSubview(container)
    container.translatesAutoresizingMaskIntoConstraints = false
    container.topAnchor.constraint(equalTo: self.cancelBtn.bottomAnchor, constant: 15).isActive = true
    container.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
    container.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
    if #available(iOS 11.0, *) {
      container.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    } else {
      container.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }
    
    let viewController = MapRouter.createModule(availableFilters: nil, navigationMapsList: [.appleMaps])
    self.addChild(viewController)
    container.addSubview(viewController.view)
    viewController.view.translatesAutoresizingMaskIntoConstraints = false
    
    viewController.view.topAnchor.constraint(equalTo: container.topAnchor, constant: 0).isActive = true
    viewController.view.leadingAnchor.constraint(equalTo: container.leadingAnchor).isActive = true
    viewController.view.trailingAnchor.constraint(equalTo: container.trailingAnchor).isActive = true
    viewController.view.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
    
  }
  
  @objc func cancel() {
    self.dismiss(animated: true, completion: nil)
  }
  
}
