//
//  RNBridge.m
//  PruebaReact
//
//  Created by Alvaro Royo on 06/04/2020.
//

#import <Foundation/Foundation.h>
#import "React/RCTBridgeModule.h"
#import "React/RCTEventEmitter.h"

@interface RCT_EXTERN_MODULE(RNBridge, RCTEventEmitter)

RCT_EXTERN_METHOD(constantsToExport)
RCT_EXTERN_METHOD(showAlert)
RCT_EXTERN_METHOD(saveData: (id))

@end
