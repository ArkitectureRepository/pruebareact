//
//  RNBridge.swift
//  PruebaReact
//
//  Created by Alvaro Royo on 06/04/2020.
//

import Foundation
import GlobileAppEngine
import SantanderUIKitLib

@objc(RNBridge)
class RNBridge: RCTEventEmitter {
  
  static let RNBridgeDidLoadNotification = NSNotification.Name("RNBridgeDidLoadNotification")
  
  @objc private(set) static var standard: RNBridge?
  
  private override init() {
    super.init()
    RNBridge.standard = self
    NotificationCenter.default.post(name: RNBridge.RNBridgeDidLoadNotification, object: nil)
  }
  
  @objc
  override func supportedEvents() -> [String]! {
    return ["onLoad"]
  }
  
  @objc
  override static func requiresMainQueueSetup() -> Bool {
    return true
  }
  
  @objc
  override func constantsToExport() -> [AnyHashable : Any]! {
    let name = (try? DataBus.shared.load(forKey: "user", withType: String.self)) ?? ""
    return ["name":name]
  }
  
}

//MARK: - Send methods
extension RNBridge {
  
}

//MARK: - Accessible Methods
extension RNBridge {
  @objc func showAlert() {
    DispatchQueue.main.async {
      let name = (try? DataBus.shared.load(forKey: "user", withType: String.self)) ?? ""
      let alert = GlobileModal("Hello \(name) from native!", nil, [.init(title: "Accept", style: .primary, action: nil)])
      UIApplication.getTopViewController()?.present(alert, animated: true, completion: nil)
    }
  }
  
  @objc func saveData(_ data: Any) {
    guard let dic = data as? [String: String],
    let search = dic["search"]
    else { return }
    
    try? DataBus.shared.save(object: search, forKey: "BLSearch")
    
    DispatchQueue.main.async {
      let vc = ShowLocationVC()
      UIApplication.getTopViewController()?.present(vc, animated: true, completion: nil)
    }
  }
}
