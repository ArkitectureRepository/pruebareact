//
//  MainViewController.swift
//  PruebaReact
//
//  Created by Alvaro Royo on 06/04/2020.
//

import UIKit

@objc(MainViewController)
class MainViewController: UIViewController, RCTBridgeDelegate {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    NotificationCenter.default.addObserver(forName: RNBridge.RNBridgeDidLoadNotification, object: nil, queue: nil) { (notification) in
      self.didLoadReact()
    }
    
    let bridge = RCTBridge(delegate: self, launchOptions: nil)!
    let rootView = RCTRootView(bridge: bridge, moduleName: "PruebaReact", initialProperties: nil)
    self.view = rootView
    
  }
  
  func didLoadReact() {
    
  }
  
  func sourceURL(for bridge: RCTBridge!) -> URL! {
    let bundleUrl = Bundle(for: Self.self).url(forResource: "ReactBundle", withExtension: "bundle")
    let bundle = Bundle(url: bundleUrl!)?.url(forResource: "main", withExtension: "jsbundle")
    return bundle
//    return RCTBundleURLProvider.sharedSettings()?.jsBundleURL(forBundleRoot: "index", fallbackResource: nil)
  }
  
}
