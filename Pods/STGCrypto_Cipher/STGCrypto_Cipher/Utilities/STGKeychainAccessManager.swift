import STGKeychain_Access

public class STGKeyChainAccessManager {
    public let keychainAccess: STGKeychainAccessProtocol!

    public init(keychainAccess: STGKeychainAccessProtocol = STGKeychainAccess()) {
        self.keychainAccess = keychainAccess

        if !UserDefaults.standard.bool(forKey: "launchedBefore") {
            _ = purgeSecretKey()
            print("The secret key failed to be removed (or has never been created yet)")
            UserDefaults.standard.set(true, forKey: "launchedBefore")
        }
    }

    public func isHandshakeDone() -> Bool {
        var handShakeDone = false
        if let _: Data = self.keychainAccess.retrieveKeychainValue(key: STGKeyChainAlias.SKAlia.rawValue, promptTitle: "We need to authenticate you to retrieve the key") {
            handShakeDone = true
        }

        return handShakeDone
    }

    public func secretKey() -> Data? {
        if let secret: Data = self.keychainAccess.retrieveKeychainValue(key: STGKeyChainAlias.SKAlia.rawValue, promptTitle: "We need to authenticate you to retrieve the key") {
            return secret
        }
        return nil
    }

    public func purgeSecretKey() -> Bool {
        return keychainAccess.removeKeychainValue(key: STGKeyChainAlias.SKAlia.rawValue)
    }
}
