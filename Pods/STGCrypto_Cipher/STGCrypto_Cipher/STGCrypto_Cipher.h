#import <UIKit/UIKit.h>
#import "GMEllipticCurveCrypto.h"
#import "GMEllipticCurveCrypto+hash.h"

//! Project version number for STGCrypto_Cipher.
FOUNDATION_EXPORT double STGCrypto_CipherVersionNumber;

//! Project version string for STGCrypto_Cipher.
FOUNDATION_EXPORT const unsigned char STGCrypto_CipherVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <STGCrypto_Cipher/PublicHeader.h>
