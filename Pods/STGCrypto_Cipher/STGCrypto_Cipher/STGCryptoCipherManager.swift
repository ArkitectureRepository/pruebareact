import STGKeychain_Access

public class STGCryptoCipherManager {
    private let ivData: Data!
    private let cryptoCipher: STGCryptoCipherProtocol!
    let keyChainAccessManager: STGKeyChainAccessManager!

    public init(cryptoCipher: STGCryptoCipherProtocol = STGCryptoCipher(), keyChainAccessManager: STGKeyChainAccessManager = STGKeyChainAccessManager()) {
        self.cryptoCipher = cryptoCipher
        self.keyChainAccessManager = keyChainAccessManager
        ivData = self.cryptoCipher.generateRandomBytes(blockSize: 12)
    }

    public func encryptData(data: String) -> (ivData: Data?, encryptedData: Data?) {
        guard let secret = self.keyChainAccessManager.secretKey() else {
            return (nil, nil)
        }

        do {
            let encryptedData = try cryptoCipher.aesGCMEncrypt(data: data.data(using: .utf8)!, key: secret, iv: ivData)
            return (ivData, encryptedData)
        } catch _ {
            print("Could not encrypt data")
        }

        return (nil, nil)
    }

    public func decryptData(text: String, iv: String) -> String? {
        let textData: Data = Data(hex: text)

        guard let secret = self.keyChainAccessManager.secretKey() else {
            return nil
        }

        guard let ivFactorData = Data(base64Encoded: iv) else {
            return nil
        }

        do {
            let decryptedData = try cryptoCipher.aesGCMDecrypt(data: textData, key: secret, iv: ivFactorData)
            return String(data: decryptedData, encoding: .utf8)
        } catch _ {
            print("Could not decrypt data")
        }

        return nil
    }

    public func handshakeDoneSuccessfully(text: String, iv: String) -> Bool {
        if let _: String = self.decryptData(text: text, iv: iv) {
            return true
        }
        return false
    }
}
