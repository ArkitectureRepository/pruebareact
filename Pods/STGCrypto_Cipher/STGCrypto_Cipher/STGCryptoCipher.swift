//
//  STGCryptoCipher.swift
//  STGCrypto_Cipher
//
//  Created by Eduardo Nieto Delgado on 04/04/2019.
//  Copyright © 2019 ProjectOrganization. All rights reserved.
//

import Foundation
import CommonCrypto

enum AESGCMError: Error {
    case emptyDataError(String)
}

enum AESCBCError: Error {
    case keyError((String, Int))
    case iVError((String, Int))
    case cryptoError((String, Int))
}


public protocol STGCryptoCipherProtocol: class {
    func generateRandomBytes(blockSize: UInt8) -> Data?
    func aesGCMEncrypt(data: Data, key: Data, iv: Data) throws -> Data
    func aesGCMDecrypt(data: Data, key: Data, iv: Data) throws -> Data
    func aesCBCEncrypt(data: Data, key: Data, iv: Data) throws -> Data
    func aesCBCDecrypt(data: Data, key: Data, iv: Data) throws -> Data
    func encryptSHA256(string: String) -> String
}

public class STGCryptoCipher: STGCryptoCipherProtocol {
    
    public init() {
        
    }
    
    public func generateRandomBytes(blockSize: UInt8) -> Data? {
        let size = Int(blockSize)
        var keyData = Data(count: size)
        let result = keyData.withUnsafeMutableBytes { mutableBytes -> Int32 in
            
            SecRandomCopyBytes(kSecRandomDefault, size, mutableBytes)
        }
        
        if result == errSecSuccess {
            return keyData
        } else {
            return nil
        }

    }

    public func aesGCMEncrypt(data: Data, key: Data, iv: Data) throws -> Data {
        do {
            let gcm = GCM(iv: iv.bytes, mode: .combined)
            let aes = try AES(key: key.bytes, blockMode: gcm, padding: .noPadding)
            let cryptData = try aes.encrypt(data.bytes)
            return NSData(bytes: cryptData, length: cryptData.count) as Data
        } catch {
              throw error
        }
        
    }
    
    public func aesGCMDecrypt(data: Data, key: Data, iv: Data) throws -> Data {

        do {
            let gcm = GCM(iv: iv.bytes, mode: .combined)
            let aes = try AES(key: key.bytes, blockMode: gcm, padding: .noPadding)
            let decryptData = try aes.decrypt(data.bytes)
            return NSData(bytes: decryptData, length: decryptData.count) as Data
        } catch {
            throw error

        }
    }
    
    public func aesCBCEncrypt(data: Data, key: Data, iv: Data) throws -> Data {
        let keyLength = key.count
        let validKeyLengths = [kCCKeySizeAES128, kCCKeySizeAES192, kCCKeySizeAES256]
        if validKeyLengths.contains(keyLength) == false {
            throw AESCBCError.keyError(("Invalid key length", keyLength))
        }
        
        let cryptLength = size_t(data.count + kCCBlockSizeAES128)
        var cryptData = Data(count: cryptLength)
        
        var numBytesEncrypted: size_t = 0
        let options   = CCOptions(kCCOptionPKCS7Padding)
        let cryptStatus = cryptData.withUnsafeMutableBytes {cryptBytes in
            data.withUnsafeBytes {dataBytes in
                iv.withUnsafeBytes {ivBytes in
                    key.withUnsafeBytes {keyBytes in
                        CCCrypt(CCOperation(kCCEncrypt),
                                CCAlgorithm(kCCAlgorithmAES),
                                options,
                                keyBytes, keyLength,
                                ivBytes,
                                dataBytes, data.count,
                                cryptBytes, cryptLength,
                                &numBytesEncrypted)
                    }
                }
            }
        }
        
        if UInt32(cryptStatus) == UInt32(kCCSuccess) {
            cryptData.removeSubrange(numBytesEncrypted..<cryptData.count)
        } else {
            throw AESCBCError.cryptoError(("Invalid data encrypted length", Int(UInt32(cryptStatus))))
        }
        
        return cryptData
    }
    
    public func aesCBCDecrypt(data: Data, key: Data, iv: Data) throws -> Data {
        let keyLength = key.count
        let validKeyLengths = [kCCKeySizeAES128, kCCKeySizeAES192, kCCKeySizeAES256]
        if validKeyLengths.contains(keyLength) == false {
            throw AESCBCError.keyError(("Invalid key length", keyLength))
        }
        
        let clearLength = size_t(data.count)
        var clearData = Data(count: clearLength)
        
        var numBytesDecrypted: size_t = 0
        let options   = CCOptions(kCCOptionPKCS7Padding)
        
        let cryptStatus = clearData.withUnsafeMutableBytes {cryptBytes in
            data.withUnsafeBytes {dataBytes in
                iv.withUnsafeBytes {ivBytes in
                    key.withUnsafeBytes {keyBytes in
                        CCCrypt(CCOperation(kCCDecrypt),
                                CCAlgorithm(kCCAlgorithmAES),
                                options,
                                keyBytes, keyLength,
                                ivBytes,
                                dataBytes, clearLength,
                                cryptBytes, clearLength,
                                &numBytesDecrypted)
                    }
                }
            }
        }
        
        if UInt32(cryptStatus) == UInt32(kCCSuccess) {
            clearData.count = numBytesDecrypted
        } else {
            throw AESCBCError.cryptoError(("Decryption failed", Int(cryptStatus)))
        }
        
        return clearData
    }
    
    public func encryptSHA256(string: String) -> String {
        
        return string.sha256()
    }
    
}



