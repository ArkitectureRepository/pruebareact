import STGKeychain_Access

enum STGKeyChainAlias: String {
    case SKAlia
}

public enum STGEllipticCurve: String {
    case STGEllipticCurveSecp256 = "secp256r1"
    case STGEllipticCurveSecp384 = "secp384r1"

    public static var allCurves: [String] {
        return [STGEllipticCurveSecp256.rawValue, STGEllipticCurveSecp384.rawValue]
    }

    public static func curve(curveString: String) -> STGEllipticCurve? {
        switch curveString {
        case "secp256r1":
            return .STGEllipticCurveSecp256
        case "secp384r1":
            return .STGEllipticCurveSecp384
        default:
            return nil
        }
    }

    public func size() -> Int {
        switch self {
        case .STGEllipticCurveSecp256:
            return 256
        case .STGEllipticCurveSecp384:
            return 384
        }
    }

    public func gmEllipticCurve() -> GMEllipticCurve {
        switch self {
        case .STGEllipticCurveSecp256: return GMEllipticCurveSecp256r1
        case .STGEllipticCurveSecp384: return GMEllipticCurveSecp384r1
        }
    }
}

public struct EllipticCurveKeyPair {
    public let publicKey: Data
    public let privateKey: Data

    public init(publicKey: Data, privateKey: Data) {
        self.publicKey = publicKey
        self.privateKey = privateKey
    }
}

public class STGEllipticCurveCryptoManager {
    private let selectedCurve: STGEllipticCurve
    private var crypto: GMEllipticCurveCrypto
    private let keychainAccess: STGKeychainAccessProtocol

    public init(
        curve: STGEllipticCurve,
        existingKeyPair: EllipticCurveKeyPair? = nil,
        keychainAccess: STGKeychainAccessProtocol = STGKeychainAccess()
    ) {
        self.keychainAccess = keychainAccess
        self.selectedCurve = curve

        if let keyPair = existingKeyPair {
            crypto = GMEllipticCurveCrypto.init(curve: curve.gmEllipticCurve())
            crypto.publicKey = keyPair.publicKey
            crypto.privateKey = keyPair.privateKey
        } else {
            crypto = GMEllipticCurveCrypto.generateKeyPair(for: curve.gmEllipticCurve())
        }
    }

    public var keyPair: EllipticCurveKeyPair {
        return EllipticCurveKeyPair(publicKey: crypto.publicKey, privateKey: crypto.privateKey)
    }

    public func createSecret(publicKey: String, curve: STGEllipticCurve, pbdk: String) -> Bool {
        let exportImportManager = CryptoExportImportManager()

        let dert = Data(base64Encoded: publicKey)

        let data = exportImportManager.importPublicKeyFromX509(dert!, curve: curve)

        if let commonSecret = crypto.sharedSecret(forPublicKey: data) {
            let combineSecret = "\(publicKey)\(pbdk)\(commonSecret.base64EncodedString())"

            if let secretData = combineSecret.data(using: .utf8) {
                let finalSecret = secretData.sha256()
                if keychainAccess.saveKeychainValue(key: STGKeyChainAlias.SKAlia.rawValue, data: finalSecret, biometric: false) {
                }
            }
        }
        
        return true
    }

    @available(*, deprecated, message: "Use the method without params instead.")
    public func publicKey(curve: STGEllipticCurve) -> String? {
        guard curve == selectedCurve else {
            preconditionFailure("You are using a different curve than the keyPair generated one.")

        }
        return publicKey()
    }

    public func publicKey() -> String? {
        let keyData: Data = (crypto.publicKey)!

        let exportImportManager = CryptoExportImportManager()

        if let exportablePEMKey = exportImportManager.exportPublicKeyToDER(
            keyData,
            keyType: kSecAttrKeyTypeEC as String,
            keySize: selectedCurve.size()
        ) {
            return exportablePEMKey.base64EncodedString()
        }

        return nil
    }

    public func sign(data: Data) -> Data? {
        return crypto.hashSHA256AndSignDataEncoded(data)
    }
}
