//
//  STDefaultSecureStorage.swift
//  STGSecure_Storage
//
//  Created by Eduardo Nieto Delgado on 14/02/2019.
//  Copyright © 2019 Santander. All rights reserved.
//

import SCAL_iOS

// Default implementation of Secure Storage interface.
public class STGSecureFiles: SCALSecureFilesInterface {

    private let secureFilesDataProvider: STGSecureFilesDataProvider = STGSecureFilesDataProvider()
    
    public init() {}

    public func writeToSecureFile(alias: String, data: Data?, fileMode: STGFileMode, key: Data, initializationVector: Data) -> Bool {
        let function = { try self.secureFilesDataProvider.writeToSecureFile(alias: alias, data: data, fileMode: fileMode, key: key, initializationVector: initializationVector) }
        return try2Bool(function: function, failure: false)
    }
    
    public func writeToSecureFile(alias: String, data: Data?, key: Data, initializationVector: Data) -> Bool {
        let function = { try self.secureFilesDataProvider.writeToSecureFile(alias: alias, data: data, key: key, initializationVector: initializationVector) }
        return try2Bool(function: function, failure: false)
    }
    
    public func clearSecureFile(alias: String, key: Data, initializationVector: Data) -> Bool {
        let function = { try self.secureFilesDataProvider.clearSecureFile(alias: alias, key: key, initializationVector: initializationVector) }
        return try2Bool(function: function, failure: false)
    }
    
    public func readFromSecureFile(alias: String, key: Data, initializationVector: Data) -> Data? {
        let function = { try self.secureFilesDataProvider.readFromSecureFile(alias: alias, key: key, initializationVector: initializationVector) }
        return try2Bool(function: function, failure: nil)
    }
    
    public func generateEncryptionKey() -> Data? {
        return secureFilesDataProvider.generateEncryptionKey()
    }
    
    public func generateEncryptionIV() -> Data? {
        return secureFilesDataProvider.generateEncryptionIV()
    }
    
    public func getEncryptionKey(alias: String) -> Data? {
        return secureFilesDataProvider.getEncryptionKey(alias: alias)
    }
    
    public func getEncryptionIV(alias: String) -> Data? {
        return secureFilesDataProvider.getEncryptionIV(alias: alias)
    }
    
    public func removeSecureFile(alias: String) -> Bool {
        let function = { try self.secureFilesDataProvider.removeSecureFile(alias: alias) }
        return try2Bool(function: function, failure: false)
    }
    
    private func try2Bool<T>(function: () throws -> T, failure: T) -> T {
        do {
            return try function()
        } catch {
            print("Error STGSecureFiles: \(error.localizedDescription)")
            return failure
        }
    }
}
