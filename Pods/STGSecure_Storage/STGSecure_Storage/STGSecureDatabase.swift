//
//  STGSQLCipher.swift
//  STGSecure_Storage
//
//  Created by Eduardo Nieto Delgado on 06/03/2019.
//  Copyright © 2019 Santander. All rights reserved.
//

import SCAL_iOS
import STGCrypto_Cipher
import CommonCrypto

final public class STGSecureDatabase: SCALSecureDatabaseInterface {
    
    enum Error: Swift.Error {
        case randomKeyFailed
        case saveKeyFailed
    }
    
    private let secureDatabaseDataProvider = STGSecureDatabaseDataProvider()
    private let secureFilesDataProvider = STGSecureFilesDataProvider()
    private let statementFormatter = SQLStatementFormatter()
    private let byteGenerator: STGByteGeneratorDataProviderProtocol
    private let cryptoChiper: STGCryptoCipherProtocol
    private var database: OpaquePointer?

    public init(byteGenerator: STGByteGeneratorDataProviderProtocol = STGByteGeneratorDataProvider(), cryptoChiper: STGCryptoCipherProtocol = STGCryptoCipher()) {
        self.byteGenerator = byteGenerator
        self.cryptoChiper = cryptoChiper
    }

    public func initDatabase(name: String) throws {
        let storageKey = "STGDB\(name)"
        let data = try byteGenerator.getRandomBytes(key: storageKey) ?? generateAndSave(name: name, key: storageKey)
        let password = String(decoding: data, as: UTF8.self)
        database = secureDatabaseDataProvider.openSecureDatabase(name: name, password: password)
    }

    public func createSecureTable(query: SCALCreateTableQuery) -> SCALQueryResult {
        let preparedStatement = statementFormatter.format(query: query)
        return queryPreparedStatement(query: preparedStatement, args: nil)
    }

    public func selectFromSecureDatabase(query: SCALSelectQuery) -> SCALQueryResult {
        let preparedStatement = statementFormatter.format(query: query)
        return queryPreparedStatement(query: preparedStatement.0, args: preparedStatement.1)
    }

    public func insertIntoSecureDatabase(query: SCALInsertQuery) -> Int {
        let preparedStatememt = statementFormatter.format(query: query)
        return queryPreparedStatement(query: preparedStatememt.0, args: preparedStatememt.1)
    }

    public func updateFromSecureDatabase(query: SCALUpdateQuery) -> Int {
        let preparedStatement = statementFormatter.format(query: query)
        return queryPreparedStatement(query: preparedStatement.0, args: preparedStatement.1)
    }

    public func deleteFromSecureDatabase(query: SCALDeleteQuery) -> Int {
        let preparedStatement = statementFormatter.format(query: query)
        return queryPreparedStatement(query: preparedStatement.0, args: preparedStatement.1)
    }

    public func removeDatabase(name: String) -> Bool {
        return secureDatabaseDataProvider.removeDatabase(name: name)
    }

    private func queryPreparedStatement(query: String, args: [Any]?) -> SCALQueryResult {
        guard let database = database else { return SCALQueryResult(error: SCALError(code: .databaseError, userInfo: nil), success: false, result: nil) }
        let results = secureDatabaseDataProvider.querySecureDatabase(db: database, query: query, args: args)
        guard let successfulResults = results else { return SCALQueryResult(error: SCALError(code: .databaseError, userInfo: nil), success: false, result: nil) }
        return SCALQueryResult(result: successfulResults)
    }

    private func queryPreparedStatement(query: String, args: [Any]?) -> Int {
        guard let database = database else { return 0 }
        return secureDatabaseDataProvider.modifySecureDatabase(db: database, query: query, args: args)
    }
    
    private func generateAndSave(name: String, key: String) throws -> Data {
        guard let data = cryptoChiper.generateRandomBytes(blockSize: UInt8(kCCBlockSizeAES128)) else { throw Error.randomKeyFailed }
        guard byteGenerator.saveRandomBytes(data: data, key: key, overwrite: true) else { throw Error.saveKeyFailed }
        return data
    }
}
