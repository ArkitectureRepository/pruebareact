//
//  STGUserDefaultStorage.swift
//  STGSecure_Storage
//
//  Created by Hassan, Waseem (Isban) on 18/02/2019.
//  Copyright © 2019 Santander. All rights reserved.
//
import SCAL_iOS

public class STGSecureUserDefault: SCALSecureUserDefaultsInterface {
    
    let secureUserDefaultsDataProvider: STGSecureUserDefaultsDataProvider
	
    public init(userDefaults: UserDefaults = UserDefaults.standard) {
        self.secureUserDefaultsDataProvider = STGSecureUserDefaultsDataProvider(userDefaults: userDefaults)
    }
    
    public func set(_ value: Any, forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws {
        try setObject(value, forKey: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
    }
    
    public func set(_ value: Int, forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws {
        try set(NSNumber(value: value), forKey: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
    }
    
    public func set(_ value: Float, forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws {
        try set(NSNumber(value: value), forKey: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
    }
    
    public func set(_ value: Double, forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws {
        try set(NSNumber(value: value), forKey: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)

    }
    
    public func set(_ value: Bool, forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws {
        try set(NSNumber(value: value), forKey: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
    }
    
    public func set(_ url: URL, forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws {
        let urlValue: String = url.absoluteString
        try set(urlValue, forKey: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
    }
    
    public func object(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> Any? {
        let data = try secureUserDefaultsDataProvider.getFromUserDefault(key: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
        return unarchiveObjectWithCast(data: data)
    }

    public func string(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> String? {
        let data = try secureUserDefaultsDataProvider.getFromUserDefault(key: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
        return unarchiveObjectWithCast(data: data)
    }
    
    public func array(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> [Any]? {
        let data = try secureUserDefaultsDataProvider.getFromUserDefault(key: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
        return unarchiveObjectWithCast(data: data)
    }
    
    public func dictionary(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> [String: Any]? {
        let data = try secureUserDefaultsDataProvider.getFromUserDefault(key: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
        return unarchiveObjectWithCast(data: data)
    }
    
    public func data(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> Data? {
        let data = try secureUserDefaultsDataProvider.getFromUserDefault(key: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
        return unarchiveObjectWithCast(data: data)
    }
    
    public func stringArray(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> [String]? {
        let data = try secureUserDefaultsDataProvider.getFromUserDefault(key: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
        return unarchiveObjectWithCast(data: data)
    }
    
    public func integer(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> Int? {
        let number = try nsNumber(forKey: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
        return number?.intValue
    }
    
    public func float(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> Float? {
        let number = try nsNumber(forKey: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
        return number?.floatValue
    }
    
    public func double(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> Double? {
        let number = try nsNumber(forKey: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
        return number?.doubleValue
    }
    
    public func bool(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> Bool? {
        let number = try nsNumber(forKey: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
        return number?.boolValue
    }
    
    public func url(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> URL? {
        let data = try secureUserDefaultsDataProvider.getFromUserDefault(key: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
        guard let string: String = unarchiveObjectWithCast(data: data), let url = URL(string: string) else { return nil }
        return url
    }
    
    public func removeObject(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) {
        secureUserDefaultsDataProvider.removeFromUserDefault(key: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
    }
    
    private func nsNumber(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> NSNumber? {
        let data = try secureUserDefaultsDataProvider.getFromUserDefault(key: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
        return unarchiveObjectWithCast(data: data)
    }

    private func setObject(_ value: Any, forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws {
        let data: Data = NSKeyedArchiver.archivedData(withRootObject: value)
        try secureUserDefaultsDataProvider.addToUserDefault(data: data, key: defaultName, encryptionKey: encryptionKey, initializationVector: initializationVector)
    }
    
    private func unarchiveObjectWithCast<T>(data: Data?) -> T? {
        guard let data = data else { return nil }
        return NSKeyedUnarchiver.unarchiveObject(with: data) as? T
    }
    
    public func generateEncryptionKey() -> Data? {
        return secureUserDefaultsDataProvider.generateEncryptionKey()
    }
    
    public func generateEncryptionIV() -> Data? {
        return secureUserDefaultsDataProvider.generateEncryptionIV()
    }
    
    public func getEncryptionKey(defaultName: String) -> Data? {
        return secureUserDefaultsDataProvider.getEncryptionKey(defaultName: defaultName)
    }
    
    public func getEncryptionIV(defaultName: String) -> Data? {
        return secureUserDefaultsDataProvider.getEncryptionIV(defaultName: defaultName)
    }
}
