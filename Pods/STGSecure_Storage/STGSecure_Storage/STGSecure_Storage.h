//
//  STGSecure_Storage.h
//  STGSecure_Storage
//
//  Created by Eduardo Nieto Delgado on 07/02/2019.
//  Copyright © 2019 Santander. All rights reserved.
//

#import <UIKit/UIKit.h>


//! Project version number for STGSecure_Storage.
FOUNDATION_EXPORT double STGSecure_StorageVersionNumber;

//! Project version string for STGSecure_Storage.
FOUNDATION_EXPORT const unsigned char STGSecure_StorageVersionString[];

// In this header, you git should import all the public headers of your framework using statements like #import <STGSecure_Storage/PublicHeader.h>


