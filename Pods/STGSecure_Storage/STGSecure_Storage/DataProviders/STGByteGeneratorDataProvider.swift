//
//  STGByteGeneratorDataProvider.swift
//  STGSecure_Storage
//
//  Created by Eduardo Nieto Delgado on 07/05/2019.
//  Copyright © 2019 Santander. All rights reserved.
//

import Foundation
import STGKeychain_Access

public protocol STGByteGeneratorDataProviderProtocol {
    var keychainAccess: STGKeychainAccessProtocol { get }
    func getRandomBytes(key: String) -> Data?
    func saveRandomBytes(data: Data, key: String, overwrite: Bool) -> Bool
}

public class STGByteGeneratorDataProvider: STGByteGeneratorDataProviderProtocol {
    public let keychainAccess: STGKeychainAccessProtocol

    public init(keychainAccess: STGKeychainAccessProtocol = STGKeychainAccess()) {
        self.keychainAccess = keychainAccess
    }

    public func getRandomBytes(key: String) -> Data? {
        guard let randomBytes: Data = keychainAccess.retrieveKeychainValue(key: key, promptTitle: "We need to authenticate you to retrieve the key") else {
            return nil
        }
        return randomBytes
    }

    public func saveRandomBytes(data: Data, key: String, overwrite: Bool) -> Bool {
        if overwrite {
            return keychainAccess.updateKeychainValue(key: key, data: data, biometric: false)
        } else {
            return keychainAccess.saveKeychainValue(key: key, data: data, biometric: false)
        }
    }
}
