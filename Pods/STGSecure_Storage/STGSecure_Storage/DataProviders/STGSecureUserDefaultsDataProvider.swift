//
//  File.swift
//  STGSecure_Storage
//
//  Created by Eduardo Nieto Delgado on 07/05/2019.
//  Copyright © 2019 Santander. All rights reserved.
//

import CommonCrypto
import Foundation
import STGCrypto_Cipher
import STGKeychain_Access

class STGSecureUserDefaultsDataProvider {
    private let cryptoCipher: STGCryptoCipherProtocol
    private let keychainAccess: STGKeychainAccessProtocol
    private let byteGeneratorDataProvider: STGByteGeneratorDataProviderProtocol
    private let userDefaults: UserDefaults

    init(cryptoCipher: STGCryptoCipherProtocol = STGCryptoCipher(), keychainAccess: STGKeychainAccessProtocol = STGKeychainAccess(), byteGeneratorDataProvider: STGByteGeneratorDataProviderProtocol = STGByteGeneratorDataProvider(), userDefaults: UserDefaults = UserDefaults.standard) {
        self.cryptoCipher = cryptoCipher
        self.keychainAccess = keychainAccess
        self.byteGeneratorDataProvider = byteGeneratorDataProvider
        self.userDefaults = userDefaults
    }

    func addToUserDefault(data: Data, key: String, encryptionKey: Data, initializationVector: Data) throws {
        let statusIV = byteGeneratorDataProvider.saveRandomBytes(data: initializationVector, key: "\(key)_ud_iv", overwrite: true)
        let statusKey = byteGeneratorDataProvider.saveRandomBytes(data: encryptionKey, key: "\(key)_ud_iv_key", overwrite: true)

        if statusIV && statusKey {
            let cryptData = try cryptoCipher.aesCBCEncrypt(data: data, key: encryptionKey, iv: initializationVector)
            userDefaults.setValue(cryptData, forKey: key)
        }
    }

    func getFromUserDefault(key: String, encryptionKey: Data, initializationVector: Data) throws -> Data? {
        guard let cryptedData = userDefaults.data(forKey: key) else { return nil }
        let decryptedData = try cryptoCipher.aesCBCDecrypt(data: cryptedData, key: encryptionKey, iv: initializationVector)
        return decryptedData
    }

    func removeFromUserDefault(key: String, encryptionKey: Data, initializationVector: Data) {
        let removedIV = keychainAccess.removeKeychainValue(key: "\(key)_ud_iv")
        let removedKey = keychainAccess.removeKeychainValue(key: "\(key)_ud_iv_key")
        if removedIV && removedKey {
            userDefaults.removeObject(forKey: key)
        }
    }

    func generateEncryptionKey() -> Data? {
        return cryptoCipher.generateRandomBytes(blockSize: UInt8(32))
    }

    func generateEncryptionIV() -> Data? {
        return cryptoCipher.generateRandomBytes(blockSize: UInt8(kCCBlockSizeAES128))
    }

    func getEncryptionKey(defaultName: String) -> Data? {
        let fileName = cryptoCipher.encryptSHA256(string: defaultName)
        return byteGeneratorDataProvider.getRandomBytes(key: "\(fileName)_key")
    }

    func getEncryptionIV(defaultName: String) -> Data? {
        let fileName = cryptoCipher.encryptSHA256(string: defaultName)
        return byteGeneratorDataProvider.getRandomBytes(key: "\(fileName)_iv")
    }
}
