//
//  STSQLCipherDataProvider.swift
//  STGSecure_Storage
//
//  Created by Eduardo Nieto Delgado on 06/03/2019.
//  Copyright © 2019 Santander. All rights reserved.
//

import Foundation
import SQLCipher

final public class STGSecureDatabaseDataProvider {
    
    public init() {}
    
    func openSecureDatabase(name: String, password: String) -> OpaquePointer? {
        var database: OpaquePointer?

        let manager = FileManager.default
        var documentsURL: URL?

        do {
            documentsURL = try manager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("\(name).db")
        } catch let error {
            print("Error: \(error)")
            return nil
        }

        if sqlite3_open(documentsURL?.path, &database) == SQLITE_OK {
            print("Successfully opened connection to database at \(documentsURL?.path ?? "")")
        } else {
            print("Unable to open database. Verify that you created the directory described " +
                "in the Getting Started section.")
            return nil
        }

        if sqlite3_key(database, password, Int32(password.utf8CString.count)) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(database))
            NSLog("Error preparing SQL", errmsg)
            return nil
        }

        return database
    }

    func sqlDatabase(database: OpaquePointer, query: String) -> Bool {
        var rc: Int32?
        var stmt: OpaquePointer?

        if sqlite3_prepare(database, query, -1, &stmt, nil) == SQLITE_OK {
            if sqlite3_step(stmt) == SQLITE_DONE {
                print("Successfully queried db")
            } else {
                print("Could not query db.")
                return false
            }
        } else {
            let errorMessage = String(cString: sqlite3_errmsg(database))
            print("statement could not be prepared: \(errorMessage) ")
        }

        rc = sqlite3_prepare(database, "PRAGMA cipher_version;", -1, &stmt, nil)
        if rc != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(database))
            NSLog("Error preparing SQL: \(errmsg)")
            return false
        }
        rc = sqlite3_step(stmt)
        if rc == SQLITE_ROW {
            NSLog("cipher_version: %s", sqlite3_column_text(stmt, 0))
        } else {
            let errmsg = String(cString: sqlite3_errmsg(database))
            NSLog("Error retrieiving cipher_version: \(errmsg)")
            return false
        }
        if stmt != nil {
            finalizeQuery(stmt: stmt!)
        }
        return true
    }

    func querySecureDatabase(db: OpaquePointer, query: String, args: [Any]?) -> [[String: AnyObject]]? {
        var stmt: OpaquePointer?
        print("query \(query)")

        if sqlite3_prepare_v2(db, query, -1, &stmt, nil) == SQLITE_OK {
            var queryResult: [[String: AnyObject]] = [[String: AnyObject]]()

            let numberOfColumns = sqlite3_column_count(stmt)
            print("numberOfColumns \(numberOfColumns)")

            if let args = args {
                bindArguments(args: args, stmt: stmt)
            }
            
            while sqlite3_step(stmt) == SQLITE_ROW {
                var rowDictionary: [String: AnyObject] = [String: AnyObject]()
                let numberOfColumns = sqlite3_column_count(stmt)

                for index in 0 ... numberOfColumns - 1 {
                    if let columnNamePointer = sqlite3_column_name(stmt, Int32(index)) {
                        let columnName = String(cString: UnsafePointer<Int8>(columnNamePointer))
                        switch sqlite3_column_type(stmt, Int32(index)) {
                        case SQLITE_INTEGER:

                            rowDictionary[columnName] = Int(sqlite3_column_int64(stmt, Int32(index))) as AnyObject
                        case SQLITE_TEXT:
                            print(sqlite3_column_text(stmt, Int32(index)))
                            rowDictionary[columnName] = sqlite3_column_text(stmt, Int32(index)) as AnyObject
                            rowDictionary[columnName] = String(cString: sqlite3_column_text(stmt, Int32(index))) as AnyObject

                        case SQLITE_BLOB:
                            let len = sqlite3_column_bytes(stmt, Int32(index))
                            let bytes = sqlite3_column_blob(stmt, Int32(index))
                            rowDictionary[columnName] = NSData(bytes: bytes, length: Int(len))
                        default:
                            rowDictionary[columnName] = nil
                        }
                    }
                }
                queryResult.append(rowDictionary)
            }
            if stmt != nil {
                finalizeQuery(stmt: stmt!)
            }
            return queryResult
        } else {
            let errorMessage = String(cString: sqlite3_errmsg(db))
            print("statement could not be prepared: \(errorMessage) ")
        }
        if stmt != nil {
            finalizeQuery(stmt: stmt!)
        }
        return nil
    }
    
    func modifySecureDatabase(db: OpaquePointer, query: String, args: [Any]?) -> Int {
        var stmt: OpaquePointer?
        if sqlite3_prepare_v2(db, query, -1, &stmt, nil) == SQLITE_OK {
            if let args = args {
                bindArguments(args: args, stmt: stmt)
            }
            while sqlite3_step(stmt) == SQLITE_ROW {}
            finalizeQuery(stmt: stmt!)
            return Int(sqlite3_changes(stmt))
        }
        return 0
    }
    
    private func bindArguments(args: [Any], stmt: OpaquePointer?) {
            var pos = 1

            for arg in args {
                switch arg {
                case is NSString:
                    if sqlite3_bind_text(stmt, Int32(pos), (arg as? NSString)?.utf8String, -1, nil) != SQLITE_OK {
                        print("error binding String ", pos, arg)
                    }
                case is Int:
                    if let val = arg as? Int,
                        sqlite3_bind_int(stmt, Int32(pos), Int32(val)) != SQLITE_OK {
                        print("error binding Int", pos, arg)
                    }

                case is Double:

                    if let val = arg as? Double,
                        sqlite3_bind_double(stmt, Int32(pos), val) != SQLITE_OK {
                        print("error binding Int", pos, arg)
                    }

                case is Bool:

                    if let val = arg as? Bool,
                        sqlite3_bind_int(stmt, Int32(pos), val ? 1 : 0) != SQLITE_OK {
                        print("error binding Int", pos, arg)
                    }

                case is NSNull:
                    if let val = arg as? NSNull,
                        sqlite3_bind_null(stmt, Int32(pos)) != SQLITE_OK {
                        print("error binding Int", pos, val)
                    }
                default:
                    print("error binding unknown", pos)
                }

                pos += 1
            }
    }

    func closeDatabase(database: OpaquePointer) {
        sqlite3_close(database)
    }

    func finalizeQuery(stmt: OpaquePointer) {
        sqlite3_finalize(stmt)
    }

    func closeSecureDatabase(database: OpaquePointer?, stmt: OpaquePointer?) {
        sqlite3_finalize(stmt)
        sqlite3_close(database)
    }

    func removeDatabase(name: String) -> Bool {
        let manager = FileManager.default
        var documentsURL: URL?

        do {
            documentsURL = try manager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("\(name).db")
            if let url = documentsURL {
                try manager.removeItem(at: url)
                return true
            }

        } catch let error {
            print("Error: \(error)")
        }

        return false
    }

    func checkSecureDatabase(name: String, password: String) -> Bool {
        var rc: Int32?
        var database: OpaquePointer?
        var stmt: OpaquePointer?

        let manager = FileManager.default
        var documentsURL: URL?
        do {
            documentsURL = try manager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("\(name).db")

        } catch let error {
            print("Error: \(error)")
            return false
        }

        rc = sqlite3_open(documentsURL?.path, &database)
        if rc != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(database))
            NSLog("Error opening database: \(errmsg)")
            return false
        }
        rc = sqlite3_key(database, password, Int32(password.utf8CString.count))
        if rc != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(database))
            NSLog("Error setting key", errmsg)
        }
        rc = sqlite3_prepare(database, "PRAGMA cipher_version;", -1, &stmt, nil)
        if rc != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(database))
            NSLog("Error preparing SQL: \(errmsg)")
        }
        rc = sqlite3_step(stmt)
        if rc == SQLITE_ROW {
            NSLog("cipher_version: %s", sqlite3_column_text(stmt, 0))
        } else {
            let errmsg = String(cString: sqlite3_errmsg(database))
            NSLog("Error retrieiving cipher_version: \(errmsg)")
        }
        sqlite3_finalize(stmt)
        sqlite3_close(database)
        return true
    }
}
