//
//  STGPersistanceDataProvider.swift
//  STGSecure_Storage
//
//  Created by Eduardo Nieto Delgado on 07/05/2019.
//  Copyright © 2019 Santander. All rights reserved.
//

import Foundation

protocol STGPersistanceDataProviderProtocol {
    func saveDocumentDirectory(fileName: String, fileData: Data) -> Bool
    func retrieveDocumentDirectory(fileName: String) -> Data?
    
}

public class STGPersistanceDataProvider: STGPersistanceDataProviderProtocol {
	
	public init() {
		
	}
	
	public func saveDocumentDirectory(fileName: String, fileData: Data) -> Bool {
		do {
			let documentDirURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
			let fileURL = documentDirURL.appendingPathComponent(fileName)
			try fileData.write(to: fileURL, options: .noFileProtection)
			return true
		} catch {
			return false
		}
	}
	
	public func retrieveDocumentDirectory(fileName: String) -> Data? {
		do {
			let documentDirURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
			let fileURL = documentDirURL.appendingPathComponent(fileName)
			let fileData = try Data(contentsOf: fileURL)
			return fileData
		} catch {
			return nil
		}
	}
}
