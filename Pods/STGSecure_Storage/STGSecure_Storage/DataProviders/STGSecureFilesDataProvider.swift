//
//  STSecureStorageDataProvider.swift
//  STGSecure_Storage
//
//  Created by Eduardo Nieto Delgado on 14/02/2019.
//  Copyright © 2019 Santander. All rights reserved.
//

import Foundation
import SCAL_iOS
import STGCrypto_Cipher
import STGKeychain_Access
import CommonCrypto
import SQLite3

// Default implementation of context detection interface.

class STGSecureFilesDataProvider {
    let cryptoCipher: STGCryptoCipherProtocol
    let persistanceDataProvider: STGPersistanceDataProviderProtocol
    let byteGeneratorDataProvider: STGByteGeneratorDataProviderProtocol
    
    init(cryptoCipher: STGCryptoCipherProtocol = STGCryptoCipher(), persistanceDataProvider: STGPersistanceDataProviderProtocol = STGPersistanceDataProvider(), byteGeneratorDataProvider: STGByteGeneratorDataProviderProtocol = STGByteGeneratorDataProvider(keychainAccess: STGKeychainAccess())) {
        
        self.cryptoCipher = cryptoCipher
        self.persistanceDataProvider = persistanceDataProvider
        self.byteGeneratorDataProvider = byteGeneratorDataProvider
    }

    func writeToSecureFile(alias: String, data: Data?, fileMode: STGFileMode = .defaultMode, key: Data, initializationVector: Data) throws -> Bool {
		guard let fileName = encryptSHA256(string: alias) else { return false }
		
		let statusIV = byteGeneratorDataProvider.saveRandomBytes(data: initializationVector, key: "\(fileName)_iv", overwrite: true)
		let statusKey = byteGeneratorDataProvider.saveRandomBytes(data: key, key: "\(fileName)_key", overwrite: true)
		
		if statusIV && statusKey {
			var newData = Data()
			
            if fileMode == .appendMode, let oldFile = try readFromSecureFile(alias: alias, key: key, initializationVector: initializationVector) {
				newData.append(oldFile)
			}
			
			if let data = data {
				newData.append(data)
			}
			
            let cryptData = try cryptoCipher.aesCBCEncrypt(data: newData, key: key, iv: initializationVector)
            return persistanceDataProvider.saveDocumentDirectory(fileName: fileName, fileData: cryptData)

		}
		return false
    }
    
    func removeSecureFile(alias: String) throws -> Bool {
        let fileName = cryptoCipher.encryptSHA256(string: alias)

        let statusIV = byteGeneratorDataProvider.keychainAccess.removeKeychainValue(key: "\(fileName)_iv")
        let statusKey = byteGeneratorDataProvider.keychainAccess.removeKeychainValue(key: "\(fileName)_key")
        let filemanager = FileManager.default
        guard let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first as NSString? else {
            return false
        }
        let destinationPath = documentsPath.appendingPathComponent(fileName)
        try filemanager.removeItem(atPath: destinationPath)

        return statusIV && statusKey
    }

    func clearSecureFile(alias: String, key: Data, initializationVector: Data) throws -> Bool {
        let fileName = cryptoCipher.encryptSHA256(string: alias)
        let cryptData = try cryptoCipher.aesCBCEncrypt(data: Data(), key: key, iv: initializationVector)
        return persistanceDataProvider.saveDocumentDirectory(fileName: fileName, fileData: cryptData)
    }
    
	func readFromSecureFile(alias: String, key: Data, initializationVector: Data) throws -> Data? {
		let fileName = cryptoCipher.encryptSHA256(string: alias)
		guard let cryptedData = persistanceDataProvider.retrieveDocumentDirectory(fileName: fileName) else { return nil }
        let decryptedData = try cryptoCipher.aesCBCDecrypt(data: cryptedData, key: key, iv: initializationVector)
        return decryptedData
    }
    
    func encryptSHA256(string: String) -> String? {
        return self.cryptoCipher.encryptSHA256(string: string)
    }
    
    func generateEncryptionKey() -> Data? {
        return cryptoCipher.generateRandomBytes(blockSize: UInt8(32))
    }
    
    func generateEncryptionIV() -> Data? {
        return cryptoCipher.generateRandomBytes(blockSize: UInt8(kCCBlockSizeAES128))
    }
    
    func getEncryptionKey(alias: String) -> Data? {
        let fileName = self.cryptoCipher.encryptSHA256(string: alias)
        return byteGeneratorDataProvider.getRandomBytes(key: "\(fileName)_key")
    }
    
    func getEncryptionIV(alias: String) -> Data? {
        let fileName = self.cryptoCipher.encryptSHA256(string: alias)
        return byteGeneratorDataProvider.getRandomBytes(key: "\(fileName)_iv")
    }
}



