//  Copyright © 2019 Santander. All rights reserved.

import SCAL_iOS

class SQLStatementFormatter {
    func format(query: SCALSelectQuery) -> (String, [Any]?) {
        var statement = "SELECT "
        let args = [query.selection?.arguments, query.having?.arguments]
            .compactMap { $0 }
            .flatMap { $0 }
        if query.distinct {
            statement += "DISTINCT "
        }

        statement += query.columns.joined(separator: ", ")
        statement += " FROM \(query.tableName) "

        if let selection = query.selection {
            statement += "WHERE \(selection.function) "
        }
        if let groupBy = query.groupBy {
            statement += "GROUP BY \(groupBy) "
        }
        if let having = query.having {
            statement += "HAVING \(having.function) "
        }
        if let orderBy = query.orderBy {
            let columns = orderBy.map { "\($0.columName) \($0.order.sqlString)" }.joined(separator: ", ")
            statement += "ORDER BY \(columns) "
        }
        if let limit = query.limit {
            statement += "LIMIT \(limit) "
        }
        return (statement, args)
    }

    func format(query: SCALCreateTableQuery) -> String {
        let names = query.columns.map { "\($0.key) \($0.value)" }.joined(separator: ", ")
        let statement = "CREATE TABLE \(query.tableName)(\(names))"
        return statement
    }

    func format(query: SCALInsertQuery) -> (String, [Any]?) {
        let columnNames = query.values.map { $0.key }.joined(separator: ", ")
        let columnValues = query.values.map { _ in "?" }.joined(separator: ", ")
        let statement = "INSERT INTO \(query.tableName) (\(columnNames)) VALUES (\(columnValues))"
        let parameters = query.values.map { $0.value }
        return (statement, parameters)
    }

    func format(query: SCALDeleteQuery) -> (String, [Any]?) {
        let statement = "DELETE FROM \(query.tableName) WHERE \(query.selection.function)"
        return (statement, query.selection.arguments)
    }

    func format(query: SCALUpdateQuery) -> (String, [Any]?) {
        let values = query.values.map { "\($0.key) = ?" }.joined(separator: ", ")
        var statement = "UPDATE \(query.tableName) SET \(values)"
        if let selection = query.selection {
            statement += " WHERE \(selection.function)"
        }
        let parameters = [query.values.map { $0.value }, query.selection?.arguments]
            .compactMap { $0 }
            .flatMap { $0 }
        return (statement, parameters)
    }
}

extension SCALDatabaseOrderBy.Order {
    var sqlString: String {
        switch self {
        case .ascending:
            return "ASC"
        case .descending:
            return "DESC"
        }
    }
}
