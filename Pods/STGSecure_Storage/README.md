# SecureStorage

# Table of Contents ###
- [Summary](#summary)
- [Relation of Dependencies ](#relation-of-dependencies)
- [Control of Versions ](#control-of-versions)
- [Requirements ](#requirements)
- [Quick Start ](#quick-start)
- [Description ](#description)


----------------------------
# Relation of Dependencies
This module has dependency on SCAL module which provides interface for accessing different methods within secure storage module. Currently we are using default implementation of interface but end project can pass their own implementation by confirming to interface defined in SCAL.


----------------------------
# Control of Versions

|     Component Version   |         Date            |     Status        |           Description                 |
| --------------------------- |  -------------------| --------------   |  ------------------------------- |
|             *v2.0*                 |      18/12/2019     |     Released   | STGSecure_Storage_iOS     |
|             *v2.0*                 |      18/12/2019     |     Released   |                 SCAL iOS              |


----------------------------
# Requirements

iOS 10+

Xcode 11+

Swift 5.0


----------------------------
# Quick Start

Module responsible of handling all functionalities related to storing data on the device with a layer of security on top. This functionalities include Secure User Defaults, Secure Storage on the device, SecureDatabase and Keychain. All of this functionalities contain methods for CRUD operations.

----------------------------


## Installation

### CocoaPods

For cocoa project we have dependency on **CocoaPods**. Cocoapods can be installed from self service software available for Santander otherwise following command can be used for installation.

$ gem install cocoapods

> CocoaPods 1.5+ is required.

For integration of **STGSecure_Storage** in Xcode project, add following lines to your Podfile.

source 'ssh://git@gitlab.alm.gsnetcloud.corp:2220/Globile/Globile-Spec.git'
source 'ssh://git@gitlab.alm.gsnetcloud.corp:2220/Globile/Globile-Spec.git'
use_frameworks!

target '<Your Target Name>' do
pod 'STGSecure_Storage'
end

Then run following command.

$ pod install

### Submodule based installation

If you don't want to use aforementioned method, you can alternatively add it manually using following commands:

* Considering you have workspace in your project proceed to next step. If not create workspace in your project and than open the workspace in xcode.

* Open up Terminal, navigate to project directory by cd into your top-level project directory, and run the following command "if" your project is not initialized as a git repository:

$ git init

* Add **STGSecure_Storage** as a git submodule by running the following command:

$ git submodule add ssh://git@gitlab.alm.gsnetcloud.corp:2220/mobi-sec/STGSecure_Storage_iOS.git
* After cloning repository as submodule. Open workspace of your main project and than drag xcodeproj of STGSecure_Storage module directory into main workspace.

<!--![](Media/0.png)-->
<!---->
<!--![](Media/1.png)-->
<!---->
<!--![](Media/2.png)-->


* Open up Terminal, navigate to project directory by cd into your top-level project directory and add **SCAL-iOS** as a git submodule by running the following command:

$ git submodule add ssh://git@gitlab.alm.gsnetcloud.corp:2220/mobi-sec/SCAL-iOS.git

* After cloning repository as submodule. Open workspace of your main project and than drag xcodeproj of SCAL-iOS module directory into main workspace.

<!--![](Media/3.png)-->
<!---->
<!--![](Media/4.png)-->
<!---->
<!--![](Media/5.png)-->


* After addition of both modules we have to configure linking of frameworks in main project workspace. Select main project target (as an example we have SCAL-Demo-iOS project), than under General->Embedded Binaries tap + button and add framework as shown.

<!--![](Media/6.png)-->

* Select **STGSecure_Storage** target than under General -> Linked Framework and Libraries, tap on + button and add SCAL_iOS.framwork as shown

<!--![](Media/7.png)-->

* After all of this has been done, pod dependencies have to bee set up. For this run "pod init" on main project folder to create a new podfile. Inside this podfile include the dependencie of this repository after use_frameworks!

### Framework based installation

This method will provide you frameworks for both SCAL and secure storage module which you can add into your project. Following the steps to follow for this method.

* Clone **SCAL-iOS** from git repository.

$ git clone ssh://git@gitlab.alm.gsnetcloud.corp:2220/mobi-sec/SCAL-iOS.git
* Open .xcodeproj from SCAL project directory. You will find two targets as **SCAL_iOS** and **SCALAggregate**.

* Select SCALAggregate target and build cmd+b. It will open finder with frame created as **SCAL_iOS.framework** as shown.

![](Media/8.png)


* Copy **SCAL_iOS.framework** to safe place later we have to add this framework in secure storage  module.

* Clone **STGSecure_Storage** from git repository.

$ git clone ssh://git@gitlab.alm.gsnetcloud.corp:2220/mobi-sec/STGSecure_Storage_iOS.git

* Copy **SCAL_iOS.framework** which we have already created to STGSecure_Storage module directory. Open STGSecure_Storage.workspace from module directory.  Then add SCAL_iOS.framework from directory to STGSecure_Storage project in xcode as shown.

<!--![](Media/9.png)-->
<!---->
<!--![](Media/10.png)-->
<!---->
<!--![](Media/11.png)-->
<!---->
<!--![](Media/12.png)-->


* Select STGSecureStorageAggregate target and build cmd+b. It will open finder with framework created as **STGSecure_Storage.framework**

* Now add both framework **SCAL_iOS.framework** and **STGSecure_Storage.framework** to main project and we are done!

## Usage

* To use secure storage functionality into main project, import both module into class where you want to use like:

`import STGSecure_Storage`
`import SCAL_iOS`

* Then declare constants that store the values for secure storage and user defaults (wichever the implementation needs):

`let secureStorage = SCALSecureStorage(secureStorage: STDefaultSecureStorage())`
`let userDefaults = SCALUserDefaults(userDefault: STGUserDefaultStorage())`
`let secureDatabase = SCALSecureDatabase(secureDatabase: STGSecureDatabase())`

* Make sure to declare SCALSecureStorage variable as class instance.

* Next need to call different methods of CALSecureStorage which will be explained in next section.

* For testing inject a mock of `STGKeychainProtocol` into `STGByteGeneratorDataProvider` simliar to this `SCALSecureDatabase(secureDatabase: STGSecureDatabase(byteGenerator: STGByteGeneratorDataProvider(keychainAccess: MockKeyChain)))`

## Description

**SCAL_iOS.framework** exposes the following methods:

### Methods for storing data securely:

* For secure storage SCALSecureStorage class will be used for handling secure CRUD statements and secure UserDefault queries. Some methods return a Bool representing the success or failure of the operation, others return the data type associated with the query.

---

**writeToSecureFile(alias: String, data: Data?, fileMode: FileMode = .defaultMode) -> Bool**

```writeToSecureFile(alias: String, data: Data?, fileMode: FileMode = .defaultMode) -> Bool```

This method is used to create, overwrite or append data to a secure file under the alias as a file name in the document directory of the application. Recieves as parameter the name of the file that will be associated to, the data that is going to be stored securely, although it could be a nil value if there's a need to create an empty file, and a fileMode, that represents the type of writing to be used. The options are ".defaultMode" for a file that is stored and prevents it from being overwritten, ".overwriteMode" if the need is to overwrite the existing data inside the file or ".appendMode" if the data previously stored wants to be appended with new data. Returns a boolean representing if the operation was succesful

Example:

`let isFileStored = writeToSecureFile(alias: "fileName", data: fileData, fileMode: .overwriteMode)`

---

**removeSecureFile(fileName: String) -> Bool**

```removeSecureFile(fileName: String) -> Bool```

This method removes a previously created secure file with the fileName in the document directory of the application. If the file does not exist it returns false. Recieves as parameters the name that has previously been associated with the file to remove. Returns a boolean representing if the operation was successful.

`let isFileRemoved = secureStorage.removeSecureFile(fileName: fileName)`

---

**clearSecureFile(fileName: String) -> Bool**

```clearSecureFile(fileName: String) -> Bool```

This method removes all the data associated to a previously created secure file found under the fileName in the document directory of the application. If the file does not exist it returns false. Returns a boolean representing if the operation was successful.

`let isFileCleared = secureStorage.clearSecureFile(fileName: fileName)`

---

**readFromSecureFile(fileName: String) -> Data?**

```readFromSecureFile(fileName: String) -> Data?```

This method retrieves all the data associated to a previously created secure file found under the fileName in the document directory of the application. If the file does not exist it returns null. Recieve as parameters the name that has previously been associated with the file to read from.Returns Data representing all the bytes stored without encryption

`let fileData = secureStora  ge.readFromSecureFile(fileName: fileName)`

---

### Methods for using a SecureDatabase :

*For secure storage SCALSecureDatabase class will be used for handling secure CRUD statements to SQLite databases. This methods are for initialising, handling queries to the database and closing the database.

```
struct SCALCreateTableQuery {
    let tableName: String
    let columns: [String: String]
}
```
|name|description|
|-----------|-------------------------------------|
| tableName | the name of the table to be created |
|columns | a dictionary with the keys defining the name of the columns and the value defining the type of the columns.|

```
struct SCALInsertQuery {
    let tableName: String
    let values: [String: Any]    
}
```
|name|description|
|-----------|-------------------------------------|
|tableName| a string that represent the name of the table|
|values| A dictionary / map of key values, that has the name of the column as key of type String and the element to be inserted as value of type Any|
```
struct SCALDatabaseParameterized {
    let function: String
    let arguments: [Any] 
}
```
|name|description|
|-----------|-------------------------------------|
|function| The function is a string that contains all the logic of comparisons to be excecuted, witht the parameters being represented by ?. For example “age = ?“|
|arguments| The arguments are an array of Any that holds all the arguments that are going to replace the “?” in the function variable. They are going to be inserted in the array order into the function.|
```
struct SCALDatabaseOrderBy { 
    enum Order {
        case descending
        case ascending
    }  
    let columName: String
    let order: Order
}
```
```
struct SCALSelectQuery {
    let distinct: Bool
    let tableName: String
    let columns: [String]
    let selection: SCALDatabaseParameterized?
    let groupBy: String?
    let having: SCALDatabaseParameterized?
    let orderBy: [SCALDatabaseOrderBy]?
    let limit: Int?
}
```

We can enforce the parameters passed by the developer here, but we cannot find a way to enforce the implementation of the interface, since the file system, database creation system and so on do not have a unique way of being implemented.

|name|description|
|-|-|
|distinct| true if you want each row to be unique, false otherwise|
|tableName| The table name to compile the query against|
|columns| A list of which columns to return. Passing null will return all columns, which is discouraged to prevent reading data from storage that isn't going to be used|
|selection| A filter declaring which rows to return, using SCALDatabaseParameterized? that has the selection function as String, and the arguments as an array of any.|
|groupBy| A filter declaring how to group rows, formatted as an SQL GROUP BY clause (excluding the GROUP BY itself). Passing null will cause the rows to not be grouped.|
|having| A filter declare which row groups to include in the cursor, using SCALDatabaseParameterized? that has the selection function as String, and the arguments as an array of any|
|orderBy| How to order the rows, using the SCALDatabaseOrderBy struct will give you the option of tableName and order|
|limit| Limits the number of rows returned by the query, formatted as LIMIT clause. Passing null denotes no LIMIT clause|
```
struct SCALUpdateQuery {
    let tableName: String
    let selection: SCALDatabaseParameterized?
    let values: [String: Any]
}
```
|name|description|
|-|-|
|tableName| a string that represent the name of the table|
|selection| An SCALDatabaseParameterized? that has the selection function as String, and the arguments as an array of any.|
|values| A dictionary / map of key values, that has the name of the column as key of type String and the element to be inserted as value of type Any|
```
struct SCALDeleteQuery {
    let tableName: String
    let selection: SCALDatabaseParameterized
}
```
|name|description|
|-|-|
|tableName| a string that represent the name of the table|
|selection| An SCALDatabaseParameterized that has the selection function as String, and the arguments as an array of any.|

```
struct SCALDeleteQuery {
    let tableName: String
    let selection: SCALDatabaseParameterized
}
```
|name|description|
|-|-|
|error| A type of error defined in SCAL that provides more information about the reason of|
|selection| An SCALDatabaseParameterized that has the selection function as String, and the arguments as an array of any.|



Also in the interface, expose the following methods

`func initDatabase(name: String) throws`

This method receives a String as the name of the database to be initialized, if it doesn’t exist it creates the Database


`func removeDatabase(name: String) -> Bool`

This method receives a String as the name of the database to be removed, it returns a boolean representing the success of the operation.

`func createSecureTable(query: SCALCreateTableQuery) -> SCALQueryResult`

This method receives a String as the name of the table to be created, and an object of type SCALCreateTableQuery that contains all the necessary parameters for the table creation. It returns an object of SCALQueryResult containing all the information regarding the operation.

`func insertIntoSecureDatabase(query: SCALInsertQuery) -> Int`

This method receives an array of Any the values to be inserted and an array of strings the names of the columns to be inserted in. The result of tis operations is represented in the Int, the number of changes done.

`func selectFromSecureDatabase(query: SCALSelectQuery) -> SCALQueryResult`

This method receives an object of type SCALSelectQuery to operate with the database (this object contains all of the necessary parameters to operate with the database). It returns an object of SCALQueryResult containing all the information regarding the operation

`func updateFromSecureDatabase(query: SCALUpdateQuery) -> Int`
	
This method receives an object of type SCALUpdateQuery to operate with the database (this object contains all of the necessary parameters to operate with the database). The result of tis operations is an Integer representing the number of rows affected by this operation.

`func deleteFromSecureDatabase(query: SCALDeleteQuery) -> Int`

This method receives an object of type SCALSelectQuery to operate with the database (this object contains all of the necessary parameters to operate with the database). The result of tis operations is an Integer representing the number of rows affected by this operation.


### Methods for using secure user defaults:

* For secure storage SCALUserDefaults class will be used for handling secure user defaults queries. This works as a normal user defaults with a layer of security through encryption

**object(forKey defaultName: String) -> Any?**

```object(forKey defaultName: String) -> Any?```

This method returns the object associated with the specified key previously stored on secure user defaults.

Example:

`let object = userDefaults.object(forKey: "object")`

**set(_ value: Any, forKey defaultName: String)**

```set(_ value: Any, forKey defaultName: String)```

This method sets the object as the value of the specified default key on secure user defaults.

Example:

`userDefaults.set(object, forKey: "object")`

**removeObject(forKey defaultName: String)**

```removeObject(forKey defaultName: String)```

This method removes the object associated with the specified key previously stored on secure user defaults.

Example:

`userDefaults.removeObject(forKey: "object")`

**string(forKey defaultName: String) -> String?**

```string(forKey defaultName: String) -> String?```

This method returns the string associated with the specified key previously stored on secure user defaults.

Example:

`let string = userDefaults.string(forKey: "string")`

**array(forKey defaultName: String) -> [Any]?**

```array(forKey defaultName: String) -> [Any]?```

This method returns an array associated with the specified key previously stored on secure user defaults.

Example:

`let array = userDefaults.array(forKey: "array")`

**dictionary(forKey defaultName: String) -> [String: Any]?**

```dictionary(forKey defaultName: String) -> [String: Any]?```

This method returns a dictionary associated with the specified key previously stored on secure user defaults.

Example:

`let dictionary = userDefaults.dictionary(forKey: "dictionary")`

**data(forKey defaultName: String) -> Data?**

```data(forKey defaultName: String) -> Data?```

This method returns data associated with the specified key previously stored on secure user defaults.

Example:

`let data = userDefaults.data(forKey: "data")`

**stringArray(forKey defaultName: String) -> [String]?**

```stringArray(forKey defaultName: String) -> [String]?```

This method returns a string array associated with the specified key previously stored on secure user defaults.

Example:

`let stringArray = userDefaults.stringArray(forKey: "stringArray")`

**integer(forKey defaultName: String) -> Int?**

```integer(forKey defaultName: String) -> Int?```

This method returns an integer associated with the specified key previously stored on secure user defaults.

Example:

`let integer = userDefaults.integer(forKey: "integer")`

**float(forKey defaultName: String) -> Float?**

```float(forKey defaultName: String) -> Float?```

This method returns a float associated with the specified key previously stored on secure user defaults.

Example:

`let float = userDefaults.float(forKey: "float")`

**double(forKey defaultName: String) -> Double?**

```double(forKey defaultName: String) -> Double?```

This method returns a double associated with the specified key previously stored on secure user defaults.

Example:

`let double = userDefaults.double(forKey: "double")`

**bool(forKey defaultName: String) -> Bool?**

```bool(forKey defaultName: String) -> Bool?```

This method returns a boolean associated with the specified key previously stored on secure user defaults.

Example:

`let boolean = userDefaults.bool(forKey: "boolean")`

**url(forKey defaultName: String) -> URL?**

```url(forKey defaultName: String) -> URL?```

This method returns a url associated with the specified key previously stored on secure user defaults.

Example:

`let url = userDefaults.url(forKey: "url")`

**set(_ value: Int, forKey defaultName: String)**

```set(_ value: Int, forKey defaultName: String)```

This method sets an integer as the value of the specified default key on secure user defaults.

Example:

`userDefaults.set(1, forKey: "integer")`


**set(_ value: Float, forKey defaultName: String)**

```set(_ value: Float, forKey defaultName: String)```

This method sets an float as the value of the specified default key on secure user defaults.

Example:

`userDefaults.set(1.1, forKey: "float")`

**set(_ value: Double, forKey defaultName: String)**

```set(_ value: Double, forKey defaultName: String)```

This method sets an double as the value of the specified default key on secure user defaults.

Example:

`userDefaults.set(111111111, forKey: "double")`

**set(_ value: Bool, forKey defaultName: String)**

```set(_ value: Bool, forKey defaultName: String)```

This method sets an boolean as the value of the specified default key on secure user defaults.

Example:

`userDefaults.set(true, forKey: "boolean")`

**set(_ value: URL, forKey defaultName: String)**

```set(_ value: URL, forKey defaultName: String)```

This method sets a url as the value of the specified default key on secure user defaults.

Example:

`userDefaults.set(URL("www.google.com"), forKey: "boolean")`


## License

Copyright © 2019 Santander Technology UK. All rights reserved.
