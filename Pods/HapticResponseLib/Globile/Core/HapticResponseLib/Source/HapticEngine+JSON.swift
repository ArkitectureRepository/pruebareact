//
//  HapticEngine+JSON.swift
//  Pods
//
//  Created by Manu Mateos on 05/09/2019.
//

import UIKit

extension HapticEngine {

    static func decode<T: Codable>(_ json: String, type: T.Type) throws -> T {

        guard let data = json.data(using: .utf8) else {
            throw NSError(domain: "Unable to parse action", code: 1, userInfo: nil)
        }

        return try JSONDecoder().decode(T.self, from: data)
    }

}
