//
//  HapticEngineOperation.swift
//  Alamofire-iOS10.0
//
//  Created by Manu Mateos on 05/09/2019.
//

import Foundation

public enum HapticFeedbackType: String, Codable {
    case impactLight, impactMedium, impactHeavy, notificationSuccess, notificationWarning, notificationError, selection
}


/// Struct for request from web.
struct HapticEngineRequest: Codable {
    var hapticFeedback: HapticFeedbackType
}
