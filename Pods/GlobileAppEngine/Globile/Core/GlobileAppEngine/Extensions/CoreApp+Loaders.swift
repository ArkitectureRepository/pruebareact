//
import Foundation
import UIKit
import SantanderUIKitLib
extension UIViewController {
    //MARK: Loaders
    @objc public func showLoader(title: String, subtitle: String, gifName:UIImage?=nil) {
        if let loader = GlobileLoader.instanceFromNib() {
            if let gif = gifName {
                loader.setupLoaderNib(in: self.view, globileLoader: loader, title: title, subtitle: subtitle, gifName: gif)
                    
            } else {
                loader.setupLoaderNib(in: self.view, globileLoader: loader, title: title, subtitle: subtitle)
                    
            }
        }
    }
    
    @objc public func hideLoader() {
        for view in  self.view.subviews {
            if view.isKind(of: GlobileLoader.self) {
                view.removeFromSuperview()
                break
            }
        }
    }
}
