import UIKit

extension UIStoryboard {
    public func instantiateViewControllerIfExists(identifier: String) -> UIViewController? {
        if let allIDs = self.value(forKey: "identifierToNibNameMap") as? [String: Any] {
            if allIDs.keys.contains(identifier) {
                return self.instantiateViewController(withIdentifier: identifier)
            }
        }
        return nil
    }
}
