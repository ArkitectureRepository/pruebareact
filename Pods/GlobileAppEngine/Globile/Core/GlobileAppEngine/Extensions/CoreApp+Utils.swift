//
import Foundation
import UIKit
import SantanderUIKitLib
import GlobileUtilsLib


public func entrypoint(entrypoints: EntryObject?) -> String? {
    var entryName: String = ""
    
    if let entrypoints = entrypoints, let modules = entrypoints.components {
        let filtered = modules.filter { $0 == entrypoints.initial }
        if let initial = filtered.first {
            entryName = initial
        }
    }
    return entryName
}

public func startCoreApp(window: UIWindow?) {
    var entryObjects: EntryObject?
    do {
        if let path = Bundle.main.path(forResource: "app", ofType: "json") {
            let absolutepath = "file://\(path)"
            let data = try Data(contentsOf: URL(string: absolutepath)!, options: .mappedIfSafe)
            NSLog(String(data: data, encoding: .utf8)!)
            entryObjects = try utils_json_decode(String(bytes: data, encoding: .utf8)! , type: EntryObject.self)
            if let entryObjects = entryObjects, let window = window {
                SingletonData.shared.entrypoints = entryObjects
                if let entrypoints = SingletonData.shared.entrypoints, let defaultLanguage = entrypoints.defaultLanguage {
                    if let lang =  UserDefaults.standard.string(forKey: languageConstant) {
                        setLanguage(language: Language(rawValue: lang) ?? Language.english)
                    }else {
                        setLanguage(language: Language(rawValue: defaultLanguage) ?? Language.english)
                    }
                }
                if let initialParameters = entryObjects.initParameters {
                    initialParamsToCache(params: initialParameters)
                }
                if let appSettings = entryObjects.appSettings, let  privateMode = appSettings.disablePrivateTextViewMode, let privateModeSensor = appSettings.disablePrivateTextViewSensors {
                    if getPrivateModeEnabled() == nil {
                        setPrivateModeEnabled(active: privateMode)
                    }
                    if getPrivateModeEnableSensor() == nil {
                        setPrivateModeEnableSensor(active: privateModeSensor)
                    }
                }
                if let controller = controllerFinder(name: entryObjects.initial) {
                    window.rootViewController = controller
                }
            }
            CoreAppHandlerManager.analytics.setTealiumConfig()
        }
    } catch (let error) {
        globileAppEngineLog(logType: .error, str: error.localizedDescription)
        if let window = window {
            window.rootViewController = GlobileAlertController(title: "Error", subtitle: "GlobileAppEngine found a problem", message: error.localizedDescription, actions: nil, completion: nil)
        }
    }
}

public func controllerFinder(name: String?,  storyboardFlowName: String? = nil, identifierViewControllerStoryBoard: String? = nil) -> UIViewController? {
    if let name = name {
        var aux = name
        if initHybridComponents(name: name) {
            aux = AppEnginePlaces.hybridcomponentAlias.rawValue
        }
        if let classObject = NSClassFromString(aux) as? NSObject.Type {
            let initial = classObject.init()
            if let url = Bundle(for: classObject.self).url(forResource: name, withExtension: "bundle"),let realbundle = Bundle(url: url ) {
                var story = UIStoryboard(name: name, bundle: realbundle)
                if let storyBoardName = storyboardFlowName {
                    if let _ = realbundle.path(forResource: storyBoardName, ofType: "storyboardc") {
                        story = UIStoryboard(name: storyBoardName, bundle: realbundle)
                    }else {
                        return nil
                    }
                }
                if let idVC = identifierViewControllerStoryBoard {
                    if let vc = story.instantiateViewControllerIfExists(identifier: idVC) {
                        return vc
                    } else {
                        globileAppEngineLog(logType: .error, str: "Can't find ViewController into sotryboard \(name) with identifier: \(idVC)")
                        return nil
                    }
                }
                else if let initial = story.instantiateInitialViewController() {
                    return initial
                }
            }
            return initial as? UIViewController
        }
    }
    return nil
}

public func initHybridComponents(name: String?) -> Bool {
    var finalValue = false
    if let name = name {
        SingletonData.shared.entrypoints?.hybridBundle?.forEach{
            if name.elementsEqual($0.alias!)  {
                SingletonData.shared.hybridConfig = $0
                finalValue = true
            }
        }
    }
    return finalValue
}

//MARK: Set initial parameters in cache
public func initialParamsToCache(params: [[String: [String: Any]]]) {
    do {
        try DataBus.shared.save(object: params as! [[String: [String: CustomType]]] , forKey: AppEnginePlaces.initialParams.rawValue)
        globileAppEngineLog(logType: .info, str: "Parameters saved in cache")
    } catch {
        globileAppEngineLog(logType: .error, str: "Unable to save initalParameters in cache")
    }
}

//MARK: Get Loaded Components and Component Exist

public func getAllLoadedComponents() -> [String] {
    if let entrypoints = SingletonData.shared.entrypoints, let components = entrypoints.components {
        return components
    }
    return []
}
public func componentExist(_ nameComponent: String) -> Bool {
    if let entrypoints = SingletonData.shared.entrypoints, let components = entrypoints.components {
        for component in components {
            if nameComponent == component {
                return true
            }
        }
      }
    return false
}
public func viewControllerExist(_ nameComponent: String, storyboardFlowName: String? = nil, identifierViewControllerStoryBoard: String? = nil) -> Bool {
    if let entrypoints = SingletonData.shared.entrypoints, let components = entrypoints.components {
        for component in components {
            if nameComponent == component {
                if let _ = controllerFinder(name: nameComponent, storyboardFlowName: storyboardFlowName, identifierViewControllerStoryBoard: identifierViewControllerStoryBoard) {
                    return true
                }else {
                    return false
                }
            }
        }
      }
    return false
}
public func refreshApplicationWithLanguage(language: Language, window: UIWindow) {
    setLanguage(language: language)
    startCoreApp(window: window)
}
