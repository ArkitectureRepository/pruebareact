//
import Foundation
import UIKit
import SantanderUIKitLib


extension UIViewController {
    // MARK: - Custom
    //Routes with name and type
    @objc public  func routeTo(name: String, storyboardFlowName: String? = nil, identifierViewControllerStoryBoard: String? = nil) throws {
        let path = Bundle(for: TabBarViewController.self).path(forResource: "GlobileAppEngine", ofType: "bundle")!
        let bundle = Bundle(path: path) ?? Bundle.main
        
        let error =  GlobileAppEngineError(type: .route, code: 500, message: "GenericError".localizedString(bundle: bundle, tableName: "GlobileAppEngine"))
        globileAppEngineLog(logType: .warning, str: "GenericError".localizedString(bundle: bundle, tableName: "GlobileAppEngine"))
        
        if let entrypoints = SingletonData.shared.entrypoints, let components = entrypoints.components {
            var foundComponent = false
            for component in components {
                if component == name {
                    if name.elementsEqual("MainMenu") {
                        let tabBar = TabBarViewController()
                        tabBar.modalPresentationStyle = .fullScreen
                        self.present(tabBar, animated: true) {
                        }
                    } else {
                        if Bundle.main.path(forResource: name, ofType: "storyboardc") != nil {
                            var story = UIStoryboard(name: name, bundle: nil)
                            if let storyBoardName = storyboardFlowName {
                                story = UIStoryboard(name: storyBoardName, bundle: nil)
                            }
                            if let idVC = identifierViewControllerStoryBoard {
                                
                                if let vc = story.instantiateViewControllerIfExists(identifier: idVC) {
                                    self.navigationController?.pushViewController(vc, animated: true)
                                } else {
                                    globileAppEngineLog(logType: .error, str: "Can't find ViewController into sotryboard \(name) with identifier: \(idVC)")
                                }
                            }else {
                                guard let initial = story.instantiateInitialViewController() else { return }
                                self.navigationController?.pushViewController(initial, animated: true)
                            }
                            
                        } else {
                            if let initial = controllerFinder(name: name, storyboardFlowName: storyboardFlowName, identifierViewControllerStoryBoard: identifierViewControllerStoryBoard) {
                                self.navigationController?.pushViewController(initial, animated: true)
                            } else {
                                //Añadir technical analytcis
                                showErrorAlertGlobileAppEngine(title: "Error".localizedString(bundle: bundle, tableName: "GlobileAppEngine"), message: error.message)
                                throw error
                            }
                        }
                    }
                    
                    foundComponent = true
                    break
                }
            }
            //Añadir technical analytcis
            if !foundComponent {
                globileAppEngineLog(logType: .error, str: error.error.rawValue)
                showErrorAlertGlobileAppEngine(title: "Error".localizedString(bundle: bundle, tableName: "GlobileAppEngine"), message: error.message)
                throw error
            }
            
            
        } else {
            //Añadir technical analytcis
            showErrorAlertGlobileAppEngine(title: "Error".localizedString(bundle: bundle, tableName: "GlobileAppEngine"), message: error.message)
            throw error
        }
        
    }
    @objc public func showErrorAlertGlobileAppEngine(title: String, message: String?) {
        if let path = Bundle(for: TabBarViewController.self).path(forResource: "GlobileAppEngine", ofType: "bundle") {
            let bundle = Bundle(path: path) ?? Bundle.main
            let alertController: GlobileAlertController = GlobileAlertController(title: title, message: message)
            alertController.addAction(GlobileAlertAction(title: "Done".localizedString(bundle: bundle, tableName: "GlobileAppEngine"), style: .primary, action: {
                alertController.dismiss(animated: true)
            }))
            present(alertController, animated: true)
        }

    }
}
