//
import Foundation
import UIKit


public func setPrivateModeEnabled(active: Bool) {
    do {
        try DataBus.shared.save(object: active, forKey: AppEnginePlaces.constantPrivateMode.rawValue, expiry: .never)
    } catch {
        print("Error!!")
    }
}
public func getPrivateModeEnabled()->Bool? {
    do {
        if let privateModeEnabled = try DataBus.shared.load(forKey:AppEnginePlaces.constantPrivateMode.rawValue, withType: Bool.self) {
            return privateModeEnabled
        }
        return nil
    } catch  {
        print("Invalid User.")
    }
    return nil
}
public func setPrivateModeEnableSensor(active: Bool) {
    do {
        try DataBus.shared.save(object: active, forKey: AppEnginePlaces.constantPrivateModeEnableSensor.rawValue, expiry: .never)
    } catch {
        print("Error!!")
    }
}
public func getPrivateModeEnableSensor()->Bool? {
    do {
        if let privateModeEnabledSensor = try DataBus.shared.load(forKey:AppEnginePlaces.constantPrivateModeEnableSensor.rawValue, withType: Bool.self) {
             return privateModeEnabledSensor
         }
         return nil
        
    } catch  {
        print("Invalid User.")
    }
    return nil
}
