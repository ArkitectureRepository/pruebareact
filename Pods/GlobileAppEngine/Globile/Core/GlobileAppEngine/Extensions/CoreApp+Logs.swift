//
import UIKit
public enum LogType {
    case info
    case warning
    case error
    case trace
}
public func globileAppEngineLog(logType: LogType, str: String) {
    switch logType {
    case .info:
        print("✅✅✅✅ " + str)
        break
    case .error:
        print("❌❌❌❌ " + str)
        break
    case .trace:
        print("🔵🔵🔵🔵 " + str)
        break
    case .warning:
        print("⚠️⚠️⚠️⚠️ " + str)
        break
    }
}
