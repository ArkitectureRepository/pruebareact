//
import Foundation
import UIKit
import SantanderUIKitLib

extension UIViewController {
    
    //MARK: Errors
    @objc public func showErrorFullScreen(title: String = "", subtitle: String = "", titleIsHidden: Bool = false, subtitleIsHidden: Bool = false, buttonLabel: String? = nil , delegate: ConnectionFailedProtocol) {
        if let path = Bundle(for: TabBarViewController.self).path(forResource: "GlobileAppEngine", ofType: "bundle") {
            let bundle = Bundle(path: path) ?? Bundle.main
            if let errorConnectionView = GlobileFailedConnection.instanceGeneralErrorFromNib() {
                var strTitle =  title != "" ? title: "TitleConnectionFailed".localizedString(bundle: bundle, tableName: "GlobileAppEngine")
                if titleIsHidden  {
                    strTitle = ""
                }
                var strSubtitle = subtitle != "" ? subtitle : "SubtitleConnectionFailed".localizedString(bundle: bundle, tableName: "GlobileAppEngine")
                if subtitleIsHidden {
                    strSubtitle = ""
                }
                let buttonTitle =  buttonLabel != nil ? buttonLabel! : "Retry".localizedString(bundle: bundle, tableName: "GlobileAppEngine")                
                errorConnectionView.setupErrorNibInView(type: .fullScreenError, in: self.view, globileError: errorConnectionView, title: strTitle, subtitle: strSubtitle, buttonLabel: buttonTitle, delegate: delegate, titleHidden: titleIsHidden, subtitleHidden: subtitleIsHidden)
                
                
            }
        }
    }
    
    
    
    
    @objc public func showErrorPartial(in view: UIView, title: String =  "" , subtitle: String = "", buttonLabel: String? = nil, actionButtonHidden: Bool = false, titleIsHidden: Bool = false, subtitleIsHidden: Bool = false, delegate: ConnectionFailedProtocol) {
        
        if let path = Bundle(for: TabBarViewController.self).path(forResource: "GlobileAppEngine", ofType: "bundle") {
            let bundle = Bundle(path: path) ?? Bundle.main
            if let errorConnectionView = GlobileFailedConnection.instanceGeneralErrorFromNib() {
                var strTitle =  title != "" ? title: "TitleConnectionFailed".localizedString(bundle: bundle, tableName: "GlobileAppEngine")
                if titleIsHidden  {
                    strTitle = ""
                }
                var strSubtitle = subtitle != "" ? subtitle : "SubtitleConnectionFailed".localizedString(bundle: bundle, tableName: "GlobileAppEngine")
                if subtitleIsHidden {
                    strSubtitle = ""
                }
                let buttonTitle =  buttonLabel != nil ? buttonLabel! : "Retry".localizedString(bundle: bundle, tableName: "GlobileAppEngine")
                
                errorConnectionView.setupErrorNibInView(type: .partialError, in: view, globileError: errorConnectionView,title: strTitle, subtitle: strSubtitle, buttonLabel: buttonTitle, delegate: delegate, actionButtonHidden: actionButtonHidden, titleHidden: titleIsHidden, subtitleHidden: subtitleIsHidden)
                
            }
        }
    }
    @objc public func showErrorBannerBehindNavigationBar(title: String) {
        if let errorBanner = GlobileFailedConnectionBanner.instanceBannerErrorFromNib() {
            var distance: CGFloat = 0.0
            
            if let nav = self.navigationController  {
                let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
                    (nav.navigationBar.frame.height)
                distance = topBarHeight
            }
            errorBanner.setupBannerErrorNib(in: view, globileError: errorBanner, title: title, topConstrainConstant: distance)
        }
    }
    @objc public func showErrorBanner(in view: UIView, title: String, topConstrainConstant: CGFloat) {
        if let errorBanner = GlobileFailedConnectionBanner.instanceBannerErrorFromNib() {
            errorBanner.setupBannerErrorNib(in: view, globileError: errorBanner, title: title, topConstrainConstant: topConstrainConstant)
        }
    }
    
    @objc public func showErrorSessionExpired() {
        
        if let path = Bundle(for: TabBarViewController.self).path(forResource: "GlobileAppEngine", ofType: "bundle") {
            let bundle = Bundle(path: path) ?? Bundle.main
            let title = "TitleExpired".localizedString(bundle: bundle, tableName: "GlobileAppEngine")
            let subtitle = "SubTitleExpired".localizedString(bundle: bundle, tableName: "GlobileAppEngine")
            let buttonLabel = "GoToLogin".localizedString(bundle: bundle, tableName: "GlobileAppEngine")
            if let errorSessionExpired = GlobileFailedConnection.instanceGeneralErrorFromNib() {
                errorSessionExpired.setupErrorNibInView(type: .sessionExpiredError, in: self.view, globileError: errorSessionExpired, title: title, subtitle: subtitle, buttonLabel: buttonLabel, delegate: nil)
                errorSessionExpired.actionButton.addTarget(self, action: #selector(sessionExpired), for: .touchUpInside)
            } else { globileAppEngineLog(logType: .error, str: "GlobileAppEngine bundle not found") }
        }
    }
    
    @objc public func showSuccess(title:String, subtitle:String, delegate: ConnectionFailedProtocol) {
        
        if let path = Bundle(for: TabBarViewController.self).path(forResource: "GlobileAppEngine", ofType: "bundle") {
            let bundle = Bundle(path: path) ?? Bundle.main
            let buttonLabel = "Continue".localizedString(bundle: bundle, tableName: "GlobileAppEngine")
            if let error = GlobileFailedConnection.instanceGeneralErrorFromNib() {
                error.setupErrorNibInView(type: .successMessage, in: self.view, globileError: error, title: title, subtitle: subtitle, buttonLabel: buttonLabel, delegate: delegate)
            } else { globileAppEngineLog(logType: .error, str: "GlobileAppEngine bundle not found") }
        }
    }
    
    @objc public func showErrorUnsuccess(title:String, subtitle:String, delegate: ConnectionFailedProtocol) {
        
        if let path = Bundle(for: TabBarViewController.self).path(forResource: "GlobileAppEngine", ofType: "bundle") {
            let bundle = Bundle(path: path) ?? Bundle.main
            let buttonLabel = "Retry".localizedString(bundle: bundle, tableName: "GlobileAppEngine")
            if let error = GlobileFailedConnection.instanceGeneralErrorFromNib() {
                error.setupErrorNibInView(type: .unsuccessMessage, in: self.view, globileError: error, title: title, subtitle: subtitle, buttonLabel: buttonLabel, delegate: delegate)
            } else { globileAppEngineLog(logType: .error, str: "GlobileAppEngine bundle not found") }
        }
    }
    
    @objc func sessionExpired(sender: UIButton!) {
        print("Button tapped")
        self.navigationController?.dismiss(animated: true, completion: {
            DataBus.shared.clearCache()
        })
    }
    @objc public func hideError() {
        for view in  self.view.subviews {
            if view.isKind(of: GlobileFailedConnection.self) {
                view.removeFromSuperview()
                break
            }
        }
    }
    @objc public func hideErrorBanner() {
        for view in  self.view.subviews {
            if view.isKind(of: GlobileFailedConnectionBanner.self) {
                view.removeFromSuperview()
                break
            }
        }
    }
}
