//
import Foundation
import UIKit
import SantanderUIKitLib
import GlobileUtilsLib

public func createTabBar() -> UITabBarController? {
    let tabBar = GlobileTabBar()

    if let entrypoints = SingletonData.shared.entrypoints {
        if let filter = entrypoints.tabbar {
            var vcs: [UIViewController] = []
             filter.sorted(by: { $0.tag < $1.tag }).forEach {
                if let vc = controllerFinder(name: $0.component ?? "", storyboardFlowName: $0.storyBoardFlowName, identifierViewControllerStoryBoard: $0.idStoryboardController) {
                    var tabBarItem: UITabBarItem?
                    if let icon = $0.image, let title = $0.title {
                        let tag = $0.tag
                        let image = UIImage(named: icon, in: Bundle.main, compatibleWith: nil)
                        tabBarItem = UITabBarItem(title: title, image: image, tag: tag)
                    }
                    let nav  = UINavigationController(rootViewController: vc)
                    nav.tabBarItem = tabBarItem
                    vcs.append(nav)
                } else {
                    if let component = $0.component {
                        globileAppEngineLog(logType: .error, str: GlobileAppEngineError(type: .cantFindComponent, code: 500, message: "").error.rawValue + " \(component)")
                    }
                }
            }
            tabBar.setViewControllers(vcs, animated: true)
        }
    }
    return tabBar
}

//public func createTab(name: String?) -> UIViewController? {
//
//}
