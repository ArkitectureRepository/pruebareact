//
import Foundation
import GlobileNetworkReachabilityLib

public protocol CoreAppReachabilityDelegate {
    func reachabilityDidChange(isReachable: Bool, callbackName: String?)
}

public class CoreAppReachability: NetworkActivityObserverDelegate  {
    public var delegate: CoreAppReachabilityDelegate?
    public var callbackName = "callComponentObservable"
    public func didChangeReachability(isReachable: Network) {
        if let reachability = isReachable.reachability, let status = isReachable.status {
            SingletonData.shared.reachability = reachability
            delegate?.reachabilityDidChange(isReachable: reachability, callbackName: callbackName)
            globileAppEngineLog(logType: .trace, str: "Reachability did change: \(reachability), source: \(status)")
        }
    }
    var networkReachability: NetworkActivityObserver?
    public init() {
        networkReachability = NetworkActivityObserver()
        networkReachability?.delegate = self
    }
}

