//
import Foundation
import SantanderUIKitLib
import UIKit

open class GlobilePrivateModeViewController : GlobileViewController {
    open override func viewDidLoad() {
        super.viewDidLoad()
        if let prMode = getPrivateModeEnabled(), let enableSensor = getPrivateModeEnableSensor() {
            self.privateMode = prMode
            self.enabledSensor = enableSensor
        }
    }
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}
