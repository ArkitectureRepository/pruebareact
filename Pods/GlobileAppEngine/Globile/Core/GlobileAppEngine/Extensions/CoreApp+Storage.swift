//
import Foundation
import UIKit
import CacheLib
import Cache
import LocalStorageLib
import STGSecure_Storage
import SCAL_iOS
import GlobileUtilsLib

public enum PersistenceType {
    case local, securedlocal, both
}

public class DataBus {
    let stg_instance = SCALSecureFiles(secureFiles: STGSecureFiles())
    public static let shared = DataBus()
    let securedFileName = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String

    //MARK: Cache
    public func save<T: Codable>(object: T, forKey key: String, expiry: Expiry = .never) throws {
        do {
            try CacheLib.shared.saveCacheData(object: object, forKey: key, expiry: expiry)
        } catch let error {
            print(error.localizedDescription)
        }
    }

    public func load<T: Codable>(forKey key: String, withType type: T.Type, removeAfter: Bool = false) throws -> T? {

        if let object = try? CacheLib.shared.getCacheData(forKey: key, withType: type) {
            if removeAfter {
                self.remove(forKey: key)
            }
            return object
        }
        if let object = stg_instance.readFromSecureFile(alias: securedFileName), let result = String(bytes: object, encoding: .utf8) {
            do {
                return try utils_json_decode(result, type: type)

            } catch {

            }
            return nil
        }
        if let object = try? LocalStorageLib.shared.load(forKey: key, withType: type) {
            do {
                if removeAfter {
                    try LocalStorageLib.shared.remove(key: key)
                }
                return object
            } catch {
                globileAppEngineLog(logType: .error, str: "An error occurred searching object in memory...")
                return nil
            }
        }
        return nil
    }

    //MARK: Persistence
    public func savePersistent<T: Codable>(object: T, forKey key: String, secured: Bool = false) throws {
        switch secured {
        case true:
            do {
                let content = try utils_json_encode(object)
                _ = stg_instance.writeToSecureFile(alias: securedFileName, data: content.data(using: .utf8), fileMode: .defaultMode)
            }
        case false: LocalStorageLib.shared.save(object: object, forKey: key)
        }
    }

    public func remove(forKey key: String) {
        try? CacheLib.shared.removeCacheData(forKey: key)
        try? LocalStorageLib.shared.remove(key: key)
    }

    public func clearAll() {
        clearCache()
        clearPersistence(storageType: .both)
    }

    public func clearCache() {
        do {
            try  CacheLib.shared.clearCache()
        } catch let error {
            print(error.localizedDescription)
        }
    }
    public func clearPersistence(storageType: PersistenceType) {
        switch storageType {
        case .local: try? LocalStorageLib.shared.clearLocalStorage()
        case .securedlocal: if stg_instance.clearSecureFile(alias: securedFileName) {globileAppEngineLog(logType: .info, str: "Secure removed.")}
        case .both:
            do {
                if stg_instance.clearSecureFile(alias: securedFileName) {globileAppEngineLog(logType: .info, str: "Secure removed.")}
                try LocalStorageLib.shared.clearLocalStorage()
            } catch {
                globileAppEngineLog(logType: .error, str: "Exception while clearing data...")
            }
        }
    }

    public func takeMyInitialParameters(componentName: String) -> [String:Any]? {
        var returnParams: [String: Any] = [:]
        do {
            if let auxParams = try DataBus.shared.load(forKey: AppEnginePlaces.initialParams.rawValue, withType: [[String: [String: CustomType]]].self) {
                auxParams.forEach{
                    for (k, v) in $0 {
                        if k.elementsEqual(componentName) {
                            for (c, s) in v {
                                switch s.value {
                                case let v as String: returnParams[c] = v
                                case let v as Int: returnParams[c] = v
                                case let v as Float: returnParams[c] = v
                                case let v as Bool: returnParams[c] = v
                                default: globileAppEngineLog(logType: .error, str: "Unrecognized type when casting...")
                                }
                            }
                        }
                    }
                }
            }
        } catch {
            globileAppEngineLog(logType: .error, str: "Uanable to get initial params from cache")
        }
        return returnParams
    }
}

