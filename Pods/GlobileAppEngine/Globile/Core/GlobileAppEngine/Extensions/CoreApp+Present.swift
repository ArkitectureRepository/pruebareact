//
import Foundation
import UIKit

//Extension for present tasks

extension UIViewController {
    public  func beginPresent(name: String) {
        if let controller = controllerFinder(name: name) {
            controller.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: controller, action: #selector(self.close))
            let navigationController = UINavigationController(rootViewController: controller)
            if #available(iOS 13.0, *) {
                navigationController.modalPresentationStyle = .fullScreen
            } else {
                // Fallback on earlier versions
            }
            present(navigationController, animated: true)
        }
    }
    @objc func close(){
        self.dismiss(animated: true)
    }
}
