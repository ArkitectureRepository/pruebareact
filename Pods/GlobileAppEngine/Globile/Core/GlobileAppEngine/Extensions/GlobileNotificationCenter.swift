//
import Foundation
import UIKit

public extension Notification.Name {
    static let tokenExpired = Notification.Name("tokenExpired")
    static let tokenInvalid = Notification.Name("tokenInvalid")
}
public class GlobileNotificationCenter: NotificationCenter {
   public static let shared = GlobileNotificationCenter()
    public func  addObserverNotification(notificationSelector: Selector, viewController: UIViewController, notificationName: Notification.Name) {
        addObserver(viewController, selector: notificationSelector, name: notificationName, object: nil)
    }
}
extension UIViewController {
    @objc public func registerNotificationExpired(){
        GlobileNotificationCenter.shared.addObserverNotification(notificationSelector:#selector(logout(_:)) ,viewController: self, notificationName: Notification.Name.tokenExpired)
    }
     @objc func logout(_ notification: Notification) {
        self.navigationController?.dismiss(animated: true, completion: {
            DataBus.shared.clearCache()
        })
    }
}
