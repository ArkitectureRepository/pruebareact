//
import Foundation
import GlobileNetworkReachabilityLib
import GlobileUtilsLib

public class SingletonData {
    public static let shared = SingletonData()
    public var entrypoints: EntryObject?
    public var reachability: Bool?
    public var reachabilityObject: CoreAppReachability?
    public var dependencyInyection: [String: Any] = [:]
    public var hybridConfig: HybridBundle?
    public var analyticsConfig: FAConfig?
    public init() { }
}
