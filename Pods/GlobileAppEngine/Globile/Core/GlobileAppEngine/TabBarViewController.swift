//
//  TabBarViewController.swift
//  CoreAppEngine
//
//  Created by Marco Daniel Iñiguez Ollero on 31/10/2019.
//  Copyright © 2019 globileplatform. All rights reserved.
//

import Foundation
import UIKit
import SantanderUIKitLib

@objc(MainMenu)
public class TabBarViewController: UIViewController {

    // MARK: - Properties


    // MARK: - Oveerride
    override public func viewDidLoad() {
        super.viewDidLoad()
        createMenuTab()
    }
    func createMenuTab() {
        let initial = createTabBar()
        if let initial = initial {
            self.add(initial)
        }
    }
    func add(_ child: UIViewController) {
       addChild(child)
       child.view.frame = view.frame
       UIView.transition(with: self.view, duration: 0.25, options: [.transitionFlipFromLeft], animations: {
           self.view.addSubview(child.view)
       }, completion: nil)
       child.didMove(toParent: self)
   }

  
}
