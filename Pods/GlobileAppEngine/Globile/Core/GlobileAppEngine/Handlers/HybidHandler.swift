import GlobileUtilsLib

public protocol HybridHandlerProtocol {
     func requestInitParams()
}

open class HybridHandler: HybridHandlerProtocol {
    public static let shared: HybridHandler = HybridHandler()
    public static var delegate: HybridHandlerProtocol?

    public func requestInitParams() {

    }
}
