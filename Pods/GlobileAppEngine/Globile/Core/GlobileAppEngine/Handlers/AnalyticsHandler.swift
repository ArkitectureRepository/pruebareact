import GlobileUtilsLib

public protocol AnalyticsHandlerProtocol {
    func trackView(screenName: String?, data: [String: Any]?)
    func trackEvent(eventName: String?, data: [String: Any]?)
}

open class AnalyticsHandler: AnalyticsHandlerProtocol {
    public static let shared: AnalyticsHandler = AnalyticsHandler()
    public static var delegate: AnalyticsHandlerProtocol?
    public var coreappLocation: CoreAppGeolocationHandler?
    var tagging = defaultTagging

    public func trackView(screenName: String?, data: [String: Any]?) {
        if let screenName = screenName, let data = data {
            AnalyticsHandler.delegate?.trackView(screenName: screenName, data: data)
        }
    }

    public func trackEvent(eventName: String?, data: [String: Any]?) {
        if let eventName = eventName, let data = data {
            AnalyticsHandler.delegate?.trackEvent(eventName: eventName, data: data)
        }
    }

    public func sendDefaultTagging(defaultValues: [String: Any]) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppEnginePlaces.analyticsdefaulttagging.rawValue), object: nil, userInfo: defaultValues)
    }

    public func setTealiumConfig() {
        coreappLocation = CoreAppGeolocationHandler()
        CoreAppGeolocationHandler.delegate = self
        CoreAppHandlerManager.data.analyticsConfig = plistDecoder(forResource: AppEnginePlaces.analyticsconfigfile.rawValue, type: FAConfig.self)
        tagging["Language"] = getLanguage().translateLangName()
        if let uuid = info_uuid() { tagging["Uuid"] = uuid }
        tagging["environment"] = info_environment()
        tagging["AppName"] = info_appname()
        tagging["AppVersion"] = info_appFullVersion()
    }
}

extension AnalyticsHandler: CoreAppGeolocationHandlerDelegate {
    public func fetchCountry(_ country: String) {
        tagging["Country"] = country
        globileAppEngineLog(logType: .info, str: "THE COUNTRY IS:::::\(country)")
        sendDefaultTagging(defaultValues: tagging)
    }
}
