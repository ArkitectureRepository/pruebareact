//
public struct CoreAppHandlerManager {
    public static let analytics = AnalyticsHandler.shared
    public static let hybrid = HybridHandler.shared
    public static let storage = DataBus.shared
    public static let data = SingletonData.shared
}
