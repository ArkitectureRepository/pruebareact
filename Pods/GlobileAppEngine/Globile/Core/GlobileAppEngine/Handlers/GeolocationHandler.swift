import GeolocationLib
import GlobileUtilsLib
import CoreLocation

public protocol CoreAppGeolocationHandlerDelegate {
    func fetchCountry(_ country: String)
    func didReceiveLocation(_ location: CLLocation)
}

public extension CoreAppGeolocationHandlerDelegate {
    func fetchCountry(_ country: String) {}
    func didReceiveLocation(_ location: CLLocation) {}
}

public class CoreAppGeolocationHandler {
    var geolocation = GeolocationLib()
    public static var delegate: CoreAppGeolocationHandlerDelegate?

    public init() {
        geolocation.delegate = self
    }
}

extension CoreAppGeolocationHandler: GeolocationLibDelegate {
    public func didReceiveLocation(_ location: GeolocationLibResponse) {
        CoreAppGeolocationHandler.delegate?.didReceiveLocation(location.toCLLocation())

        geolocation.fetchCountry(from: location.toCLLocation()) { (country, error) in
            if error == nil {
                if let countryName = country {
                    print(Locale.current)
                    CoreAppGeolocationHandler.delegate?.fetchCountry(countryName)
                }
            }
        }
    }
}
