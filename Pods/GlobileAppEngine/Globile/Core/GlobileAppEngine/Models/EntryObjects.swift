//
import Foundation
import GlobileUtilsLib

public enum AppEnginePlaces: String {
    case initialParams = "initialParameters",
    constantPrivateMode = "privateModeEnabled",
    hybridcomponentAlias = "BridgeKit",
    constantPrivateModeEnableSensor = "constantPrivateModeEnableSensor",
    analyticsdefaulttagging = "arch_functionalanlytics_defaultTagging",
    analyticsconfigfile = "FunctionalAnalytics"
}

public struct EntryObject: Codable {
    let components: [String]?
    let initial: String?
    public let defaultLanguage: String?
    var initParameters: [[String: [String: CustomType]]]?
    let tabbar: [Tabbar]?
    public let hybridBundle: [HybridBundle]?
    public let appSettings: AppSettings?
    
    enum CodingKeys: String, CodingKey {
        case components = "components"
        case initial = "initial"
        case defaultLanguage = "default_language"
        case tabbar = "tabbar"
        case initParameters = "initParameters"
        case hybridBundle = "hybridBundle"
        case appSettings = "appSettings"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        initParameters = try values.decodeIfPresent([[String: [String: CustomType]]].self, forKey: .initParameters)
        components = try values.decodeIfPresent([String].self, forKey: .components)
        initial = try values.decodeIfPresent(String.self, forKey: .initial)
        defaultLanguage = try values.decodeIfPresent(String.self, forKey: .defaultLanguage)
        tabbar = try values.decodeIfPresent([Tabbar].self, forKey: .tabbar)
        hybridBundle = try values.decodeIfPresent([HybridBundle].self, forKey: .hybridBundle)
        appSettings = try values.decodeIfPresent(AppSettings.self, forKey: .appSettings)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(components, forKey: .components)
        try container.encode(initial, forKey: .initial)
        try container.encode(tabbar, forKey: .tabbar)
        try container.encode(hybridBundle, forKey: .hybridBundle)
    }
}

public struct AppSettings: Codable {
    public let disablePrivateTextViewSensors: Bool?
    public let disablePrivateTextViewMode: Bool?
    public let flavour: Flavour?
    
    enum CodingKeys: String, CodingKey {
        case disablePrivateTextViewSensors = "disablePrivateTextViewSensors"
        case disablePrivateTextViewMode = "disablePrivateTextViewMode"
        case flavour = "flavour"
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        disablePrivateTextViewSensors = try values.decodeIfPresent(Bool.self, forKey: .disablePrivateTextViewSensors)
        disablePrivateTextViewMode = try values.decodeIfPresent(Bool.self, forKey: .disablePrivateTextViewMode)
        flavour = try values.decodeIfPresent(Flavour.self, forKey: .flavour)
    }
}

//TODO: cleanup rest of model for Swift 5 and improve parsing of filepath with space chars
public struct Flavour: Codable {
    public let type: FlavourType?
    public let country: FlavourCountry?
    public enum FlavourType: String, Codable {
        case SME, retail, none
    }
    public enum FlavourCountry: String, Codable {
        case ES, PT, UK, US, global
    }
    public init(type: FlavourType, country: FlavourCountry) {
        self.type = type
        self.country = country
    }
}

public struct HybridBundle: Codable {
    public let alias: String?
    public let id: String?
    public let initModule: String?
    public let initUrl: String?
    public let webParams: [String: String]?
    public let navigationBarHidden: Bool?
    
    public init(alias: String?, id: String?, initModule: String?, initUrl: String?, webParams: [String: String], navigationBarHidden: Bool?) {
        self.alias = alias
        self.id = id
        self.initUrl = initUrl
        self.initModule = initModule
        self.webParams = webParams
        self.navigationBarHidden = navigationBarHidden
    }
}

public struct Tabbar: Codable {
    let component: String?
    let image: String?
    let tag: Int
    let title: String?
    let storyBoardFlowName: String?
    let idStoryboardController: String?
    enum CodingKeys: String, CodingKey {
        case component = "component"
        case image = "image"
        case tag = "tag"
        case title = "title"
        case storyBoardFlowName = "storyBoardFlowName"
        case idStoryboardController = "idStoryboardController"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        component = try values.decodeIfPresent(String.self, forKey: .component)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        tag = try (values.decodeIfPresent(Int.self, forKey: .tag) ?? 0)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        storyBoardFlowName = try values.decodeIfPresent(String.self, forKey: .storyBoardFlowName)
        idStoryboardController = try values.decodeIfPresent(String.self, forKey: .idStoryboardController)
    }
}

public struct CustomType: Codable {
    var value: Any?
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(String.self) {
            value = x
        }
        if let x = try? container.decode(Int.self) {
            value = x
        }
        if let x = try? container.decode(Float.self) {
            value = x
        }
        if let x = try? container.decode(Bool.self) {
            value = x
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch value {
        case let v as String: try? container.encode(v)
        case let v as Int: try? container.encode(v)
        case let v as Float: try? container.encode(v)
        case let v as Bool: try? container.encode(v)
            
        default:
            try? container.encode(value as! String)
        }
    }
}



