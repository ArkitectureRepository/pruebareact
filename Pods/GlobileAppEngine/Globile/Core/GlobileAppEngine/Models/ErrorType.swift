//
import Foundation

/// Custom error for CoreLib.
public struct GlobileAppEngineError: Error {
    public enum ErrorType: String {
        case route  = "route"
        case present = "present"
        case cantFindController = "cantFindController"
        case cantFindComponent = "cantFindComponent"
        case whileGeneratingTabBar = "whileGeneratingTabBar"
        case saveDataError = "saveDataError"
        case loadDataError = "loadDataError"
        case loadDataErrorCasting = "loadDataErrorCasting"
        case cantDecodeJSONfile = "cantDecodeJSONfile"
    }
    public var errorCode: Int
    public var message: String
    public var error: ErrorType

    public init(type: ErrorType, code: Int, message: String) {
              errorCode = code
              error = type
              self.message = message
          }
}
