//
//  GlobileCards.swift
//  Cards
//
//  Created by l.arranz.martinez on 10/04/2019.
//  Copyright © 2019 l.arranz.martinez. All rights reserved.
//

import Foundation

import UIKit
import CoreGraphics

public class GlobileCards: UIView {

    override public init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
}

extension GlobileCards {

    func setup() {
        layer.borderColor = UIColor.globileMediumSky.cgColor
        layer.borderWidth = 1.0
        layer.cornerRadius = 5.0
        backgroundColor = .globileWhite
        addShadow()
    }

    private func addShadow() {
        clipsToBounds = false
        layer.masksToBounds = false
        layer.shadowColor = UIColor.globileLightGrey.cgColor
        layer.shadowOpacity = 0.7
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowRadius = 3
    }
}
