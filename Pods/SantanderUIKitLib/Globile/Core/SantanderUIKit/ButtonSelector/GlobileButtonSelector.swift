//
import UIKit

public class GlobileButtonSelector: UISegmentedControl {

    private let backgroundTintColor = UIColor.globileButtonSelectorBackgroundColor
    private let borderColor = UIColor.globileMediumSky
    private let selectedTextColor = UIColor.globileSantanderRed
    private let textColor = UIColor.globileDarkGrey
    private let cornerRadius: CGFloat = 5.0

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    public override init(items: [Any]?) {
        super.init(items: items)
        setupView()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    // MARK: Private

    private func setupView() {
        selectedSegmentIndex = 0

        backgroundColor = backgroundTintColor
        tintColor = .globileWhite

        layer.cornerRadius = cornerRadius
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = 1.0
        layer.masksToBounds = true

        setTitleTextAttributes([NSAttributedString.Key.foregroundColor: selectedTextColor,
                                NSAttributedString.Key.font: UIFont.bold(size: 14)], for: .selected)
        setTitleTextAttributes([NSAttributedString.Key.foregroundColor: textColor,
                                NSAttributedString.Key.font: UIFont.regular(size: 16)], for: .normal)

        subviews.forEach { segment in
            segment.layer.cornerRadius = 0.0
            segment.layer.borderWidth = 1.0
            segment.layer.borderColor = self.borderColor.cgColor
            segment.layer.masksToBounds = true
        }

    }

}
