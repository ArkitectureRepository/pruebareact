//
//  GlobilePageController.swift
//  SantanderUIKit
//
import UIKit

public class GlobilePageController: UIView {

    var numberOfPages: Int = 0

    lazy var pageControl: ISPageControl = {
        let pc = ISPageControl(frame: self.frame, numberOfPages: numberOfPages)
        pc.inactiveTintColor = .globileLightGrey
        pc.currentPageTintColor = .globileSantanderRed
        return pc
    }()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
}

// MARK: Override methods
extension GlobilePageController {
    public override var intrinsicContentSize: CGSize {
        return pageControl.intrinsicContentSize
    }
}

// MARK: Public methods
extension GlobilePageController {

    public func getCurrentPage() -> Int {
        return pageControl.currentPage
    }

    public func setCurrentPage(_ page: Int) {
        if page >= 0 && page < numberOfPages {
            pageControl.currentPage = page
        }
    }

    public func setNumberOfPages(_ pages: Int) {
        numberOfPages = pages
        pageControl.numberOfPages = pages
    }

    public func increase(_ offset: Int) {
        let nextPage = pageControl.currentPage + offset
        if nextPage >= 0 && nextPage < pageControl.numberOfPages {
            setCurrentPage(nextPage)
        }
    }

    public func decrease(_ offset: Int) {
        let nextPage = pageControl.currentPage - offset
        if nextPage >= 0 && nextPage < pageControl.numberOfPages {
            setCurrentPage(nextPage)
        }
    }
}

// MARK: Configuration
extension GlobilePageController {
    fileprivate func setup() {
        backgroundColor = .clear
        addSubview(pageControl)
        pageControl.anchor(left: leftAnchor, top: topAnchor, right: rightAnchor, bottom: bottomAnchor)
    }
}
