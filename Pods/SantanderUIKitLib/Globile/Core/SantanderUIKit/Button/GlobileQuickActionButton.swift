//

import UIKit

public enum GlobileQuickActionButtonIconStyle {
    case red, darksky, accesibleSky, grey, white
}

public class GlobileQuickActionButton: GlobileButton {
    
    /// Sets the button style
    public var style: GlobileQuickActionButtonIconStyle = .accesibleSky
    
    /// Boolean value to set  button enable or disabled
    public override var isEnabled: Bool {
        didSet {
            changeState()
        }
    }
        
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    let iconBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
}

// MARK: Override methods
extension GlobileQuickActionButton {
    public override var intrinsicContentSize: CGSize {
        var labelSizeWidth = titleLabel?.sizeThatFits(CGSize(width: frame.width, height: .greatestFiniteMagnitude)).width ?? 80
        if labelSizeWidth < CGFloat(44.0) { labelSizeWidth = 44.0 }
        return CGSize(width: labelSizeWidth, height: 72.0)
    }
}

extension GlobileQuickActionButton {
    
    override func setup() {
        super.setup()
        addSubview(iconBackgroundView)
        addImageBackground()
        sendSubviewToBack(iconBackgroundView)
        
        addTarget(self, action: #selector(actionSelectorEndEditing), for: .touchUpInside)
    }
    
    /// Sets the icon to use on the left side.
    ///
    /// - Parameter icon: The icon to use
    public func setIcon(_ icon: UIImage?) {
        guard let icon = icon else { return }
        setImage(icon.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), position: .top, withPadding: 8.0)
    }
    
    public func setCachedIcon(_ url: URL?, placeholder: UIImage) {
        guard let url = url else { return }
        setCachedImage(url, position: .top, scale: 1.0, placeholder: placeholder)
    }
    
    override func setupSubviews() {
        
        alignVerticalIcon(withSpacing: 12.0)
        
        changeState()
        
        if imageView?.image == nil {
            let emptyView = UIView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
            emptyView.alpha = 0.0
            setIcon(emptyView.asActionButtonImage())
        }
        
        setTitleColor(isEnabled ? .globileMediumGrey: .globileDarkGreyDisabled, for: .normal)
        titleLabel?.numberOfLines = 1
        titleLabel?.font = .regular(size: fontSize)
        titleLabel?.lineBreakMode = .byTruncatingTail
        
        adjustsImageWhenHighlighted = false
    }
    
    private func alignVerticalIcon(withSpacing spacing: CGFloat) {
        self.imageView?.contentMode = .scaleAspectFit
        guard let imageSize = self.imageView?.image?.size
            else { return }
        
        titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -imageSize.width, bottom: -(imageSize.height + spacing), right: 0.0)
        let buttonWidth = self.frame.width
        let imageWidth = self.imageView!.frame.width
        imageEdgeInsets = UIEdgeInsets(top: -(30 + spacing), left: (buttonWidth - imageWidth)/2, bottom: 0, right: (buttonWidth - imageWidth)/2) //30 for single line
        
        let edgeOffset = abs(30 - imageSize.height) / 2.0 //30 for single line
        contentEdgeInsets = UIEdgeInsets(top: edgeOffset, left: 0.0, bottom: edgeOffset, right: 0.0)
        
    }
    
    private func addImageBackground() {
        
        iconBackgroundView.layer.cornerRadius = 22.0
        iconBackgroundView.clipsToBounds = true
        iconBackgroundView.layer.masksToBounds = false
        iconBackgroundView.layer.shadowColor = UIColor.globileLightGrey.cgColor
        iconBackgroundView.layer.shadowOpacity = 0.7
        iconBackgroundView.layer.shadowOffset = CGSize(width: 2, height: 2)
        iconBackgroundView.layer.shadowRadius = 3.0
        
        changeState()
        
        NSLayoutConstraint.activate([
            iconBackgroundView.heightAnchor.constraint(equalToConstant: 44),
            iconBackgroundView.widthAnchor.constraint(equalToConstant: 44),
            iconBackgroundView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        if let image = imageView {
            iconBackgroundView.centerYAnchor.constraint(equalTo: image.centerYAnchor).isActive = true
        } else {
            iconBackgroundView.topAnchor.constraint(equalTo: self.topAnchor, constant: -7).isActive = true
        }

    }
    
    fileprivate func changeState() {
        
        if isEnabled {
            switch style {
            case .red, .darksky, .accesibleSky, .grey:
                imageView?.tintColor = .globileWhite
            case .white:
                imageView?.tintColor = .globileDarkGreyAllModes
            }
            titleLabel?.textColor = .globileMediumGrey
            
        } else {
            imageView?.tintColor = .globileGreyTint50
            titleLabel?.textColor = .globileLightGrey
        }
        
        if isEnabled {
            switch style {
            case .red:
                iconBackgroundView.backgroundColor = .globileSantanderRed
            case .darksky:
                iconBackgroundView.backgroundColor = .globileDarkSky
            case .accesibleSky:
                iconBackgroundView.backgroundColor = .globileAccesibleSky
            case .grey:
                iconBackgroundView.backgroundColor = .globileDarkGrey
            case .white:
                iconBackgroundView.backgroundColor = .globileWhite
            }
        } else {
            iconBackgroundView.backgroundColor = .globileWhite
        }
    }
    
    @objc fileprivate func actionSelectorEndEditing() {
        self.parentViewController?.view.endEditing(true)
    }
    
    public override func addTarget(_ target: Any?, action: Selector, for controlEvents: UIControl.Event) {
        super.addTarget(target, action: action, for: controlEvents)
        let tap = UITapGestureRecognizer(target: target, action: action)
        iconBackgroundView.addGestureRecognizer(tap)
    }
    
}

extension GlobileQuickActionButton {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

extension UIView {
    fileprivate func asActionButtonImage() -> UIImage {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: bounds)
            return renderer.image { rendererContext in
                layer.render(in: rendererContext.cgContext)
            }
        } else {
            UIGraphicsBeginImageContext(self.frame.size)
            self.layer.render(in:UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return UIImage(cgImage: image!.cgImage!)
        }
    }
}
