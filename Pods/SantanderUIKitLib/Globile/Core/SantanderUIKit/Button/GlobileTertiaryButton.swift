//
//  GlobileInitialButton.swift
//  SantanderUIKit
//
//  Created by adrian.a.fernandez on 06/02/2019.
//
import UIKit

public enum GlobileTertiaryButtonStyle {
    case primaryRed, secondaryTurquoise, secondaryYellow, secondaryLimeGreen, secondaryBlue, secondaryPurple
}

public class GlobileTertiaryButton: GlobileButton {

    public var style: GlobileTertiaryButtonStyle = .primaryRed
    private let iconPadding = 14.0

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }


    /// Sets the icon to use on the left side.
    ///
    /// - Parameter icon: The icon to use
    public func setIcon(_ icon: UIImage?) {
        guard let icon = icon else { return }
        setImage(icon, position: .left, withPadding: 14.0)
    }


    /// A Boolean value indicating whether the button shows a chevron icon on the right side.
    public var chevronEnabled = false {
        didSet {
            if chevronEnabled {
                setImage(getImage(named: "chevron"), position: .right)
            } else {
                setImage(nil, for: .normal)
            }
        }
    }


    /// Sets the title to use with an optional underlined text.
    ///
    /// - Parameters:
    ///   - link: The link to use. It is underlined.
    ///   - text: The text to use.
    public func setTitle(link: String?, text: String) {
        if let link = link {
            let underlinedTextAttributes: [NSAttributedString.Key: Any] = [
                NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
                NSAttributedString.Key.font: UIFont.microTextRegular(size: fontSize)
            ]
            let underlinedText = NSMutableAttributedString(string: link, attributes: underlinedTextAttributes)

            let space = NSMutableAttributedString(string: " ", attributes: nil)

            let normalTextAttributes: [NSAttributedString.Key: Any] = [
                NSAttributedString.Key.foregroundColor: UIColor.globileBlack,
                NSAttributedString.Key.font: UIFont.microTextRegular(size: fontSize)
            ]
            let normalText = NSMutableAttributedString(string: text, attributes: normalTextAttributes)

            let attributedText = NSMutableAttributedString()
            attributedText.append(underlinedText)
            attributedText.append(space)
            attributedText.append(normalText)

            setAttributedTitle(attributedText, for: .normal)
        } else {
            setTitle(text, for: .normal)
        }
    }

}

extension GlobileTertiaryButton {

     override func setupSubviews() {
        super.setupSubviews()

        switch style {
        case .primaryRed:
            tintColor = .globileSantanderRed
        case .secondaryTurquoise:
            tintColor = .globileTurquoise
        case .secondaryYellow:
            tintColor = .globileYellow
        case .secondaryLimeGreen:
            tintColor = .globileLimeGreen
        case .secondaryBlue:
            tintColor = .globileBlue
        case .secondaryPurple:
            tintColor = .globilePurple
        }
    }
}
