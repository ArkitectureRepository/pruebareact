//
//  GlobileInitialButton.swift
//  SantanderUIKit
//
//  Created by adrian.a.fernandez on 06/02/2019.
//
import UIKit

public enum GlobileInitialButtonIconStyle {
    case red, darksky, accesibleSky, grey, white
}

public class GlobileInitialButton: GlobileButton {

    public var iconTintColor: GlobileInitialButtonIconStyle = .red

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}

// MARK: Override methods
extension GlobileInitialButton {
    public override var intrinsicContentSize: CGSize {
        return CGSize(width: 300.0, height: 88.0)
    }
}

extension GlobileInitialButton {

    override func setup() {
        super.setup()

        backgroundColor = .globileWhite
        setTitleColor(.globileDarkGrey, for: .normal)
        titleLabel?.font = .regular(size: fontSize)
        titleLabel?.numberOfLines = 2
        titleLabel?.lineBreakMode = .byTruncatingTail

        addShadow()

        addTarget(self, action: #selector(actionSelectorEndEditing), for: .touchUpInside)
    }

    override func setupSubviews() {
        super.setupSubviews()

        layer.borderColor = UIColor.globileMediumSky.cgColor
        layer.borderWidth = 1.0
        layer.cornerRadius = 5.0

        //Defaul case
        imageView?.tintColor = .globileSantanderRed

        switch iconTintColor {
        case .red:
            imageView?.tintColor = .globileSantanderRed
        case .darksky:
            imageView?.tintColor = .globileDarkSky
        case .accesibleSky:
            imageView?.tintColor = .globileAccesibleSky
        case .grey:
            imageView?.tintColor = .globileDarkGrey
        case .white:
            imageView?.tintColor = .globileWhite
        }
    }

    private func addShadow() {
        clipsToBounds = false

        layer.masksToBounds = false
        layer.shadowColor = UIColor.globileLightGrey.cgColor
        layer.shadowOpacity = 0.7
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowRadius = 3
    }

    @objc fileprivate func actionSelectorEndEditing() {
        self.parentViewController?.view.endEditing(true)
    }

}

extension GlobileInitialButton {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
