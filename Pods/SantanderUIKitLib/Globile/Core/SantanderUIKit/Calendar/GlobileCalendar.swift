//
//  CalendarDateRangePickerViewController.swift
//  CalendarDateRangePickerViewController
//
//

import UIKit

public enum GlobileCalendarColorStyle {
    case red, accesibleSky, darksky, darkGray
}

public enum GlobileCalendarHeaderColorStyle {
    case white
}

@objc public enum CalendarMode: Int {
    case singleDate
    case rangeDate
}

@objc public enum FirstWeekDay: Int {
    case monday
    case sunday
}

public protocol GlobileCalendarDelegate: class {
    func didCancelPickingDateRange()
    func didPickDateRange(startDate: Date!, endDate: Date!)
    func didSelectStartDate(startDate: Date!)
    func didSelectEndDate(endDate: Date!)
}

open class GlobileCalendar: UICollectionViewController {
    @objc let cellReuseIdentifier = "CalendarDateRangePickerCell"
    @objc let headerReuseIdentifier = "CalendarDateRangePickerHeaderView"
    
    weak public var delegate: GlobileCalendarDelegate!
    
    @objc let itemsPerRow = 7
    @objc let itemHeight: CGFloat = 40
    @objc let collectionViewInsets = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 25)
    
    @objc public var minimumDate: Date!
    @objc public var maximumDate: Date!
    var calendarMode = CalendarMode.rangeDate
    @objc public var firstDayOfWeek = FirstWeekDay.sunday
    
    var selectedStartDate: Date?
    var selectedEndDate: Date?
    @objc var selectedStartCell: IndexPath?
    @objc var selectedEndCell: IndexPath?
    
    @objc public var disabledDates: [Date]?
    
    private var selectedRangeColor = UIColor.globileLightGrey
    //    private var selectedRangeColor = UIColor.hex(hex: "#F1F1F1", alpha: 0.94)!
    private var cellFont: UIFont = .microTextRegular(size: 14.0)
    private var daysFont: UIFont = .microTextRegular(size: 14.0)
    private var monthHeaderFont: UIFont = .microTextRegular(size: 14.0)
    private var monthAndDayHeaderFontColor: GlobileCalendarColorStyle = .darkGray
    private var monthHeaderBackgroundColor: GlobileCalendarHeaderColorStyle = .white
    private var generalStyleColor: GlobileCalendarColorStyle = .accesibleSky
    @objc public var selectedLabelColor = UIColor.globileWhite
    @objc public var highlightedLabelColor = UIColor.globileDarkGrey
    
    public var titleText = ""
    var mainHeaderPosition: CGFloat = 0.0
    
    var calendar = Calendar.current
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .globileWhite
        
        var selectDaterRangeStr = "Select Date Range"
        var cancelStr = "Cancel"
        var doneStr = "Done"
        
        if let path = Bundle(for: GlobileCalendar.self).path(forResource: "SUKit", ofType: "bundle"){
            
            let bundle = Bundle(path: path) ?? Bundle.main
            selectDaterRangeStr = "SelectDateRange".localizedString(bundle: bundle, tableName: "SUIKit_strings")
            cancelStr = "Cancel".localizedString(bundle: bundle, tableName: "SUIKit_strings")
            doneStr = "Done".localizedString(bundle: bundle, tableName: "SUIKit_strings")
        }
        
        self.titleText = selectDaterRangeStr
        self.title = self.titleText
        if let navContr = self.navigationController {
            
            if #available(iOS 13.0, *) {
                mainHeaderPosition = navContr.navigationBar.frame.height
            } else {
                mainHeaderPosition = navContr.navigationBar.frame.height + 12
            }
        }
        
        if firstDayOfWeek == FirstWeekDay.monday {
            calendar.firstWeekday =  2
        } else {
            calendar.firstWeekday =  1
        }
        collectionView?.backgroundColor = .globileWhite
        collectionView?.register(CalendarDateRangePickerCell.self, forCellWithReuseIdentifier: cellReuseIdentifier)
        collectionView?.register(CalendarDateRangePickerHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerReuseIdentifier)
        collectionView?.contentInset = collectionViewInsets
        
        if minimumDate == nil {
            minimumDate = Date()
        }
        if maximumDate == nil {
            maximumDate = calendar.date(byAdding: .year, value: 3, to: minimumDate)
        }
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: cancelStr, style: .plain, target: self, action: #selector(self.didTapCancel))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: doneStr, style: .done, target: self, action: #selector(self.didTapDone))
        self.navigationItem.rightBarButtonItem?.isEnabled = selectedStartDate != nil && selectedEndDate != nil
        if self.calendarMode == .singleDate {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let padding = collectionViewInsets.left + collectionViewInsets.right
        let mainHeader = setMainheaderWeek()
        
        mainHeader.frame = CGRect(x: 25.0, y: mainHeaderPosition, width: collectionView.frame.width - padding, height: itemHeight)
        mainHeader.backgroundColor = .globileYellow
        self.view.addSubview(mainHeader)
        self.view.bringSubviewToFront(mainHeader)
        
        self.title = self.titleText
        
        collectionView?.frame = CGRect(x: self.collectionView.frame.origin.x, y: mainHeaderPosition + itemHeight, width: self.collectionView.frame.width, height: self.collectionView.frame.height - (mainHeaderPosition + itemHeight))
        collectionView?.dataSource = self
        collectionView?.delegate = self
        self.collectionView.performBatchUpdates(nil) { (isLoded) in
            let difference = self.calendar.dateComponents([.month], from: self.minimumDate, to: Date())
            let currentMonth = difference.month!

            if isLoded{
                self.collectionView.scrollToItem(at: IndexPath(item: 0, section: currentMonth), at: [.centeredVertically, .centeredHorizontally], animated: false)
            }
        }
    }
    
    
    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    @objc public func initSingleMode(startDate: Date?) {
        self.calendarMode = .singleDate
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        self.selectedStartDate = startDate
        
    }
    
    @objc public func initRangeMode(startDate: Date, endDate: Date) {
        self.calendarMode = .rangeDate
        self.selectedStartDate = startDate
        self.selectedEndDate = endDate
    }
    
    @objc public func initRangeMode() {
        self.calendarMode = .rangeDate
    }
    
    @objc func didTapCancel() {
        delegate.didCancelPickingDateRange()
    }
    
    @objc func didTapDone() {
        if self.calendarMode == CalendarMode.singleDate {
            if selectedStartDate == nil {
                return
            }
            delegate.didPickDateRange(startDate: selectedStartDate!, endDate: nil)
        } else {
            if selectedStartDate == nil || selectedEndDate == nil {
                return
            }
            delegate.didPickDateRange(startDate: selectedStartDate!, endDate: selectedEndDate!)
        }
    }
}

extension GlobileCalendar {
    
    // UICollectionViewDataSource
    
    override open func numberOfSections(in collectionView: UICollectionView) -> Int {
        let difference = calendar.dateComponents([.month], from: minimumDate, to: maximumDate)
        return difference.month! + 1
    }
    
    override open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let firstDateForSection = getFirstDateForSection(section: section)
        let weekdayRowItems = 0
        
        var blankItems = getWeekday(date: firstDateForSection) - 2
        
        if firstDayOfWeek == FirstWeekDay.monday {
            blankItems = getWeekday(date: firstDateForSection) - 2
            if blankItems < 0 {
                blankItems = 6
            }
        } else {
            blankItems = getWeekday(date: firstDateForSection) - 1
        }
        
        let daysInMonth = getNumberOfDaysInMonth(date: firstDateForSection)
        return weekdayRowItems + blankItems + daysInMonth
    }
    
    override open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as? CalendarDateRangePickerCell {
            
            
            cell.highlightedColor = self.selectedRangeColor
            switch generalStyleColor {
            case .red:
                cell.selectedColor = .globileSantanderRed
            case .accesibleSky:
                cell.selectedColor = .globileAccesibleSky
            case .darksky:
                cell.selectedColor = .globileDarkSky
            case .darkGray:
                cell.selectedColor = .globileDarkGrey
            }
            
            cell.selectedLabelColor = self.selectedLabelColor
            cell.highlightedLabelColor = self.highlightedLabelColor
            cell.font = self.cellFont
            cell.reset()
            
            var blankItems = getWeekday(date: getFirstDateForSection(section: indexPath.section)) - 2
            if firstDayOfWeek == FirstWeekDay.monday {
                blankItems = getWeekday(date: getFirstDateForSection(section: indexPath.section)) - 2
                if blankItems < 0 {
                    blankItems = 6
                }
            } else {
                blankItems = getWeekday(date: getFirstDateForSection(section: indexPath.section)) - 1
            }
            if indexPath.item < blankItems {
                cell.label.text = ""
            } else {
                let dayOfMonth = indexPath.item - (blankItems) + 1
                let date = getDate(dayOfMonth: dayOfMonth, section: indexPath.section)
                
                //
                if selectedStartDate != nil && selectedEndDate != nil && isBefore(dateA: date, dateB: selectedStartDate!) && isBefore(dateA: selectedEndDate!, dateB: date) {
                    //Backwards tints internal range days.
                    let endDate = selectedStartDate
                    let startDate = selectedEndDate
                    selectedStartDate = nil
                    selectedEndDate = nil
                    selectedStartDate = startDate
                    selectedEndDate = endDate
                } else if selectedStartDate != nil && selectedEndDate != nil && areSameDay(dateA: date, dateB: selectedStartDate!) && isBefore(dateA: selectedEndDate!, dateB: date) {
                    //Backwards startData & endDate are togheter.
                    let endDate = selectedStartDate
                    let startDate = selectedEndDate
                    selectedStartDate = nil
                    selectedEndDate = nil
                    selectedStartDate = startDate
                    selectedEndDate = endDate
                }
                
                cell.date = date
                cell.drawBorderCurrentDay()
                cell.label.text = "\(dayOfMonth)"
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                
                if disabledDates != nil {
                    if (disabledDates?.contains(cell.date!))! {
                        cell.disable()
                    }
                }
                if isBefore(dateA: date, dateB: minimumDate) {
                    cell.disable()
                }
                if isAfter(dateA: date, dateB: maximumDate) {
                    cell.disable()
                }
                if selectedStartDate != nil && selectedEndDate != nil &&
                    isBefore(dateA: selectedStartDate!, dateB: date) && isBefore(dateA: date, dateB: selectedEndDate!) {
                    // Cell falls within selected range
                    //Tints internal range days.
                    cell.drawBorderCurrentDayRange()
                    if dayOfMonth == 1 {
                        if #available(iOS 9.0, *) {
                            if UIView.appearance().semanticContentAttribute == .forceRightToLeft {
                                cell.highlightLeft()
                            } else {
                                cell.highlightRight()
                            }
                        }
                        cell.label.textColor = self.highlightedLabelColor
                    } else if dayOfMonth == getNumberOfDaysInMonth(date: date) {
                        if #available(iOS 9.0, *) {
                            if UIView.appearance().semanticContentAttribute == .forceRightToLeft {
                                cell.highlightRight()
                            } else {
                                cell.highlightLeft()
                            }
                        }
                        cell.label.textColor = self.highlightedLabelColor
                    } else {
                        cell.highlight()
                    }
                } else if selectedStartDate != nil && areSameDay(dateA: date, dateB: selectedStartDate!) {
                    // Cell is selected start date
                    // Tints starting range date.
                    if selectedStartDate != nil && selectedEndDate != nil && areSameDay(dateA: selectedEndDate!, dateB: selectedStartDate!) {
                        //Case startDate and endDate are the same.
                        cell.select()
                    } else {
                        cell.select()
                        if selectedEndDate != nil {
                            //Forwards starting date.
                            if #available(iOS 9.0, *) {
                                if UIView.appearance().semanticContentAttribute == .forceRightToLeft {
                                    cell.highlightLeft()
                                } else {
                                    cell.highlightRight()
                                }
                            }
                        }
                    }
                } else if selectedEndDate != nil && areSameDay(dateA: date, dateB: selectedEndDate!) {
                    // Tints ending range date.
                    if selectedStartDate != nil && selectedEndDate != nil && isBefore(dateA: selectedStartDate!, dateB: selectedEndDate!) {
                        cell.select()
                        //Forwards ending date.
                        if #available(iOS 9.0, *) {
                            if UIView.appearance().semanticContentAttribute == .forceRightToLeft {
                                cell.highlightRight()
                            } else {
                                cell.highlightLeft()
                            }
                        }
                    } else {
                        cell.select()
                        //Backwards ending date.
                        if #available(iOS 9.0, *) {
                            if UIView.appearance().semanticContentAttribute == .forceLeftToRight {
                                cell.highlightLeft()
                            } else {
                                cell.highlightRight()
                            }
                        }
                    }
                }
            }
            return cell
        }
        return CalendarDateRangePickerCell()
    }
    
    override open func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            if let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerReuseIdentifier, for: indexPath) as? CalendarDateRangePickerHeaderView {
                headerView.font = monthHeaderFont
                
                switch monthHeaderBackgroundColor {
                case .white:
                    headerView.backgroundColor = .globileWhite
                }
                
                let label = UILabel(frame: CGRect(x: 0, y: 0, width: headerView.frame.width, height: headerView.frame.height))
                label.font = monthHeaderFont
                
                switch monthAndDayHeaderFontColor {
                case .red:
                    label.textColor = .globileSantanderRed
                case .accesibleSky:
                    label.textColor = .globileAccesibleSky
                case .darksky:
                    label.textColor = .globileDarkSky
                case .darkGray:
                    label.textColor = .globileDarkGrey
                }
                
                label.textAlignment = NSTextAlignment.left
                label.text = getMonthLabel(date: getFirstDateForSection(section: indexPath.section))
                for view in headerView.subviews {
                    view.removeFromSuperview()
                }
                headerView.addSubview(label)
                
                //
                //            if indexPath.section != 0 {
                //                let blankView = UIView(frame: CGRect(x: 0, y: 0, width: headerView.frame.width, height: 4.0))
                //                blankView.backgroundColor = SantanderColor.globileWhite()
                //                headerView.addSubview(blankView)
                //                headerView.bringSubviewToFront(blankView)
                //            }
                
                return headerView
            }
            return CalendarDateRangePickerHeaderView()
        default:
            fatalError("Unexpected element kind")
        }
    }
}

extension GlobileCalendar: UICollectionViewDelegateFlowLayout {
    
    override open func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? CalendarDateRangePickerCell {
            if cell.date == nil {
                return
            }
            if isBefore(dateA: cell.date!, dateB: minimumDate) {
                return
            }
            if isAfter(dateA: cell.date!, dateB: maximumDate) {
                return
            }
            if disabledDates != nil {
                if (disabledDates?.contains(cell.date!))! {
                    return
                }
            }
            if selectedStartDate == nil {
                selectedStartDate = cell.date
                selectedStartCell = indexPath
                delegate.didSelectStartDate(startDate: selectedStartDate)
            } else if selectedEndDate == nil {
                //if let cellDate = cell.date, let startDate = selectedStartDate,  let indexPathStart = selectedStartCell, isBefore(dateA: startDate, dateB: cellDate) && !isBetween(indexPathStart, and: indexPath){
                if self.calendarMode == CalendarMode.singleDate {
                    selectedStartDate = cell.date
                    delegate.didSelectStartDate(startDate: cell.date)
                } else {
                    selectedEndDate = cell.date
                    delegate.didSelectEndDate(endDate: selectedEndDate)
                }
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                //            } else {
                // If a cell before the currently selected start date is selected then just set it as the new start date
                //TODO: Modificar aqui calendario.
                //                selectedStartDate = cell.date
                //                selectedStartCell = indexPath
                //                delegate.didSelectStartDate(startDate: selectedStartDate)
                
                //            }
            } else {
                selectedStartDate = cell.date
                selectedStartCell = indexPath
                delegate.didSelectStartDate(startDate: selectedStartDate)
                selectedEndDate = nil
            }
            collectionView.reloadData()
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding = collectionViewInsets.left + collectionViewInsets.right
        let availableWidth = view.frame.width - padding
        let itemWidth = availableWidth / CGFloat(itemsPerRow)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.size.width, height: 50)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension GlobileCalendar {
    
    // Helper functions
    
    @objc func getFirstDate() -> Date {
        var components = calendar.dateComponents([.month, .year], from: minimumDate)
        components.day = 1
        return calendar.date(from: components)!
    }
    
    @objc func getFirstDateForSection(section: Int) -> Date {
        return calendar.date(byAdding: .month, value: section, to: getFirstDate())!
    }
    
    @objc func getMonthLabel(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM yyyy"
        return dateFormatter.string(from: date)
    }
    
    @objc func getWeekdayLabel(weekday: Int) -> String {
        var components = DateComponents()
        components.calendar = calendar
        components.weekday = weekday
        let date = calendar.nextDate(after: Date(), matching: components, matchingPolicy: Calendar.MatchingPolicy.strict)
        if date == nil {
            return "E"
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEEE"
        return dateFormatter.string(from: date!)
        
    }
    
    @objc func getWeekday(date: Date) -> Int {
        return calendar.dateComponents([.weekday], from: date).weekday!
    }
    
    @objc func getNumberOfDaysInMonth(date: Date) -> Int {
        return calendar.range(of: .day, in: .month, for: date)!.count
    }
    
    @objc func getDate(dayOfMonth: Int, section: Int) -> Date {
        var components = calendar.dateComponents([.month, .year], from: getFirstDateForSection(section: section))
        components.day = dayOfMonth
        return calendar.date(from: components)!
    }
    
    @objc func areSameDay(dateA: Date, dateB: Date) -> Bool {
        return calendar.compare(dateA, to: dateB, toGranularity: .day) == ComparisonResult.orderedSame
    }
    
    @objc func isBefore(dateA: Date, dateB: Date) -> Bool {
        return calendar.compare(dateA, to: dateB, toGranularity: .day) == ComparisonResult.orderedAscending
    }
    
    @objc func isAfter(dateA: Date, dateB: Date) -> Bool {
        return calendar.compare(dateA, to: dateB, toGranularity: .day) == ComparisonResult.orderedDescending
    }
    
    @objc func isBetween(_ startDateCellIndex: IndexPath, and endDateCellIndex: IndexPath) -> Bool {
        if disabledDates == nil {
            return false
        }
        
        var index = startDateCellIndex.row
        var section = startDateCellIndex.section
        var currentIndexPath: IndexPath
        var cell: CalendarDateRangePickerCell?
        
        while !(index == endDateCellIndex.row && section == endDateCellIndex.section) {
            currentIndexPath = IndexPath(row: index, section: section)
            cell = collectionView?.cellForItem(at: currentIndexPath) as? CalendarDateRangePickerCell
            if cell?.date == nil {
                section += 1
                let blankItems = getWeekday(date: getFirstDateForSection(section: section)) - 1
                index = 7 + blankItems
                currentIndexPath = IndexPath(row: index, section: section)
                cell = collectionView?.cellForItem(at: currentIndexPath) as? CalendarDateRangePickerCell
            }
            
            if cell != nil && (disabledDates?.contains((cell!.date)!))! {
                return true
            }
            index += 1
        }
        
        return false
    }
    
    func setMainheaderWeek() -> UIStackView {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .fillEqually
        stackView.backgroundColor = .globileYellow
        
        let selectedDayColor: UIColor?
        switch monthAndDayHeaderFontColor {
        case .red:
            selectedDayColor = .globileSantanderRed
        case .accesibleSky:
            selectedDayColor = .globileAccesibleSky
        case .darksky:
            selectedDayColor = .globileDarkSky
        case .darkGray:
            selectedDayColor = .globileDarkGrey
        }
        
        for day in 1...7 {
            let label = UILabel()
            label.font = daysFont
            label.textColor = selectedDayColor
            label.textAlignment = .center
            if calendar.firstWeekday == 2 {
                if day < 7 {
                    label.text = getWeekdayLabel(weekday: day + 1).uppercased()
                } else {
                    label.text = getWeekdayLabel(weekday: 1).uppercased()
                }
            } else {
                label.text = getWeekdayLabel(weekday: day).uppercased()
            }
            stackView.addArrangedSubview(label)
        }
        return stackView
    }
}
