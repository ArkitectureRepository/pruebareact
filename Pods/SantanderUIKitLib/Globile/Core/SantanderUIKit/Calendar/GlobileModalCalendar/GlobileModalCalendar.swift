//
//  GlobileModalCalendar.swift
//  SantanderUIKitLib
//
//  Created by marco.iniguez.ollero on 26/02/2020.
//

import Foundation
import GlobileUtilsLib
public protocol GlobileModalCalendarDelegate: class {
    func selectDateRange(startDate: Date!, endDate: Date!)
    func selectDate(date:Date!)
}
public class GlobileModalCalendar: UIView {
    
    @IBOutlet weak var dateLabel: UILabel!
    private var dateLabelFont: UIFont = .headlineRegular(size: 16.0)
    private var dateLabelFontColor: GlobileCalendarColorStyle = .darkGray
    
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var containerCalendarView: UIView!
    
    var globileCalendar: GlobileCalendar?
    var delegate:GlobileModalCalendarDelegate?
    let dateFormatter = DateFormatter()
    var startDate:Date?
    var endDate:Date?
    var minDate:Date?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public class func instanceFromNib() -> GlobileModalCalendar? {
        if let path = Bundle(for: GlobileModalCalendar.self).path(forResource: "SUKit", ofType: "bundle") {
            if let bundle = Bundle(path: path) {
                if let nib = bundle.loadNibNamed("GlobileModalCalendar", owner: nil, options: nil)?[0] as? GlobileModalCalendar {
                    return nib
                }
            }
        }
        return nil
    }
    
    public func setUp(delegate: GlobileModalCalendarDelegate, in view: UIView, modalCalendar: GlobileModalCalendar, mode: CalendarMode = .singleDate, minimunDate:Date?, maximumDate: Date?, startDate: Date?, endDate: Date?, generalStyleColor: GlobileCalendarColorStyle = .accesibleSky, firstDayOfWeek:FirstWeekDay = .monday, selectedLabelColor: UIColor = .globileWhite, highlightedLabelColor: UIColor = .globileDarkGrey ) {
        
        saveButton.layer.borderColor = UIColor.globileAccesibleSky.cgColor
        saveButton.layer.borderWidth = 1
        saveButton.layer.cornerRadius = 16
        saveButton.titleLabel?.font = .microTextRegular(size: 14.0)
        saveButton.tintColor = .globileAccesibleSky
        
        var saveStr = "Save"
        var clearStr = "Clear"
        var todayStr = "Today"
        var initialDateStr = "Initial Date"
        var endDateStr = "End Date"
        var toUnionStr = " to "
        
        if let path = Bundle(for: GlobileModalCalendar.self).path(forResource: "SUKit", ofType: "bundle"){

            let bundle = Bundle(path: path) ?? Bundle.main
            saveStr = "save".localizedString(bundle: bundle, tableName: "SUIKit_strings")
            clearStr = "clear".localizedString(bundle: bundle, tableName: "SUIKit_strings")
            todayStr = "Today".localizedString(bundle: bundle, tableName: "SUIKit_strings")
            initialDateStr = "InitialDate".localizedString(bundle: bundle, tableName: "SUIKit_strings")
            endDateStr = "EndDate".localizedString(bundle: bundle, tableName: "SUIKit_strings")
            toUnionStr = "to.union".localizedString(bundle: bundle, tableName: "SUIKit_strings")
        }
        saveButton.setTitle(saveStr, for: .normal)
        
        clearButton.tintColor = .globileDarkGrey
        clearButton.titleLabel?.font = .microTextRegular(size: 14.0)
        clearButton.setTitle(clearStr, for: .normal)
        switch dateLabelFontColor {
        case .red:
            dateLabel.textColor = .globileSantanderRed
        case .accesibleSky:
            dateLabel.textColor = .globileAccesibleSky
        case .darksky:
            dateLabel.textColor = .globileDarkSky
        case .darkGray:
            dateLabel.textColor = .globileDarkGrey
        }
        
        dateFormatter.dateFormat = "d MMM, yyyy"
        globileCalendar = GlobileCalendar(collectionViewLayout: UICollectionViewFlowLayout())
        if let dateRangePickerViewController = globileCalendar {
            self.delegate = delegate
            dateRangePickerViewController.delegate = self
            dateRangePickerViewController.minimumDate = minimunDate
            dateRangePickerViewController.maximumDate = maximumDate
            
            self.minDate = minimunDate
            
            dateLabel.text = todayStr
            dateLabel.font = dateLabelFont
            
            if let minimumD = minimunDate {
                if Date() < minimumD {
                    dateLabel.text = initialDateStr
                } else {
                    dateLabel.text = initialDateStr + " - " + endDateStr
                }
            }
            
            if let sDate = startDate, let eDate = endDate {
                dateLabel.text = dateFormatter.string(from: sDate) + toUnionStr + dateFormatter.string(from: eDate)
            }
            self.startDate = startDate
            self.endDate = endDate
            switch mode {
            case .rangeDate:
                if let start = startDate, let end = endDate {
                    dateRangePickerViewController.initRangeMode(startDate: start, endDate: end)
                }
            default:
                if let start = startDate {
                    dateRangePickerViewController.initSingleMode(startDate: start)
                }
            }
            dateRangePickerViewController.firstDayOfWeek = firstDayOfWeek
            dateRangePickerViewController.selectedLabelColor = selectedLabelColor
            dateRangePickerViewController.highlightedLabelColor = highlightedLabelColor
            self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height - 64)
            self.containerCalendarView.addSubview(dateRangePickerViewController.view)
            
            modalCalendar.alpha = 0.0
            view.addSubview(modalCalendar)
            modalCalendar.fadeIn(0.25 ,completion: {
                (finished: Bool) -> Void in
                view.bringSubviewToFront(modalCalendar)
            })
            
            if let pickerView =  dateRangePickerViewController.view {
                pickerView.translatesAutoresizingMaskIntoConstraints = false
                let horizontalConstraint = NSLayoutConstraint(item: pickerView, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: containerCalendarView, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
                let verticalConstraint = NSLayoutConstraint(item: pickerView, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: containerCalendarView, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
                let widthConstraint = NSLayoutConstraint(item: pickerView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: containerCalendarView, attribute: NSLayoutConstraint.Attribute.width, multiplier: 1, constant: 0)
                let heightConstraint = NSLayoutConstraint(item:pickerView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: containerCalendarView, attribute: NSLayoutConstraint.Attribute.height, multiplier: 1, constant: 0)
                //
                //                let top = NSLayoutConstraint(item:pickerView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: containerCalendarView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)
                //
                //                let bottom = NSLayoutConstraint(item:pickerView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: containerCalendarView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0)
                
                NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
                
                //                , top, bottom
                
                
            }
        }
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.fadeOut(0.25, completion: {
            (finished: Bool) -> Void in
            self.removeFromSuperview()
            
        })
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if let del = delegate, let sDate = self.startDate, let eDtate = self.endDate, globileCalendar?.calendarMode == .rangeDate {
            del.selectDateRange(startDate: sDate, endDate: eDtate)
            closeModalCalendar()
        } else if let del = delegate, let sDate = self.startDate, globileCalendar?.calendarMode == .singleDate {
            del.selectDate(date:sDate)
            closeModalCalendar()
        } else if let del = delegate {
            // In everything is nil, returns nil.
            if globileCalendar?.calendarMode == .singleDate {
                del.selectDate(date: nil)
            } else {
                del.selectDateRange(startDate: nil, endDate: nil)
            }
        }
    }
    
    @IBAction func clearAction(_ sender: Any) {
        var initialDateStr = "Initial Date"
        var endDateStr = "End Date"
        var todayStr = "Today"
        
         if let path = Bundle(for: GlobileModalCalendar.self).path(forResource: "SUKit", ofType: "bundle"){

           let bundle = Bundle(path: path) ?? Bundle.main
           todayStr = "Today".localizedString(bundle: bundle, tableName: "SUIKit_strings")
           initialDateStr = "InitialDate".localizedString(bundle: bundle, tableName: "SUIKit_strings")
           endDateStr = "EndDate".localizedString(bundle: bundle, tableName: "SUIKit_strings")
               }
        if let minimumD = minDate {
            if Date() < minimumD {
                dateLabel.text = initialDateStr
            } else {
                dateLabel.text = initialDateStr + " - " + endDateStr
            }
        }
        
        dateLabel.textColor = .darkGray
        dateLabel.font = UIFont.headlineRegular(size: 16.0)
        startDate = nil
        endDate = nil
        dateLabel.text = todayStr
        self.globileCalendar?.selectedEndDate = nil
        self.globileCalendar?.selectedStartDate = nil
        self.globileCalendar?.collectionView.reloadData()
        
    }
    
    private func closeModalCalendar() {
        self.fadeOut(0.25, completion: {
            (finished: Bool) -> Void in
            self.removeFromSuperview()
            
        })
    }
}

extension GlobileModalCalendar: GlobileCalendarDelegate {
    public func didCancelPickingDateRange() {
        self.removeFromSuperview()
    }
    
    public func didPickDateRange(startDate: Date!, endDate: Date!) {
        
    }
    
    public func didSelectStartDate(startDate: Date!) {
        if  let sDate = startDate {
            self.startDate = sDate

            var endDateStr = "End Date"
             if let path = Bundle(for: GlobileModalCalendar.self).path(forResource: "SUKit", ofType: "bundle"){

                  let bundle = Bundle(path: path) ?? Bundle.main
                  endDateStr = "EndDate".localizedString(bundle: bundle, tableName: "SUIKit_strings")
              }
                    
            let string = dateFormatter.string(from: sDate) + " - " + endDateStr
            let startingDateString = dateFormatter.string(from: sDate)
            let restOfString = " - " + endDateStr
            
            let range = (string as NSString).range(of: startingDateString)
            let range2 = (string as NSString).range(of: restOfString)
            
            let attribute = NSMutableAttributedString.init(string: string)
            attribute.addAttribute(NSAttributedString.Key.font, value: UIFont.headlineBold(size: 16.0), range: range)
            attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.globileAccesibleSky , range: range)
            attribute.addAttribute(NSAttributedString.Key.font, value: UIFont.headlineRegular(size: 16.0), range: range2)
            attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray , range: range2)
            
            dateLabel.attributedText = attribute
            
        }
    }
    
    public func didSelectEndDate(endDate: Date!) {
        if  let sDate = self.startDate, let eDate = endDate {
            self.startDate = sDate
            self.endDate = eDate

            var toUnionStr = " to "
            
           if let path = Bundle(for: GlobileModalCalendar.self).path(forResource: "SUKit", ofType: "bundle"){

                let bundle = Bundle(path: path) ?? Bundle.main
                toUnionStr = "to.union".localizedString(bundle: bundle, tableName: "SUIKit_strings")
            }
            
            if sDate < endDate {
                dateLabel.text = dateFormatter.string(from: sDate) + toUnionStr + dateFormatter.string(from: eDate)
                dateLabel.textColor = .globileAccesibleSky
                dateLabel.font = UIFont.headlineBold(size: 16.0)
            } else {
                dateLabel.text = dateFormatter.string(from: sDate) + toUnionStr + dateFormatter.string(from: eDate)
                dateLabel.textColor = .globileAccesibleSky
                dateLabel.font = UIFont.headlineBold(size: 16.0)
            }
        
            
        }
        print(dateFormatter.string(from: endDate))
    }
}
