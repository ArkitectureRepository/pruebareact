//
//  CalendarDateRangePickerHeaderView.swift
//  CalendarDateRangePickerViewController
//
//

import UIKit

class CalendarDateRangePickerHeaderView: UICollectionReusableView {

    @objc var label: UILabel!
    @objc var font = UIFont.microTextRegular(size: 14.0) {
        didSet {
            label?.font = font
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
       // initLabel()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
       // initLabel()
    }

    @objc func initLabel() {
        label = UILabel(frame: frame)
        label.center = CGPoint(x: frame.size.width / 8, y: frame.size.height / 2)
        label.font = font
        label.textColor = UIColor.darkGray
        label.textAlignment = NSTextAlignment.left
        self.addSubview(label)
    }
}
