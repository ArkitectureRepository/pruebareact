//
//  CalendarDateRangePickerCell.swift
//  CalendarDateRangePickerViewController
//
//

import UIKit

class CalendarDateRangePickerCell: UICollectionViewCell {
    private let defaultTextColor = UIColor.globileBlack

    var highlightedColor = UIColor.globileSantanderRed
    private let disabledColor = UIColor.globileLightGrey
    var font = UIFont.headlineRegular(size: 19.0) {
        didSet {
            label?.font = font
        }
    }
    var headerColor = UIColor.globileWhite

    @objc var selectedColor: UIColor!
    @objc var selectedLabelColor: UIColor!
    @objc var highlightedLabelColor: UIColor!
    @objc var disabledDates: [Date]!
    @objc var disabledTimestampDates: [Int]?
    @objc var date: Date?
    @objc var selectedView: UIView?
    @objc var todayView: UIView?

    @objc var halfBackgroundView: UIView?
    @objc var roundHighlightView: UIView?

    @objc var label: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        initLabel()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        initLabel()
    }

    @objc func initLabel() {
        label = UILabel(frame: frame)
        label.center = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        label.font = font
        label.textColor = headerColor
        label.textAlignment = NSTextAlignment.center
        self.addSubview(label)
    }

    @objc func reset() {
        self.backgroundColor = UIColor.clear
        label.textColor = defaultTextColor
        label.backgroundColor = UIColor.clear
        if selectedView != nil {
            selectedView?.removeFromSuperview()
            selectedView = nil
        }
        if halfBackgroundView != nil {
            halfBackgroundView?.removeFromSuperview()
            halfBackgroundView = nil
        }
        if roundHighlightView != nil {
            roundHighlightView?.removeFromSuperview()
            roundHighlightView = nil
        }
        self.todayView?.removeFromSuperview()
    }

    // MARK: Draw border current day

    func drawBorderCurrentDay() {
        if let dateCell = self.date,
            Calendar.current.compare(dateCell, to: Date(), toGranularity: Calendar.Component.day) == ComparisonResult.orderedSame {
        let width = self.frame.size.width
        let height = self.frame.size.height
        todayView = UIView(frame: CGRect(x: (width - height) / 2, y: 0, width: height, height: height))
        todayView?.layer.cornerRadius = height / 2
        todayView?.layer.borderColor = selectedColor.cgColor
        todayView?.layer.borderWidth = 1.0
            todayView?.backgroundColor = .globileWhite
        self.addSubview(todayView!)
        self.sendSubviewToBack(todayView!)
        }
    }
    func drawBorderCurrentDayRange() {
        if let dateCell = self.date,
            Calendar.current.compare(dateCell, to: Date(), toGranularity: Calendar.Component.day) == ComparisonResult.orderedSame {
        let width = self.frame.size.width
        let height = self.frame.size.height
        todayView?.removeFromSuperview()
        todayView = UIView(frame: CGRect(x: (width - height) / 2, y: 0, width: height, height: height))
        todayView?.layer.cornerRadius = height / 2
        todayView?.layer.borderColor = selectedColor.cgColor
        todayView?.layer.borderWidth = 1.0
        todayView?.backgroundColor = highlightedColor

        self.addSubview(todayView!)
        self.sendSubviewToBack(todayView!)
        }
    }

    @objc func select() {
        if let dateCell = self.date,
            Calendar.current.compare(dateCell, to: Date(), toGranularity: Calendar.Component.day) == ComparisonResult.orderedSame {
            self.todayView?.removeFromSuperview()
        }
        let width = self.frame.size.width
        let height = self.frame.size.height
        selectedView = UIView(frame: CGRect(x: (width - height) / 2, y: 0, width: height, height: height))
        selectedView?.backgroundColor = selectedColor
        selectedView?.layer.cornerRadius = height / 2
        self.addSubview(selectedView!)
        self.sendSubviewToBack(selectedView!)
        label.textColor = selectedLabelColor
    }

    @objc func highlightRight() {
        // This is used instead of highlight() when we need to highlight cell with a rounded edge on the left
        let width = self.frame.size.width
        let height = self.frame.size.height
        halfBackgroundView = UIView(frame: CGRect(x: width / 2, y: 0, width: width / 2, height: height))
        halfBackgroundView?.backgroundColor = highlightedColor
        self.addSubview(halfBackgroundView!)
        self.sendSubviewToBack(halfBackgroundView!)
        label.textColor = selectedLabelColor
        addRoundHighlightView()
    }

    @objc func highlightLeft() {
        // This is used instead of highlight() when we need to highlight the cell with a rounded edge on the right.
        let width = self.frame.size.width
        let height = self.frame.size.height
        halfBackgroundView = UIView(frame: CGRect(x: 0, y: 0, width: width / 2, height: height))
        halfBackgroundView?.backgroundColor = highlightedColor
        self.addSubview(halfBackgroundView!)
        self.sendSubviewToBack(halfBackgroundView!)
        label.textColor = selectedLabelColor
        addRoundHighlightView()
    }

    @objc func addRoundHighlightView() {
        let width = self.frame.size.width
        let height = self.frame.size.height
        roundHighlightView = UIView(frame: CGRect(x: (width - height) / 2, y: 0, width: height, height: height))
        roundHighlightView?.backgroundColor = highlightedColor
        roundHighlightView?.layer.cornerRadius = height / 2
        self.addSubview(roundHighlightView!)
        self.sendSubviewToBack(roundHighlightView!)
    }

    @objc func highlight() {
        self.backgroundColor = highlightedColor
        label.textColor = highlightedLabelColor
    }

    @objc func disable() {
        label.textColor = disabledColor
    }
}
