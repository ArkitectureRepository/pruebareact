//
//  GlobileLoaders.swift
//  SantanderUIKitLib
//
//  Created by r.a.sanz.hinojosas on 04/02/2020.
//

import Foundation
import GlobileUtilsLib

public class GlobileLoader: UIView {
    
    @IBOutlet public var gifImage: UIImageView?
    @IBOutlet weak public var titleLabel: UILabel?
    @IBOutlet weak public var subTitleLabel: UILabel?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public class func instanceFromNib() -> GlobileLoader? {
        if let path = Bundle(for: GlobileLoader.self).path(forResource: "SUKit", ofType: "bundle") {
            if let bundle = Bundle(path: path) {
                if let nib = bundle.loadNibNamed("GlobileLoader", owner: nil, options: nil)?[0] as? GlobileLoader {
                    return nib
                }
            }
        }
        return nil
    }
    
    public func setupLoaderNib(in view: UIView?, globileLoader: GlobileLoader?, title: String, subtitle: String, gifName: UIImage? = nil) {
        let path = Bundle(for: GlobileLoader.self).path(forResource: "SUKit", ofType: "bundle")!
        let bundle = Bundle(path: path) ?? Bundle.main
        let pathGif : String = bundle.path(forResource: "SmallLoader", ofType: "gif")!
        let url = URL(fileURLWithPath: pathGif)
        do {
            let gifData = try Data(contentsOf: url)
            var gif: UIImage = try UIImage.init(gifData: gifData)
            if let customGif = gifName {
                gif = customGif
            }
            configureNibElements(title: title, subtitle: subtitle, gifName: gif)
            if let loader = globileLoader {
                view?.addSubview(loader)
                view?.bringSubviewToFront(loader)
            }
            addBlurEffect()
            self.translatesAutoresizingMaskIntoConstraints = false
            let horizontalConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
            let verticalConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
            let widthConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.width, multiplier: 1, constant: 0)
            let heightConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.height, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
        } catch (_) {
            
        }
    }
    
    private func configureNibElements(title: String, subtitle: String, gifName: UIImage) {
        titleLabel?.text = title
        subTitleLabel?.text = subtitle
        gifImage?.setGifImage(gifName)
    }
    
    private func addBlurEffect() {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
        self.bringSubviewToFront(gifImage!)
        self.bringSubviewToFront(titleLabel!)
        self.bringSubviewToFront(subTitleLabel!)
    }
}

