//
//  SegmentedProgressBar.swift
//  GlobileCheckBox
//
//  Created by Naufal Aros el Morabet on 18/03/2019.
//  Copyright © 2019 Naufal Aros el Morabet. All rights reserved.
//

import UIKit

public class GlobileStepper: UIView {

    private var padding: CGFloat = 4.0
    private var segments = [UIView]()
    public var currentIndex = 0

    /// The tint color to apply to apply to the background.
    @IBInspectable
    public var backgroundTintColor: UIColor = .globileLightGrey

    /// The tint color to apply to previous segments.
    @IBInspectable
    public var previousSegmentTintColor: UIColor = .globileSantanderRed

    /// The tint color to apply to the current segment.
    @IBInspectable
    public var currentSegmentTintColor: UIColor = .globileRubyRed

    /// The number of segments.
    @IBInspectable
    public var numberOfSegments: Int = 2

    /// Initializes and returns a segmented progress bar that uses the number of segments specified.
    ///
    /// - Parameter numberOfSegments: number of Segments.
    public init(numberOfSegments: Int) {
        super.init(frame: .zero)
        self.numberOfSegments = numberOfSegments
        addSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public override func awakeFromNib() {
        super.awakeFromNib()
        addSubviews()
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2

        let width = (frame.width - (padding * CGFloat(segments.count - 1)) ) / CGFloat(segments.count)
        for (index, segment) in segments.enumerated() {
            let segFrame = CGRect(x: CGFloat(index) * (width + padding), y: 0, width: width, height: frame.height)
            segment.frame = segFrame
        }
    }

    // MARK: Private

    private func addSubviews() {

        if numberOfSegments > 4 {
            padding = 0.0
            currentSegmentTintColor = previousSegmentTintColor
        }

        for _ in 0 ..< numberOfSegments {
            let segment = UIView()
            segments.append(segment)
            addSubview(segment)
        }

        setupView()
    }

    private func setupView() {
        clipsToBounds = true

        segments.first?.backgroundColor = currentSegmentTintColor
        for segment in segments.dropFirst() {
            segment.backgroundColor = backgroundTintColor
        }
    }


    /// Sets the next segment as selected if possible.
    public func next() {
        let currentSegment = segments[currentIndex]
        currentSegment.backgroundColor = previousSegmentTintColor

        let newIndex = currentIndex + 1
        guard newIndex < segments.count else { return }
        let nextSegment = segments[newIndex]
        nextSegment.backgroundColor = currentSegmentTintColor
        currentIndex += 1
    }


    /// Sets the previous segment as selected if possible.
    public func previous() {
        guard currentIndex != 0 else { return }

        let currentSegment = segments[currentIndex]
        currentSegment.backgroundColor = backgroundTintColor

        let newIndex = currentIndex - 1
        let previousSegment = segments[newIndex]
        previousSegment.backgroundColor = currentSegmentTintColor
        currentIndex -= 1
    }
}
