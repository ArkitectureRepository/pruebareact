//

import UIKit

public protocol GlobileAlertBarDelegate: class {
    func onClosePressed()
    func onActionPressed()
}

public class GlobileAlertBar: UIView {
    
    public var alertBarDelegate: GlobileAlertBarDelegate?
    
    var yPosition: CGFloat = 0
    
    let closeAlertBarButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 58, height: 58))
        button.setImage(getImage(named: "close"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 18, left: 18, bottom: 18, right: 18)
        return button
    }()
    
    let messageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.textColor = .globileBlack
        label.text = "E"
        label.numberOfLines = 2
        return label
    }()

    let actionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .bold(size: 14.0)
        label.textAlignment = .left
        label.textColor = .globileTurquoise
        label.numberOfLines = 1
        return label
    }()

    fileprivate lazy var mediumBackground: UIView = {
        let view = UIView()
        view.backgroundColor = .globileBackground
        return view
    }()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        yPosition = self.frame.origin.y
        addSubviews()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addSubviews()
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        setupViews()
    }

    private func addSubviews() {
        self.addSubview(mediumBackground)
        self.addSubview(closeAlertBarButton)
        self.addSubview(messageLabel)
        self.addSubview(actionLabel)

        setupLayout()

        self.isHidden = true
    }

    private func setupLayout() {
        NSLayoutConstraint.activate([
            closeAlertBarButton.topAnchor.constraint(equalTo: topAnchor),
            closeAlertBarButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4),
            closeAlertBarButton.bottomAnchor.constraint(equalTo: bottomAnchor),
            closeAlertBarButton.heightAnchor.constraint(equalToConstant: 36.0),
            closeAlertBarButton.widthAnchor.constraint(equalTo: closeAlertBarButton.heightAnchor)
            ])

        NSLayoutConstraint.activate([
            actionLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            actionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -18)
            ])

        NSLayoutConstraint.activate([
            messageLabel.topAnchor.constraint(equalTo: topAnchor),
            messageLabel.leadingAnchor.constraint(equalTo: closeAlertBarButton.trailingAnchor, constant: 0),
            messageLabel.trailingAnchor.constraint(lessThanOrEqualTo: actionLabel.leadingAnchor, constant: -12),
            messageLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
        actionLabel.setContentCompressionResistancePriority(UILayoutPriority.required, for: .horizontal)

        mediumBackground.anchor(left: leftAnchor, top: topAnchor, right: rightAnchor, height: 56.0)

    }

    private func setupViews() {
        backgroundColor = .globileMediumSky
        closeAlertBarButton.addTarget(self, action: #selector(closePressed), for: .touchUpInside)

        let tap = UITapGestureRecognizer(target: self, action: #selector(actionPressed))
        actionLabel.isUserInteractionEnabled = true
        actionLabel.addGestureRecognizer(tap)

    }

    @objc func closePressed() {
        hideAlertBar()
        alertBarDelegate?.onClosePressed()
    }

    @objc func actionPressed() {
        hideAlertBar()
        alertBarDelegate?.onActionPressed()
    }

    private var hideTask: DispatchWorkItem?

    /// Show alertBar with message and label.
    ///
    /// - Parameters:
    ///   - messageHTML: main message of the alert. Allow HTML text to show important words in red and bold style
    ///   - actionText: Clickable text to perform and action.
    func showAlertBar(messageHTML: String, actionText: String) {

        let attr = try? NSMutableAttributedString(htmlString: messageHTML, font: .light(size: 14.0), useDocumentFontSize: false)
        messageLabel.attributedText = attr

        actionLabel.text = actionText

        let xPosition = self.frame.origin.x

        let height = self.frame.height
        let width = self.frame.width
        if self.isHidden == true {
            yPosition = self.frame.origin.y
            self.frame = CGRect(x: xPosition, y: self.yPosition-height, width: width, height: height)
            self.isHidden = false
            hideTask = DispatchWorkItem { self.hideAlertBar() }

            UIView.animate(withDuration: 0.275, animations: {
                self.frame = CGRect(x: xPosition, y: self.yPosition, width: width, height: height)

            }, completion: {(_: Bool) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.000, execute: self.hideTask!)

            })
        }
    }

    /// Hide alertBar
    func hideAlertBar() {

        let xPosition = self.frame.origin.x

        let height = self.frame.height
        let width = self.frame.width
        if self.isHidden == false {
            UIView.animate(withDuration: 0.275, animations: {
                self.frame = CGRect(x: xPosition, y: self.yPosition-height, width: width, height: height)

            }, completion: {(_: Bool) in
                self.isHidden = true
                self.hideTask?.cancel()
                self.frame = CGRect(x: xPosition, y: self.yPosition, width: width, height: height)
            })
        }
    }

}

extension NSAttributedString {

    convenience init(htmlString html: String, font: UIFont? = nil, useDocumentFontSize: Bool = true) throws {
        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]

        let data = html.data(using: .utf8, allowLossyConversion: true)
        guard (data != nil), let fontFamily = font?.familyName, let attr = try? NSMutableAttributedString(data: data!, options: options, documentAttributes: nil) else {
            try self.init(data: data ?? Data(html.utf8), options: options, documentAttributes: nil)
            return
        }

        let fontSize: CGFloat? = useDocumentFontSize ? nil : font!.pointSize
        let range = NSRange(location: 0, length: attr.length)
        attr.enumerateAttribute(.font, in: range, options: .longestEffectiveRangeNotRequired) { attrib, range, _ in
            if let htmlFont = attrib as? UIFont {
                let traits = htmlFont.fontDescriptor.symbolicTraits
                var descrip = htmlFont.fontDescriptor.withFamily(fontFamily)
                var font = UIFont.light(size: 15.0)

                if (traits.rawValue & UIFontDescriptor.SymbolicTraits.traitBold.rawValue) != 0 {
                    if let descriptor = descrip.withSymbolicTraits(.traitBold) {
                    descrip = descriptor
                    font = UIFont(descriptor: descrip, size: fontSize ?? htmlFont.pointSize)
                    }
                }

                attr.addAttribute(.font, value: font, range: range)
            }

            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineBreakMode = .byTruncatingTail

            attr.addAttributes([ NSAttributedString.Key.paragraphStyle: paragraphStyle ],
                                   range: range)
        }

        self.init(attributedString: attr)
    }

}

extension GlobileAlertBar {
    open func show(messageHTML: String, actionText: String) {
        showAlertBar(messageHTML: messageHTML, actionText: actionText)
    }
    open func hide() {
        hideAlertBar()
    }
}
