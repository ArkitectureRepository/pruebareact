//
import UIKit

public enum GlobileSliderColor {
    case red
    case turquoise
}

public protocol GlobileSliderDelegate: class {
    func globileSlider(_ slider: GlobileSlider, didChange minimumValue: Float, maximumValue: Float)
}

public class GlobileSlider: UIView {

    // MARK: - Public properties

    /// The object that acts as the delegate for the slider.
    public weak var delegate: GlobileSliderDelegate?

    /// The tint color to apply to the slider's track line and thumbs.
    public var color: GlobileSliderColor = .red {
        didSet {
            switch color {
            case .red:
                slider.colorBetweenHandles = .globileSantanderRed;                slider.handleImage = getImage(named: "thumb_red")
            case .turquoise:
                slider.colorBetweenHandles = .globileTurquoise
                slider.handleImage = getImage(named: "thumb_turquoise")
            }
        }
    }

    /// The minimum value of the slider.
    public var minimumValue: Float {
        get {
            return Float(slider.minValue)
        }
        set {
            slider.minValue = CGFloat(newValue)
        }
    }

    /// The maximum value of the slider.
    public var maximumValue: Float {
        get {
            return Float(slider.maxValue)
        }
        set {
            slider.maxValue = CGFloat(newValue)
        }
    }

    /// The selected minimum value.
    public var selectedMinimumValue: Float {
        get {
            return Float(slider.selectedMinValue)
        }
        set {
            slider.selectedMinValue = CGFloat(newValue)
        }
    }

    /// The selected maximum value.
    public var selectedMaximumValue: Float {
        get {
            return Float(slider.selectedMaxValue)
        }
        set {
            slider.selectedMaxValue = CGFloat(newValue)
        }
    }

    /// A Boolean value indicating whether changes in the slider's value generate continuous update events.
    public var isContinuous: Bool {
        get {
            return !slider.enableStep
        }
        set {
            slider.enableStep = !newValue
        }
    }

    // The step value when the slider is not continous. The default value is 0.
    public var step: Float {
        get {
            return Float(slider.step)
        }
        set {
            slider.step = CGFloat(newValue)
        }
    }

    /// A Boolean value indicating whether the slider is a range. When set to false, use the selectedMaximumValue to get the current selected value. The default value is false.
    public var isRangeable: Bool {
        get {
            return !slider.disableRange
        }
        set {
            slider.disableRange = !newValue
        }
    }

    /// The current text that is displayed by the minimum label.
    public var minimumText: String? {
        didSet {
            minLabel.text = minimumText
        }
    }

    /// The current text that is displayed by the maximum label.
    public var maximumText: String? {
        didSet {
            maxLabel.text = maximumText
        }
    }

    // MARK: - View components

    private var slider: RangeSlider = {
        let slider = RangeSlider()
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.hideLabels = true
        slider.labelsFixed = true
        slider.lineHeight = 3.0
        slider.selectedHandleDiameterMultiplier = 1.0
        slider.tintColor = .globileLightGrey
        return slider
    }()

    private var minLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 1
        label.textAlignment = .left
        label.font = .regular(size: 16.0)
        label.textColor = .globileMediumGrey
        var currencyStr = "€"
        if let path = Bundle(for: GlobileModalCalendar.self).path(forResource: "SUKit", ofType: "bundle"){

             let bundle = Bundle(path: path) ?? Bundle.main
             currencyStr = "Currency".localizedString(bundle: bundle, tableName: "SUIKit_strings")
         }
        
         label.text = "0" + currencyStr
        return label
    }()

    private var maxLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 1
        label.textAlignment = .right
        label.font = .regular(size: 16.0)
        label.textColor = .globileMediumGrey
        var currencyStr = "€"
        if let path = Bundle(for: GlobileModalCalendar.self).path(forResource: "SUKit", ofType: "bundle"){

            let bundle = Bundle(path: path) ?? Bundle.main
            currencyStr = "Currency".localizedString(bundle: bundle, tableName: "SUIKit_strings")
        }
       
        label.text = "5" + currencyStr
        return label
    }()

    // MARK: - Intializer

    public override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addSubviews()
    }

    // MARK: - Private

    private func addSubviews() {
        addSubview(slider)
        addSubview(minLabel)
        addSubview(maxLabel)

        setupLayout()
        setupView()
    }

    private func setupLayout() {
        NSLayoutConstraint.activate([
            slider.topAnchor.constraint(equalTo: topAnchor),
            slider.leadingAnchor.constraint(equalTo: leadingAnchor),
            slider.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])

        NSLayoutConstraint.activate([
            minLabel.topAnchor.constraint(equalTo: slider.bottomAnchor, constant: -4.0),
            minLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16.0),
            minLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0.0)
        ])

        NSLayoutConstraint.activate([
            maxLabel.topAnchor.constraint(equalTo: slider.bottomAnchor, constant: -4.0),
            maxLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16.0),
            maxLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0.0)
        ])
    }

    private func setupView() {
        color = .red
        isRangeable = false
        slider.delegate = self
    }

}

extension GlobileSlider: RangeSliderDelegate {

    public func rangeSeekSlider(_ slider: RangeSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        delegate?.globileSlider(self, didChange: Float(minValue), maximumValue: Float(maxValue))
    }
}
