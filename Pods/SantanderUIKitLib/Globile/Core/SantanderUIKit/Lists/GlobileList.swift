//
//  GlobileAlertController.swift
//  GlobileCore
//
//  Created by Jose Sanchez Olivero on 06/05/2019.
//  Copyright © 2019 Accenture. All rights reserved.
//

import UIKit

public class GlobileList: UITableView {

    //Styles:
    // - Informative: Non-clickable, paddings, transparent background, separator
    // - Card Informative: Non-clickable, inside a card, rounded borders, shadow, separator
    // - Tappable list: Tappable, match parent width, separator
    // - Card with tappable elements: Tappable, inside a card, rounded borders, shadow, separator

    override public init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        initialFrame = self.frame
        backgroundColor = UIColor.clear
        separatorMargins(alignBorder: true)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private var initialFrame: CGRect?

    private var isCardStyle: Bool = false

    override public func layoutSubviews() {
        super.layoutSubviews()
        separatorColor = .globileMediumSky
        bounces = false
    }

    func separatorMargins(alignBorder: Bool) {
        cellLayoutMarginsFollowReadableWidth = false
        if alignBorder {
            separatorInset = UIEdgeInsets.zero
        } else {
            separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        }

        //Top separator line
        if !isCardStyle {
            let tableTopLayout: UIView = {
                let view = UIView()
                view.backgroundColor = UIColor.clear
                view.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: 1/UIScreen.main.scale)
                return view
            }()

            let tableTopLine: UIView = {
                let view = UIView()
                view.backgroundColor = .globileMediumSky
                view.frame = CGRect(x: separatorInset.left, y: 0, width: self.frame.size.width - separatorInset.left*2, height: 1/UIScreen.main.scale)
                return view
            }()

            tableTopLayout.addSubview(tableTopLine)
            tableHeaderView = tableTopLayout
        }

    }

    public func setInteractionEnabled(enabled: Bool) {
        isUserInteractionEnabled = enabled
        if enabled || isCardStyle {
            separatorMargins(alignBorder: true)
        } else {
            separatorMargins(alignBorder: false)
        }
    }

    public func setCardStyle() {

        isCardStyle = true

        tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: 1))
        tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: 1))

        layer.cornerRadius = 5.0
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.globileMediumSky.cgColor

        backgroundColor = .globileWhite

        separatorMargins(alignBorder: true)

        addShadow()

    }

    func addShadow() {
        clipsToBounds = false

        layer.masksToBounds = false
        layer.shadowColor = UIColor(red: 204/255, green: 204/255, blue: 204/255, alpha: 1.0).cgColor
        layer.shadowOpacity = 0.7
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowRadius = 3
    }

    override public var contentSize: CGSize {
        didSet {
            invalidateIntrinsicContentSize()
            layoutIfNeeded()
        }
    }

    override public var intrinsicContentSize: CGSize {
        if isCardStyle {
            frame = CGRect(x: 16, y: self.frame.origin.y, width: (initialFrame?.width ?? self.frame.width) - 32, height: self.frame.height)
                setCardStyle()
        } else {
            frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.width, height: self.frame.height)
        }
        return CGSize(width: self.frame.width, height: contentSize.height)
    }

}
