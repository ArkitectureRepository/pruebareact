//
import UIKit

public class GlobileCell: UITableViewCell {

    var cellView: UIView?

    public var leftLabel: GlobileLabel = {
        let label = GlobileLabel()
        label.edges = UIEdgeInsets(top: 0.0, left: 22, bottom: 0.0, right: 0)
        label.font = .regular(size: 14)
        label.textColor = .globileDarkGrey
        label.textAlignment = .left
        return label
    }()

    public var rightLabel: GlobileLabel = {
        let label = GlobileLabel()
        label.edges = UIEdgeInsets(top: 0.0, left: 0, bottom: 0.0, right: 25)
        label.font = .bold(size: 14)
        label.textColor = .globileDarkGrey
        label.textAlignment = .right
        return label
    }()

    private var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.spacing = 0
        stack.alignment = .center
        return stack
    }()

    /// Init function
    ///
    /// - Parameters:
    ///   - style: cell style
    ///   - reuseIdentifier: name of reuse cell

    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        backgroundColor = UIColor.clear

        // setup()
        contentView.addSubview(stackView)
        stackView.addArrangedSubview(leftLabel)
        stackView.addArrangedSubview(rightLabel)

        leftLabel.translatesAutoresizingMaskIntoConstraints = false
        rightLabel.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false

        // StackView
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])

        // LeftLabel
        NSLayoutConstraint.activate([
            leftLabel.topAnchor.constraint(equalTo: stackView.topAnchor, constant: 1.0),
            leftLabel.bottomAnchor.constraint(equalTo: stackView.bottomAnchor, constant: -1.0)
            ])

        // RightLabel
        NSLayoutConstraint.activate([
            rightLabel.topAnchor.constraint(equalTo: stackView.topAnchor, constant: 1.0),
            rightLabel.bottomAnchor.constraint(equalTo: stackView.bottomAnchor, constant: -1.0)
            ])

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
