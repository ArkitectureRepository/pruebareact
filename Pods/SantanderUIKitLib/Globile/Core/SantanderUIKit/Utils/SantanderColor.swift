//
import UIKit

public func getBundle() -> Bundle {
    let santanderBundle: Bundle = {
        var santanderBundle = Bundle()
        if let url = Bundle(for: SantanderImage.self).url(forResource: "SUKit", withExtension: "bundle"), let realbundle = Bundle(url: url ) {
            santanderBundle = realbundle
        }
        return santanderBundle
    }()
    return santanderBundle
}

extension UIColor {

    public static var redGradient: CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [UIColor.globileBostonRed, UIColor.globileSantanderRed]
        gradient.locations = [0.0, 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        return gradient
    }

    public static var globileBostonRed: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileBostonRed", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#CC0000", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSantanderRed: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSantanderRed", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#EC0000", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSantanderRedButton: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSantanderRedButton", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#EC0000", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileRubyRed: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileRubyRed", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#990000", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileBurntRed: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileBurntRed", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#420716", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileDarkerRed: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileDarkerRed", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#823645", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileDarkRed: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileDarkRed", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#990000", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileMediumRed: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileMediumRed", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#CC0000", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileDarkError: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileDarkError", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#990000", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileError: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileError", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#CC0000", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileLighterError: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileLighterError", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FEE5E5", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileDarkSuccess: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileDarkSuccess", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#3A8340", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSuccess: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSuccess", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#63BA68", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileLighterSuccess: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileLighterSuccess", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F0F8F0", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileDarkWarning: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileDarkWarning", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#946F00", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileWarning: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileWarning", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FFCC33", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileLighterWarning: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileLighterWarning", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FFFAEB", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileDarkSky: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileDarkSky", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#9BC3D3", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileMediumSky: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileMediumSky", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#CEDEE7", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileLightSky: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileLightSky", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#DEEDF2", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileAccesibleSky: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileAccesibleSky", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#257FA4", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileWhite: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileWhite", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FFFFFF", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileWhiteButton: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileWhiteButton", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FFFFFF", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileDarkGrey: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileDarkGrey", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#444444", alpha: 1.0) {
                return color

            }
        }
        return .clear
    }

    public static var globileDarkGreyDisabled: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileDarkGreyDisabled", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#444444", alpha: 1.0) {
                return color

            }
        }
        return .clear
    }

    public static var globileDarkGreyAllModes: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileDarkGreyAllModes", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#444444", alpha: 1.0) {
                return color

            }
        }
        return .clear
    }

    public static var globileMediumGrey: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileMediumGrey", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#767676", alpha: 1.0) {
                return color

            }
        }
        return .clear
    }

    public static var globileLightGrey: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileLightGrey", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#CCCCCC", alpha: 1.0) {
                return color

            }
        }
        return .clear
    }

    public static var globileYellow: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileYellow", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FFCC33", alpha: 1.0) {
                return color

            }
        }
        return .clear
    }

    public static var globileDarkYellow: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileDarkYellow", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#D39E00", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileLimeGreen: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileLimeGreen", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#63BA68", alpha: 1.0) {
                return color

            }
        }
        return .clear
    }

    public static var globileTurquoise: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileTurquoise", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#1BB3BC", alpha: 1.0) {
                return color

            }
        }
        return .clear
    }

    public static var globileDarkTurquoise: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileDarkTurquoise", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#137E84", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileBlue: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileBlue", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#3366FF", alpha: 1.0) {
                return color

            }
        }
        return .clear
    }

    public static var globileDarkBlue: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileDarkBlue", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#34466B", alpha: 1.0) {
                return color

            }
        }
        return .clear
    }

    public static var globilePurple: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globilePurple", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#9E3667", alpha: 1.0) {
                return color

            }
        }
        return .clear
    }

    public static var globileDarkPurple: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileDarkPurple", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#732645", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileBlack: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileBlack", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#000000", alpha: 1.0) {
                return color

            }
        }
        return .clear
    }

    public static var globileBackground: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileBackground", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F5F9FB", alpha: 1.0) {
                return color

            }
        }
        return .clear
    }

    public static var globileAlertBarTopBackground: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileAlertBarTopBackground", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FBFBFB", alpha: 0.8) {
                return color

            }
        }
        return .clear
    }

    public static var globileButtonSelectorBackgroundColor: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileButtonSelectorBackgroundColor", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#1C1C1E", alpha: 0.8) {
                return color

            }
        }
        return .clear
    }

    public static var globileClearButtonGrey: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileClearButtonGrey", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#808080", alpha: 1.0) {
                return color

            }
        }
        return .clear
    }

    public static var globileRedBadgeColor: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileRedBadgeColor", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#CA0813", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileAlertViewBackground: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileAlertViewBackground", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#000000", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGreyAlertBackground: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGreyAlertBackground", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#1C1C1E", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGreen: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGreen", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#008437", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileDarkGreen: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileDarkGreen", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#3A8340", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGraphite: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGraphite", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#6F7779", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileAnthracite: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileAnthracite", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#1D252D", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileBarDefaultTheme: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileBarDefaultTheme", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#EC0000", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSantanderRedTextLightTheme: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSantanderRedTextLightTheme", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#EC0000", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileBarBackButton: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileBarBackButton", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FFFFFF", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    //Tonos de colores

    public static var globileGreyTint90: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGreyTint90", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#575757", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGreyTint80: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGreyTint80", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#6A6A6A", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGreyTint70: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGreyTint70", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#7C7C7C", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGreyTint60: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGreyTint60", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#8F8F8F", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGreyTint50: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGreyTint50", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#A2A2A2", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGreyTint40: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGreyTint40", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#B5B5B5", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGreyTint30: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGreyTint30", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#C7C7C7", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGreyTint20: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGreyTint20", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#DADADA", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGreyTint10: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGreyTint10", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#ECECEC", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGreyTint05: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGreyTint05", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F6F6F6", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileBlueTint90: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileBlueTint90", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#4775FF", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileBlueTint80: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileBlueTint80", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#5C85FF", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileBlueTint70: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileBlueTint70", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#7094FF", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileBlueTint60: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileBlueTint60", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#85A4FF", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileBlueTint50: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileBlueTint50", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#99B3FF", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileBlueTint40: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileBlueTint40", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#AEC2FF", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileBlueTint30: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileBlueTint30", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#C2D1FF", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileBlueTint20: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileBlueTint20", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#D7E1FF", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileBlueTint10: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileBlueTint10", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#EBF0FF", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileBlueTint05: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileBlueTint05", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F5F8FF", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileLimeGreenTint90: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileLimeGreenTint90", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#73C177", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileLimeGreenTint80: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileLimeGreenTint80", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#83C887", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileLimeGreenTint70: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileLimeGreenTint70", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#92CF96", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileLimeGreenTint60: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileLimeGreenTint60", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#A2D6A5", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileLimeGreenTint50: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileLimeGreenTint50", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#B1DDB4", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileLimeGreenTint40: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileLimeGreenTint40", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#C1E4C3", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileLimeGreenTint30: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileLimeGreenTint30", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#D0EBD2", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileLimeGreenTint20: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileLimeGreenTint20", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#E0F2E1", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileLimeGreenTint10: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileLimeGreenTint10", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F0F8F0", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileLimeGreenTint05: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileLimeGreenTint05", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F8FCF8", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globilePurpleTint90: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globilePurpleTint90", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#A84A76", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globilePurpleTint80: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globilePurpleTint80", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#B25F86", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globilePurpleTint70: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globilePurpleTint70", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#BB7295", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globilePurpleTint60: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globilePurpleTint60", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#C587A4", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globilePurpleTint50: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globilePurpleTint50", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#CF9BB3", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globilePurpleTint40: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globilePurpleTint40", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#D9AFC3", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globilePurpleTint30: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globilePurpleTint30", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#E2C3D2", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globilePurpleTint20: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globilePurpleTint20", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F8FCF8", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globilePurpleTint10: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globilePurpleTint10", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F6EBF0", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globilePurpleTint05: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globilePurpleTint05", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FBF5F8", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSantanderTint90: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSantanderTint90", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#EE1919", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSantanderTint80: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSantanderTint80", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F03333", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSantanderTint70: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSantanderTint70", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F24C4C", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSantanderTint60: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSantanderTint60", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F46666", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSantanderTint50: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSantanderTint50", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F67F7F", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSantanderTint40: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSantanderTint40", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F89999", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSantanderTint30: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSantanderTint30", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FAB2B2", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSantanderTint20: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSantanderTint20", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FCCCCC", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSantanderTint10: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSantanderTint10", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FEE5E5", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSantanderTint05: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSantanderTint05", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FFF2F2", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSkyTint90: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSkyTint90", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#E2EFF4", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSkyTint80: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSkyTint80", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#E5F1F5", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSkyTint70: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSkyTint70", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#E8F3F6", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSkyTint60: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSkyTint60", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#ECF5F8", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSkyTint50: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSkyTint50", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#EFF6F9", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSkyTint40: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSkyTint40", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F2F8FA", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSkyTint30: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSkyTint30", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F6FAFC", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSkyTint20: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSkyTint20", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F9FCFD", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileSkyTint10: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileSkyTint10", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FCFEFE", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileTurquoiseTint90: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileTurquoiseTint90", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#32BBC3", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileTurquoiseTint80: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileTurquoiseTint80", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#49C3CA", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileTurquoiseTint70: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileTurquoiseTint70", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#5FCAD0", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileTurquoiseTint60: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileTurquoiseTint60", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#77D2D7", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileTurquoiseTint50: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileTurquoiseTint50", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#8DD9DE", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileTurquoiseTint40: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileTurquoiseTint40", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#A4E1E5", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileTurquoiseTint30: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileTurquoiseTint30", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#BBE9EB", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileTurquoiseTint20: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileTurquoiseTint20", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#D2F0F2", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileTurquoiseTint10: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileTurquoiseTint10", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#E8F8F9", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileTurquoiseTint05: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileTurquoiseTint05", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F4FCFC", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileYellowTint90: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileYellowTint90", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FFD147", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileYellowTint80: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileYellowTint80", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FFD75C", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileYellowTint70: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileYellowTint70", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FFDC70", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileYellowTint60: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileYellowTint60", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FFE185", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileYellowTint50: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileYellowTint50", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FFE699", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileYellowTint40: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileYellowTint40", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FFEBAE", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileYellowTint30: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileYellowTint30", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FFF0C2", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileYellowTint20: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileYellowTint20", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FFF5D7", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileYellowTint10: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileYellowTint10", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FFFAEB", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileYellowTint05: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileYellowTint05", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#FFFDF5", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileAnthraciteTint90: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileAnthraciteTint90", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#333A42", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileAnthraciteTint80: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileAnthraciteTint80", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#4A5157", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileAnthraciteTint70: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileAnthraciteTint70", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#60666C", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileAnthraciteTint60: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileAnthraciteTint60", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#777C81", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileAnthraciteTint50: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileAnthraciteTint50", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#8E9296", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileAnthraciteTint40: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileAnthraciteTint40", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#A5A8AB", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileAnthraciteTint30: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileAnthraciteTint30", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#BBBDC0", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileAnthraciteTint20: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileAnthraciteTint20", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#D2D3D5", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileAnthraciteTint10: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileAnthraciteTint10", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#E8E9EA", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileAnthraciteTint05: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileAnthraciteTint05", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F3F4F4", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGraphiteTint90: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGraphiteTint90", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#7D8486", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGraphiteTint80: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGraphiteTint80", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#8C9294", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGraphiteTint70: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGraphiteTint70", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#9A9FA1", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGraphiteTint60: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGraphiteTint60", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#A9ADAF", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGraphiteTint50: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGraphiteTint50", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#B7BBBC", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGraphiteTint40: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGraphiteTint40", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#C5C9C9", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGraphiteTint30: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGraphiteTint30", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#D3D6D6", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGraphiteTint20: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGraphiteTint20", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#E2E4E4", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGraphiteTint10: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGraphiteTint10", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F0F1F1", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

    public static var globileGraphiteTint05: UIColor {
        if #available(iOS 11.0, *) {
            if let color = UIColor(named: "globileGraphiteTint05", in: getBundle(), compatibleWith: nil) {
                return color
            }
        } else {
            if let color = UIColor.hex(hex: "#F8F8F8", alpha: 1.0) {
                return color
            }
        }
        return .clear
    }

}

public extension UIColor {

    class func rgb(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: 1.0)
    }

    class func hex(hex: String, alpha: CGFloat) -> UIColor? {

        let r, g, b, a: CGFloat

        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])

            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0

                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255

                    return UIColor(red: r, green: g, blue: b, alpha: a)
                }
            } else if hexColor.count == 6 {

                let scanner = Scanner(string: hexColor)
                scanner.scanLocation = 0

                var rgbValue: UInt64 = 0

                scanner.scanHexInt64(&rgbValue)

                let r = (rgbValue & 0xFF0000) >> 16
                let g = (rgbValue & 0xFF00) >> 8
                let b = rgbValue & 0xFF

                return UIColor(
                    red: CGFloat(r) / 0xff,
                    green: CGFloat(g) / 0xff,
                    blue: CGFloat(b) / 0xff, alpha: alpha
                )
            }
        }

        return nil
    }
}
