//
import UIKit
import WebKit

public var registeredFonts: [String] = []

extension UIFont {


    private class func registerCustomFont(withName name: String) {
        if !registeredFonts.contains(name) {
            registeredFonts.append(name)
            let santanderBundle: Bundle = {
                var santanderBundle = Bundle()
                if let url = Bundle(for: SantanderImage.self).url(forResource: "SUKit", withExtension: "bundle"), let realbundle = Bundle(url: url ) {
                    santanderBundle = realbundle
                }
                return santanderBundle
            }()
            registerFont(withFilenameString: "Fonts/OTF/\(name).otf", bundle: santanderBundle)
        }
    }

     public class func microTextRegular(size: CGFloat) -> UIFont {
        let fontName: String = "SantanderMicroText"
        registerCustomFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

     public class func microTextIt(size: CGFloat) -> UIFont {
        let fontName: String = "SantanderMicroText-It"
        registerCustomFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

     public class func microTextBold(size: CGFloat) -> UIFont {

        let fontName: String = "SantanderMicroText-Bd"
        registerCustomFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

     public class func microTextBoldIt(size: CGFloat) -> UIFont {

        let fontName: String = "SantanderMicroText-BdIt"
        registerCustomFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

     public class func microTextBoldLt(size: CGFloat) -> UIFont {

        let fontName: String = "SantanderMicroText-BdLt"
        registerCustomFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

     public class func microTextLtIt(size: CGFloat) -> UIFont {

        let fontName: String = "SantanderMicroText-LtIt"
        registerCustomFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

    public class func microTextLt(size: CGFloat) -> UIFont {

        let fontName: String = "SantanderMicroText-Lt"
        registerCustomFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

     public class func headlineBold(size: CGFloat) -> UIFont {
        let fontName: String = "SantanderHeadline-Bold"
        registerCustomFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

     public class func headlineBoldIt(size: CGFloat) -> UIFont {
        let fontName: String = "SantanderHeadline-BoldIt"
        registerCustomFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

     public class func headlineIt(size: CGFloat) -> UIFont {
        let fontName: String = "SantanderHeadline-It"
        registerCustomFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

     public class func headlineRegular(size: CGFloat) -> UIFont {
        let fontName: String = "SantanderHeadline-Regular"
        registerCustomFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

     public class func bold(size: CGFloat) -> UIFont {
        let fontName: String = "SantanderText-Bold"
        registerCustomFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

     public class func boldIt(size: CGFloat) -> UIFont {
        let fontName: String = "SantanderText-BoldItalic"
        registerCustomFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

     public class func it(size: CGFloat) -> UIFont {
        let fontName: String = "SantanderText-Italic"
        registerCustomFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

     public class func light(size: CGFloat) -> UIFont {
        let fontName: String = "SantanderText-Light"
        registerCustomFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

     public class func lightItalic(size: CGFloat) -> UIFont {
        let fontName: String = "SantanderText-LightItalic"
        registerCustomFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

    public class func regular(size: CGFloat) -> UIFont {
        let fontName: String = "SantanderText-Regular"
        registerCustomFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }
}

@available(*, deprecated, message: "Use UIFont extension feature instead this class.")
public class SantanderFont {

    private class var resourceName: String {
        return "SantanderUIKit"
    }

    private class func registerFont(withName name: String) {
        if !registeredFonts.contains(name) {
            registeredFonts.append(name)
            let santanderBundle: Bundle = {
                var santanderBundle = Bundle()
                if let url = Bundle(for: SantanderFont.self).url(forResource: "SUKit", withExtension: "bundle"), let realbundle = Bundle(url: url ) {
                    santanderBundle = realbundle
                }
                return santanderBundle
            }()
            UIFont.registerFont(withFilenameString: "Fonts/OTF/\(name).otf", bundle: santanderBundle)
        }
    }

    public class func microTextRegular(size: CGFloat) -> UIFont {
        let fontName: String = "SantanderMicroText"
        registerFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

    public class func microTextIt(size: CGFloat) -> UIFont {
        let fontName: String = "SantanderMicroTextIt"
        registerFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

    public class func microTextBold(size: CGFloat) -> UIFont {

        let fontName: String = "SantanderMicroTextBd"
        registerFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

    public class func microTextBoldIt(size: CGFloat) -> UIFont {

        let fontName: String = "SantanderMicroTextBdIt"
        registerFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

    public class func headlineBold(size: CGFloat) -> UIFont {
        //00
        let fontName: String = "SantanderHeadline-Bold"
        registerFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

    public class func headlineBoldIt(size: CGFloat) -> UIFont {
        //00
        let fontName: String = "SantanderHeadline-BoldIt"
        registerFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

    public class func headlineIt(size: CGFloat) -> UIFont {
        //00
        let fontName: String = "SantanderHeadline-It"
        registerFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

    public class func headlineRegular(size: CGFloat) -> UIFont {
        //00
        let fontName: String = "SantanderHeadline-Regular"
        registerFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

    public class func bold(size: CGFloat) -> UIFont {
        //00
        let fontName: String = "SantanderText-Bold"
        registerFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

    public class func boldIt(size: CGFloat) -> UIFont {
        //00
        let fontName: String = "SantanderText-BoldItalic"
        registerFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

    public class func it(size: CGFloat) -> UIFont {
        //00
        let fontName: String = "SantanderText-Italic"
        registerFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

    public class func light(size: CGFloat) -> UIFont {
        //00
        let fontName: String = "SantanderText-Light"
        registerFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

    public class func lightItalic(size: CGFloat) -> UIFont {
        //00
        let fontName: String = "SantanderText-LightItalic"
        registerFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }

    public class func regular(size: CGFloat) -> UIFont {
        //00
        let fontName: String = "SantanderText-Regular"
        registerFont(withName: fontName)
        return UIFont(name: fontName, size: size)!
    }
}

internal extension UIFont {

    static func registerFont(withFilenameString filenameString: String, bundle: Bundle) {

        guard let pathForResourceString = bundle.path(forResource: filenameString, ofType: nil) else {
            print("UIFont+:  Failed to register font - path for resource not found.")
            return
        }

        guard let fontData = NSData(contentsOfFile: pathForResourceString) else {
            print("UIFont+:  Failed to register font - font data could not be loaded.")
            return
        }

        guard let dataProvider = CGDataProvider(data: fontData) else {
            print("UIFont+:  Failed to register font - data provider could not be loaded.")
            return
        }

        guard let font = CGFont(dataProvider) else {
            print("UIFont+:  Failed to register font - font could not be loaded.")
            return
        }

        var errorRef: Unmanaged<CFError>?
        if CTFontManagerRegisterGraphicsFont(font, &errorRef) == false {
            print("UIFont+:  Failed to register font - register graphics font failed - this font may have already been registered in the main bundle.")
        }
    }
}
