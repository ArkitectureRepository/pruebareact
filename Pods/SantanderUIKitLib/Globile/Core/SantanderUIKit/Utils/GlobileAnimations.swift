//
//  GlobileAnimations.swift
//  Alamofire-iOS10.0
//
//  Created by r.a.sanz.hinojosas on 10/12/2019.
//

import Foundation
import Lottie

public class GlobileAnimations {
    
    public class func loadAnimation(name: String) -> Animation? {
        if let path = Bundle(for: GlobileAnimations.self).path(forResource: "SUKit", ofType: "bundle") {
            if let bundle = Bundle(path: path) {
                return Animation.named(name, bundle: bundle, subdirectory: "Animations")
            }
        }
        return Animation.named("ConnectionFailure")
    }
    
    public class func playLottieAnimation(view: AnimationView) {
        view.play(fromProgress: 0.7,
                           toProgress: 1,
                           loopMode: LottieLoopMode.loop,
                           completion: { (finished) in
                            if finished {
                              print("Animation Complete")
                            } else {
                              print("Animation cancelled")
                            }
        })
    }
    
    public class func stopLottieAnimation(view: AnimationView) {
        view.stop()
    }
    
    
    public class func endingButtonNativeAnimation(view: UIView) {
        view.isHidden = false
        view.transform = CGAffineTransform(scaleX: 0.0, y: 1)
        view.alpha = 1.0
        
        UIView.animate(withDuration: 0.4,
                       animations: {
                        view.transform = CGAffineTransform.identity
        },
                       completion: { _ in
        })
        UIView.animate(withDuration: 0.1, delay: 0.3,
                       animations: {
                        view.alpha = 0.0
        },
                       completion: { _ in
                        view.isHidden = true
        })
    }
}
