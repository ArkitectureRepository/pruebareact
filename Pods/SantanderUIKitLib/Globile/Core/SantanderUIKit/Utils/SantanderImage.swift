//
//  SantanderImage.swift
//  SantanderUIKit
//
//  Created by adrian.a.fernandez on 08/02/2019.
//
import UIKit

public func getImage(named: String) -> UIImage {
    let santanderBundle: Bundle = {
          var santanderBundle = Bundle()
          if let url = Bundle(for: SantanderImage.self).url(forResource: "SUKit", withExtension: "bundle"), let realbundle = Bundle(url: url ) {
              santanderBundle = realbundle
          }
          return santanderBundle
      }()

    return UIImage(named: "\(named)", in: santanderBundle, compatibleWith: nil) ?? UIImage()
}

public class SantanderImage {

    static var back: UIImage {
        return getImage(named: "back").withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
    }

    static var logo: UIImage {
        return getImage(named: "logo").withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
    }

    static var menu: UIImage {
        return getImage(named: "menu_thin").withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
    }

    static var clear: UIImage {
        return getImage(named: "clear").withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
    }

    static var info: UIImage {
        return getImage(named: "info").withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
    }
}
