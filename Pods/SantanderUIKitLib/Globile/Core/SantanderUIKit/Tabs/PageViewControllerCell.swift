//
//  PageViewControllerCell.swift

import Foundation
import UIKit

class PageViewControllerCell: UICollectionViewCell {

    // MARK: - HostedView

    var hostedView: UIView? {
        didSet {
            guard let hostedView = hostedView else {
                return
            }

            hostedView.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(hostedView)
            NSLayoutConstraint.activate([
                hostedView.topAnchor.constraint(equalTo: contentView.topAnchor),
                hostedView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
                hostedView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
                hostedView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
                ])
        }
    }

    // MARK: - Reuse

    override func prepareForReuse() {
        super.prepareForReuse()

        if hostedView?.superview == contentView { //Make sure that hostedView hasn't been added as a subview to a different cell
            hostedView?.removeFromSuperview()
        } else {
            print("hostedView is no longer attached to this cell")
        }

        hostedView = nil
    }
}
