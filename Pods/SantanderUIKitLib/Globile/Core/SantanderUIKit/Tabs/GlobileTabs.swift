import UIKit


public class GlobileTabs: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    // MARK: - Public properties

    /// Set tabs configuration with label, icon and view controller
    public var tabsList: [GlobileTab] = []

    private var menuBar: MenuBar!

    private var tabsCount: Int {
        return tabsList.count
    }

    public init() {
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        super.init(collectionViewLayout: collectionViewFlowLayout)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View lifecycle

    override public func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }

    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - Private

    private func setupViews() {
        view.backgroundColor = .globileWhite

        setupMenuBar()
        setupCollectionView()
    }

    private func setupMenuBar() {
        menuBar = MenuBar(tabs: tabsList)
        menuBar.tabsController = self

        menuBar.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(menuBar)

        if #available(iOS 11.0, *) {
            menuBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            menuBar.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        }

        NSLayoutConstraint.activate([
            menuBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            menuBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            menuBar.heightAnchor.constraint(equalToConstant: tabHeight)
            ])
    }

    private func setupCollectionView() {
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
        }

        collectionView?.register(PageViewControllerCell.self, forCellWithReuseIdentifier: "ControllerCellIdentifier")

        // Setup collection view layout
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: menuBar.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: menuBar.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: menuBar.trailingAnchor)
            ])

        if #available(iOS 11.0, *) {
            collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }

        // Setup collection view style
        collectionView?.showsHorizontalScrollIndicator = false
        collectionView?.backgroundColor = .white
        collectionView?.isPagingEnabled = true
    }

    public func scrollToMenuIndex(menuIndex: Int, animated: Bool) {
        collectionView?.layoutIfNeeded()
        let indexPath = IndexPath(item: menuIndex, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: animated)
        menuBar.collectionView.selectItem(at: indexPath, animated: animated, scrollPosition: .centeredHorizontally)
    }

    private var tabHeight: CGFloat {
        for item in tabsList {
            if item.icon != nil {
                return 60.0
            }
        }
        return 50.0
    }

    // MARK: - UIScrollViewDelegate

    override public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        menuBar.horizontalBarLeftAnchorConstraint?.constant = scrollView.contentOffset.x / CGFloat(tabsCount)
    }

    override public func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let index = Int(targetContentOffset.pointee.x / view.frame.width)
        let indexPath = IndexPath(item: index, section: 0)
        menuBar.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
    }

    // MARK: - UICollectionViewDataSource

    override public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabsCount
    }

    override public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ControllerCellIdentifier", for: indexPath) as? PageViewControllerCell

        let viewController = tabsList[indexPath.row].viewController

        guard let contentView = viewController.view else {
            fatalError("There is no content view available.")
        }

        cell?.hostedView = contentView

        self.addChild(viewController)
        viewController.didMove(toParent: self)

        return cell ?? PageViewControllerCell()

    }

    // MARK: - UICollectionViewDelegate

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        let height = collectionView.frame.height
        let size = CGSize(width: width, height: height)
        return size
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
