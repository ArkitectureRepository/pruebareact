import UIKit

class TabCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .regular(size: 14.0)
        label.textColor = .globileDarkGrey
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        label.lineBreakMode = .byClipping
        label.textAlignment = .center
        return label
    }()

    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.tintColor = .globileDarkGrey
        return iv
    }()

    let separatorView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 1.0, height: 12.0))
        view.backgroundColor = .globileMediumSky
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = false
        return view
    }()

    override public var isHighlighted: Bool {
        didSet {
            imageView.tintColor = isHighlighted ? UIColor.globileSantanderRed : UIColor.globileMediumGrey
            titleLabel.font = isHighlighted ? .bold(size: 14.0) : .regular(size: 14.0)
            titleLabel.textColor = isHighlighted ? UIColor.globileDarkGrey : UIColor.globileMediumGrey
        }
    }

    override public var isSelected: Bool {
        didSet {
            imageView.tintColor = isSelected ? UIColor.globileSantanderRed : UIColor.globileMediumGrey
            titleLabel.font = isSelected ? .bold(size: 14.0) : .regular(size: 14.0)
            titleLabel.textColor = isSelected ? UIColor.globileDarkGrey : UIColor.globileMediumGrey
        }
    }

    func setupViews() {
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(separatorView)
    }

    func setupLayout() {
        if imageView.image == nil {
            imageView.removeFromSuperview()
            addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
            addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))

        } else {
            addConstraintsWithFormat("H:[v0(24)]", views: imageView)
            addConstraintsWithFormat("V:[v0(24)]", views: imageView)

            addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
            addConstraint(NSLayoutConstraint(item: imageView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 4))
            addConstraint(NSLayoutConstraint(item: imageView, attribute: .bottom, relatedBy: .equal, toItem: titleLabel, attribute: .top, multiplier: 1, constant: 0))

            addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
            addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: imageView, attribute: .bottom, multiplier: 1, constant: 0))
            addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -4))

        }

        let cellHeightConstant = frame.height == 60 ? 2:0

        separatorView.widthAnchor.constraint(equalToConstant: 1).isActive = true
        separatorView.topAnchor.constraint(equalTo: topAnchor, constant: CGFloat(8 + cellHeightConstant)).isActive = true
        separatorView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: CGFloat(-12 - cellHeightConstant)).isActive = true
        separatorView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 1).isActive = true
    }
}
