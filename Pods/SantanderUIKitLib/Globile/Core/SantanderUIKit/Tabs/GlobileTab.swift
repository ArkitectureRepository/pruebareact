//
//  GlobileTab.swift

import Foundation
import UIKit

public struct GlobileTab {
    var title: String
    var icon: UIImage?
    var viewController: UIViewController

    /// Tab structure
    ///
    /// - Parameters:
    ///   - title: Tab label text.
    ///   - icon: UIImage to show in tab.
    ///   - viewController: UIViewController to load under the tabs view.
    public init(title: String, icon: UIImage?, viewController: UIViewController) {
        self.title = title
        self.icon = icon
        self.viewController = viewController
    }
}
