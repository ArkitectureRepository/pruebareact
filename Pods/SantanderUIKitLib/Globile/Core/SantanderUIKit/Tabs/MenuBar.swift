//
//  MenuBar.swift

import UIKit

class MenuBar: UIView {

    private var tabs: [GlobileTab] = []

    private var tabsCount: Int {
        return tabs.count
    }

    // MARK: - View components

    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .globileWhite
        return collectionView
    }()

    var horizontalBarLeftAnchorConstraint: NSLayoutConstraint?
    weak var tabsController: GlobileTabs?

    init(tabs: [GlobileTab]) {
        self.tabs = tabs

        super.init(frame: .zero)

        setupCollectionView()
        setupHorizontalBar()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private

    private func setupCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(TabCell.self, forCellWithReuseIdentifier: "cellId")

        addSubview(collectionView)
        addConstraintsWithFormat("H:|[v0]|", views: collectionView)
        addConstraintsWithFormat("V:|[v0]|", views: collectionView)

        let selectedIndexPath = IndexPath(item: 0, section: 0)
        collectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: .bottom)
    }

    private func setupHorizontalBar() {
        let horizontalBarView = UIView()
        horizontalBarView.backgroundColor = .globileSantanderRed
        horizontalBarView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(horizontalBarView)

        horizontalBarLeftAnchorConstraint = horizontalBarView.leftAnchor.constraint(equalTo: self.leftAnchor)
        horizontalBarLeftAnchorConstraint?.isActive = true

        horizontalBarView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        horizontalBarView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/CGFloat(tabsCount)).isActive = true
        horizontalBarView.heightAnchor.constraint(equalToConstant: 3).isActive = true

        let bottomBar = UIView()
        bottomBar.backgroundColor = .globileMediumSky
        bottomBar.translatesAutoresizingMaskIntoConstraints = false
        addSubview(bottomBar)

        bottomBar.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        bottomBar.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1).isActive = true
        bottomBar.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }

}

extension MenuBar: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabsCount
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as? TabCell
        if tabs[indexPath.item].icon == nil {
            cell?.imageView.image = nil
        } else {
            cell?.imageView.image = tabs[indexPath.item].icon?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
            cell?.tintColor = .globileSantanderRed
        }
        cell?.titleLabel.text = tabs[indexPath.item].title
        if indexPath.item >= tabs.count-1 {
            cell?.separatorView.isHidden = true
        }
        cell?.setupLayout()

        return cell ?? TabCell()
    }
}

extension MenuBar: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        tabsController?.scrollToMenuIndex(menuIndex: indexPath.item, animated: true)
    }
}

extension MenuBar: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width / CGFloat(tabsCount), height: frame.height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
