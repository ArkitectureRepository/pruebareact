//
//  CircleFrame.swift
//  ProfilePhoto
//
//  Created by l.arranz.martinez on 04/04/2019.
//  Copyright © 2019 l.arranz.martinez. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
public class CircleFrame: UIView {

    var circleLayer = CAShapeLayer()
    var circleLayerBorder: CGFloat = 2.0

    public var badge: String? {
        didSet {
            addBadgeToImage(badge: badge)
        }
    }

    public var initials: String? {
        didSet {
            addInitialsToImage(initials: initials)
        }
    }

/* valores por defecto de la etiqueta del profile photo */

    public let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .globileDarkGrey
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        label.font = .bold(size: 20)
        return label
    }()

    public override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        setCircleLayouts()
    }

    private func addSubviews() {
        addSubview(label)

        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: topAnchor, constant: circleLayerBorder),
            label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: circleLayerBorder),
            label.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -circleLayerBorder),
            label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -circleLayerBorder)
            ])
    }

    override public func draw(_ rect: CGRect) {
        circleLayer.lineWidth = circleLayerBorder
        //cicleLayer.fillColor se tinta en cada método
        layer.addSublayer(circleLayer)
        addSubviews()
    }

    func addBadgeToImage(badge: String?) {
        label.text = badge
        label.textColor = .white
        label.layer.cornerRadius = frame.height/2
        label.layer.masksToBounds = true
        addSubview(label)
    }

    func addInitialsToImage(initials: String?) {
        label.text = initials
        label.textColor = .globileDarkGrey
        label.layer.cornerRadius = frame.height/2
        label.layer.masksToBounds = true
        addSubview(label)
    }

    internal var setCircleRadius: CGFloat {
        let width = bounds.width
        let height = bounds.height

        let length = width > height ? height : width
        return (length - circleLayerBorder) / 2
    }

    private var setCircleFrame: CGRect {
        let width = bounds.width
        let height = bounds.height
        let radius = setCircleRadius
        let x: CGFloat
        let y: CGFloat

        if width > height {
            y = circleLayerBorder / 2
            x = (width / 2) - radius
        } else {
            x = circleLayerBorder / 2
            y = (height / 2) - radius
        }

        let diameter = 2 * radius
        return CGRect(x: x, y: y, width: diameter, height: diameter)
    }

    private var circlePath: UIBezierPath {
        return UIBezierPath(roundedRect: setCircleFrame, cornerRadius: setCircleRadius)
    }

    private var fillCirclePath: UIBezierPath {
        let trueGap = circleLayerBorder / 2
        return UIBezierPath(roundedRect: setCircleFrame.insetBy(dx: CGFloat(trueGap), dy: CGFloat(trueGap)), cornerRadius: setCircleRadius)
    }

    private func setCircleLayouts() {
        circleLayer.frame = bounds
        circleLayer.lineWidth = circleLayerBorder
        circleLayer.path = circlePath.cgPath
    }
}
