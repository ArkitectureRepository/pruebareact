//
//  GlobileProfilePhoto.swift
//  ProfilePhoto
//
//  Created by l.arranz.martinez on 27/03/2019.
//  Copyright © 2019 l.arranz.martinez. All rights reserved.
//

import Foundation

import UIKit
import CoreGraphics

enum ProfilePhoto: Error {
    case invalidName
    case invalidNumOfInitials
}

public class GlobileProfilePhoto: UIView {

    public let imageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()

    private var hasNumber: Bool
    private var hasBadge: Bool
    private var hasImage: Bool

    var photoBadge: CircleFrame = {
        let badge = CircleFrame()
        return badge
    }()

    var initialsProfile: CircleFrame = {
        let initials = CircleFrame()
        return initials
    }()

    public override init (frame: CGRect) {
        hasNumber = false
        photoBadge = .init(frame: .zero)
        initialsProfile = .init(frame: .zero)
        hasBadge = false
        hasImage = true
        super.init(frame: frame)
    }

    public required init?(coder aDecoder: NSCoder) {
        hasNumber = false
        photoBadge = .init(frame: .zero)
        initialsProfile = .init(frame: .zero)
        hasBadge = false
        hasImage = true
        super.init(coder: aDecoder)
    }

    public override func layoutSubviews() {

        if hasImage {
            addSubview(imageView)

            NSLayoutConstraint.activate([
                imageView.topAnchor.constraint(equalTo: topAnchor, constant: 1.0),
                imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 1.0),
                imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -1.0),
                imageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -1.0)
                ])
            translatesAutoresizingMaskIntoConstraints = false
            imageView.frame.size = (CGSize(width: 56.0, height: 56.0))
            imageView.layer.cornerRadius = imageView.frame.height / 2.0
            imageView.layer.masksToBounds = true
            imageView.contentMode = .scaleAspectFit

        } else {
            addSubview(initialsProfile)

            NSLayoutConstraint.activate([
                initialsProfile.topAnchor.constraint(equalTo: topAnchor, constant: 1.0),
                initialsProfile.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 1.0),
                initialsProfile.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -1.0),
                initialsProfile.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -1.0)
                ])
            translatesAutoresizingMaskIntoConstraints = false
            initialsProfile.frame.size = (CGSize(width: 56.0, height: 56.0))
            initialsProfile.layer.cornerRadius = initialsProfile.frame.height / 2.0
            initialsProfile.layer.masksToBounds = true
            initialsProfile.contentMode = .scaleAspectFit
        }

        self.contentMode = .scaleToFill

        if hasBadge {
            addSubview(photoBadge)

            NSLayoutConstraint.activate([
                photoBadge.topAnchor.constraint(equalTo: topAnchor, constant: 1.0),
                photoBadge.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -1.0)
                ])
        }

        super.layoutSubviews()
    }

    /// Sets the image on the profile photo
    ///
    /// - Parameter photo: image that want to appear on the profile photo

    public func setImage (_ photo: UIImage?) {
        imageView.image = photo
    }

    /// Sets the initials as profile photo. The profile initials can be one or two letters
    ///
    /// - Parameter initials: string that contains the caracters that want to appear in the profile photo

    public func setInitials(initials: String?) throws {

        hasImage = false
        var finalChars = ""
        if let initials = initials {
            let auxTrimmed = initials.replacingOccurrences(of: " ", with: "")
            if !auxTrimmed.isEmpty && auxTrimmed.count <= 2 {
                for char in auxTrimmed {
                    finalChars.append(String(char).uppercased())
                }
                initialsProfile.label.text = finalChars
            } else {
                throw ProfilePhoto.invalidNumOfInitials
            }
        }
        initialsProfile.circleLayer.fillColor = UIColor.globileLightGrey.cgColor
    }

    /// Sets the initials as profile photo getting the first letter of the words.
    /// Gets the first letter of the first and last word
    ///
    /// - Parameter name: string that contains the full name of the user

    public func setInitialsWithName(name: String?) throws {

        hasImage = false

        let slice = name?.split(separator: " ")
        var allInitials = [String]()

        switch slice!.count {
        case 1:
            allInitials.append(String((slice!.first?.first)!).uppercased())
        case 0:
            throw ProfilePhoto.invalidName
        default:
            allInitials.append(String((slice!.first?.first)!).uppercased())
            allInitials.append(String((slice!.last?.first)!).uppercased())
        }

        initialsProfile.label.text = allInitials.joined()
        initialsProfile.circleLayer.fillColor = UIColor.globileLightGrey.cgColor
    }
}

extension GlobileProfilePhoto {

    /// Add the badge with the number that is passed as a parameter
    ///
    /// - Parameter badgeNumber: number that appears in the badge

    public func addBadge(badgeNumber: Int) {

        var badgeText = String(badgeNumber)
        hasBadge = true
        photoBadge.frame = CGRect(x: frame.size.width/2 - 83, y: frame.size.height/2 - 65, width: 20.0, height: 20.0)

        if badgeNumber > 0 {
            hasNumber = true
            if badgeNumber > 99 {
                badgeText = "99+"
                photoBadge.frame = CGRect(x: frame.size.width/2 - 83, y: frame.size.height/2 - 65, width: 22.0, height: 22.0)
            }
        }
        photoBadge.label.text = badgeText
        photoBadge.label.textColor = .white
        photoBadge.label.font = .bold(size: 12)
        photoBadge.circleLayer.fillColor = UIColor.globileBostonRed.cgColor

    }
}
