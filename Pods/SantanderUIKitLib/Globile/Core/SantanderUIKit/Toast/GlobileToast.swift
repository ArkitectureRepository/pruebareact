//
//  GlobileToast.swift
//  SantanderUIKitLib-iOS10.0
//
//  Created by l.arranz.martinez on 24.02.20.
//

import Foundation
import UIKit
import GlobileUtilsLib
public enum DurationToast: TimeInterval {
    case long = 1.0
    case short = 0.25
}

public class GlobileToast: UIView {
    
    @IBOutlet weak var messageLabel: UILabel!
//    public var bottomSpace:CGFloat = 80.0
    override public init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public class func instanceFromNib() -> GlobileToast? {
        if let path = Bundle(for: GlobileToast.self).path(forResource: "SUKit", ofType: "bundle") {
            if let bundle = Bundle(path: path) {
                if let nib = bundle.loadNibNamed("GlobileToast", owner: nil, options: nil)?[0] as? GlobileToast {
                    return nib
                }
            }
        }
        return nil
    }
    public func show(message: String, duration: DurationToast = .short, topView: CGFloat, in view: UIView) {
        view.addSubview(self)
        messageLabel.textColor = .globileDarkGrey
        messageLabel?.text = message
        messageLabel.textAlignment = .center
        messageLabel.font = .microTextBold(size: 14)
        backgroundColor = .globileWhite
        layer.masksToBounds = false
        layer.borderWidth = 0.5
        layer.cornerRadius = (frame.height / 2)
        
        addShadow()
        
        self.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0.0)
        let verticalConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: topView)
        let widthConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.lessThanOrEqual, toItem: view, attribute: NSLayoutConstraint.Attribute.width, multiplier: 0.8, constant: 0)
        let heightConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.greaterThanOrEqual, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 36)
        NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])

        setUpAnimation(duration)
    }
    
    private func addShadow(){
        layer.borderColor = UIColor.globileGreyTint10.cgColor
        layer.shadowColor = UIColor.globileGreyTint10.cgColor
        layer.shadowOffset = CGSize(width: -1.0, height: 1.0)
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 1.0
    }
}

extension GlobileToast {
    
    //Métodos de configuracion
    private func setUpAnimation(_ duration: DurationToast){
        self.alpha = 0.0
        self.fadeIn(duration.rawValue, delay: 0.0, completion: {_ in
                self.fadeOut(duration.rawValue, delay: 1.5) { (_) in
                        }
        })
    }
}

