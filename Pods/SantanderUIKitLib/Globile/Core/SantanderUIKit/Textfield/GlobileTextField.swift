//

import UIKit
import GlobileUtilsLib

public enum GlobileFloatTextFieldState {
    case `default`
    case error
}

public enum GlobileIconColors {
    case santanderRed, mediumGrey, darkGrey, darkSky
}

public protocol GlobileTextFieldDelegate: UITextFieldDelegate {}

public class GlobileTextField: UIView {
    
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var tfPlaceholder: UILabel!
    @IBOutlet weak private(set) public var textField: UITextField!
    
    @IBOutlet weak var buttonsStackView: UIStackView!
    @IBOutlet weak var actionButton: UIButton!
    
    @IBOutlet weak var bottomLine: UIView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var bottomLabel: GlobileLabel!
    
    @IBOutlet weak var container: UIView!
    
    @IBOutlet var placeholderCenterY: NSLayoutConstraint!
    
    var amount : Int64 = 0
    // MARK: Properties
    //Colors.
    enum Colors {
        enum BottomLine {
            static let `default` = UIColor.globileAccesibleSky
        }
        static let `default` = UIColor.globileDarkGrey
        static let highlighted = UIColor.globileMediumGrey
        static let error = UIColor.globileBostonRed
    }
    
    //Fonts.
    enum Fonts {
        static let `default` = UIFont.regular(size: 18.0)
        static let highlighted = UIFont.regular(size: 14.0)
        static let bottom = UIFont.regular(size: 14.0)
    }
    
    public enum BackgroundMode {
        case sky, white
    }
    
    //Input methods.
    public enum InputType {
        case ´default´
        case amount
        case amountWCurrency
    }
    
    //Sizes
    fileprivate let padding: CGFloat = 12.0
    fileprivate let bottomPlaceholderPadding: CGFloat = 2.0
    fileprivate let bottomPadding: CGFloat = 4.0
    fileprivate let buttonWidth: CGFloat = 24.0
    fileprivate let buttonPadding: CGFloat = 12.0
    
    //Constraints
    fileprivate var placeholderConstraint: NSLayoutConstraint!
    fileprivate var placeholderConstraintConstant: CGFloat!
    
    //State
    internal var santanderState: GlobileFloatTextFieldState = .default
    
    //OnClicks
    fileprivate var iconAction: (() -> Void)?
    internal var clearAction: (() -> Void)?
    
    internal var hasContent: Bool {
        return !(text ?? "").isEmpty
    }
    
    private let bottomBorder = CALayer()
    
    public var backgroundTheme: BackgroundMode = .white {
        didSet {
            switch backgroundTheme {
                case .white:
                    container.backgroundColor = .globileWhite
                    bottomBorder.backgroundColor = UIColor.globileWhite.cgColor
                    iconColor = .darkGrey
                case .sky:
                    container.backgroundColor = UIColor.withAlphaComponent(.globileLightSky)(0.3)
                    bottomBorder.backgroundColor = UIColor.withAlphaComponent(.globileLightSky)(0.3).cgColor
                    iconColor = .mediumGrey
                }
        }
    }
    
    public var inputMode: InputType = .´default´ {
        didSet {
            switch inputMode {
                case .´default´:
                    textField.keyboardType = .default
                case .amount:
                    textField.keyboardType = .decimalPad
                case .amountWCurrency:
                    textField.keyboardType = .decimalPad
            }
        }
    }
    
    public var hideSeparator: Bool = false {
        didSet {
            if buttonsStackView.isHidden == false {
            
                separatorView.isHidden = hideSeparator
            }
        }
    }
    
    public var hideHelpText: Bool = false {
        didSet {
             bottomLabel.isHidden = hideHelpText
            }
    }
    
    @IBInspectable public var placeholder: String? {
        didSet {
            setcustomPlaceholder()
        }
    }
    
    @IBInspectable public var helpText: String? {
        didSet {
            setHelpText()
        }
    }
    
    public var rightIcon: UIImage? = nil {
        didSet {
            addRightIcon()
        }
    }
    
    public var iconColor: GlobileIconColors = .darkGrey {
        didSet {
            tintIcon()
        }
    }
    
    public var isSecureTextEntry: Bool = false {
        didSet {
            switch isSecureTextEntry {
            case true:
                textField.isSecureTextEntry = true
            case false:
                textField.isSecureTextEntry = false
            }
        }
    }
    
    public var text: String? {
        didSet {
            textField.text = text
            if (!textField.isFirstResponder && (!(oldValue ?? "").isEmpty && !hasContent)) || (!textField.isFirstResponder && hasContent) {
                maximizePlaceHolder()
                minimizePlaceHolder()
            }
            configureRightView()
        }
    }
    
    public var returnKeyType: UIReturnKeyType = .default {
        didSet {
            textField.returnKeyType = returnKeyType
        }
    }
    
    public var delegate: GlobileTextFieldDelegate?
    
    var nibName:String? { return String(describing: type(of: self)) }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        awakeFromNib()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        guard let nibName = self.nibName,
            let bundle = Bundle(for: GlobileTextField.self).url(forResource: "SUKit", withExtension: "bundle").flatMap(Bundle.init),
            let view = bundle.loadNibNamed(nibName, owner: self, options: nil)?.first as? UIView
            else { return }
        self.layoutIfNeeded()
        contentView = view
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        self.addSubview(contentView)
        setupView()
    }
    
    deinit {
        self.removeRegisterDidChangeNotification()
    }
    
    public func onIconClick(_ action: (() -> Void)?) {
        self.iconAction = action
    }
    
    public func onClearClick(_ action: (() -> Void)?) {
        self.clearAction = action
    }
}

// MARK: Private methods.
extension GlobileTextField {
    
    func addNotification() {
        registerDidChangeNotification()
    }
    
    fileprivate func registerDidChangeNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(textDidChange),
                                               name: NSNotification.Name(rawValue: "UITextFieldTextDidChangeNotification"),
                                               object: textField)
    }
    
    fileprivate func removeRegisterDidChangeNotification() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func textDidChange() {
        self.undoManager?.removeAllActions()
        text = textField.text
        configureRightView()
        santanderState = .default
        stateDidChanged()
    }
    
    func updateAmount(amt: Int64) -> String{
        
        var currency : String
        
        switch getLanguage() {
            case "es":
                currency = "es-ES"
            case "en":
                currency = "en-US"
            case "de":
                currency = "de-DE"
            case "it":
                currency = "it-IT"
            default:
                currency = getLanguage()
        }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currencyAccounting
        
        formatter.locale = Locale(identifier: currency)
        
        let amount = Double(amt/100) + Double(amt%100)/100

        
        if let textAmount = formatter.string(from: NSNumber(value: amount)) {
            return textAmount
        }
        
        return ""
    }
    
    func setcustomPlaceholder() {
        if let lab = placeholder {
            tfPlaceholder?.text = lab
        }
    }
    
    func setHelpText() {
        if let help = helpText {
            bottomLabel?.text = help
        }
    }
}

// MARK: Public methods.
extension GlobileTextField {
    
    public func setBottomLabel(_ helpText: String, action: (() -> Void)? = nil) {
        bottomLabel.onClick(action)
        bottomLabel.text = helpText
        self.helpText = helpText
    }
    
    public func changeState(_ state: GlobileFloatTextFieldState) {
        bottomLabel.text = helpText
        santanderState = state
        stateDidChanged()
    }
    
    override public func becomeFirstResponder() -> Bool {
        return super.becomeFirstResponder() || textField.becomeFirstResponder()
    }
    
}

// MARK: Configuration
extension GlobileTextField {
    private func setupView(){
        self.heightAnchor.constraint(equalToConstant: 70).isActive = true
        
        backgroundTheme = .white
        inputMode = .´default´
        bottomLine.backgroundColor = Colors.BottomLine.default
        separatorView.backgroundColor = .globileMediumSky
        
        textField.borderStyle = .none
        textField.contentVerticalAlignment = .bottom
        textField.autocorrectionType = .no
        textField.tintColor = .globileBlack
        textField.textColor = Colors.default
        textField.font = Fonts.default
        textField.delegate = self
        
        tfPlaceholder.font = Fonts.default
        tfPlaceholder.textColor = Colors.highlighted
        
        bottomLabel.numberOfLines = 2
        bottomLabel.font = Fonts.bottom
        bottomLabel.textColor = Colors.default
        bottomLabel.lineBreakMode = .byWordWrapping
        
        bottomLine.constraints.filter { $0.firstAttribute == .height }.first?.constant = 1
        
        container.layer.borderWidth = 1.0
        container.layer.borderColor = UIColor.globileMediumSky.cgColor
        
        bottomBorder.frame = CGRect(x:0, y: container.frame.size.height - 1, width: container.frame.size.width, height:1)
        bottomBorder.backgroundColor = UIColor.globileWhite.cgColor
        container.layer.addSublayer(bottomBorder)
        
        if #available(iOS 11.0, *) {
            container.layer.cornerRadius = 3
            container.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        } else {
            let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 3, height: 3))
            let maskLayer = CAShapeLayer()
            maskLayer.path = path.cgPath
            container.layer.mask = maskLayer
        }
        
        setcustomPlaceholder()
        setHelpText()
        configureRightView()
        addNotification()
    }
    
    fileprivate func stateDidChanged(force: Bool? = nil) {
        switch santanderState {
        case .default:
            bottomLine.backgroundColor = Colors.BottomLine.default
            bottomLabel.textColor = Colors.default
        case .error:
            bottomLine.backgroundColor = Colors.error
            bottomLabel.textColor = Colors.error
        }
    }
    
    internal func configureRightView() {
        buttonsStackView.isHidden = rightIcon == nil
        actionButton.isHidden = rightIcon == nil
       
        if rightIcon == nil {
            separatorView.backgroundColor = UIColor(white: 1, alpha: 0.0)
        } else {
            separatorView.backgroundColor = .globileMediumSky
        }
    }
    
    fileprivate func addRightIcon() {
        if let rightIcon = rightIcon {
            let image = rightIcon.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
            actionButton.setImage(image, for: .normal)
            actionButton.addTarget(self, action: #selector(imageTapped), for: .touchUpInside)
        }
        configureRightView()
    }
    
    fileprivate func tintIcon() {
        switch iconColor {
            case .darkGrey:
                actionButton.tintColor = .globileDarkGrey
            case .santanderRed:
                actionButton.tintColor = .globileSantanderRed
            case .darkSky:
                actionButton.tintColor = .globileDarkSky
            case .mediumGrey:
                actionButton.tintColor = .globileMediumGrey
        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        iconAction?()
    }
    
    internal func minimizePlaceHolder() {
        guard let label = self.tfPlaceholder else { return }
        label.layoutIfNeeded()
        let oldFrame = label.frame
        let newFrame = CGRect(x: label.frame.origin.x, y: label.frame.origin.y - 4.5, width: label.frame.width*14/18, height: label.frame.height*14/18)
        
        let translation = CGAffineTransform(translationX: newFrame.midX - oldFrame.midX, y: -(oldFrame.minY + oldFrame.minY * 0.3))
        let scaling = CGAffineTransform(scaleX: newFrame.width / oldFrame.width, y: newFrame.height / oldFrame.height)
        let transform = scaling.concatenating(translation)
        
        UIView.animate(withDuration: 0.2, animations: {
            label.transform = transform
        })
    }
    
    internal func maximizePlaceHolder() {
        UIView.animate(withDuration: 0.2, animations: {
            self.tfPlaceholder.transform = CGAffineTransform.identity
        })
    }
}
