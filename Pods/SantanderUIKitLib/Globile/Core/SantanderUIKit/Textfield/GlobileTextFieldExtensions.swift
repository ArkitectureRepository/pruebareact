//

import Foundation

 extension GlobileTextField: UITextFieldDelegate {
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        bottomLine.constraints.filter { $0.firstAttribute == .height }.first?.constant = 2
        if !hasContent {
            minimizePlaceHolder()
            configureRightView()
        }
        self.layoutIfNeeded()
        santanderState = .default
        delegate?.textFieldDidBeginEditing?(textField)
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        bottomLine.constraints.filter { $0.firstAttribute == .height }.first?.constant = 1
        self.layoutIfNeeded()
        if !hasContent {
            maximizePlaceHolder()
            configureRightView()
        }
        delegate?.textFieldDidEndEditing?(textField)
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        santanderState = .default
        clearAction?()
        //Resets the amount to 0
        amount = 0
        return delegate?.textFieldShouldClear?(textField) ?? true
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return delegate?.textFieldShouldBeginEditing?(textField) ?? true
    }

    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return delegate?.textFieldShouldEndEditing?(textField) ?? true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            
        if inputMode == .amountWCurrency {
        
        if let digit = Int64(string) {
            amount = amount * 10 + digit
            let billion : Int64 = 1_000_000_000_00
            if Int64(amount) > billion{
                changeState(.error)
                var insertNumberBelowBillionStr = "Please insert a number below a billion"
                   if let path = Bundle(for: GlobileRadioButton.self).path(forResource: "SUKit", ofType: "bundle"){
                       let bundle = Bundle(path: path) ?? Bundle.main
                       insertNumberBelowBillionStr = "PleaseInsertNumberBelowBillion".localizedString(bundle: bundle, tableName: "SUIKit_strings")
                   }
                
                setBottomLabel(insertNumberBelowBillionStr)
                
            }else{
                text = updateAmount(amt: amount)
                changeState(.default)
                setBottomLabel("")
            }
         }
            
         if string == "" {
                amount = amount/10
                text = updateAmount(amt: amount)
          }
             
          if text == "" {
                amount = 0
                text = updateAmount(amt: amount)
                setBottomLabel("")
                changeState(.default)
                text = updateAmount(amt: amount)
          }
            return false
        }else{
            return delegate?.textField?(textField, shouldChangeCharactersIn: range, replacementString: string) ?? true
        }
    }

   

    @available(iOS 13.0, *)
    public func textFieldDidChangeSelection(_ textField: UITextField){
        delegate?.textFieldDidChangeSelection?(textField)
    }

    public func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        return delegate?.textFieldShouldReturn?(textField) ?? true
    }
    
}
