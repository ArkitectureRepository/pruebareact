//
//  AlertViewController.swift
//  timbrit
//
//  Created by Alvaro Royo on 11/06/2019.
//  Copyright © 2019 Sitwa Group. All rights reserved.
//

import UIKit

public struct GlobileModalAction {
    public typealias GlobileModalActionCompletion = (GlobileModal)->()
    
    var title: String
    var style: GlobileEndingButtonStyle
    var action: GlobileModalActionCompletion?
    
    public init(title: String, style: GlobileEndingButtonStyle, action: GlobileModalActionCompletion?) {
        self.title = title
        self.style = style
        self.action = action
    }
}

public class GlobileModal: UIViewController {
    
    @IBOutlet private weak var alertContainer: UIView!
    
    @IBOutlet weak var contentStack: UIStackView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var titleContainer: UIView!
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var subtitleContainer: UIView!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    @IBOutlet weak var messageContainer: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var buttonsStack: UIStackView!
    
    private(set) public var radioButtonGroup: GlobileRadioButtonGroup?

    private var image: UIImage?
    private var titleStr: String?
    private var subtitleStr: String?
    private var messageStr: String?
    public var customView: UIView?
    private var actions: [GlobileModalAction]?
    
    public convenience init(_ image: UIImage?, _ title: String?, _ subtitle: String?, _ message: String?, _ actions: [GlobileModalAction]) {
        self.init(image, title, subtitle, message, nil, actions)
    }
    
    public convenience init(_ title: String?, _ customView: UIView?, _ actions: [GlobileModalAction]) {
        self.init(nil, title, nil, nil, customView, actions)
    }
    
    convenience init(_ image: UIImage?, _ title: String?, _ subtitle: String?, _ message: String?, _ customView: UIView?, _ actions: [GlobileModalAction]) {
        self.init(nibName: nil, bundle: nil)
        self.image = image
        self.titleStr = title
        self.subtitleStr = subtitle
        self.messageStr = message
        self.customView = customView
        self.actions = actions
    }
    
    override init(nibName: String?, bundle: Bundle?) {
        let path = Bundle(for: GlobileModal.self).path(forResource: "SUKit", ofType: "bundle")!
        let bundle = Bundle(path: path) ?? Bundle.main
        super.init(nibName: "GlobileModal", bundle: bundle)
        self.modalPresentationStyle = .overCurrentContext
        self.modalTransitionStyle = .crossDissolve
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .globileAlertViewBackground
        self.alertContainer.layer.cornerRadius = 10
        
        self.imageView.image = self.image
        self.imageView.isHidden = self.image == nil
        
        self.titleLabel.text = self.titleStr

        self.titleLabel.font = .microTextBold(size: 18)

        self.titleContainer.isHidden = (self.titleStr ?? "").isEmpty

        self.subtitleLabel.text = self.subtitleStr

        self.subtitleLabel.font = .microTextBold(size: 14)
        self.subtitleContainer.isHidden = (self.subtitleStr ?? "").isEmpty
        
        self.messageLabel.text = self.messageStr
        self.messageLabel.font = .microTextRegular(size: 12)
        self.messageContainer.isHidden = (self.messageStr ?? "").isEmpty
        
        //CustomView
        if let customView = self.customView {
            self.contentStack.insertArrangedSubview(customView, at: 2)
        }
        
        //Actions
        if let actions = actions, actions.count > 0
        {
            actions.prefix(2).enumerated().forEach{
                let btn: UIButton
                switch $1.style {
                case .tertiary:
                    btn = GlobileTertiaryButton()
                    btn.contentHorizontalAlignment = .left
                    btn.setTitleColor(.globileBarDefaultTheme, for: .normal)
                    
                    
                default:
                    let endingBtn = GlobileEndingButton()
                    endingBtn.style = $1.style
                    btn = endingBtn
                }
                btn.setTitle($1.title, for: .normal)
                btn.tag = $0
                btn.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
                self.buttonsStack.addArrangedSubview(btn)
            }
        } else {
            assertionFailure("You MUST add actions to this component.")
        }

    }
    
    @objc func buttonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        self.actions?[sender.tag].action?(self)
    }
}
