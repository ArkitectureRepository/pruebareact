//
//  GlobileAlertController.swift
//  SantanderUIKit
//
//  Created by adrian.a.fernandez on 08/01/2019.
//

import UIKit


public struct GlobileAlertAction {
    var title: String
    var style: GlobileEndingButtonStyle
    var action: () -> Void

    public init(title: String, style: GlobileEndingButtonStyle, action: @escaping () -> Void) {
        self.title = title
        self.style = style
        self.action = action
    }
}


public protocol ErrorHandlingDelegate: class {

    func answerPushed(answer: String)

}

public class GlobileAlertController: UIViewController {

    public weak var delegate: ErrorHandlingDelegate?

    private struct SantanderAlertRequest: Codable {
        var title: String
        var subtitle: String
        var message: String
        var positiveButtonText: String
        var negativeButtonText: String
    }

    private struct SantanderAlertResult: Codable {
        var code: Int
    }

    //Outlets
    let contentView: UIView = {
        let view = UIView()
        //background alert
        view.backgroundColor = .globileWhite
        return view
    }()

    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .headlineBold(size: 18.0)
        label.textAlignment = .left
        var titleStr = "Title"
        
        if let path = Bundle(for: GlobileAlertController.self).path(forResource: "SUKit", ofType: "bundle"){
            let bundle = Bundle(path: path) ?? Bundle.main
            titleStr = "Title".localizedString(bundle: bundle, tableName: "SUIKit_strings")
        }
        
        label.text = titleStr
        label.textColor = .globileSantanderRed
        return label
    }()

    let closeButton: UIButton = {
        let button = UIButton()
        let image = getImage(named: "close").withRenderingMode(.alwaysTemplate)
        button.setImage(image, for: .normal)
        button.tintColor = .globileSantanderRed
        return button
    }()

    let higherSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .globileSantanderRed
        return view
    }()

    let subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = .regular(size: 16.0)
        label.textAlignment = .left
        label.textColor = .globileDarkGrey
        label.numberOfLines = 0
        return label
    }()

    let messageLabel: UILabel = {
        let label = UILabel()
        label.font = .light(size: 16.0)
        label.textAlignment = .left
        label.textColor = .globileDarkGrey
        label.numberOfLines = 0
        return label
    }()

    let lowerSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .globileLightGrey
        return view
    }()

    let buttonsStackview: UIStackView = {
        let sv = UIStackView(frame: .zero)
        sv.axis = .horizontal
        sv.distribution = .fillEqually
        sv.alignment = .center
        sv.spacing = 16.0
        return sv
    }()

    public var actions: [GlobileAlertAction] = []
    public var completion: (() -> Void)?


    public convenience init(title: String? = nil, subtitle: String? = nil, message: String? = nil, actions: [GlobileAlertAction]? = nil, completion: (() -> Void)? = nil) {

        self.init(nibName: nil, bundle: nil)
        titleLabel.text = title
        subtitleLabel.text = subtitle
        messageLabel.text = message
        self.completion = completion
        modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        if let actions = actions {
            self.actions = actions
            setupActions()
        }
    }

    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupView()
    }
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    public var header: String?
    public var text: String?

    public override func viewDidLoad() {
        super.viewDidLoad()
    }

    public func addAction(_ action: GlobileAlertAction) {
        actions.append(action)
        setupActions()
    }

    private func setupActions() {
        buttonsStackview.removeAllArrangedSubviews()
        for (index, action) in actions.prefix(2).enumerated() {
            let button = GlobileEndingButton()
            button.style = action.style
            button.setTitle(action.title, for: .normal)
            button.tag = index
            button.anchor(height: 44)
            button.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(buttonClicked(gesture:))))
            buttonsStackview.addArrangedSubview(button)
        }

    }

    @objc private func buttonClicked(gesture: UITapGestureRecognizer) {
        if let index = (gesture.view as? UIButton)?.tag {
            Array(actions)[index].action()
        }
    }

    /// Function to perform hybrid actions
    ///
    /// - Parameter json: values to perform the hybrid action
    /// - Returns: output of the hybrid action
    @objc public func hybridInterface(_ json: String) -> String {

        guard let request = try? GlobileAlertController.decode(json, type: SantanderAlertRequest.self) else {
            return ""
        }

        titleLabel.text = request.title
        subtitleLabel.text = request.subtitle
        messageLabel.text = request.message
        modalTransitionStyle = UIModalTransitionStyle.crossDissolve

        if !request.negativeButtonText.isEmpty {
            actions.append(GlobileAlertAction(title: request.negativeButtonText, style: .secondary, action: {
                self.delegate?.answerPushed(answer: (try? GlobileAlertController.encode(SantanderAlertResult(code: 2))) ?? "2")
                self.dismiss(animated: true)

            }))
        }

        if !request.positiveButtonText.isEmpty {
            actions.append(GlobileAlertAction(title: request.positiveButtonText, style: .primary) {
                self.delegate?.answerPushed(answer: (try? GlobileAlertController.encode(SantanderAlertResult(code: 1))) ?? "1")
                self.dismiss(animated: true)
            })
        }

        setupActions()

        return ""
    }
}

//Setup Views
extension GlobileAlertController {

    private func setupView() {

        //background view
        view.backgroundColor = .globileAlertViewBackground //SantanderColor.blackAlertViewBackground(alpha: 0.70)

        view.addSubview(contentView)
        setupContentView()

        contentView.addSubview(titleLabel)
        contentView.addSubview(closeButton)
        contentView.addSubview(higherSeparatorView)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(messageLabel)
        contentView.addSubview(lowerSeparatorView)
        contentView.addSubview(buttonsStackview)

        setupTitleLabel()
        setupCloseButton()
        setupHigherSeparatorView()
        setupSubtitleLabel()
        setupMessageLabel()
        setupLowerSeparatorView()
        setupButtonsStackview()

        closeButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeButtonTouchUpInside(_:))))

        modalPresentationStyle = .overCurrentContext

    }

    @objc private func closeButtonTouchUpInside(_ gesture: UITapGestureRecognizer) -> String {
        dismiss(animated: true, completion: completion)
        delegate?.answerPushed(answer: (try? GlobileAlertController.encode(SantanderAlertResult(code: 3))) ?? "3")
        return (try? GlobileAlertController.encode(SantanderAlertResult(code: 3))) ?? "3"
    }

    private func setupContentView() {
        contentView.layer.cornerRadius = 5.0
        contentView.anchor(left: view.leftAnchor, right: view.rightAnchor, padding: UIEdgeInsets(top: 0, left: 16, bottom: 0, right: -16))
        contentView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0).isActive = true
    }

    private func setupTitleLabel() {
        titleLabel.anchor(left: contentView.leftAnchor, top: contentView.topAnchor, padding: UIEdgeInsets(top: 9, left: 16, bottom: 0, right: 0))
    }

    private func setupCloseButton() {
        closeButton.anchor(top: contentView.topAnchor, right: contentView.rightAnchor, bottom: higherSeparatorView.topAnchor, width: 24, height: 24, padding: UIEdgeInsets(top: 16, left: 0, bottom: -16, right: -16))
    }

    private func setupHigherSeparatorView() {
        higherSeparatorView.anchor(left: contentView.leftAnchor, top: titleLabel.bottomAnchor, right: contentView.rightAnchor, height: 1.0, padding: UIEdgeInsets(top: 9, left: 0, bottom: 0, right: 0))
    }

    private func setupSubtitleLabel() {
        subtitleLabel.anchor(left: contentView.leftAnchor, top: higherSeparatorView.bottomAnchor, right: contentView.rightAnchor, padding: UIEdgeInsets(top: 9, left: 16, bottom: 0, right: -16))
    }

    private func setupMessageLabel() {
        messageLabel.anchor(left: contentView.leftAnchor, top: subtitleLabel.bottomAnchor, right: contentView.rightAnchor, padding: UIEdgeInsets(top: 16, left: 16, bottom: 16, right: -16))
    }

    private func setupLowerSeparatorView() {
        lowerSeparatorView.anchor(left: contentView.leftAnchor, top: messageLabel.bottomAnchor, right: contentView.rightAnchor, height: 1.0, padding: UIEdgeInsets(top: 9, left: 0, bottom: 0, right: 0))
    }

    private func setupButtonsStackview() {
        buttonsStackview.anchor(left: contentView.leftAnchor, top: lowerSeparatorView.bottomAnchor, right: contentView.rightAnchor, bottom: contentView.bottomAnchor, padding: UIEdgeInsets(top: 16, left: 8, bottom: -16, right: -8))
    }

    private static func decode<T: Codable>(_ json: String, type: T.Type) throws -> T {

        guard let data = json.data(using: .utf8) else {
            throw NSError(domain: "Unable to parse action", code: 1, userInfo: nil)
        }

        return try JSONDecoder().decode(T.self, from: data)
    }

    private static func encode<T: Codable>(_ result: T) throws -> String {
        let data = try JSONEncoder().encode(result)
        if let result = String(data: data, encoding: .utf8) {
            return result
        }

        throw NSError(domain: "JSON could't be encoded into a string.", code: 1, userInfo: nil)
    }
}

extension UIStackView {

    func removeAllArrangedSubviews() {

        let removedSubviews = arrangedSubviews.reduce([]) { (allSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            return allSubviews + [subview]
        }

        // Deactivate all constraints
        NSLayoutConstraint.deactivate(removedSubviews.flatMap({ $0.constraints }))

        // Remove the views from self
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
}
