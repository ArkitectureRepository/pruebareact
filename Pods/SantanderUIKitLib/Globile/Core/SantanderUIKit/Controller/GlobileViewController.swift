//
//  GlobileAlertController.swift
//  GlobileCore
//
//  Created by adrian.a.fernandez on 08/01/2019.
//

import UIKit
import SensorsLib

public enum GlobileTitle {
    case image
    case title(String)
}

// MARK: Santander Button struct
public struct GlobileBarButton {

    //Style: Back, Menu and custom
    public enum Style {
        case back
        case menu
        case custom(UIImage)
    }
    var style: Style
    var action: (() -> Void)?

    public init(style: Style, action: (() -> Void)? = nil) {
        self.style = style
        self.action = action
    }

    public func returnImage() -> UIImage? {
        switch style {
        case .back:
            return SantanderImage.back
        case .menu:
            return SantanderImage.menu
        case .custom(let image):
            return image
        }
    }

}

open class GlobileViewController: UIViewController {

    public var santanderNavigationController: GlobileNavigationBar? {
        return navigationController as? GlobileNavigationBar
    }

    public var globileTitle: GlobileTitle = GlobileTitle.title("") {
        didSet {
            setup()
        }
    }

    public var visualEffect: UIBlurEffect.Style = .extraLight

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
    }

    open var leftNavigationButton: GlobileBarButton? {
        didSet {
            setupLeftNavigationButton()
        }
    }

    open var rightNavigationButton: GlobileBarButton? {
        didSet {
            setupRightNavigationButton()
        }
    }
       
    open var privateMode: Bool = false {
        didSet {
            var statusPrivacyMode = PrivacyMode.off
            if self.privateMode {
                statusPrivacyMode = .on
            }
            switchAllPrivateLablesProgrammatically(privacyMode: statusPrivacyMode)
        }
    }
    
    
    open var enabledSensor: Bool = false {
        didSet {
                enableProximitySensor(enabled: self.enabledSensor)
        }
    }
    private var leftNavigationButtonAction: (() -> Void)?
    private var rightNavigationButtonAction: (() -> Void)?

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    open override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .globileBackground
    }

    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @objc private func leftTouchUpInside() {
        leftNavigationButtonAction?()
    }

    @objc private func rightTouchUpInside() {
        rightNavigationButtonAction?()
    }

    private let alertBar = GlobileAlertBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 58))

    open var santanderAlertBarDelegate: GlobileAlertBarDelegate?

    open func showGlobileAlertBar(messageHTML: String, actionText: String) {

        alertBar.alertBarDelegate = santanderAlertBarDelegate

        self.view.addSubview(alertBar)
        alertBar.showAlertBar(messageHTML: messageHTML, actionText: actionText)

    }

}

extension GlobileViewController {

    private func setup() {
        switch globileTitle {
        case GlobileTitle.image: addNavigationControllerImage()
        case GlobileTitle.title(let title):
            navigationItem.titleView = nil
            self.title = title
        }
    }

    private func addNavigationControllerImage() {

        self.title = nil
        let titleImageView = UIImageView(image: SantanderImage.logo)

        //.dark
        titleImageView.tintColor = santanderNavigationController?.style == .default ? .white : .white
        //.light
        titleImageView.tintColor = santanderNavigationController?.style == .default ? .white : .globileSantanderRed

        titleImageView.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        titleImageView.contentMode = .scaleAspectFit

        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        titleView.addSubview(titleImageView)

        navigationItem.titleView = titleView


        let leftMargin: CGFloat = 12
        var rightMargin: CGFloat = 12
        var hasLeftButton = false
        var hasRightButton = false

        let vc = self.navigationController?.viewControllers.first
        if vc == self.navigationController?.visibleViewController {
            hasLeftButton = false
        } else {
            hasLeftButton = true
        }
        if navigationItem.rightBarButtonItem != nil {
            hasRightButton = true
            rightMargin = 24
        }

        var leadMargin: CGFloat = 12
        var trailMargin: CGFloat = 12
        if hasLeftButton && !hasRightButton {
            leadMargin = 0
            trailMargin = -rightMargin + 48
        } else if !hasLeftButton && hasRightButton {
            leadMargin = rightMargin + leftMargin
            trailMargin = 0
        } else if hasLeftButton && hasRightButton {
            leadMargin = 0
            trailMargin = -20 + 44 - rightMargin
        }

        titleImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            //            titleImageView.heightAnchor.constraint(equalToConstant: 16.0),
            //            titleImageView.widthAnchor.constraint(equalToConstant: 100.0),
            titleImageView.topAnchor.constraint(equalTo: titleView.topAnchor, constant: 8),
            titleImageView.leadingAnchor.constraint(equalTo: titleView.leadingAnchor, constant: leadMargin),
            titleImageView.trailingAnchor.constraint(equalTo: titleView.trailingAnchor, constant: -trailMargin),
            titleImageView.bottomAnchor.constraint(equalTo: titleView.bottomAnchor, constant: -8)
        ])

    }

    private func setupRightNavigationButton() {
        if let rightButton = rightNavigationButton {
            let button = UIButton(type: .system)
            let buttonImage = rightButton.returnImage()
            button.setImage(buttonImage, for: .normal)
            button.addTarget(self, action: #selector(rightTouchUpInside), for: .touchUpInside)

            let menuBarItem = UIBarButtonItem(customView: button)
            menuBarItem.customView?.translatesAutoresizingMaskIntoConstraints = false
            menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 24).isActive = true
            menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 24).isActive = true

            rightNavigationButtonAction = rightButton.action
            navigationItem.rightBarButtonItem = menuBarItem

        } else {
            navigationItem.rightBarButtonItem = nil
        }
    }

    private func setupLeftNavigationButton() {
        if let leftButton = leftNavigationButton {
            leftNavigationButtonAction = leftButton.action
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: leftButton.returnImage(), style: .plain, target: self, action: #selector(leftTouchUpInside))
        } else {
            navigationItem.setHidesBackButton(true, animated: false)
        }
    }

    public func enableProximitySensor(enabled: Bool) {
        let operation: SensorsOperation = enabled ? .START : .STOP
        SensorsLib().sensorOperation(sensorType: .PROXIMITY_SENSOR, operation: operation) { status in
            if let sensorData = status.sensorData as? ProximityStatus {
                let proxStatus = sensorData.proximityStatus
                if proxStatus == .NEAR {
                    self.privateMode = !self.privateMode
                }
            }
        }
    }
    public func enabledPrivateLabels() {
        let privateLabels = self.getSubviewsOfView(v: self.view)
        privateLabels.forEach {
            if $0.isPrivate == false {
                $0.setPrivate(privacyMode: .on, blurEffect: self.visualEffect)
            } else {
                $0.setPrivate(privacyMode: .off)
            }
        }
    }

    public func switchAllPrivateLablesProgrammatically(privacyMode: PrivacyMode) {
        let privateLabels = getSubviewsOfView(v: view)
        privateLabels.forEach {
            switch privacyMode {
            case .on: $0.setPrivate(privacyMode: .on)
            case .off: $0.setPrivate(privacyMode: .off)
            }
        }
    }

    @objc func getSubviewsOfView(v: UIView) -> [GlobilePrivateLabel] {
        var privateLabels = [GlobilePrivateLabel]()
        for subview in v.subviews {
            privateLabels += getSubviewsOfView(v: subview)
            if let subv = subview as? GlobilePrivateLabel {
                privateLabels.append(subv)
            }
        }
        return privateLabels
    }
    //  Close keyboard when touch outside.
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
