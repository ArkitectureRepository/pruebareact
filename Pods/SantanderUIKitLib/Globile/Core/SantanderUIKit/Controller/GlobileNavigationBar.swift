//
//  SantanderNavigationController.swift
//  SantanderUIKit
//
//  Created by adrian.a.fernandez on 08/01/2019.
//

import UIKit

public enum GlobileNavigationStyle {
    case `default`
    case light
}

public class GlobileNavigationBar: UINavigationController {

    public var style: GlobileNavigationStyle = .default {
        didSet {
            setup()
        }
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    func setup() {
        //        navigationBar.heightAnchor.constraint(equalToConstant: 44.0)
        setupBackgroundGradient()
        setupBackButton()
    }

    private func setupBackgroundGradient() {
        navigationBar.isTranslucent = false
        navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.headlineBold(size: 19.0), NSAttributedString.Key.foregroundColor: style == .default ? UIColor.globileWhite : UIColor.globileSantanderRedTextLightTheme]

        if style == .default {
            navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.headlineBold(size: 19.0), NSAttributedString.Key.foregroundColor: UIColor.globileWhiteButton]
            navigationBar.barTintColor = .globileBarDefaultTheme
            navigationBar.backgroundColor = .globileWhite
        } else {
            navigationBar.setBackgroundImage(nil, for: .default)
            navigationBar.barTintColor = .globileWhite
            navigationBar.backgroundColor = .globileSantanderRed
        }
    }

    private func setupBackButton() {

        navigationBar.tintColor = style == .default ? UIColor.globileBarBackButton :              UIColor.globileSantanderRed
        navigationBar.backIndicatorImage = SantanderImage.back
        navigationBar.backIndicatorTransitionMaskImage = SantanderImage.back
    }

    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return style == .default ? .lightContent : .default
    }

}

private extension CAGradientLayer {

    func createGradientImage() -> UIImage? {
        var image: UIImage?
        UIGraphicsBeginImageContext(bounds.size)
        if let context = UIGraphicsGetCurrentContext() {
            render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
        return image
    }

}

private extension UINavigationBar {

    func setGradientBackground(gradient: CAGradientLayer) {

        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: frame.height)
        gradient.colors = [UIColor.globileBostonRed.cgColor, UIColor.globileSantanderRed.cgColor]
        gradient.locations = [0.0, 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)

        setBackgroundImage(gradient.createGradientImage(), for: UIBarMetrics.default)
    }
}
