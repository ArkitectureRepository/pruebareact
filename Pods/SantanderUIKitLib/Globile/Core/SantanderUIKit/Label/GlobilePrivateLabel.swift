//
//  GlobileLabel.swift
//  SantanderUIKit
//
//  Created by adrian.a.fernandez on 07/02/2019.
//
import UIKit

 public enum PrivacyMode {
    case on
    case off
}

public class GlobilePrivateLabel: GlobileLabel {
    public convenience init(privateMode: PrivacyMode, animatedDuration: TimeInterval = 0.5) {
        self.init()
        setPrivate(privacyMode: privateMode, animateDuration: animatedDuration)
    }
    
   
    public var isPrivate: Bool? = false

    public func setPrivate(privacyMode: PrivacyMode, blurEffect: UIBlurEffect.Style = .extraLight, animateDuration: TimeInterval = 0.5 ) {
        switch privacyMode {
            case .on: privateModeOn(blurMode: blurEffect,animateDuration: animateDuration )
            isPrivate = true
            case .off: privateModeOff()
            isPrivate = false
        }
    }
    var privateView: UIView = {
        let privateView = UIView(frame: .zero)
        return privateView
    }()

    func privateModeOn(blurMode: UIBlurEffect.Style, animateDuration: TimeInterval = 0.5) {

        if #available(iOS 10.0, *) {
            switch blurMode {
                case .extraLight: privateView = UIVisualEffectView(effect: UIBlurEffect(style: .extraLight))
                case .light: privateView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
                case .regular: privateView = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
                case .prominent: privateView = UIVisualEffectView(effect: UIBlurEffect(style: .prominent))
                case .dark: privateView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
                @unknown default:
                    print("Error")
            }
        } else {
            switch blurMode {
                case .extraLight: privateView = UIVisualEffectView(effect: UIBlurEffect(style: .extraLight))
                case .light: privateView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
                case .dark: privateView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
                default:
                    print("Error")
            }
        }

        privateView.translatesAutoresizingMaskIntoConstraints = false
        privateView.layer.speed = 0.5
        privateView.layer.timeOffset = 0.5
        addSubview(privateView)
        privateView.backgroundColor = .clear
        privateView.alpha = 0.0
        self.privateView.isHidden = false

        NSLayoutConstraint.activate([
            privateView.topAnchor.constraint(equalTo: topAnchor),
            privateView.leadingAnchor.constraint(equalTo: leadingAnchor),
            privateView.trailingAnchor.constraint(equalTo: trailingAnchor),
            privateView.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
        privateView.layer.masksToBounds = true
        privateView.layer.cornerRadius = 5.0

        UIView.animate(withDuration: animateDuration) {
            self.privateView.alpha = 1.0
        }
    }

    func privateModeOff() {
        UIView.animate(withDuration: 0.5, animations: {
            self.privateView.alpha = 0.0
        }) { (_) in
            self.privateView.removeFromSuperview()
        }
    }


}
