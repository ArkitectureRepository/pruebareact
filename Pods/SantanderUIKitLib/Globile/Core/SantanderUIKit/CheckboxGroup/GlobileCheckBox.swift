//
//  GlobileCheckBox.swift
//  GlobileCheckBox
//
//  Created by Naufal Aros el Morabet on 14/03/2019.
//  Copyright © 2019 Naufal Aros el Morabet. All rights reserved.
//

import UIKit

public enum GlobileCheckboxTintColor {
    case red
    case turquoise
}

public class GlobileCheckBox: UIControl {

    // MARK: Colors

    private let infoButtonColor: UIColor = .globileMediumGrey


    /// A Boolean value indicating whether the info button is enabled.
    public var infoButtonEnabled: Bool = false {
        didSet {
            infoButton.isHidden = !infoButtonEnabled
        }
    }
    
    /// A Boolean value indicating whether the checkbox  is enabled.
    public var enabledCheckbox: Bool = true {
        didSet {
            self.isEnabled = !enabledCheckbox
        }
    }

    /// A Boolean value indicating whether the control is in the selected state.
    public override var isSelected: Bool {
        didSet {
            checkboxButton.isSelected = isSelected
            updateLabel()
        }
    }

    /// The tint color to apply to the checkbox button.
    public var color: GlobileCheckboxTintColor = .red


    /// The current text that is displayed by the button.
    public var text: String?

    // MARK: Subviews

    private let checkboxButton: UIButton = {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(getImage(named: "empty_checkbox"), for: .normal)
        return button
    }()

    private let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = .regular(size: 16.0)
        return label
    }()

    private let infoButton: UIButton = {
        let button = UIButton(type: .infoDark)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isHidden = true
        return button
    }()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addSubviews()
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        setupViews()
    }

    // MARK: Private

    private func addSubviews() {
        addSubview(checkboxButton)
        addSubview(label)
        addSubview(infoButton)

        setupLayout()
    }

    private func setupLayout() {
        NSLayoutConstraint.activate([
            checkboxButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4),
            checkboxButton.trailingAnchor.constraint(equalTo: label.leadingAnchor, constant: -12),
            checkboxButton.heightAnchor.constraint(equalToConstant: 30.0),
            checkboxButton.widthAnchor.constraint(equalTo: checkboxButton.heightAnchor),
            checkboxButton.topAnchor.constraint(equalTo: topAnchor, constant: -3)
            ])

        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: topAnchor),
            label.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])

        NSLayoutConstraint.activate([
            infoButton.leadingAnchor.constraint(equalTo: label.trailingAnchor, constant: 12),
            infoButton.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -4),
            infoButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            infoButton.heightAnchor.constraint(equalToConstant: 18.0),
            infoButton.widthAnchor.constraint(equalTo: infoButton.heightAnchor)
            ])
    }

    private func setupViews() {
        backgroundColor = .clear
        label.text = text
        if enabledCheckbox == false {
            label.textColor = .globileLightGrey
            infoButton.tintColor = .globileLightGrey
            infoButtonEnabled = false
            checkboxButton.isSelected = false
            checkboxButton.isUserInteractionEnabled = false
            if #available(iOS 13.0, *) {
                checkboxButton.setImage(getImage(named: "empty_checkbox").withTintColor(.globileLightGrey), for: .normal)
            } else {
                checkboxButton.setImage(getImage(named: "disabled_grey_checkbox"), for: .normal)
            }
        } else {
            self.isEnabled = true
            updateLabel()
            infoButton.tintColor = infoButtonColor
            checkboxButton.isUserInteractionEnabled = true
            checkboxButton.addTarget(self, action: #selector(checkboxButtonTapped(_:)), for: .touchUpInside)
            addTarget(self, action: #selector(selectView(_:)), for: .touchUpInside)
            switch color {
            case .red:
                checkboxButton.setImage(getImage(named: "selected_red_checkbox"), for: .selected)
            case .turquoise:
                checkboxButton.setImage(getImage(named: "selected_turquoise_checkbox"), for: .selected)
            }
            
        }
    }

    private func updateLabel() {
            label.textColor = isSelected ? .globileBlack : .globileMediumGrey
    }
    
    // MARK: Actions

    @objc func selectView(_ sender: UITapGestureRecognizer) {
        checkboxButton.isSelected.toggle()
        isSelected = checkboxButton.isSelected
        sendActions(for: .valueChanged)
    }

    @objc func checkboxButtonTapped(_ sender: UIButton) {
        sender.isSelected.toggle()
        isSelected = sender.isSelected
        updateLabel()
        sendActions(for: .valueChanged)
    }
}
