//
//  GlobileCheckBoxGroup.swift
//  GlobileCheckBox
//
//  Created by Naufal Aros el Morabet on 15/03/2019.
//  Copyright © 2019 Naufal Aros el Morabet. All rights reserved.
//

import UIKit

public protocol GlobileCheckboxGroupDelegate: class {
    func didSelect(checkbox: GlobileCheckBox)
    func didDeselect(checkbox: GlobileCheckBox)
}

public class GlobileCheckBoxGroup: UIView {
    
    /// The object that acts as the delegate of the checkbox group.
    public var delegate: GlobileCheckboxGroupDelegate?
    
    /// The group's checkboxes.
    public var checkboxes: [GlobileCheckBox]
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 20.0
        stackView.distribution = .fill
        return stackView
    }()


    /// Returns a new checkbo group that manages the provided checkboxes.
    ///
    /// - Parameter checkboxes: The checkboxes to be arranged by the group view.
    public init(checkboxes: [GlobileCheckBox]) {
        self.checkboxes = checkboxes
        super.init(frame: .zero)
        addSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        setupViews()
    }


    /// Select all checkboxes.
    public func selectAll() {
        checkboxes.forEach { $0.isSelected = true }
    }


    /// Deselect all checkboxes.
    public func deselectAll() {
        checkboxes.forEach { $0.isSelected = false }
    }

    // MARK: Private

    private func addSubviews() {
        addSubview(stackView)

        checkboxes.forEach { self.stackView.addArrangedSubview($0) }

        setupLayout()
    }

    private func setupLayout() {
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: stackView.topAnchor),
            leadingAnchor.constraint(equalTo: stackView.leadingAnchor),
            trailingAnchor.constraint(equalTo: stackView.trailingAnchor),
            bottomAnchor.constraint(equalTo: stackView.bottomAnchor)
        ])
    }

    private func setupViews() {
        checkboxes.forEach { checkbox in
            checkbox.addTarget(self, action: #selector(checkboxTapped(_:)), for: .valueChanged)
        }
    }

    // MARK: Actions

    @objc func checkboxTapped(_ sender: GlobileCheckBox) {
        if sender.isSelected {
            delegate?.didSelect(checkbox: sender)
        } else {
            delegate?.didDeselect(checkbox: sender)
        }

    }

}
