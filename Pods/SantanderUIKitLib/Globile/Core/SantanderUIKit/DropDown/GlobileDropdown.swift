//    ___________________________________________
//   |                                    |     |
//   |    Select one option....           |  V  |
//   |____________________________________|_____|
//   |                                          |
//   |    item 1                                |
//   |__________________________________________|
//   |                                          |
//   |    item 2                                |
//   |__________________________________________|
//   |                                          |
//   |    item 3                                |
//   |__________________________________________|

import UIKit

public protocol GlobileDropDownDelegate: class {
    func dropDownSelected <T>(_ item: GlobileDropdownData<T>, _ sender: GlobileDropdown<T>)
    func dropdownExpanded <T>(_ sender: GlobileDropdown<T>)
    func dropdownCompressed <T>(_ sender: GlobileDropdown<T>)
}

public extension GlobileDropDownDelegate {
    func dropdownExpanded <T>(_ sender: GlobileDropdown<T>) {}
    func dropdownCompressed <T>(_ sender: GlobileDropdown<T>) {}
}

public enum SantanderDropDownErrorState {
    case error
    case normal
}

public enum SantanderDropDownColorTheme {
    case white
    case sky
}

public class GlobileDropdown<T>: UIView, UITableViewDelegate, UITableViewDataSource {
    
                          
    // MARK: - Vars
    public var items: [GlobileDropdownData<T>]?
    public var theme: SantanderDropDownColorTheme = .white {
        didSet {
            dropButton.theme = theme
        }
    }
    var openclose = false
    private let dropButton = GlobileDropdownButton()
    private let tableView = SantanderDropDownTableView()
    /// The tint color to apply to the checkbox button.
    public var color: GlobileDropDownTintColor = .red
    public var dropDownDelegate: GlobileDropDownDelegate?
    public var itemSelected: Int? = 0 {
        didSet {
            if  self.itemSelected! < tableView.numberOfRows(inSection: 0) {
                tableView.selectRow(at: IndexPath(row: self.itemSelected!, section: 0) , animated: false, scrollPosition: .middle)
                dropButton.setPlaceholderText(text: items![self.itemSelected!].label)
                dropDownDelegate?.dropDownSelected(items![self.itemSelected!], self)
            }
        }
    }
    private var tableViewheightConstraint: NSLayoutConstraint?
    
    public var hintMessage: String {
        get {
            var selectOptionStr = "Select an option..."
            if let path = Bundle(for: GlobileDropdown.self).path(forResource: "SUKit", ofType: "bundle"){

                let bundle = Bundle(path: path) ?? Bundle.main
                selectOptionStr = "SelectOption".localizedString(bundle: bundle, tableName: "SUIKit_strings")
            }
            return selectOptionStr
        }
        set {
            dropButton.labelView.text = newValue
        }
    }

    public var floatingMessage: String {
        get {
            return " "
        }
        set {
            dropButton.floatingLabel.text = newValue
        }
    }

    // MARK: Bottom line Separator & Label
    fileprivate lazy var bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = .globileBostonRed
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()

    fileprivate lazy var bottomLabel: GlobileLabel = {
        let label = GlobileLabel()
        label.edges = UIEdgeInsets(top: 0.0, left: 15, bottom: 0.0, right: 0)
        label.numberOfLines = 2
        label.text = errorMessage
        label.font = .regular(size: 14)
        label.textColor = .globileBostonRed
        label.translatesAutoresizingMaskIntoConstraints = false
        label.isHidden = true
        return label
    }()


    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        //Put here your visual code in real time.
    }

    // MARK: - Custom setup view
    private func setup() {

        
        // General
        translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self

        // Subviews
        addSubview(dropButton)
        addSubview(bottomLine)
        addSubview(bottomLabel)
        addSubview(tableView)

        //Button target
        dropButton.addTarget(self, action: #selector(GlobileDropdown.pressButton), for: .touchUpInside)

        // Layout Constraints
        dropButton.translatesAutoresizingMaskIntoConstraints = false

        // Button
        NSLayoutConstraint.activate([
            dropButton.topAnchor.constraint(equalTo: topAnchor, constant: 0.0),
            dropButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 0.0),
            dropButton.rightAnchor.constraint(equalTo: rightAnchor, constant: 0.0),
           dropButton.heightAnchor.constraint(equalToConstant: 49.0)
            ])

        // Bottomline
        NSLayoutConstraint.activate([
            bottomLine.topAnchor.constraint(equalTo: dropButton.bottomAnchor, constant: 0.0),
            bottomLine.leftAnchor.constraint(equalTo: leftAnchor, constant: 0.0),
            bottomLine.rightAnchor.constraint(equalTo: rightAnchor, constant: 0.0),
            bottomLine.heightAnchor.constraint(equalToConstant: 1.0)
            ])

        // Bottom label
        NSLayoutConstraint.activate([
            bottomLabel.topAnchor.constraint(equalTo: bottomLine.bottomAnchor, constant: 3.0),
            bottomLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 0.0),
            bottomLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: 0.0)
            ])

        // Tableview
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: dropButton.bottomAnchor),
            tableView.leftAnchor.constraint(equalTo: leftAnchor),
            tableView.rightAnchor.constraint(equalTo: rightAnchor),
            bottomAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 0.0)
            ])
        tableViewheightConstraint = tableView.heightAnchor.constraint(equalToConstant: 0.0)
        tableViewheightConstraint?.isActive = true
    }

    // MARK: - Show / Hide TableView
     func show() {

        var tableHeight = 47 * 4
        let itemsCount = self.items?.count ?? 0
        if itemsCount < 4 {
            tableHeight = itemsCount * 47
        }

        if #available(iOS 10.0, *) {
            UIViewPropertyAnimator(duration: 0.5, dampingRatio: 0.8) {
                self.tableViewheightConstraint?.constant = CGFloat(tableHeight)
                self.layoutSubviews()
                }.startAnimation()
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.tableViewheightConstraint?.constant = CGFloat(tableHeight)
                self.layoutSubviews()
            })
        }

        dropButton.rotateUpArrow()
        dropDownDelegate?.dropdownExpanded(self)
    }

     func hide() {
        if #available(iOS 10.0, *) {
            UIViewPropertyAnimator(duration: 0.5, dampingRatio: 0.8) {
                self.tableViewheightConstraint?.constant = 0.0
                self.tableView.bounds.origin.y = 0.0
                self.layoutSubviews()
                }.startAnimation()
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.tableViewheightConstraint?.constant = 0.0
                self.tableView.bounds.origin.y = 0.0
                self.layoutSubviews()
            })
        }
        dropButton.rotateDownArrow()
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return items!.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "dropcell", for: indexPath) as? GlobileDropdownCell
        cell?.textLabel?.text = items![indexPath.row].label
        cell?.color = self.color
        return cell ?? GlobileDropdownCell()
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dropButton.setPlaceholderText(text: items![indexPath.row].label)
        dropDownDelegate?.dropDownSelected(items![indexPath.row], self)
        pressButton()
    }

    //Button Target function
    @objc func pressButton() {
        if !openclose {
            show()
            openclose = !openclose
        } else {
            hide()
            openclose = !openclose
        }
    }

    private var message = ""
    public var errorMessage: String {
        get {
            if message.isEmpty {
                var selectOption1Str = "Select an option before going on"
                if let path = Bundle(for: GlobileDropdown.self).path(forResource: "SUKit", ofType: "bundle"){
                    let bundle = Bundle(path: path) ?? Bundle.main
                    selectOption1Str = "SelectOption1".localizedString(bundle: bundle, tableName: "SUIKit_strings")
                }
                
                return selectOption1Str
            } else {
                return message
            }
        }
        set {
            message = "Error! " + newValue
            bottomLabel.text = message
        }
    }

    public func errorState(state: SantanderDropDownErrorState) {
        switch state {
        case .error:
            bottomLine.isHidden = false
            bottomLabel.isHidden = false

        case .normal:
            bottomLine.isHidden = true
            bottomLabel.isHidden = true

        }
    }
}
