//
import UIKit

/// Component color options
///
/// - red: BostonRed (default)
/// - turquoise: Turquoise
public enum GlobileDropDownTintColor {
    case red
    case turquoise
}


public class GlobileDropdownCell: UITableViewCell {

    var backView: UIView?

    /// The tint color to apply to the checkbox button.
    public var color: GlobileDropDownTintColor = .red


    /// Override selection function for custom aspect.
    ///
    /// - Parameters:
    ///   - selected: item selected
    ///   - animated: set animation
    public override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            self.textLabel?.textColor = .globileWhite
        } else {
            self.textLabel?.textColor = .globileDarkGrey
        }
    }


    /// Init function
    ///
    /// - Parameters:
    ///   - style: cell style
    ///   - reuseIdentifier: name of reuse cell.
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        switch color {
        case .red:
            backView!.backgroundColor = .globileBostonRed
        case .turquoise:
            backView!.backgroundColor = .globileTurquoise
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // Setup cell properties.
    func setupCell() {
        isUserInteractionEnabled = true
        textLabel?.font = .regular(size: 16)
        textLabel?.textColor = .globileLightGrey//SantanderColor.lightGrey(alpha: 0.5)
        setupView()
    }

    // Setup view inside cell.
    func setupView() {
        backView = UIView()
        contentView.translatesAutoresizingMaskIntoConstraints = false
        backView!.backgroundColor = .globileBostonRed
        backView!.layer.cornerRadius = 4
        selectedBackgroundView = backView

        NSLayoutConstraint.activate([
            contentView.rightAnchor.constraint(equalTo: rightAnchor, constant: 5.0),
            contentView.leftAnchor.constraint(equalTo: leftAnchor, constant: -5.0),
            contentView.topAnchor.constraint(equalTo: topAnchor, constant: 0.0),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0.0)
            ])
    }
}
