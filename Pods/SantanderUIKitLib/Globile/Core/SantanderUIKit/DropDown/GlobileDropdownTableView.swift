//
import UIKit


public class SantanderDropDownTableView: UITableView {

    public var theme: SantanderDropDownColorTheme = .white {
        didSet {
            switch theme {
            case .white:
                backgroundColor = .globileWhite
            case .sky:
                backgroundColor = .globileLightSky
            }
        }
    }

    override public init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        separatorStyle = .none
        bounces = false
    }

    func setupView() {
        isUserInteractionEnabled = true
        register(GlobileDropdownCell.self, forCellReuseIdentifier: "dropcell")
        translatesAutoresizingMaskIntoConstraints = false
        layer.cornerRadius = 3.0
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.globileMediumSky.cgColor
    }
}
