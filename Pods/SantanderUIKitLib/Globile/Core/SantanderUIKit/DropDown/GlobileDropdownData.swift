import Foundation

public class GlobileDropdownData<T> {
    public var label: (String)
    public var value: (T)?

    public init(label: String, value: T?) {
        self.label = label
        self.value = value
    }
}
