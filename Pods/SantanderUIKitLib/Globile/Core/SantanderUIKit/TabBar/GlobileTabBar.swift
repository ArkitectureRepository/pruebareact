import UIKit

public enum TabBarError: Error {
    case indexOutOfBounds
}

/// Main class extends UITabBarController
public class GlobileTabBar: UITabBarController, UITabBarControllerDelegate {

    private var numberOfControllers = 0
    private var numberOfTabBar = 0

    override public func viewDidLoad() {
        setupTabBar()
    }

    /// Function for add viewcontrollers to TabBar component.
    ///
    /// - Parameters:
    ///   - viewControllers: array of viewcontrollers to assign to tabs.
    ///   - animated: animated true/flase.
    override public func setViewControllers(_ viewControllers: [UIViewController]?, animated: Bool) {
        self.delegate = self
        if let viewControllers = viewControllers {
            numberOfControllers = viewControllers.count
            numberOfTabBar = numberOfControllers
            if numberOfControllers > 4 {
                numberOfTabBar = 5
            }

            viewControllers.forEach {
                if #available(iOS 10.0, *) {
                    $0.tabBarItem.badgeColor = .globileRedBadgeColor
                    $0.tabBarItem.setBadgeTextAttributes([NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.globileWhite], for: .normal)
                } else {
                    // Badge color gets color auto
                }
            }
            setupHorizontalBar()
        }

        super.setViewControllers(viewControllers, animated: animated)
    }

    /// Set budget and internal number indicator to icon of tab.
    ///
    /// - Parameters:
    ///   - count: number into badge.
    ///   - item: item index of tab bar for add the badge.
    /// - Throws: throws if anything is wrong.
    public func setBadgeNumber(_ count: Int, for item: Int) throws {

        guard (viewControllers?.count)! > item else {
            throw TabBarError.indexOutOfBounds
        }
        if let controller = viewControllers?[item] {
            let tabBarItem = controller.tabBarItem
            if count > 99 {
                tabBarItem?.badgeValue = "99+"
            } else {
                tabBarItem?.badgeValue = "\(count)"
            }
        }
    }

    override public var selectedIndex: Int {
        didSet {
            handleTabSelection(selectedIndex: selectedIndex)
        }
    }

    public func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        //Tab tapped
        guard let viewControllers = tabBarController.viewControllers else { return }
        
        if let tappedIndex = viewControllers.firstIndex(of: viewController) {
            //Tab tapped at tappedIndex
            handleTabSelection(selectedIndex: tappedIndex)
        }else {
            handleTabSelection(selectedIndex: 4)
        }
        
    }
    private func handleTabSelection(selectedIndex: Int) {
        let currentPosition = horizontalBarLeftAnchorConstraint?.constant ?? 0
        
        let finalPosition = CGFloat((Int(tabBar.frame.width)/numberOfTabBar)*selectedIndex)
        UIView.animate(withDuration: 0.2) {
            self.horizontalBarView.transform = CGAffineTransform(translationX: finalPosition-currentPosition, y: 0)
        }
    }

    private var horizontalBarLeftAnchorConstraint: NSLayoutConstraint?

    private var horizontalBarView: UIView = {
        let horizontalBarView = UIView()
        horizontalBarView.backgroundColor = .globileSantanderRed
        horizontalBarView.translatesAutoresizingMaskIntoConstraints = false
        return horizontalBarView
    }()

    private func setupHorizontalBar() {

        tabBar.addSubview(horizontalBarView)

        horizontalBarLeftAnchorConstraint = horizontalBarView.leftAnchor.constraint(equalTo: tabBar.leftAnchor)
        horizontalBarLeftAnchorConstraint?.isActive = true

        horizontalBarView.topAnchor.constraint(equalTo: tabBar.topAnchor).isActive = true
        horizontalBarView.widthAnchor.constraint(equalTo: tabBar.widthAnchor, multiplier: 1/CGFloat(numberOfTabBar)).isActive = true
        horizontalBarView.heightAnchor.constraint(equalToConstant: 2).isActive = true

    }
}

private extension GlobileTabBar {

    func setupTabBar() {
        tabBar.barTintColor = .globileWhite
        tabBar.tintColor = .globileSantanderRed
        tabBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        tabBar.layer.shadowColor = UIColor.globileLightSky.cgColor

        let lineView = UIView(frame: CGRect(x: 0, y: 0, width: tabBar.frame.size.width, height: 1))
        lineView.backgroundColor = .globileLightSky
        tabBar.addSubview(lineView)

        if #available(iOS 10.0, *) {
            tabBar.unselectedItemTintColor = .globileMediumGrey
        } else {
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.globileSantanderRed], for: .selected)
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.globileMediumGrey], for: .normal)
        }
    }

}
