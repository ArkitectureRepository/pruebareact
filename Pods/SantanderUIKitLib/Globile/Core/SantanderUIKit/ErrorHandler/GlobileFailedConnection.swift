//
//  GlobileErrorHandler.swift
//  SantanderUIKitLib
//
//  Created by r.a.sanz.hinojosas on 12/02/2020.
//

import Foundation
import Lottie

@objc public protocol ConnectionFailedProtocol {
    func errorActionOnPress()
}

public enum ErrorShowType {
    case fullScreenError            //Error con TabBar inferior encima.
    case sessionExpiredError        //Pantalla completa con boton pegado abajo "Go to Login".
    case partialError               //Error posicionado entero abajo con imagen nube.
    case successMessage             //Pantalla completa success
    case unsuccessMessage           //Pantalla completa unsuccess
}

public class GlobileFailedConnection: UIView {
    
    @IBOutlet weak var gifImage: UIImageView!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet public weak var actionButton: GlobileEndingButton!
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var buttonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonBottomConstraint: NSLayoutConstraint!
    
    var delegate: ConnectionFailedProtocol?
    let lottieAnimationView = AnimationView()
    
    
    @IBAction func touchUpInside(_ sender: Any) {
        if let actionDelegate = delegate {
            actionDelegate.errorActionOnPress()
        }
        lottieAnimationView.stop()
        self.removeFromSuperview()
    }
    
    @IBAction func closeButtonTouchUpInside(_ sender: Any) {
        lottieAnimationView.stop()
        self.removeFromSuperview()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public class func instanceGeneralErrorFromNib() -> GlobileFailedConnection? {
        if let path = Bundle(for: GlobileFailedConnection.self).path(forResource: "SUKit", ofType: "bundle") {
            if let bundle = Bundle(path: path) {
                if let nib = bundle.loadNibNamed("GlobileFailedConnection", owner: nil, options: nil)?[0] as? GlobileFailedConnection {
                    return nib
                }
            }
        }
        return nil
    }
    //MARK: Lottie Animation
    func loadLottieAnimation(name: String) {
        let animation = GlobileAnimations.loadAnimation(name: name)
        lottieAnimationView.animation = animation
        self.addSubview(lottieAnimationView)
        self.bringSubviewToFront(lottieAnimationView)
        lottieAnimationView.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = NSLayoutConstraint(item: lottieAnimationView, attribute: .centerX, relatedBy: .equal, toItem:  gifImage, attribute: .centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: lottieAnimationView, attribute: .centerY, relatedBy: .equal, toItem: gifImage, attribute: .centerY, multiplier: 1, constant: 0)
         let bottomConstraint = NSLayoutConstraint(item: lottieAnimationView, attribute: .bottom, relatedBy: .equal, toItem: gifImage, attribute: .bottom, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: lottieAnimationView, attribute: .width, relatedBy: .equal, toItem:  nil, attribute: .width, multiplier: 1, constant: 170)
        let heightConstraint = NSLayoutConstraint(item: lottieAnimationView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 170)
        NSLayoutConstraint.activate([horizontalConstraint, widthConstraint, heightConstraint, bottomConstraint])
        GlobileAnimations.playLottieAnimation(view: lottieAnimationView)
    }
    
    public func setupErrorNibInView(type: ErrorShowType, in view: UIView? = nil, globileError: GlobileFailedConnection?, title: String? = "", subtitle: String? = "", buttonLabel: String, delegate: ConnectionFailedProtocol? = nil, actionButtonHidden: Bool = false, titleHidden: Bool = false, subtitleHidden: Bool = false) {
        self.delegate = delegate
        titleLabel?.text = title
        subTitleLabel?.text = subtitle
        actionButton.setTitle(buttonLabel, for: .normal)
        let path = Bundle(for: GlobileLoader.self).path(forResource: "SUKit", ofType: "bundle")!
        let bundle = Bundle(path: path) ?? Bundle.main
        let pathGif : String = bundle.path(forResource: "Loader", ofType: "gif")!
        let url = URL(fileURLWithPath: pathGif)
        do {
            let gifData = try Data(contentsOf: url)
            let gif: UIImage = try UIImage.init(gifData: gifData)
            
            if let errorView = globileError {
                switch type {
                case .fullScreenError:
                    gifImage.isHidden = true
                    loadLottieAnimation(name: "ConnectionFailure")
                    loadViewConstraints(error: errorView, type: type, in: view)
                    buttonTopConstraint.constant = 24.0
                    buttonWidthConstraint.constant = 164.0
                    closeButton.isHidden = true
                    
                case .sessionExpiredError:
                    gifImage.setImage(getImage(named: "token_expired"))
                    loadViewConstraints(error: errorView, type: type)
                    buttonTopConstraint.isActive = false
                    buttonBottomConstraint.constant = 24
                    buttonWidthConstraint.constant = 320.0
                    closeButton.isHidden = true
                    
                case .partialError:
                    gifImage.setImage(getImage(named: "partial"))
                    loadViewConstraints(error: errorView, type: type, in: view)
                    buttonTopConstraint.constant = 24.0
                    buttonWidthConstraint.constant = 164.0
                    closeButton.isHidden = true
                    actionButton.isHidden = actionButtonHidden
                    
                case .successMessage:
                    gifImage.setImage(getImage(named: "successMessageIcon"))
                    loadViewConstraints(error: errorView, type: type)
                    buttonTopConstraint.isActive = false
                    buttonBottomConstraint.constant = 24
                    buttonWidthConstraint.constant = 320
                    
                case .unsuccessMessage:
                    gifImage.setImage(getImage(named: "unsuccessMessageIcon"))
                    loadViewConstraints(error: errorView, type: type)
                    buttonTopConstraint.isActive = false
                    buttonBottomConstraint.constant = 24
                    buttonWidthConstraint.constant = 320
                }
            }
        } catch (_) {
            print("Error while loading gif animation")
        }
    }
    
    
    private func loadViewConstraints(error: GlobileFailedConnection?, type: ErrorShowType, in view: UIView? = nil) {
        self.translatesAutoresizingMaskIntoConstraints = false
        if let errorView = error {
            switch type {
            case .fullScreenError, .partialError:
                view?.addSubview(errorView)
                view?.bringSubviewToFront(errorView)
                let horizontalConstraint = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
                let verticalConstraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1, constant: 0)
                let widthConstraint = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 1, constant: 0)
                let heightConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: view, attribute: .height, multiplier: 1, constant: 0)
                NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
                
            case .successMessage, .unsuccessMessage, .sessionExpiredError:
                UIApplication.shared.keyWindow!.addSubview(errorView)
                UIApplication.shared.keyWindow!.bringSubviewToFront(errorView)
                let horizontalConstraint = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem:  UIApplication.shared.keyWindow!, attribute: .centerX, multiplier: 1, constant: 0)
                let verticalConstraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem:  UIApplication.shared.keyWindow!, attribute: .centerY, multiplier: 1, constant: 0)
                let widthConstraint = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem:  UIApplication.shared.keyWindow!, attribute: .width, multiplier: 1, constant: 0)
                let heightConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem:  UIApplication.shared.keyWindow!, attribute: .height, multiplier: 1, constant: 0)
                NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
                
            }
        }
    }
}
