//
//  GlobileFailedConnectionBanner.swift
//  SantanderUIKitLib
//
//  Created by r.a.sanz.hinojosas on 13/02/2020.
//

import Foundation

public class GlobileFailedConnectionBanner: UIView {
    @IBOutlet weak var titleLabel: UILabel?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public class func instanceBannerErrorFromNib() -> GlobileFailedConnectionBanner? {
        if let path = Bundle(for: GlobileFailedConnectionBanner.self).path(forResource: "SUKit", ofType: "bundle") {
            if let bundle = Bundle(path: path) {
                if let nib = bundle.loadNibNamed("GlobileFailedConnectionBanner", owner: nil, options: nil)?[0] as? GlobileFailedConnectionBanner {
                    return nib
                }
            }
        }
        return nil
    }
                
    public func setupBannerErrorNib(in view: UIView?, globileError: GlobileFailedConnectionBanner?, title: String, topConstrainConstant: CGFloat = 0.0) {
        backgroundColor = .globileGreyTint90
        alpha = 0.85
        titleLabel?.text = title
        if let error = globileError {
            view?.addSubview(error)
            view?.bringSubviewToFront(error)
        }        
        self.translatesAutoresizingMaskIntoConstraints = false
        let heightConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 44)
        let horizontalConstraint = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: topConstrainConstant)
        let widthConstraint = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
        self.alpha = 0.0
        self.fadeIn(1.0, delay: 0.0, completion: {_ in
            self.fadeOut(1.0, delay: 8.0) { (_) in
                self.removeFromSuperview()
                }
        })

    }
}
