//

import Foundation

public class GlobileToggleButton: UIButton {

    override public var isSelected: Bool {
        didSet {
            self.isSelected ? setSelected() : setUnselected()
        }
    }

    var config = GlobileToggleButtonListData(title: "", toggleButtonTag: "", module: nil)

    override init(frame: CGRect) {
        super.init(frame: frame)
        configButton()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configButton()
    }

    func configButton() {
        layer.borderWidth = 1
        layer.borderColor = UIColor.globileMediumSky.cgColor
        layer.cornerRadius = 16.5
        layer.backgroundColor = UIColor.globileWhiteButton.cgColor

        showShadow(show: true)

        titleLabel?.font = .regular(size: 14)
        setTitleColor(.globileDarkGreyAllModes, for: .normal)
        titleLabel?.lineBreakMode = .byTruncatingTail
    }

    func setSelected() {
        layer.borderWidth = 0
        layer.backgroundColor = UIColor.globileTurquoiseTint30.cgColor
        showShadow(show: false)
    }

    func setUnselected() {
        layer.borderWidth = 1
        layer.backgroundColor = UIColor.globileWhiteButton.cgColor
        showShadow(show: true)
    }

    func showShadow(show: Bool) {
        // Shadow
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = show ? 0.1 : 0.0
        layer.shadowOffset = show ? CGSize(width: 0, height: 2) : CGSize(width: 0, height: 0)
        layer.shadowRadius = show ? 3 : 0
    }

}
