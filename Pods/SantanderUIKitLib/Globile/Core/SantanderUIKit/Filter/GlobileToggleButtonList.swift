//

import Foundation

public protocol ToggleButtonListDelegate: class {
    func toggleButtonSelectedChanged(selectedButton: GlobileToggleButtonListData?)
}

public class GlobileToggleButtonList: UIStackView {

    var buttons: [GlobileToggleButton] = []
    var selectedButton: GlobileToggleButton?
    var rows = 0

    public weak var delegate: ToggleButtonListDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        configView()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configView()
    }

    private func configView() {
        axis = .vertical
        distribution = .equalSpacing
        alignment = .leading
        spacing = 8
    }

    /// Set toggleButtons List with their buttons to add views
    ///
    /// - Parameters:
    ///   - buttonsList: GlobileToggleButtonListData array
    public func setToggleButtons(_ buttonsList: [GlobileToggleButtonListData]) {
        buttons = []
        removeAllArrangedSubviews()
        rows = 0
        let arrayBythree = buttonsList.chunked(into: 3)

        for row in arrayBythree {
            let newRow = createNewRow()
            for button in row {
                let newButton = createNewButton(toggleButtonList: button, parentView: newRow)
                buttons.append(newButton)
            }
        }

    }

    func createNewRow() -> UIStackView {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.alignment = .firstBaseline
        stack.spacing = 8
        addArrangedSubview(stack)
        rows += 1
        return stack
    }

    func createNewButton(toggleButtonList: GlobileToggleButtonListData, parentView: UIStackView) -> GlobileToggleButton {
        let button = GlobileToggleButton()
        button.config = toggleButtonList
        button.setTitle(toggleButtonList.title, for: .normal)
        button.accessibilityLabel = toggleButtonList.title
        button.addTarget(self, action: #selector(tapped(_:)), for: .touchUpInside)
        parentView.addArrangedSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.32).isActive = true

        return button
    }

    @objc func tapped(_ sender: GlobileToggleButton) {
        setSelectedToggleButton(button: sender)
        delegate?.toggleButtonSelectedChanged(selectedButton: (selectedButton?.config))
    }

    func setSelectedToggleButton(button: GlobileToggleButton) {
        let oldState = button.isSelected
        buttons.forEach { $0.isSelected = false}
        button.isSelected = !oldState
        selectedButton = button.isSelected ? button : nil
    }

    func getButtons() -> [GlobileToggleButton] {
        var arrayToReturn: [GlobileToggleButton] = []
        buttons.forEach {
            if $0.config.module == nil {
                arrayToReturn.append($0)
            }
        }
        return arrayToReturn
    }

    /// Get selected toggle button from buttons array
    public func getSelectedToggleButton() -> GlobileToggleButtonListData? {
        return selectedButton?.config
    }

    /// Unselect all toggle buttons
    public func unselectAllButtons() {
        buttons.forEach { $0.isSelected = false}
        selectedButton = nil
    }

    /// Set toggle button selected using their tag
    ///
    /// - Parameters:
    ///   - buttonTag: button tag to be selected from the configured buttons list
    public func selectButtonByTag(buttonTag: String) {
        selectedButton = nil
        buttons.forEach {
            if $0.config.toggleButtonTag == buttonTag {
                selectedButton = $0
                $0.isSelected = true
            } else {
                $0.isSelected = false
            }
        }
    }

}

extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }

}
