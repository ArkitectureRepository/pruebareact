//

import Foundation

// MARK: - GlobileToggleButtonList
public struct GlobileToggleButtonListData {
    public var title: String
    public var toggleButtonTag: String
    public var module: (Any)?

    public init(title: String, toggleButtonTag: String, module: (Any)? = nil) {
        self.title = title
        self.toggleButtonTag = toggleButtonTag
        self.module = module
    }
}
