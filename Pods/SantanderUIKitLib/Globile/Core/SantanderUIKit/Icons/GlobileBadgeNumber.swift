//
//  SantanderBadge.swift
//  Icons
//
//  Created by l.arranz.martinez on 11/03/2019.
//  Copyright © 2019 l.arranz.martinez. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
public class GlobileBadgeNumber: UIView {

    internal var outerCircleLayer = CAShapeLayer()
    internal let outerCircleBorderWidth: CGFloat = 2.0

    public var badgeNumber: Int = 0 {
        didSet {
            label.text = "\(badgeNumber)"
        }
    }

    public let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .globileBostonRed
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = false
        label.font = .bold(size: 11)
        return label
    }()

    public override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func addSubviews() {
        addSubview(label)

        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: topAnchor, constant: outerCircleBorderWidth),
            label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: outerCircleBorderWidth),
            label.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -outerCircleBorderWidth),
            label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -outerCircleBorderWidth)
            ])
    }

    override public func draw(_ rect: CGRect) {
        outerCircleLayer.lineWidth = outerCircleBorderWidth
        layer.addSublayer(outerCircleLayer)

        addSubviews()
    }

    internal var setCircleRadius: CGFloat {
        let width = bounds.width
        let height = bounds.height

        let length = width > height ? height : width
        return (length - outerCircleBorderWidth) / 2
    }

    private var setCircleFrame: CGRect {
        let width = bounds.width
        let height = bounds.height

        let radius = setCircleRadius
        let x: CGFloat
        let y: CGFloat

        if width > height {
            y = outerCircleBorderWidth / 2
            x = (width / 2) - radius
        } else {
            x = outerCircleBorderWidth / 2
            y = (height / 2) - radius
        }

        let diameter = 2 * radius
        return CGRect(x: x, y: y, width: diameter, height: diameter)
    }

    private var circlePath: UIBezierPath {
        return UIBezierPath(roundedRect: setCircleFrame, cornerRadius: setCircleRadius)
    }

    private var fillCirclePath: UIBezierPath {
        let trueGap = outerCircleBorderWidth / 2
        return UIBezierPath(roundedRect: setCircleFrame.insetBy(dx: CGFloat(trueGap), dy: CGFloat(trueGap)), cornerRadius: setCircleRadius)
    }

    private func setCircleLayouts() {
        outerCircleLayer.frame = bounds
        outerCircleLayer.lineWidth = outerCircleBorderWidth
        outerCircleLayer.path = circlePath.cgPath
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        setCircleLayouts()
    }
}
