//
//  GlobileIcons.swift
//  Icons
//
//  Created by l.arranz.martinez on 20/02/2019.
//  Copyright © 2019 l.arranz.martinez. All rights reserved.
//

import UIKit
import CoreGraphics
public enum GlobileIconColor {
    case red
    case gray
    case white
}

public enum GlobileIconSize {
    case small
    case medium
    case large
}

public class GlobileIcons: UIImageView {

    private var iconSize: GlobileIconSize?
    private var iconColor: GlobileIconColor?
    private var hasNumber: Bool

    private var iconBadge: GlobileBadgeNumber = {
        let badge = GlobileBadgeNumber()
        return badge
    }()

    public override init(image: UIImage?) {
        hasNumber = false
        iconBadge = .init(frame: .zero)
        super.init(image: image)
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
    }

    public override init(frame: CGRect) {
        hasNumber = false
        iconBadge = .init(frame: .zero)
        super.init(frame: frame)
    }

    public required init?(coder aDecoder: NSCoder) {
        hasNumber = false
        iconBadge = .init(frame: .zero)
        super.init(coder: aDecoder)
    }

   public convenience init(icon: UIImage?, color: GlobileIconColor, size: GlobileIconSize) {
        let renderingImage = icon?.withRenderingMode(.alwaysTemplate)
        self.init(image: renderingImage)
        iconColor = color
        iconSize = size
    }

    private func setImageSize(_ icon: UIImage?, size: GlobileIconSize) {

        let templateImage = self.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        self.image = templateImage
        switch size {
        case .small:
            frame = CGRect(x: 0, y: 0, width: 24.0, height: 24.0)
        case .medium:
            frame = CGRect(x: 0, y: 0, width: 48.0, height: 48.0)
        case .large:
            frame = CGRect(x: 0, y: 0, width: 96.0, height: 96.0)
        }
   }

    /// Sets the image of the icon
    ///
    /// - Parameters:
    ///   - icon: UIImage. The image that wants to be set
    ///   - color: the color of the UIImage. It can be white, gray or red
    ///   - size: The size of the UIImage. It can be large, medium or small

    public func setImage (_ icon: UIImage?, color: GlobileIconColor, size: GlobileIconSize) {

        iconColor = color
        iconSize = size

        self.image = icon
        setImageSize(icon, size: iconSize!)

        setImageColor(icon, color: iconColor!)

    }
    
    public func setCachedImage(_ url: URL?, color: GlobileIconColor, size: GlobileIconSize, placeholder: UIImage?){
        let cachedImage = UIImageView()
        cachedImage.kf.setImage(
            with: url,
            placeholder: placeholder,
            options: [.transition(.fade(1)), .loadDiskFileSynchronously]
        )
        setImage(cachedImage.image, color: color, size: size)
    }
    
}

extension UIImageView {

    func setImageColor(_ icon: UIImage?, color: GlobileIconColor) {

        let templateImage = self.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        self.image = templateImage
        switch color {
        case .red:
            self.tintColor = .globileBostonRed
        case .gray:
            self.tintColor = .globileDarkGrey
        case .white:
            self.tintColor = .globileWhite
        }
    }
}

extension GlobileIcons {

    /// Adds the badge to the icon.
    ///
    /// - Parameter badgeNumber: indicates the number that appears in the badge. If it´s less than 0, the badge will appear empty

    public func addBadge(badgeNumber: Int) {

        var badgeText = String(badgeNumber)

        if badgeNumber > 0 {
            hasNumber = true
            if badgeNumber > 99 {
                badgeText = "99+"
            }
        }

        switch iconColor {
        case .white?:
            iconBadge.outerCircleLayer.fillColor = UIColor.globileWhite.cgColor
            iconBadge.outerCircleLayer.strokeColor = UIColor.globileBostonRed.cgColor
            backgroundColor = .globileBostonRed

            switch iconSize {
            case .large?:
                if hasNumber {
                    iconBadge.frame = CGRect(x: frame.size.width/1.35, y: frame.size.height/16, width: 25.0, height: 25.0)
                    iconBadge.label.text = badgeText
                } else {
                    iconBadge.frame = CGRect(x: frame.size.width/1.35, y: frame.size.height/16, width: 13.0, height: 13.0)
                }
            case .medium?:
                if hasNumber {
                    iconBadge.frame = CGRect(x: frame.size.width/1.35, y: frame.size.height/45, width: 25.0, height: 25.0)
                    iconBadge.label.text = badgeText
                } else {
                    iconBadge.frame = CGRect(x: frame.size.width/1.35, y: frame.size.height/16, width: 11.0, height: 11.0)
                }
            default:
                if hasNumber {
                    iconBadge.frame = CGRect(x: frame.size.width/1.70, y: frame.size.height/50, width: 19.0, height: 19.0)
                    iconBadge.label.text = badgeText
                    iconBadge.label.font = .bold(size: 9)
                } else {
                    iconBadge.frame = CGRect(x: frame.size.width/1.70, y: frame.size.height/16, width: 8.0, height: 8.0)
                }
            }
        default:
            iconBadge.outerCircleLayer.fillColor = UIColor.globileBostonRed.cgColor
            iconBadge.outerCircleLayer.strokeColor = UIColor.globileWhite.cgColor
            iconBadge.label.textColor = .globileWhite
            backgroundColor = .globileWhite

            switch iconSize {
            case .large?:
                if hasNumber {
                    iconBadge.frame = CGRect(x: frame.size.width/1.35, y: frame.size.height/16, width: 25.0, height: 25.0)
                    iconBadge.label.text = badgeText
                } else {
                    iconBadge.frame = CGRect(x: frame.size.width/1.35, y: frame.size.height/16, width: 13.0, height: 13.0)
                }
            case .medium?:
                if hasNumber {
                    iconBadge.frame = CGRect(x: frame.size.width/1.35, y: frame.size.height/45, width: 25.0, height: 25.0)
                    iconBadge.label.text = badgeText
                } else {
                    iconBadge.frame = CGRect(x: frame.size.width/1.35, y: frame.size.height/16, width: 11.0, height: 11.0)
                }
            default:
                if hasNumber {
                    iconBadge.frame = CGRect(x: frame.size.width/1.70, y: frame.size.height/50, width: 19.0, height: 19.0)
                    iconBadge.label.text = badgeText
                    iconBadge.label.font = .bold(size: 9)
                } else {
                    iconBadge.frame = CGRect(x: frame.size.width/1.70, y: frame.size.height/16, width: 8.0, height: 8.0)
                }
            }
        }
        addSubview(iconBadge)
    }
}
