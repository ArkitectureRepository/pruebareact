//
import Foundation
import RealmSwift

/**
 Realm Object for save in database.
 - Parameters:
     - value: content of object.
     - key: id of object.
 */
public class RealmObject: Object {
    @objc dynamic public var value = ""
    @objc dynamic public var key = ""

    override public static func primaryKey() -> String? {
        return "key"
    }
}

enum RealmError: Error {
    case runtimeError(String)
}
