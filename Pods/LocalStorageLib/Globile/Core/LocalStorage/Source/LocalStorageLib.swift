//
import Foundation
import RealmSwift
import GlobileUtilsLib

public protocol LocalStorageLibProtocol {
    func saveObject(object: Object) throws
    func readObject(key: String) -> String?
    func updateObject(key: String, value: String) throws
    func deleteObject(key: String) throws
}
@objc(LocalStorageLib)
public class LocalStorageLib: NSObject {

    public static var shared = LocalStorageLib()

    private var realm: Realm {
        get {
            do {
                let realm = try Realm()
                return realm
            } catch {
                print("Could not access database: ", error)
            }
            return self.realm
        }
    }

    /**
     Function for save objects in Realm
     
     - Parameters:
     - object: Object subclass object for save.
     */
    internal func saveObject(object: Object) throws {
        if let saveObject = object as? RealmObject {
            if readObject(key: saveObject.key) != nil {
                try remove(key: saveObject.key)
                try realm.write {
                    realm.add(object)
                }
            } else {
                try realm.write {
                    realm.add(object)
                }
            }
        }
    }
    public func save<T: Codable>(object: T, forKey key: String) {
        do {
            let objectToSave = RealmObject()
            objectToSave.key = key
            objectToSave.value = try utils_json_encode(object)
            try saveObject(object: objectToSave)

        } catch let error {
            print(error.localizedDescription)
        }
    }

    public func load<T: Codable>(forKey key: String, withType type: T.Type) throws -> T? {
        do {
            if let result = readObject(key: key) {
                return try utils_json_decode(result , type: type)
            }
        } catch let error {
            print(error.localizedDescription)
        }
        return nil
    }

    /**
     Function for read content of objects.
     
     - Parameters:
     - key: key of object to read in String format.
     - Returns: String optional with content of object.
     */
    internal func readObject(key: String) -> String? {
        if let objectContent = realm.objects(RealmObject.self).filter("key == '\(key)'").first?.value {
           return objectContent
        }
        return nil
    }

    /**
     Function for save objects in Realm
     
     - Parameters:
     - key: identity key object in String formaat.
     - content: content to update object in String format.
     */
    internal func updateObject(key: String, value: String) throws {
        if let entity = realm.object(ofType: RealmObject.self, forPrimaryKey: key) {
            do {
                try realm.write {
                    entity.value = value
                }
            } catch {
                throw RealmError.runtimeError("Could not update object")
            }
        }
    }

    /**
     Function for delete objects in Realm
     
     - Parameters:
     - key: identity key for delete object.
     */
    public func remove(key: String) throws {
        if let entity = realm.objects(RealmObject.self).filter("key == '\(key)'").first {
            do {
                try realm.write {
                    realm.delete(entity)
                }
            } catch {
                throw RealmError.runtimeError("Could not update object")
            }
        } else {
            throw RealmError.runtimeError("No key finded")
        }
    }

    public func clearLocalStorage() throws {
        do{
            try realm.write {
                realm.deleteAll()
            }
        } catch {
            throw RealmError.runtimeError("Exception while disposing all objects...")
        }
    }

    /// Function to perform hybrid actions
    ///
    /// - Parameter json: values to perform the hybrid action
    /// - Returns: output of the hybrid action
    @objc public class func hybridInterface(_ json: String) -> String {

        var result: String?

        do {
            let request = try utils_json_decode(json, type: LocalStorageRequest.self)
            let operation = request.operation
            switch operation {
            case .save:
                let realmObject = RealmObject(value: [request.plainData, request.alias])
                try shared.saveObject(object: realmObject)
                result = try utils_json_encode(LocalStorageResult(operation: operation))
            case .load:
                let value = shared.readObject(key: request.alias)
                result = try utils_json_encode(LocalStorageResult(operation: operation, plainData: value))
            case .update:
                try shared.updateObject(key: request.alias, value: request.plainData)
                result = try utils_json_encode(LocalStorageResult(operation: operation))
            case .remove:
                try shared.remove(key: request.alias)
                result = try utils_json_encode(LocalStorageResult(operation: operation))
            }
        } catch {
            result = try? utils_json_encode(LocalStorageResult(false, operation: nil))
        }

        guard let resultValue = result else { return (try? utils_json_encode(LocalStorageResult(false, operation: nil))) ?? "" }
        return resultValue
    }

}
