//
import Foundation

public enum LocalStorageOperation: String, Codable {
    case save, load, update, remove
}

public struct LocalStorageRequest: Codable {

    var operation: LocalStorageOperation
    var alias: String
    var plainData: String
}

public struct LocalStorageResult: Codable {

    var success: Bool
    var operation: LocalStorageOperation?
    var plainData: String?

    init(_ success: Bool = true, operation: LocalStorageOperation?, plainData: String? = nil) {
        self.success = success
        self.operation = operation
        self.plainData = plainData
    }
}
