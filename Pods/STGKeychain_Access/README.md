# KeychainAccess

# Table of Contents ###
- [Summary](#summary)
- [Control of Versions ](#control-of-versions)
- [Requirements ](#requirements)
- [Quick Start ](#quick-start)
- [Description ](#description)

----------------------------
# Control of Versions

|     Component Version   |         Date            |     Status        |           Description                 |
| --------------------------- |  -------------------| --------------   |  ------------------------------- |
|             *v1.0*                 |      28/03/2019     |     Released   |       STGKeychain_Access     |
|             *v1.0*                 |      28/03/2019     |     Released   |                 SCAL iOS              |


----------------------------
# Requirements

iOS 9.X,10.X,11.X, 12.X

Xcode 9.0+

iPhone 5/5c, iPhone SE, iPhone 6 / 6 Plus, iPhone 6S / 6S Plus, iPhone 7 / 7 Plus, iPhone 8 / 8 Plus, iPhone X, iPhone XS/Max, iPhone XR

Swift 4.2


----------------------------
# Quick Start

Module responsible of handling all functionalities related to storing values into Keychain (Secure Enclave) using different methods of authentication and of security.

----------------------------


## Installation

### CocoaPods

For cocoa project we have dependency on **CocoaPods**. Cocoapods can be installed from self service software available for Santander otherwise following command can be used for installation.

$ gem install cocoapods

> CocoaPods 1.5+ is required.

For integration of **STGKeychain_Access** in Xcode project, add following lines to your Podfile.

source 'ssh://git@gitlab.alm.gsnetcloud.corp:2220/Globile/Globile-Spec.git'
use_frameworks!

target '<Your Target Name>' do
pod 'STGKeychain_Access'
end

Then run following command.

$ pod install

### Framework based installation

This method will provide you frameworks for both keychain access module which you can add into your project. Following the steps to follow for this method.

* Clone **STGKeychain_Access** from git repository.


$ git clone ssh://git@gitlab.alm.gsnetcloud.corp:2220/mobi-sec/STGKetchain_Access_iOS.git

* Select STGKeychainAccessAggregate target and build cmd+b. It will open finder with framework created as **STGKeychain_Access.framework**

* Now add **STGKeychain_Access.framework** to main project and we are done!

## Usage

* To use keychain functionality into main project, import both module into class where you want to use like:

$ import STGKeychain_Access

* Then declare constants that store the values for keychain access  

$ let keychainAccess = STGKeychainAccessDataProvider()

## Description

### Methods for storing credentials:

* For keychainAccess STGKeychainAccessDataProvider class will be used for storing, retrieving and deleting credentials inside Keychain. Some methods return a Bool representing the success or failure of the operation, others return the data type associated with the query.

**storeCredentialAuthenticated(key: String, value: String, biometric: Bool) -> Bool**

```storeCredentialAuthenticated(key: String, value: String, biometric: Bool) -> Bool```

This method is used to store store credentials or any string using Keychain to store this data behind iOS Secure Enclave through biometric authentication. Receives as parameter credential to be stored as a String and the key associated with this credential to access it later. It returns a boolean value that represents if the operation was successful.

Example:

$   let isFileStored = storeCredentialAuthenticated(key: "key", value: "value", biometric: true)

**getCredentialAuthenticated(key: String) -> String?**

```getCredentialAuthenticated(key: String) -> String?```

This method obtains a previously stored credential using biometric authentication. It receives the key as a String and returns the value associated with this credential if it exists. It will prompt the user to authenticate using either touchId or faceId. If authentication fails, the value is not returned.

$   let credential = getCredentialAuthenticated(key: "key")


**removeCredentialAuthenticated(key: String) -> Bool**

```removeCredentialAuthenticated(key: String) -> Bool```

This method removes a previously stored credential using biometric authentication. It receives the key as a String and returns a boolean representing if the operation was successful.

$   let isFileRemoved = removeCredentialAuthenticated(key: "key")


## License

Copyright © 2019 Santander Technology UK. All rights reserved.

