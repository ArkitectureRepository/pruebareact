import Foundation
import LocalAuthentication

public protocol STGKeychainAccessProtocol: class {
    func saveKeychainValue(key: String, value: String, biometric: Bool) -> Bool
    func saveKeychainValue(key: String, data: Data, biometric: Bool) -> Bool
    func retrieveKeychainValue(key: String, promptTitle: String) -> String?
    func retrieveKeychainValue(key: String, promptTitle: String) -> Data?
    func removeKeychainValue(key: String) -> Bool
    func updateKeychainValue(key: String, value: String, biometric: Bool) -> Bool
    func updateKeychainValue(key: String, data: Data, biometric: Bool) -> Bool
}

public class STGKeychainAccess: STGKeychainAccessProtocol {
    let keychain: STGKeychainProtocol

    public init(keychain: STGKeychainProtocol = STGKeychain()) {
        self.keychain = keychain
    }
    
    public func saveKeychainValue(key: String, value: String, biometric: Bool = false) -> Bool {
        guard let stringData = value.data(using: .utf8) else { return false }
        return keychain.save(key: key, data: stringData, overwrite: false, biometric: biometric)
    }
    
    public func saveKeychainValue(key: String, data: Data, biometric: Bool = false) -> Bool {
        return keychain.save(key: key, data: data, overwrite: false, biometric: biometric)
    }
    
    public func retrieveKeychainValue(key: String, promptTitle: String) -> String? {
        guard let data = self.keychain.retrieve(key: key, promptTitle: promptTitle) else { return nil }
        guard let stringRetrieved = String(data: data, encoding: .ascii) else { return nil }
        return stringRetrieved
    }
    
    public func retrieveKeychainValue(key: String, promptTitle: String) -> Data? {
        guard let data = self.keychain.retrieve(key: key, promptTitle: promptTitle) else { return nil }
        return data
    }

    public func updateKeychainValue(key: String, value: String, biometric: Bool) -> Bool {
        guard let stringData = value.data(using: .utf8) else { return false }
        return keychain.save(key: key, data: stringData, overwrite: true, biometric: biometric)
    }

    public func updateKeychainValue(key: String, data: Data, biometric: Bool) -> Bool {
        return keychain.save(key: key, data: data, overwrite: true, biometric: biometric)
    }

    public func removeKeychainValue(key: String) -> Bool {
       return keychain.remove(key: key)
    }
}
