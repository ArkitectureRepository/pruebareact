import Foundation
import LocalAuthentication

public protocol STGKeychainProtocol: class {
    func save(key: String, data: Data, overwrite: Bool, biometric: Bool) -> Bool
    func retrieve(key: String, promptTitle: String) -> Data?
    func remove(key: String) -> Bool
}

public class STGKeychain: STGKeychainProtocol {
    public init() {}

    public func save(key: String, data: Data, overwrite: Bool = false, biometric: Bool = false) -> Bool {
        var query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword as String,
            kSecAttrAccount as String: key
        ]

        if biometric {
            let context: LAContext = LAContext()
            context.touchIDAuthenticationAllowableReuseDuration = 10
            query[String(kSecAttrAccessControl)] = SecAccessControlCreateWithFlags(nil, kSecAttrAccessibleWhenPasscodeSetThisDeviceOnly, .userPresence, nil) as Any
            query[String(kSecUseAuthenticationContext)] = context
        }

        var status = SecItemCopyMatching(query as CFDictionary, nil)

        switch status {
        case errSecSuccess:
            guard overwrite else { return false }
            var attributesToUpdate: [String: Any] = [:]
            attributesToUpdate[String(kSecValueData)] = data

            status = SecItemUpdate(query as CFDictionary,
                                   attributesToUpdate as CFDictionary)
            if status != errSecSuccess {
                return false
            }
        case errSecItemNotFound:
            query[String(kSecValueData)] = data

            status = SecItemAdd(query as CFDictionary, nil)
            if status != errSecSuccess {
                return false
            }
        default:
            return false
        }

        return true
    }
    
    public func retrieve(key: String, promptTitle: String) -> Data? {
        let query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecMatchLimit as String: kSecMatchLimitOne,
            kSecReturnData as String: kCFBooleanTrue ?? true,
            kSecAttrAccount as String: key,
            kSecUseOperationPrompt as String: promptTitle
        ]
    
        var dataTypeRef: AnyObject?
        
        let status: OSStatus = SecItemCopyMatching(query as CFDictionary, &dataTypeRef)
        var contentsOfKeychain: Data?
        
        if status == errSecSuccess, let retrievedData = dataTypeRef as? Data {
            contentsOfKeychain = retrievedData
            
        } else {
            contentsOfKeychain = nil
        }
        
        return contentsOfKeychain
    }
    
    public func remove(key: String) -> Bool {
        let query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: key
        ]
        
        let lastResultCode = SecItemDelete(query as CFDictionary)
        if lastResultCode == noErr {
            return true
        }
        
        return false
    }

}
