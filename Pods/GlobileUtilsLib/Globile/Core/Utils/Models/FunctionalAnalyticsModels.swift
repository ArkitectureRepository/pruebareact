public enum Environment: String, Codable {
    case test = "test",
    ante = "ante",
    prod = "prod"
}

public struct FAConfig: Codable {
    public var defaultValues: [String: String]?
    public var environment: Environment?
    public var tealiumInstanceIdentifier: String?
    public var account: String?
    public var profile: String?
}

public enum FAErrorType: Error {
    case emptyData
    case invalidType
    case invalidConfig
}

public struct FAError: Error {
    public var message: String
    public var cause: FAErrorType

    public init(message: String, cause: FAErrorType) {
        self.message = message
        self.cause = cause
    }
}

public var defaultTagging: [String: Any] = [
    "account":"santander",
    "profile":"globile",
    "environment":"prod",
    "Language":"",
    "Country":"",
    "AppType":"Internal",
    "AppName":"",
    "AppVersion":"",
    "BuildType":"dev",
    "Platform":"ios",
    "Uuid":""
]
