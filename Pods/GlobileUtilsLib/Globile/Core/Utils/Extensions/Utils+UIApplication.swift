//
import Foundation
import UIKit

extension UIApplication {

    public class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)
        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)
        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }

    public class func urlSchemeLauncher(url: String) {
        if let url = URL(string: url) {
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.canOpenURL(url)
                }
            }
        }
    }

    public class func urlSchemeLauncher(urls: [String]) {
        urls.compactMap {URL(string: $0)}
            .filter { UIApplication.shared.canOpenURL($0)}
            .forEach {
                urlSchemeLauncher(url: $0.absoluteString)
        }
    }
}
