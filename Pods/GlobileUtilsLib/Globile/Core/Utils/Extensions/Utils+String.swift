//
import Foundation

public extension String {
    func dictionary() throws -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        }
        return nil
    }

    func localizedString(bundle: Bundle, tableName:String) -> String {
        let mainBundle = Bundle.main

        if  NSLocalizedString(self, tableName: tableName, bundle: mainBundle,comment: "") != self {
            if var path = mainBundle.path(forResource: getLanguage(), ofType: "lproj") {
                if path .isEmpty {
                    if let pathEn = mainBundle.path(forResource: "en", ofType: "lproj") {
                        path = pathEn
                    }
                }
                return NSLocalizedString(self, tableName: tableName, bundle: Bundle(path: path) ?? mainBundle,comment: "")
            }

        }

        if var path = bundle.path(forResource: getLanguage(), ofType: "lproj") {
            if path .isEmpty {
                if let pathEn = Bundle.main.path(forResource: "en", ofType: "lproj") {
                    path = pathEn
                }
            }
            return NSLocalizedString(self, tableName: tableName, bundle: Bundle(path: path) ?? bundle,comment: "")
        }
        return self
    }

    func translateLangName() -> String {
        if self.elementsEqual("es") {return "spanish"}
        if self.elementsEqual("es-MX") {return "spanish"}
        if self.elementsEqual("en") {return "english"}
        if self.elementsEqual("en-GB") {return "english"}
        if self.elementsEqual("de") {return "german"}
        if self.elementsEqual("pl-PL") {return "polish"}
        if self.elementsEqual("it") {return "italian"}
        if self.elementsEqual("pt-PT") {return "portuguese"}
        if self.elementsEqual("pt-BR") {return "portuguese"}
        return self
    }
}

public func NSLocalizedString(_ key: String) -> String {
    return NSLocalizedString(key, comment: "")
}
