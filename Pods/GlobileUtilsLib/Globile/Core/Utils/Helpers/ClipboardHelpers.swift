//
import Foundation

public func copyToPasteboard(text: String) {
    UIPasteboard.general.string = text
}
