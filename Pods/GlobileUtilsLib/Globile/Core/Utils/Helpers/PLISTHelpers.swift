//
import Foundation

public func plistDecoder<T:Codable>(forResource: String, type: T.Type) -> T? {
     if let path = Bundle.main.path(forResource: forResource, ofType: "plist") {
         do {
             if let xml = FileManager.default.contents(atPath: path) {
                let decoded = try PropertyListDecoder().decode(type, from: xml)
                return decoded
             }
         } catch let error {
             print(error.localizedDescription)
         }
     }
     return nil
 }
