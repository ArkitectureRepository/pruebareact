//
import Foundation

public func info_uuid() -> String? {
    if let uuid = UIDevice.current.identifierForVendor?.description {
               return uuid
           }
    return nil
}

public func info_environment() -> String {
    #if DEBUG
    return "dev"
    #else
    return "prod"
    #endif
}

public func info_appname() -> String {
    var appName = ""
    if let name = Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String {
        appName = name
    }
    return appName
}

public func info_appFullVersion() -> String {
    var appVersion = ""
    if let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String,
       let build = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String  {
        appVersion =  "\(version).\(build)"
    }
    return appVersion
}
