//
//  CoreApp+Language.swift
//  GlobileAppEngine
//
//  Created by marco.iniguez.ollero on 02/03/2020.
//

import Foundation
public let languageConstant = "Language"
public enum Language: String {
    case spanish = "es"
    case mexican = "es-MX"
    case english = "en-GB"
    case deutsch = "de"
    case poland = "pl-PL"
    case englishUSA = "en"
    case portuguese = "pt-PT"
    case brazilian = "pt-BR"
    case italian = "it"
}

public func setLanguage(language: Language) {
    UserDefaults.standard.set(language.rawValue, forKey: languageConstant)
    UserDefaults.standard.synchronize()
}

public func getLanguage() -> String {
    if let lang =  UserDefaults.standard.string(forKey: languageConstant) {
        return lang
    }
    return Language.spanish.rawValue
}
