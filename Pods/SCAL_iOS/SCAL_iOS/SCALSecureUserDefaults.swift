//
//  CALUserDefaults.swift
//  SCAL_iOS
//
//  Created by Hassan, Waseem (Isban) on 15/02/2019.
//  Copyright © 2019 Hassan, Waseem (Isban). All rights reserved.
//
import Foundation

public protocol SCALSecureUserDefaultsInterface {
    
    func set(_ value: Any, forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws
    
    func set(_ value: Int, forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws
    
    func set(_ value: Float, forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws
    
    func set(_ value: Double, forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws
    
    func set(_ value: Bool, forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws
    
    func set(_ url: URL, forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws

    func object(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> Any?
    
    func string(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> String?
    
    func array(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> [Any]?
    
    func dictionary(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> [String: Any]?
    
    func data(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> Data?
    
    func stringArray(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> [String]?
    
    func integer(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> Int?
    
    func float(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> Float?
    
    func double(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> Double?
    
    func bool(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> Bool?
    
    func url(forKey defaultName: String, encryptionKey: Data, initializationVector: Data) throws -> URL?
    
    func removeObject(forKey defaultName: String, encryptionKey: Data, initializationVector: Data)
    
    func generateEncryptionKey() -> Data?
    
    func generateEncryptionIV() -> Data?
    
    func getEncryptionKey(defaultName: String) -> Data?
    
    func getEncryptionIV(defaultName: String) -> Data?
}

public class SCALUserDefaults {

    private let secureUserDefaults: SCALSecureUserDefaultsInterface
    
    public init(secureUserDefaults: SCALSecureUserDefaultsInterface) {
        self.secureUserDefaults = secureUserDefaults
        
    }
    
    public func set(_ value: Any, forKey defaultName: String) throws {
        guard let ivAndKey = ivAndKey(defaultName: defaultName) else { return }
        try secureUserDefaults.set(value, forKey: defaultName, encryptionKey: ivAndKey.key, initializationVector: ivAndKey.initializationVector)
    }
    
    public func set(_ value: Int, forKey defaultName: String) throws {
        guard let ivAndKey = ivAndKey(defaultName: defaultName) else { return }
        try secureUserDefaults.set(value, forKey: defaultName, encryptionKey: ivAndKey.key, initializationVector: ivAndKey.initializationVector)
    }
    
    public func set(_ value: Float, forKey defaultName: String) throws {
        guard let ivAndKey = ivAndKey(defaultName: defaultName) else { return }
        try secureUserDefaults.set(value, forKey: defaultName, encryptionKey: ivAndKey.key, initializationVector: ivAndKey.initializationVector)
    }
    
    public func set(_ value: Double, forKey defaultName: String) throws {
        guard let ivAndKey = ivAndKey(defaultName: defaultName) else { return }
        try secureUserDefaults.set(value, forKey: defaultName, encryptionKey: ivAndKey.key, initializationVector: ivAndKey.initializationVector)
    }
    
    public func set(_ value: Bool, forKey defaultName: String) throws {
        guard let ivAndKey = ivAndKey(defaultName: defaultName) else { return }
        try secureUserDefaults.set(value, forKey: defaultName, encryptionKey: ivAndKey.key, initializationVector: ivAndKey.initializationVector)
    }
    
    public func set(_ url: URL, forKey defaultName: String) throws {
        guard let ivAndKey = ivAndKey(defaultName: defaultName) else { return }
        try secureUserDefaults.set(url, forKey: defaultName, encryptionKey: ivAndKey.key, initializationVector: ivAndKey.initializationVector)
    }

    public func object(forKey defaultName: String) throws -> Any? {
        guard let ivAndKey = ivAndKey(defaultName: defaultName) else { return nil }
        return try secureUserDefaults.object(forKey: defaultName, encryptionKey: ivAndKey.key, initializationVector: ivAndKey.initializationVector)
    }

    public func string(forKey defaultName: String) throws -> String? {
        guard let ivAndKey = ivAndKey(defaultName: defaultName) else { return nil }
        return try secureUserDefaults.string(forKey: defaultName, encryptionKey: ivAndKey.key, initializationVector: ivAndKey.initializationVector)
    }
    
    public func array(forKey defaultName: String) throws -> [Any]? {
       guard let ivAndKey = ivAndKey(defaultName: defaultName) else { return nil }
       return try secureUserDefaults.array(forKey: defaultName, encryptionKey: ivAndKey.key, initializationVector: ivAndKey.initializationVector)
    }
    
    public func dictionary(forKey defaultName: String) throws -> [String: Any]? {
        guard let ivAndKey = ivAndKey(defaultName: defaultName) else { return nil }
        return try secureUserDefaults.dictionary(forKey: defaultName, encryptionKey: ivAndKey.key, initializationVector: ivAndKey.initializationVector)
    }
    
    public func data(forKey defaultName: String) throws -> Data? {
        guard let ivAndKey = ivAndKey(defaultName: defaultName) else { return nil }
        return try secureUserDefaults.data(forKey: defaultName, encryptionKey: ivAndKey.key, initializationVector: ivAndKey.initializationVector)
    }
    
    public func stringArray(forKey defaultName: String) throws -> [String]? {
        guard let ivAndKey = ivAndKey(defaultName: defaultName) else { return nil }
        return try secureUserDefaults.stringArray(forKey: defaultName, encryptionKey: ivAndKey.key, initializationVector: ivAndKey.initializationVector)
    }
    
    public func integer(forKey defaultName: String) throws -> Int? {
        guard let ivAndKey = ivAndKey(defaultName: defaultName) else { return nil }
        return try secureUserDefaults.integer(forKey: defaultName, encryptionKey: ivAndKey.key, initializationVector: ivAndKey.initializationVector)
    }
    
    public func float(forKey defaultName: String) throws -> Float? {
        guard let ivAndKey = ivAndKey(defaultName: defaultName) else { return nil }
        return try secureUserDefaults.float(forKey: defaultName, encryptionKey: ivAndKey.key, initializationVector: ivAndKey.initializationVector)
    }
    
    public func double(forKey defaultName: String) throws -> Double? {
        guard let ivAndKey = ivAndKey(defaultName: defaultName) else { return nil }
        return try secureUserDefaults.double(forKey: defaultName, encryptionKey: ivAndKey.key, initializationVector: ivAndKey.initializationVector)
    }
    
    public func bool(forKey defaultName: String) throws -> Bool? {
        guard let ivAndKey = ivAndKey(defaultName: defaultName) else { return nil }
        return try secureUserDefaults.bool(forKey: defaultName, encryptionKey: ivAndKey.key, initializationVector: ivAndKey.initializationVector)
    }
    
    public func url(forKey defaultName: String) throws -> URL? {
        guard let ivAndKey = ivAndKey(defaultName: defaultName) else { return nil }
        return try secureUserDefaults.url(forKey: defaultName, encryptionKey: ivAndKey.key, initializationVector: ivAndKey.initializationVector)
    }

    public func removeObject(forKey defaultName: String) {
        guard let ivAndKey = ivAndKey(defaultName: defaultName) else { return }
        secureUserDefaults.removeObject(forKey: defaultName, encryptionKey: ivAndKey.key, initializationVector: ivAndKey.initializationVector)
    }
    
    func generateEncryptionKey() -> Data? {
        let key = secureUserDefaults.generateEncryptionKey()
        return key?.count == 32 ? key : nil
    }
    
    func generateEncryptionIV() -> Data? {
        let initializationVector = secureUserDefaults.generateEncryptionIV()
        return initializationVector?.count == 32 ? initializationVector : nil
    }
    
    func getEncryptionKey(defaultName: String) -> Data? {
        let key = secureUserDefaults.getEncryptionKey(defaultName: defaultName)
        return key?.count == 32 ? key : nil
    }
    
    func getEncryptionIV(defaultName: String) -> Data? {
        let initializationVector = secureUserDefaults.getEncryptionIV(defaultName: defaultName)
        return initializationVector?.count == 32 ? initializationVector : nil
    }
    
    func ivAndKey(defaultName: String) -> KeyAndIV? {
        if let initializationVector = getEncryptionIV(defaultName: defaultName), let key = getEncryptionKey(defaultName: defaultName) {
            return KeyAndIV(initializationVector: initializationVector, key: key)
        }
        if let initializationVector = generateEncryptionIV(), let key = generateEncryptionKey() {
            return KeyAndIV(initializationVector: initializationVector, key: key)
        }
        return nil
    }
}
