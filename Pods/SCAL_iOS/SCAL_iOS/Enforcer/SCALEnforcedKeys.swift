//
//  SCALEnforcedKeys.swift
//  SCAL_iOS
//
//  Created by Gilles Guillemin on 26/09/2019.
//  Copyright © 2019 Hassan, Waseem (Isban). All rights reserved.
//

import Foundation

public enum CDRiskLevel: String {
    case low
    case medium
    case high
}

public enum CDReportKey: String {
    case appRunningMode = "app_running_mode"
    case biometricType = "supported_biometric_type"
    case bluetoothState = "bluetooth_state"
    case carriers
    case defaultValueKey = "value"
    case deviceType = "device_type"
    case error
    case ipAddress = "ip_address"
    case ipGeoData = "ip_geo_data"
    case location
    case networkType = "type"
    case networkValueDetected = "value_detected"
    case osVersion = "version"
    case passcodeImplemented = "is_passcode_implemented"
    case riskFactorCalculated = "risk_calculated"
    case vpnConnectionStatus = "vpn_connection_status"
}

public enum CDGeoLocationKey: String {
    case location
    case coordinates = "location_coordinates"
    case countryCode = "country_code"
    case address
}

public enum CDIPGeoLocationKey: String {
    case alias = "as"
    case businessName
    case businessWebsite
    case city
    case continent
    case country
    case countryCode
    case ipName
    case ipType
    case isp
    case latitude = "lat"
    case longitude = "lon"
    case organisation = "org"
    case query
    case region
    case regionName
    case status
    case timezone
    case zip
}

public enum CDAppRunningModeKey: String {
    case debug = "Debug"
    case release = "Release"
}

public enum CDDeviceTypeKey: String {
    case device
    case simulator
}

public enum ContextConnectionStatus: String {
    case connected
    case disconnected
}

public enum CDNetworkType: String {
    case none
    case wwan2g = "2g"
    case wwan3g = "3g"
    case wwan4g = "4g"
    case wifi
}

public enum ContextCarriersReportKey: String {
    case carrierName
    case isoCountryCode
    case mobileCountryCode
    case mobileNetworkCode
}

public enum CDAuthSetting: String {
    case touchId = "Touch ID"
    case faceId = "Face ID"
    case none = "none"
}
