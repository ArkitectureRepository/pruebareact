//
//  CALSecureStorage.swift
//  SCAL_iOS
//
//  Created by Eduardo Nieto Delgado on 14/02/2019.
//  Copyright © 2019 Hassan, Waseem (Isban). All rights reserved.
//

import Foundation

public enum STGFileMode {
    case overwriteMode
    case appendMode
    case defaultMode
}

// Context detection interface
public protocol SCALSecureFilesInterface {
    
    func writeToSecureFile(alias: String, data: Data?, fileMode: STGFileMode, key: Data, initializationVector: Data) -> Bool
    
    func writeToSecureFile(alias: String, data: Data?, key: Data, initializationVector: Data) -> Bool
    
    func removeSecureFile(alias: String) -> Bool
    
    func clearSecureFile(alias: String, key: Data, initializationVector: Data) -> Bool
    
    func readFromSecureFile(alias: String, key: Data, initializationVector: Data) -> Data?
    
    func generateEncryptionKey() -> Data?
    
    func generateEncryptionIV() -> Data?
    
    func getEncryptionKey(alias: String) -> Data?
    
    func getEncryptionIV(alias: String) -> Data?
}

final public class SCALSecureFiles {
    
    private let secureFiles: SCALSecureFilesInterface

    public init(secureFiles: SCALSecureFilesInterface) {
        self.secureFiles = secureFiles
    }
    
    public func writeToSecureFile(alias: String, data: Data?, fileMode: STGFileMode = .defaultMode) -> Bool {
        guard let keyAndIV = ivAndKey(alias: alias) else { return false }
        return secureFiles.writeToSecureFile(alias: alias, data: data, fileMode: fileMode, key: keyAndIV.key, initializationVector: keyAndIV.initializationVector)
    }
    
    public func writeToSecureFile(alias: String, data: Data?) -> Bool {
        guard let keyAndIV = ivAndKey(alias: alias) else { return false }
        return secureFiles.writeToSecureFile(alias: alias, data: data, fileMode: .defaultMode, key: keyAndIV.key, initializationVector: keyAndIV.initializationVector)
    }
    
    public func removeSecureFile(alias: String) -> Bool {
        return secureFiles.removeSecureFile(alias: alias)
    }
    
    public func clearSecureFile(alias: String) -> Bool {
        guard let keyAndIV = ivAndKey(alias: alias) else { return false }
        return secureFiles.clearSecureFile(alias: alias, key: keyAndIV.key, initializationVector: keyAndIV.initializationVector)
    }
    
    public func readFromSecureFile(alias: String) -> Data? {
        guard let keyAndIV = ivAndKey(alias: alias) else { return nil }
        return secureFiles.readFromSecureFile(alias: alias, key: keyAndIV.key, initializationVector: keyAndIV.initializationVector)
    }
    
    func generateEncryptionKey() -> Data? {
        let key = secureFiles.generateEncryptionKey()
        return key?.count == 32 ? key : nil
    }
    
    func generateEncryptionIV() -> Data? {
        let initializationVector = secureFiles.generateEncryptionIV()
        return initializationVector?.count == 16 ? initializationVector : nil
    }
    
    func getEncryptionKey(alias: String) -> Data? {
        let key = secureFiles.getEncryptionKey(alias: alias)
        return key?.count == 32 ? key : nil
    }
    
    func getEncryptionIV(alias: String) -> Data? {
        let initializationVector = secureFiles.getEncryptionIV(alias: alias)
        return initializationVector?.count == 16 ? initializationVector : nil
    }
    
    func ivAndKey(alias: String) -> KeyAndIV? {
        if let initializationVector = getEncryptionIV(alias: alias), let key = getEncryptionKey(alias: alias) {
            return KeyAndIV(initializationVector: initializationVector, key: key)
        }
        if let initializationVector = generateEncryptionIV(), let key = generateEncryptionKey() {
            return KeyAndIV(initializationVector: initializationVector, key: key)
        }
        return nil
    }
}
