//
//  SCALAuthenticators.swift
//  SCAL_iOS
//
//  Created by Eduardo Nieto Delgado on 31/05/2019.
//  Copyright © 2019 Hassan, Waseem (Isban). All rights reserved.
//

import Foundation

public typealias AuthenticatorsCompletionHandler = ((_ values: AuthenticatorsResponse?, _ cancelled: Bool) -> Void)
public enum AuthenticatorsViewType {
    case passPosition
    case securityQuestion
    case fingerFaceId
    case faceDetection
    case login
}

public struct AuthenticatorsResponse {
    let value: Any?
    let type: AuthenticatorsViewType
    public init(value: Any?, type: AuthenticatorsViewType) {
        self.value = value
        self.type = type
    }

    public func toPassPositionResponse() -> [Int: String]? {
        return value as? [Int: String]
    }

    public func toPassPositionResponseValidated() -> Bool? {
        return value as? Bool ?? false
    }

    public func toSecurityQuestionResponse() -> String? {
        return value as? String
    }

    public func toSecurityQuestionResponseValidated() -> Bool? {
        return value as? Bool ?? false
    }

    public func toLoginResponse() -> [String: String]? {
        return value as? [String: String]
    }

    public func toFaceDetection() -> Bool {
        return value as? Bool ?? false
    }

    public func toFingerFaceId() -> Bool {
        return value as? Bool ?? false
    }
}

public protocol SCALAuthenticatorsInterface {
    /**
     Call this function to show a popup to ask the users for the positions of a password with validation. The callback of this method is defined as a delegate method called ""
     - Parameters:
     - title: the title of the popUp as String
     - text: the text or text of the popUp as String
     - completePass: the complete expected password to validate as String
     - positionsToAsk: an array of integers representing the positions to ask of the password
     - completionHandler: returns a boolean representing the validation of the password as a parameter of the callback

     ### Usage Example: ###
     ````
     raisePassPositionsPopUp(title: "title", text: "text", completePass: "123456", positionsToAsk: [2,3]) {
            (validated) in
     }

     ````
     */

    func raisePassPositionsPopUp(title: String, text: String, completePass: String, positionsToAsk: [Int], completionHandler: AuthenticatorsCompletionHandler?)

    /**
     Call this function to show a popup to ask the users for the positions of a password without validation
     - Parameters:
     - title: the title of the popUp as String
     - text: the text or text of the popUp as String
     - positionsToAsk: an array of integers representing the positions to ask of the password
     - completionHandler: returns the input of the user as a String in the parameter of the callback

     ### Usage Example: ###
     ````
     raisePassPositionsPopUp(title: "title", text: "text", completePass: "123456", positionsToAsk: [2,3]) {
            (inputPass) in

     }

     ````
     */

    func raisePassPositionsPopUpWithoutValidation(title: String, text: String, passLength: Int, positionsToAsk: [Int], completionHandler: AuthenticatorsCompletionHandler?)

    /**
     Call this function to show a popup to ask the users for the positions of a password with validation
     - Parameters:
     - title: the title of the popUp as String
     - question: the complete expected question to validate as String
     - textFieldHint: the hint as a String to show the user in the text field of the expected response
     - expectedResponse: the expected response of the question as a String to be validated
     - completionHandler: returns a boolean representing the validation of the question as a parameter of the callback

     ### Usage Example: ###
     ````
     raiseSecurityQuestionPopUp(title: "title", question: "this is the question?", textfieldHint: "hint of the question", expectedResponse: "this is the secure response") {
            (validated) in
     }

     ````
     */

    func raiseSecurityQuestionPopUp(title: String, question: String, textFieldHint: String, expectedResponse: String, completionHandler: AuthenticatorsCompletionHandler?)

    /**
     Call this function to show a login popup and get username and password from user
     - Parameters:
     - title: the title of the popup as String
     - text: the description of popup as String
     - usernameHint: the hint as a String to show inside of username textfield as placeholder.
     - passwordHint: the hint as a String to show inside of password textfield as placeholder.
     - completionHandler: returns username and password entered by user as parameters of the callback

     ### Usage Example: ###
     ````
     raiseUserLoginPopUp(title: "User Login", text: "Please enter username and password", usernameHint: "Username", passwordHind: "Password") {
     (username,password) in
     }

     ````
     */

    func raiseUserLoginPopUp(title: String, text: String, usernameHint: String, passwordHint: String, completionHandler: AuthenticatorsCompletionHandler?)

    /**
     Call this function to show a popup to ask the users for the positions of a password without validation
     - Parameters:
     - title: the title of the popUp as String
     - question: the complete expected question to validate as String
     - textFieldHint: the hint as a String to show the user in the text field of the expected response
     - completionHandler: returns the input of the user as a String in the parameter of the callback

     ### Usage Example: ###
     ````
     raiseSecurityQuestionPopUpWithoutValidation(title: "title", question: "this is the question?", textfieldHint: "hint of the question") {
            (inputResponse) in
     }

     ````
     */

    func raiseSecurityQuestionPopUpWithoutValidation(title: String, question: String, textFieldHint: String, completionHandler: AuthenticatorsCompletionHandler?)

    /**
     Call this function to show a popup to ask the users biometric authentication either with faceId or touchId depending on hardware (the OS handles the call to the available hardware in the device)
     - Parameters:
     - title: the title of the popUp as String
     - text: the text or text of the popUp as String
     - completionHandler: returns a boolean that represents if the user is authenticated

     ### Usage Example: ###
     ````
     raiseSecurityQuestionPopUpWithoutValudation(title: "title", text: "text") {
            (authenticated) in
     }

     ````
     */

    func raiseFingerFaceIDAuthenticatorFunction(title: String, text: String, completionHandler: AuthenticatorsCompletionHandler?)

    /**
     Call this function to show a the native popup to ask the users biometric authentication either with faceId or touchId depending on hardware (the OS handles the call to the available hardware in the device)
     - Parameters:
     - title: the title of the popUp as String
     - completionHandler: returns a boolean that represents if the user is authenticated

     ### Usage Example: ###
     ````
     raiseSecurityQuestionPopUpWithoutValudation(title: "title", text: "text") {
     (authenticated) in
     }

     ````
     */

    func raiseNativeFingerFaceIDAuthenticatorFunction(title: String, completionHandler: AuthenticatorsCompletionHandler?)

    /**
     Call this function to show a popup to ask the users access to the camera to detect an image and validate in server
     - Parameters:
     - title: the title of the popUp as String
     - text: the text or text of the popUp as String
     - url: the url to send the iomage for validation
     - completionHandler: returns a boolean that represents if the user is authenticated

     ### Usage Example: ###
     ````
     raisePassPositionsPopUp(title: "title", text: "text", positionsToAsk: [2,3])
     ````
     */

    func raiseFaceAuthenticator(title: String, text: String, url: URL, completionHandler: AuthenticatorsCompletionHandler?)
}

public class SCALAuthenticators {
    private let authenticators: SCALAuthenticatorsInterface
    public init(authenticators: SCALAuthenticatorsInterface) {
        self.authenticators = authenticators
    }

    public func raisePassPositionsPopUp(title: String, text: String, completePass: String, positionsToAsk: [Int], completionHandler: AuthenticatorsCompletionHandler?) {
        
        return authenticators.raisePassPositionsPopUp(title: title, text: text, completePass: completePass, positionsToAsk: positionsToAsk, completionHandler: completionHandler)
    }

    public func raisePassPositionsPopUpWithoutValidation(title: String, text: String, passLength: Int, positionsToAsk: [Int], completionHandler: AuthenticatorsCompletionHandler?) {
        return authenticators.raisePassPositionsPopUpWithoutValidation(title: title, text: text, passLength: passLength, positionsToAsk: positionsToAsk, completionHandler: completionHandler)
    }

    public func raiseSecurityQuestionPopUp(title: String, question: String, textFieldHint: String, expectedResponse: String, completionHandler: AuthenticatorsCompletionHandler?) {
        return authenticators.raiseSecurityQuestionPopUp(title: title, question: question, textFieldHint: textFieldHint, expectedResponse: expectedResponse, completionHandler: completionHandler)
    }

    public func raiseUserLoginPopUp(title: String, text: String, usernameHint: String, passwordHint: String, completionHandler: AuthenticatorsCompletionHandler?) {
        return authenticators.raiseUserLoginPopUp(title: title, text: text, usernameHint: usernameHint, passwordHint: passwordHint, completionHandler: completionHandler)
    }

    public func raiseSecurityQuestionPopUpWithoutValidation(title: String, question: String, textFieldHint: String, completionHandler: AuthenticatorsCompletionHandler?) {
        return authenticators.raiseSecurityQuestionPopUpWithoutValidation(title: title, question: question, textFieldHint: textFieldHint, completionHandler: completionHandler)
    }

    public func raiseFingerFaceIDAuthenticatorFunction(title: String, text: String, completionHandler: AuthenticatorsCompletionHandler?) {
        return authenticators.raiseFingerFaceIDAuthenticatorFunction(title: title, text: text, completionHandler: completionHandler)
    }

    public func raiseNativeFingerFaceIDAuthenticatorFunction(title: String, completionHandler: AuthenticatorsCompletionHandler?) {
        return authenticators.raiseNativeFingerFaceIDAuthenticatorFunction(title: title, completionHandler: completionHandler)
    }

    public func raiseFaceAuthenticator(title: String, text: String, url: URL, completionHandler: AuthenticatorsCompletionHandler?) {
        return authenticators.raiseFaceAuthenticator(title: title, text: text, url: url, completionHandler: completionHandler)
    }
}
