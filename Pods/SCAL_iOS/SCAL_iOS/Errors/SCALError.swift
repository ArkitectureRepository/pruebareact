//
//  SCALCompatibilityError.swift
//  Pods-BiometricVaultDemo
//
//  Created by MAC Pilot Globile 11 on 12/08/2019.
//

import Foundation

public struct SCALError: CustomNSError {
    public var domain: String
    public var code: SCALErrorType?
    public var userInfo: [String: Any]?

    public init(domain: String = "SCAL", code: SCALErrorType?, userInfo: [String: Any]?) {
        self.domain = domain
        self.code = code
        self.userInfo = userInfo
    }
}

public enum SCALErrorType: Int {
    case unknownError = 0
    case scalComplianceError = 1
    case authenticationError = 2
    case databaseError = 3
    case hardwareError = 4
    case uiError = 5
    case applicationError = 6
    case networkError = 7
    case serverError = 8
    case encryptionError = 9
    case contextDetectionError = 10

    func domain() -> String {
        switch self {
        default:
            return "default"
        }
    }
}
