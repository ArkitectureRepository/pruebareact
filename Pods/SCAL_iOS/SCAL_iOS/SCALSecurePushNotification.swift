//
//  CALSecurePushNotification.swift
//  SCAL_iOS
//
//  Created by Hassan, Waseem (Isban) on 07/03/2019.
//  Copyright © 2019 Hassan, Waseem (Isban). All rights reserved.
//

public enum APICallResult {
    case response(Data, URLResponse)
    case error(Error, Data?, URLResponse?)
}

public enum SCALSecurePushNotificationFields: String {
    case notificationIV = "mobisec_iv"
    case notificationMessageType = "type"
    case notificationTitle = "title"
    case notificationCode8 = "code8"
    case notificationBody = "body"
    
    public static func allFields() -> [String] {
        return [notificationIV.rawValue, notificationMessageType.rawValue, notificationTitle.rawValue, notificationCode8.rawValue, notificationBody.rawValue]
    }
}

public protocol SCALSecurePushNotificationInterface {
    func registerToken(token: String, completionHandler: @escaping (APICallResult) -> Void)
    
    func requestNewSecondFactorAuthentication(completionHandler: @escaping (APICallResult) -> Void)
    
    func verifyCode(inputCode: String, completionHandler: @escaping (APICallResult) -> Void)
    
    func decryptPayLoad(payload: [AnyHashable: Any]) -> [AnyHashable: Any]
}

public class SCALSecurePushNotification {
    private let securePushNotification: SCALSecurePushNotificationInterface
    
    public init(securePushNotification: SCALSecurePushNotificationInterface) {
        self.securePushNotification = securePushNotification
    }
    
    public func requestNewSecondFactorAuthentication(completionHandler: @escaping (APICallResult) -> Void) {
        self.securePushNotification.requestNewSecondFactorAuthentication { (result) in
            completionHandler(result)
        }
    }
    
    public func verifyCode(inputCode: String, completionHandler: @escaping (APICallResult) -> Void) {
        self.securePushNotification.verifyCode(inputCode: inputCode) { (result) in
            completionHandler(result)
        }
    }
    
    public func registerToken(token: String, completionHandler: @escaping (APICallResult) -> Void) {
        self.securePushNotification.registerToken(token: token) { (result) in
            completionHandler(result)
        }
    }
    
    public func decryptPayLoad(payload: [AnyHashable: Any]) -> [AnyHashable: Any] {
        return self.securePushNotification.decryptPayLoad(payload: payload)
    }
}

public class SCALSecurePushMessagingService {
    private let securePushNotification: SCALSecurePushNotificationInterface
    
    public init(securePushNotification: SCALSecurePushNotificationInterface) {
        self.securePushNotification = securePushNotification
    }
    
    public func handleSecureMessage(payload: [AnyHashable: Any]) -> [AnyHashable: Any]? {
        if self.isMobisecMessage(payload: payload) {
            return self.securePushNotification.decryptPayLoad(payload: payload)
        } else {
            return nil
        }
    }
    
    private func isMobisecMessage(payload: [AnyHashable: Any]) -> Bool {
        return (payload[SCALSecurePushNotificationFields.notificationIV.rawValue] != nil)
    }
    
}
