//
//  CALBiometricVault.swift
//  SCAL_iOS
//
//  Created by Eduardo Nieto Delgado on 01/04/2019.
//  Copyright © 2019 Hassan, Waseem (Isban). All rights reserved.
//

import Foundation



public typealias BiometricManagerCompletionHandler =  (_ authenticated: Bool, _ error: SCALError?) -> Void
public typealias BiometricVaultCompletionHandler =  (_ credential: String?, _ error: SCALError?) -> Void
public typealias BiometricVaultSuccessCompletionHandler =  (_ success: Bool?, _ error: SCALError?) -> Void

public protocol SCALBiometricVaultInterface {
    
    /**
     Call this function to store a credential as String inside keychain using biometrics
     - Parameters:
     - credential: credential in order to store it by a securely way
     - key: the key associated to the credential.
     - callback: the callback as a BiometricVaultSuccessCallback with the success of the operation
     
     ### Usage Example: ###
     ````
     storeCredentialAuthenticated("credentialToStore", key: "keyOfCredential") {
     (success, error) in
     
     }
     ````
     */
    
    func storeCredentialAuthenticated(_ credential: String, key: String, reauthenticationTitle: String, callback:  @escaping BiometricVaultSuccessCompletionHandler)
    
    /**
     Call this function to get a credential previously stored as String inside keychain using biometrics
     - Parameters:
     - key: the key associated to the credential.

     Return: String with the value of the credential
     
     ### Usage Example: ###
     ````
     let credential = getCredentialAuthenticated(key: "keyOfCredential")
     ````
     */
    
    func getCredentialAuthenticated(key: String) -> String?
    
    
    /**
     Call this function to get a rempve previously stored as String inside keychain
     - Parameters:
     - key: the key associated to the credential.
     - callback: the callback as a BiometricVaultSuccessCallback with the success of the operation
     
     ### Usage Example: ###
     ````
     removeCredentialAuthenticated(key: "keyOfCredential") { (success, error) in
     
     }
     ````
     */
    func removeCredentialAuthenticated(key: String, reauthenticationTitle: String, callback: @escaping BiometricVaultSuccessCompletionHandler)

    /**
     Call this function to check if biometric authentication is available
     
     - Return: a boolean that represents if the biometric hardware is available
     
     ### Usage Example: ###
     ````
     let isAvailable = isBiometricAuthenticationAvailable()
     ````
     */
    func isBiometricAuthenticationAvailable() -> Bool
    
    
    /**
     Call this function to check if biometric methods of authentication have changed
     
     - Return: a boolean that represents if the biometric state has changed
     
     ### Usage Example: ###
     ````
     let hasChanged = hasBiometricStateChanged()
     ````
     */

    
    func hasBiometricStateChanged() -> Bool
    
    
    /**
     Call this function to update the current state of biometrics to make it trusted. This method requires authentication
     
     - Return: a boolean that represents if the biometric state has been updated
     
     ### Usage Example: ###
     ````
     let updated = updateBiometricState()
     ````
     */
    
    
    func updateBiometricState(title: String, callback: @escaping BiometricVaultSuccessCompletionHandler)

}

public class SCALBiometricVault {

    private let biometricVault: SCALBiometricVaultInterface

    public init(biometricVault: SCALBiometricVaultInterface) {
        self.biometricVault = biometricVault
    }
    
    public func storeCredentialAuthenticated(_ credential: String, key: String, reauthenticationTitle: String, callback:  @escaping BiometricVaultSuccessCompletionHandler) {
        return self.biometricVault.storeCredentialAuthenticated(credential, key: key, reauthenticationTitle: reauthenticationTitle, callback: callback)
    }
    
    public func getCredentialAuthenticated(key: String) -> String? {
        return self.biometricVault.getCredentialAuthenticated(key: key)
    }
    
    public func removeCredentialAuthenticated(key: String, reauthenticationTitle: String, callback: @escaping BiometricVaultSuccessCompletionHandler) {
        return self.biometricVault.removeCredentialAuthenticated(key: key, reauthenticationTitle: reauthenticationTitle, callback: callback)
    }
    
    public func isBiometricAuthenticationAvailable() -> Bool {
        return self.biometricVault.isBiometricAuthenticationAvailable()
    }

    public func hasBiometricStateChanged() -> Bool {
        return self.biometricVault.hasBiometricStateChanged()
    }
    
    public func updateBiometricState(title: String, callback: @escaping BiometricVaultSuccessCompletionHandler) {
        return self.biometricVault.updateBiometricState(title: title, callback: callback)
    }
    
}
