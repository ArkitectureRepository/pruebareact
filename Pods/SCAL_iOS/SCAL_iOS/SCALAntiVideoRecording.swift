//
//  CALAntiVideoRecording.swift
//  SCAL_iOS
//
//  Created by Hassan, Waseem (Isban) on 01/03/2019.
//  Copyright © 2019 Hassan, Waseem (Isban). All rights reserved.
//
public protocol SCALVideoRecordingDetectorDelegate: class {
    /// Calling when recording string is detect.
    /// - Parameters:
    ///     - detector: The detector object.
    func recordingDetector(detectRecording detector: SCALAntiVideoRecordingInterface)
    /// Calling when is not sure the recording status.
    ///
    /// The VideoRecordingDetector is observing the property "captured" of UISCreen.
    /// This property is setted seconds after the video started to record.
    /// This function is calling in this posible situations to prevent intermediate states.
    /// - Parameters:
    ///     - detector: The detector object.
    func recordingDetector(indeterminateRecordingStatus detector: SCALAntiVideoRecordingInterface)
    /// Calling when video screen recording is finished
    /// - Parameters:
    ///     - detector: The detector object.
    func recordingDetector(endRecording detector: SCALAntiVideoRecordingInterface)
    /// Calling when app is no longer active and loses focus to prevent video recording in background scenaries
    /// - Parameters:
    ///     - detector: The detector object.
    func recordingDetector(appWillResignActive detector: SCALAntiVideoRecordingInterface)
}

public protocol SCALAntiVideoRecordingInterface {
    var delegate: SCALVideoRecordingDetectorDelegate! {get set}
    
    init(delegate: SCALVideoRecordingDetectorDelegate)
}

public class SCALVideoRecordingDetector {
    private let antiVideoRecording: SCALAntiVideoRecordingInterface
    
    public init(antiVideoRecording: SCALAntiVideoRecordingInterface) {
        self.antiVideoRecording = antiVideoRecording
    }
}
