//
//  SCALContextData.swift
//  SCAL_iOS
//
//  Created by Gilles Guillemin on 11/10/2019.
//  Copyright © 2019 Hassan, Waseem (Isban). All rights reserved.
//

import Foundation

public struct SCALContextData {
    public let deviceData: SCALDeviceData
    public let commsData: SCALCommunicationData
    public let geoData: SCALGeopositionData
    public let footprintData: SCALDeviceFootprintData

    public init(deviceData: SCALDeviceData,
                commsData: SCALCommunicationData,
                geoData: SCALGeopositionData,
                footprintData: SCALDeviceFootprintData) {
        self.deviceData = deviceData
        self.commsData = commsData
        self.geoData = geoData
        self.footprintData = footprintData
    }
}
