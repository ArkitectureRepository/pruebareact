//
//  SCALCommunicationData.swift
//  SCAL_iOS
//
//  Created by Gilles Guillemin on 11/10/2019.
//  Copyright © 2019 Hassan, Waseem (Isban). All rights reserved.
//

import Foundation

public enum SCALNetworkInterfaceType: String {
    case cellular
    case wifi
}

public struct SCALCommunicationData {
    public let ipV4: [SCALNetworkInterfaceType: [String]]
    public let ipV6: [SCALNetworkInterfaceType: [String]]
    public let isBluetoothEnabled: Bool
    public let isVpnConnected: Bool
    public let networkType: String

    public init(ipV4: [SCALNetworkInterfaceType: [String]],
                ipV6: [SCALNetworkInterfaceType: [String]],
                isBluetoothEnabled: Bool,
                isVpnConnected: Bool,
                networkType: String) throws {
        let enforcer = SCALCommunicationDataEnforcer()
        self.ipV4 = try enforcer.verify(ipV4: ipV4)
        self.ipV6 = try enforcer.verify(ipV6: ipV6)
        self.isBluetoothEnabled = isBluetoothEnabled
        self.isVpnConnected = isVpnConnected
        self.networkType = try enforcer.verify(networkType: networkType)
    }
}

public enum SCALCommunicationDataError: Error {
    case invalidIPv4
    case invalidIPv6
    case invalidNetworkType
}

class SCALCommunicationDataEnforcer {
    func verify(ipV4: [SCALNetworkInterfaceType: [String]]) throws -> [SCALNetworkInterfaceType: [String]] {
        guard areIPAddressesValid(values: ipV4, for: "^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$") else {
            throw SCALCommunicationDataError.invalidIPv4
        }
        return ipV4
    }

    func verify(ipV6: [SCALNetworkInterfaceType: [String]]) throws -> [SCALNetworkInterfaceType: [String]] {
        // The following regex comes from: https://stackoverflow.com/a/32368136
        guard areIPAddressesValid(values: ipV6, for: "(?:^|(?<=\\s))(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))(?=\\s|$)") else {
            throw SCALCommunicationDataError.invalidIPv6
        }
        return ipV6
    }

    func verify(networkType: String) throws -> String {
        guard CDNetworkType(rawValue: networkType) != nil else {
            throw SCALCommunicationDataError.invalidNetworkType
        }
        return networkType
    }

    private func areIPAddressesValid(values ipValues: [SCALNetworkInterfaceType: [String]], for pattern: String) -> Bool {
        for adresses: [String] in ipValues.values {
            for address in adresses {
                if address.range(of: pattern, options: .regularExpression) == nil {
                    return false
                }
            }
        }
        return true
    }
}
