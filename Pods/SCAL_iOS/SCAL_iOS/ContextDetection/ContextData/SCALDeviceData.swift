//
//  SCALDeviceData.swift
//  SCAL_iOS
//
//  Created by Gilles Guillemin on 11/10/2019.
//  Copyright © 2019 Hassan, Waseem (Isban). All rights reserved.
//

import Foundation

public struct SCALDeviceData {
    public let batteryLevel: Int
    public let deviceAuthSettings: String
    public let deviceModel: String
    public let deviceType: String
    public let hasFingerprintEnrolled: Bool
    public let hasFingerprintValueChanged: Bool
    public let isJailbroken: Bool
    public let isShowPasswordSettingEnabled: Bool
    public let osVersion: String
    public let vendorId: String

    public init(batteryLevel: Int,
                deviceAuthSettings: String,
                deviceModel: String,
                deviceType: String,
                hasFingerprintEnrolled: Bool,
                hasFingerprintValueChanged: Bool,
                isJailbroken: Bool,
                isShowPasswordSettingEnabled: Bool,
                vendorId: String,
                osVersion: String
    ) throws {
        let enforcer = SCALDeviceDataEnforcer()
        self.batteryLevel = try enforcer.verify(batteryLevel: batteryLevel)
        self.deviceAuthSettings = try enforcer.verify(deviceAuthSettings: deviceAuthSettings)
        self.deviceModel = try enforcer.verify(deviceModel: deviceModel)
        self.deviceType = try enforcer.verify(deviceType: deviceType)
        self.hasFingerprintEnrolled = hasFingerprintEnrolled
        self.hasFingerprintValueChanged = hasFingerprintValueChanged
        self.isJailbroken = isJailbroken
        self.isShowPasswordSettingEnabled = isShowPasswordSettingEnabled
        self.osVersion = try enforcer.verify(osVersion: osVersion)
        self.vendorId = try enforcer.verify(vendorId: vendorId)
    }
}

public enum SCALDeviceDataError: Error {
    case invalidBatteryLevel
    case invalidDeviceAuthSetting
    case invalidDeviceModel
    case invalidDeviceType
    case invalidOsVersion
    case invalidVendor
}

class SCALDeviceDataEnforcer {
    
    func verify(batteryLevel: Int) throws -> Int {
        guard batteryLevel >= 0 && batteryLevel <= 100 else {
            throw SCALDeviceDataError.invalidBatteryLevel
        }
        return batteryLevel
    }
    
    func verify(deviceAuthSettings: String) throws -> String {
        guard CDAuthSetting(rawValue: deviceAuthSettings) != nil else {
            throw SCALDeviceDataError.invalidDeviceAuthSetting
        }
        return deviceAuthSettings
    }

    func verify(deviceModel: String) throws -> String {
        guard deviceModel.count > 2 && deviceModel.count < 40 else {
            throw SCALDeviceDataError.invalidDeviceModel
        }
        return deviceModel
    }
    
    func verify(deviceType: String) throws -> String {
        guard CDDeviceTypeKey(rawValue: deviceType) != nil else {
            throw SCALDeviceDataError.invalidDeviceType
        }
        return deviceType
    }
    
     func verify(osVersion: String) throws -> String {
         guard osVersion.range(of: "^(\\d{0,2}(\\.\\d)?(\\.\\d)?)$", options: .regularExpression) != nil else {
             throw SCALDeviceDataError.invalidOsVersion
         }
         return osVersion
     }

    func verify(vendorId: String) throws -> String {
        guard vendorId.count > 2 && vendorId.count < 50 else {
            throw SCALDeviceDataError.invalidVendor
        }
        return vendorId
    }
}
