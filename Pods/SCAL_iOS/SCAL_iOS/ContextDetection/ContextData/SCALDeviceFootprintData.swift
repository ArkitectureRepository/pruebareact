//
//  SCALDeviceFootprintData.swift
//  SCAL_iOS
//
//  Created by Gilles Guillemin on 11/10/2019.
//  Copyright © 2019 Hassan, Waseem (Isban). All rights reserved.
//

import Foundation

public struct SCALDeviceFootprintData {
    public let footprint: String
    
    public init(footprint: String) throws {
        self.footprint = footprint
    }
}
