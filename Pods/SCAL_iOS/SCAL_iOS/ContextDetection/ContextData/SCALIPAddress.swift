//
//  SCALIPAddress.swift
//  SCAL_iOS
//
//  Created by Gilles Guillemin on 11/10/2019.
//  Copyright © 2019 Hassan, Waseem (Isban). All rights reserved.
//

import Foundation

public struct SCALIPAddress {
    public let alias: String
    public let businessName: String
    public let businessWebsite: String
    public let city: String
    public let continent: String
    public let country: String
    public let countryCode: String
    public let ipName: String
    public let ipType: String
    public let isp: String
    public let lat: String
    public let lon: String
    public let org: String
    public let query: String
    public let region: String
    public let regionName: String
    public let status: String
    public let timezone: String
    public let zip: String

    public init(alias: String,
                businessName: String,
                businessWebsite: String,
                city: String,
                continent: String,
                country: String,
                countryCode: String,
                ipName: String,
                ipType: String,
                isp: String,
                lat: String,
                lon: String,
                org: String,
                query: String,
                region: String,
                regionName: String,
                status: String,
                timezone: String,
                zip: String) {
        self.alias = alias
        self.businessName = businessName
        self.businessWebsite = businessWebsite
        self.city = city
        self.continent = continent
        self.country = country
        self.countryCode = countryCode
        self.ipName = ipName
        self.ipType = ipType
        self.isp = isp
        self.lat = lat
        self.lon = lon
        self.org = org
        self.query = query
        self.region = region
        self.regionName = regionName
        self.status = status
        self.timezone = timezone
        self.zip = zip
    }
}
