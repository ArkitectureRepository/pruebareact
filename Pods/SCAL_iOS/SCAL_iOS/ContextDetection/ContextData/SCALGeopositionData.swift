//
//  SCALGeopositionData.swift
//  SCAL_iOS
//
//  Created by Gilles Guillemin on 11/10/2019.
//  Copyright © 2019 Hassan, Waseem (Isban). All rights reserved.
//

import Foundation

public struct SCALGeopositionData {
    public let address: String
    public let countryCode: String
    public let ipAddress: SCALIPAddress
    public let latitude: Double
    public let longitude: Double

    public init(address: String,
                countryCode: String,
                ipAddress: SCALIPAddress,
                latitude: Double,
                longitude: Double) throws {
        let enforcer = SCALGeopositionDataEnforcer()
        self.address = try enforcer.verify(address: address)
        self.countryCode = try enforcer.verify(countryCode: countryCode)
        self.ipAddress = ipAddress
        self.latitude = try enforcer.verify(latitude: latitude)
        self.longitude = try enforcer.verify(longitude: longitude)
    }
}

public enum SCALGeopositionDataError: Error {
    case invalidAddress
    case invalidCountryCode
    case invalidLatitude
    case invalidLongitude
}

class SCALGeopositionDataEnforcer {
    func verify(address: String) throws -> String {
        guard address.count > 5 && address.count < 800 else {
            throw SCALGeopositionDataError.invalidAddress
        }
        return address
    }

    func verify(countryCode: String) throws -> String {
        let geolocationRules = GeolocationRules()
        guard geolocationRules.countryCodesAlpha2.contains(countryCode)
            || geolocationRules.countryCodesAlpha3.contains(countryCode) else {
            throw SCALGeopositionDataError.invalidCountryCode
        }
        return countryCode
    }

    func verify(latitude: Double) throws -> Double {
        guard latitude >= -90 && latitude <= 90 else {
            throw SCALGeopositionDataError.invalidLatitude
        }
        return latitude
    }

    func verify(longitude: Double) throws -> Double {
        guard longitude >= -180 && longitude <= 180 else {
            throw SCALGeopositionDataError.invalidLongitude
        }
        return longitude
    }
}
