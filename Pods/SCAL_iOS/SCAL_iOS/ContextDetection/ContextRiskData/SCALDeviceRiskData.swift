//
//  SCALDeviceRiskData.swift
//  SCAL_iOS
//
//  Created by Gilles Guillemin on 11/10/2019.
//  Copyright © 2019 Hassan, Waseem (Isban). All rights reserved.
//

import Foundation

public struct SCALDeviceRiskData {
    public let deviceType: String
    public let deviceAuthenticationMode: String
    public let hasBiometricsEnrolled: String
    public let hasBiometricsChanged: String
    public let isJailbroken: String
    public let osVersion: String

    public init(deviceType: String,
                deviceAuthenticationMode: String,
                hasBiometricsEnrolled: String,
                hasBiometricsChanged: String,
                isJailbroken: String,
                osVersion: String) throws {
        let enforcer = SCALDeviceRiskDataEnforcer()
        self.deviceType = try enforcer.verify(deviceType: deviceType)
        self.deviceAuthenticationMode = try enforcer.verify(deviceAuthenticationMode: deviceAuthenticationMode)
        self.hasBiometricsEnrolled = try enforcer.verify(hasBiometricsEnrolled: hasBiometricsEnrolled)
        self.hasBiometricsChanged = try enforcer.verify(hasBiometricsChanged: hasBiometricsChanged)
        self.isJailbroken = try enforcer.verify(isJailbroken: isJailbroken)
        self.osVersion = try enforcer.verify(osVersion: osVersion)
    }
}

public enum SCALDeviceRiskDataError: Error {
    case invalidDeviceTypeRisk
    case invalidDeviceAuthenticationModeRisk
    case invalidHasBiometricsEnrolledRisk
    case invalidHasBiometricsChangedRisk
    case invalidIsJailbrokenRisk
    case invalidOsVersionRisk
}

class SCALDeviceRiskDataEnforcer {
    func verify(deviceType: String) throws -> String {
        try verifyRiskLevel(risk: deviceType, risks: [.high, .low], error: .invalidDeviceTypeRisk)
    }

    func verify(deviceAuthenticationMode: String) throws -> String {
        try verifyRiskLevel(risk: deviceAuthenticationMode, risks: [.high, .medium, .low], error: .invalidDeviceAuthenticationModeRisk)
    }

    func verify(hasBiometricsEnrolled: String) throws -> String {
        try verifyRiskLevel(risk: hasBiometricsEnrolled, risks: [.high, .low], error: .invalidHasBiometricsEnrolledRisk)
    }

    func verify(hasBiometricsChanged: String) throws -> String {
        try verifyRiskLevel(risk: hasBiometricsChanged, risks: [.medium, .low], error: .invalidHasBiometricsChangedRisk)
    }

    func verify(isJailbroken: String) throws -> String {
        try verifyRiskLevel(risk: isJailbroken, risks: [.high, .low], error: .invalidIsJailbrokenRisk)
    }

    func verify(osVersion: String) throws -> String {
        try verifyRiskLevel(risk: osVersion, risks: [.high, .medium, .low], error: .invalidOsVersionRisk)
    }

    private func verifyRiskLevel(risk: String, risks: [CDRiskLevel], error: SCALDeviceRiskDataError) throws -> String {
        guard let riskLevel = CDRiskLevel(rawValue: risk), risks.contains(riskLevel) else {
            throw error
        }
        return risk
    }
}
