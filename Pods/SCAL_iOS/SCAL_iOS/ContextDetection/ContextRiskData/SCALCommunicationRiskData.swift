//
//  SCALCommunicationRiskData.swift
//  SCAL_iOS
//
//  Created by Gilles Guillemin on 11/10/2019.
//  Copyright © 2019 Hassan, Waseem (Isban). All rights reserved.
//

import Foundation

public struct SCALCommunicationRiskData {
    public let networkType: String
    public let bluetoothConnectivity: String
    public let vpnConnectionStatus: String

    public init(networkType: String,
                bluetoothConnectivity: String,
                vpnConnectionStatus: String) throws {
        let enforcer = SCALCommunicationRiskDataEnforcer()
        self.networkType = try enforcer.verify(networkType: networkType)
        self.bluetoothConnectivity = try enforcer.verify(bluetoothConnectivity: bluetoothConnectivity)
        self.vpnConnectionStatus = try enforcer.verify(vpnConnectionStatus: vpnConnectionStatus)
    }
}

public enum SCALCommunicationRiskDataError: Error {
    case invalidNetworkTypeRisk
    case invalidBluetoothConnectivityRisk
    case invalidVPNConnectionStatusRisk
}

class SCALCommunicationRiskDataEnforcer {
    func verify(networkType: String) throws -> String {
        try verifyRiskLevel(risk: networkType, risks: [.high, .medium, .low], error: .invalidNetworkTypeRisk)
    }

    func verify(bluetoothConnectivity: String) throws -> String {
        try verifyRiskLevel(risk: bluetoothConnectivity, risks: [.medium, .low], error: .invalidBluetoothConnectivityRisk)
    }

    func verify(vpnConnectionStatus: String) throws -> String {
        try verifyRiskLevel(risk: vpnConnectionStatus, risks: [.high, .low], error: .invalidVPNConnectionStatusRisk)
    }

    private func verifyRiskLevel(risk: String, risks: [CDRiskLevel], error: SCALCommunicationRiskDataError) throws -> String {
        guard let riskLevel = CDRiskLevel(rawValue: risk), risks.contains(riskLevel) else {
            throw error
        }
        return risk
    }
}
