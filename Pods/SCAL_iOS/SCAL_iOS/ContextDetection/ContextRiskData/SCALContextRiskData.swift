//
//  SCALContextRiskData.swift
//  SCAL_iOS
//
//  Created by Gilles Guillemin on 11/10/2019.
//  Copyright © 2019 Hassan, Waseem (Isban). All rights reserved.
//

import Foundation

public struct SCALContextRiskData {
    public let deviceRiskData: SCALDeviceRiskData
    public let communicationRiskData: SCALCommunicationRiskData
    public let footprintData: SCALDeviceFootprintData

    public init(deviceRiskData: SCALDeviceRiskData,
                communicationRiskData: SCALCommunicationRiskData,
                footprintData: SCALDeviceFootprintData) {
        self.deviceRiskData = deviceRiskData
        self.communicationRiskData = communicationRiskData
        self.footprintData = footprintData
    }
}
