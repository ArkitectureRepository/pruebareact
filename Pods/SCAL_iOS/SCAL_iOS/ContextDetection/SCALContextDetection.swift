//
//  SCALContextDetection.swift
//  SCAL-iOS
//
//  Created by Hassan, Waseem (Isban) on 11/02/2019.
//  Copyright © 2019 Hassan, Waseem (Isban). All rights reserved.
//

import Foundation

public enum ContextReportType {
    case applicationRunningMode
    case batteryLevel
    case bluetoothConnectivity
    case carriers
    case customKeyboard
    case deviceAuthenticationMode
    case deviceType
    case geoLocation
    case hasFingerprintEnrolled
    case hasFingerprintValueChanged
    case ipAddress
    case model
    case networkType
    case osVersion
    case vendor
    case vpnConnectionStatus
}

public enum StringEncoding {
    case base64
    case hex
}

// Context detection interface
public protocol SCALContextDetectionInterface {
    func getAllData(completionHandler: @escaping (Result<SCALContextData, Error>) -> Void)
    func getDeviceData(completionHandler: @escaping (Result<SCALDeviceData, Error>) -> Void)
    func getCommunicationData(completionHandler: @escaping (Result<SCALCommunicationData, Error>) -> Void)
    func getGeopositionData(completionHandler: @escaping (Result<SCALGeopositionData, Error>) -> Void)
    func getDeviceFootprint(completionHandler: @escaping (Result<SCALDeviceFootprintData, Error>) -> Void)

    func getAllRiskData(completionHandler: @escaping (Result<SCALContextRiskData, Error>) -> Void)
    func getDeviceRiskData(completionHandler: @escaping (Result<SCALDeviceRiskData, Error>) -> Void)
    func getCommunicationRiskData(completionHandler: @escaping (Result<SCALCommunicationRiskData, Error>) -> Void)
    func submit(completionHandler: @escaping (Result<Bool, Error>) -> Void, repeats: Bool, timeInterval: TimeInterval)
}

public final class SCALContextDetection {
    private let contextReport: SCALContextDetectionInterface
    public init(contextReport: SCALContextDetectionInterface) {
        self.contextReport = contextReport
    }

    public func getAllData(completionHandler: @escaping (Result<SCALContextData, Error>) -> Void) {
        contextReport.getAllData { result in
            completionHandler(result)
        }
    }

    public func getDeviceData(completionHandler: @escaping (Result<SCALDeviceData, Error>) -> Void) {
        contextReport.getDeviceData { result in
            completionHandler(result)
        }
    }

    public func getCommunicationData(completionHandler: @escaping (Result<SCALCommunicationData, Error>) -> Void) {
        contextReport.getCommunicationData { result in
            completionHandler(result)
        }
    }

    public func getGeopositionData(completionHandler: @escaping (Result<SCALGeopositionData, Error>) -> Void) {
        contextReport.getGeopositionData { result in
            completionHandler(result)
        }
    }

    public func getDeviceFootprint(completionHandler: @escaping (Result<SCALDeviceFootprintData, Error>) -> Void) {
        contextReport.getDeviceFootprint { result in
            completionHandler(result)
        }
    }

    public func getAllRiskData(completionHandler: @escaping (Result<SCALContextRiskData, Error>) -> Void) {
        contextReport.getAllRiskData { result in
            completionHandler(result)
        }
    }

    public func getDeviceRiskData(completionHandler: @escaping (Result<SCALDeviceRiskData, Error>) -> Void) {
        contextReport.getDeviceRiskData { result in
            completionHandler(result)
        }
    }

    public func getCommunicationRiskData(completionHandler: @escaping (Result<SCALCommunicationRiskData, Error>) -> Void) {
        contextReport.getCommunicationRiskData { result in
            completionHandler(result)
        }
    }

    public func submit(completionHandler: @escaping (Result<Bool, Error>) -> Void, repeats: Bool = false, timeInterval: TimeInterval = 600) {
        contextReport.submit(completionHandler: { result in
            completionHandler(result)
        }, repeats: repeats, timeInterval: timeInterval)
    }
}
