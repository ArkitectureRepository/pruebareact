//
//  CALSQLCipher.swift
//  SCAL_iOS
//
//  Created by Eduardo Nieto Delgado on 06/03/2019.
//  Copyright © 2019 Hassan, Waseem (Isban). All rights reserved.
//

import Foundation

public struct SCALCreateTableQuery {
    public let tableName: String
    public let columns: [String: String]
    
    public init(tableName: String, columns: [String: String]) {
        self.tableName = tableName
        self.columns = columns
    }
}

public struct SCALInsertQuery {
    public let tableName: String
    public let values: [String: Any]
    
    public init(tableName: String, values: [String: Any]) {
        self.tableName = tableName
        self.values = values
    }
}


/// SCALDatabaseParameterized
public struct SCALDatabaseParameterized {
    public let function: String
    public let arguments: [Any]
    
    /// Similar to prepared sql statements
    /// - Parameter function: "tablename = ?"
    /// - Parameter arguments: [5]]
    public init(function: String, arguments: [Any]) {
        self.function = function
        self.arguments = arguments
    }
}

public struct SCALDatabaseOrderBy {
    
    public enum Order {
        case descending
        case ascending
    }
    
    public let columName: String
    public let order: Order
    
    public init(columName: String, order: Order) {
        self.columName = columName
        self.order = order
    }
}

public struct SCALSelectQuery {
    public let distinct: Bool
    public let tableName: String
    public let columns: [String]
    public let selection: SCALDatabaseParameterized?
    public let groupBy: String?
    public let having: SCALDatabaseParameterized?
    public let orderBy: [SCALDatabaseOrderBy]?
    public let limit: Int?
    
    public init(distinct: Bool, tableName: String, columns: [String], selection: SCALDatabaseParameterized?, groupBy: String?, having: SCALDatabaseParameterized?, orderBy: [SCALDatabaseOrderBy]?, limit: Int?) {
        self.distinct = distinct
        self.tableName = tableName
        self.columns = columns
        self.selection = selection
        self.groupBy = groupBy
        self.having = having
        self.orderBy = orderBy
        self.limit = limit
    }
}


public struct SCALDeleteQuery {
    public let tableName: String
    public let selection: SCALDatabaseParameterized

    public init(tableName: String, selection: SCALDatabaseParameterized) {
        self.tableName = tableName
        self.selection = selection
    }
}

public struct SCALUpdateQuery {
    public let tableName: String
    public let selection: SCALDatabaseParameterized?
    public let values: [String: Any]
    
    public init(tableName: String, selection: SCALDatabaseParameterized?, values: [String: Any]) {
        self.tableName = tableName
        self.selection = selection
        self.values = values
    }
}

public struct SCALQueryResult {
    public let error: SCALError?
    public let success: Bool
    public let result: [[String: Any]]?
    public init(error: SCALError? = nil, success: Bool = true, result: [[String: Any]]?) {
        self.error = error
        self.success = success
        self.result = result
    }
}


/// Secure database Interface
public protocol SCALSecureDatabaseInterface {
    
    func initDatabase(name: String) throws

    func createSecureTable(query: SCALCreateTableQuery) -> SCALQueryResult

    func selectFromSecureDatabase(query: SCALSelectQuery) -> SCALQueryResult

    func insertIntoSecureDatabase(query: SCALInsertQuery) -> Int
    
    func updateFromSecureDatabase(query: SCALUpdateQuery) -> Int

    func deleteFromSecureDatabase(query: SCALDeleteQuery) -> Int
    
    func removeDatabase(name: String) -> Bool
}

public class SCALSecureDatabase {

    private let secureDatabase: SCALSecureDatabaseInterface
    
    public init(secureDatabase: SCALSecureDatabaseInterface) {
        self.secureDatabase = secureDatabase
    }
    
    public func createSecureTable(query: SCALCreateTableQuery) -> SCALQueryResult {
        self.secureDatabase.createSecureTable(query: query)
    }
    
    public func selectFromSecureDatabase(query: SCALSelectQuery) -> SCALQueryResult {
        self.secureDatabase.selectFromSecureDatabase(query: query)
    }
    
    public func insertIntoSecureDatabase(query: SCALInsertQuery) -> Int {
        self.secureDatabase.insertIntoSecureDatabase(query: query)
    }
    
    public func updateFromSecureDatabase(query: SCALUpdateQuery) -> Int {
        self.secureDatabase.updateFromSecureDatabase(query: query)
    }
    
    public func deleteFromSecureDatabase(query: SCALDeleteQuery) -> Int {
        self.secureDatabase.deleteFromSecureDatabase(query: query)
    }
    
    public func removeDatabase(name: String) -> Bool {
        self.secureDatabase.removeDatabase(name: name)
    }

}

