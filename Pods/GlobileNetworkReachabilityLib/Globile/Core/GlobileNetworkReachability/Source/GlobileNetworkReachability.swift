//
import Alamofire

public protocol NetworkActivityObserverDelegate {
    func didChangeReachability(isReachable: Network)
}

public class NetworkActivityObserver {
    let manager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
    var networkObject: Network?
    public var delegate: NetworkActivityObserverDelegate?
    public var callBackName: String?

    public init() {
        start()
    }

    func start() {
        DispatchQueue.global(qos: .utility).async {
            if let manager = self.manager {
                manager.listener = { status in
                    switch status {
                    case .notReachable:
                        self.networkObject = Network(reachability: false, status: .unreachable, callbackName: self.callBackName)
                        self.delegate?.didChangeReachability(isReachable: self.networkObject!)
                    case .unknown :
                        self.networkObject = Network(reachability: true, status: .unknown, callbackName: self.callBackName)
                        self.delegate?.didChangeReachability(isReachable: self.networkObject!)
                    case .reachable(.ethernetOrWiFi):
                        self.networkObject = Network(reachability: true, status: .wifi, callbackName: self.callBackName)
                        self.delegate?.didChangeReachability(isReachable: self.networkObject!)
                    case .reachable(.wwan):
                        self.networkObject = Network(reachability: true, status: .wwan, callbackName: self.callBackName)
                        self.delegate?.didChangeReachability(isReachable: self.networkObject!)
                    }
                }
                manager.startListening()
            }
        }
    }
}
