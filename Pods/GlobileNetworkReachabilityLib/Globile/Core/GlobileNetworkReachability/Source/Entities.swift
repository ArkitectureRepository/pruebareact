public enum Status: String {
    case unreachable = "Unreachable", wifi = "WiFi", wwan = "Cellular", unknown = "Unknown"
}

public struct Network {
    public var reachability: Bool?
    public var status: Status?
    public var callbackName: String?

    public init(reachability: Bool, status: Status, callbackName: String?) {
        self.reachability = reachability
        self.status = status
        self.callbackName = callbackName
    }
}
