//
//  SensorsEntities.swift
//

// MARK: Native response.
public struct SensorsStatus {
    public var sensorType: SensorType
    public var sensorData: Any?
    public var active: Bool?
    init(sensorType: SensorType, sensorData: Any?, active: Bool?) {
        self.sensorType = sensorType
        self.sensorData = sensorData
        self.active = active
    }
}


// MARK: Struct for decode.
public struct SensorsRequest: Codable {
    var sensorCode: Int
    var operation: Int

    init(sensorCode: Int, operation: Int) {
        self.sensorCode = sensorCode
        self.operation = operation
    }
}

public enum SensorsOperation: Int, Codable {
    case START = 1
    case STOP = 0
}

public struct SensorsResult: Codable {
    var sensorCode: Int
    var sensorData: String?
    var active: Bool?
    init(sensorCode: Int, sensorData: String?, active: Bool?) {
        self.sensorCode = sensorCode
        self.sensorData = sensorData
        self.active = active
    }
}

public struct SensorsError: Codable {
    var code: Int = -1 // Error code
}

// MARK: Encoder/Decoder
public extension SensorsLib {

    static func decode<T: Codable>(_ json: String, type: T.Type) throws -> T {

        guard let data = json.data(using: .utf8) else {
            throw NSError(domain: "Unable to parse action", code: 1, userInfo: nil)
        }

        return try JSONDecoder().decode(T.self, from: data)
    }

    static func encode<T: Codable>(_ result: T) throws -> String {
        let data = try JSONEncoder().encode(result)
        if let result = String(data: data, encoding: .utf8) {
            return result
        }

        throw NSError(domain: "JSON could't be encoded into a string.", code: 1, userInfo: nil)
    }
}
