//
//  ProximitySensor.swift
//

import Foundation

public class ProximitySensor {

    var proximityCompletionHandler: ((SensorsStatus) -> Void)?
    var hybridProximityCompletionHandler: ((String) -> Void)?

    public func enableHybridProximityMonitor(enabled: Bool, completion: @escaping (String) -> Void) {
        let device = getDeviceAndInit(enabled: enabled)
        if device.isProximityMonitoringEnabled {
            hybridProximityCompletionHandler = completion
        } else {
            hybridProximityCompletionHandler = nil
        }

        sendProximitySensorStatus()

    }

    public func enableProximityMonitor(enabled: Bool, completion: @escaping (SensorsStatus) -> Void) {
        let device = getDeviceAndInit(enabled: enabled)
        if device.isProximityMonitoringEnabled {
            proximityCompletionHandler = completion
        } else {
            proximityCompletionHandler = nil
        }

        sendProximitySensorStatus()

    }

    private func getDeviceAndInit(enabled: Bool) -> UIDevice {
        let device = UIDevice.current
        device.isProximityMonitoringEnabled = enabled
        NotificationCenter.default.removeObserver(self)
        if device.isProximityMonitoringEnabled {
            NotificationCenter.default.addObserver(self, selector: #selector(sendProximitySensorStatus), name: NSNotification.Name(rawValue: "UIDeviceProximityStateDidChangeNotification"), object: device)
        }
        return device
    }

    @objc private func sendProximitySensorStatus() {
        let proxy = UIDevice.current.proximityState

        if let completion = proximityCompletionHandler {
            let proximityStatus = ProximityStatus(proximityStatus: proxy ? ProximityState.NEAR : ProximityState.FAR)
            let result = SensorsStatus(sensorType: .PROXIMITY_SENSOR, sensorData: proximityStatus, active: true)
            completion(result)

        } else if let completion = hybridProximityCompletionHandler {
            let proximityStatus = ProximityResult(proximityStatus: proxy ? ProximityState.NEAR : ProximityState.FAR, value: proxy ? "near" : "far")
            let sensorData = try? SensorsLib.encode(proximityStatus)
            let result = (try? SensorsLib.encode(SensorsResult(sensorCode: SensorType.PROXIMITY_SENSOR.rawValue, sensorData: sensorData, active: true)).replacingOccurrences(of: "\\\"", with: "\"")) ?? ""
            completion(result)

        }
    }

}

// MARK: Proximity sensor entities.

public enum ProximityState: Int, Codable {
    case NEAR = 1
    case FAR = 0
}

public struct ProximityStatus {
    public var proximityStatus: ProximityState
    init(proximityStatus: ProximityState) {
        self.proximityStatus = proximityStatus
    }
}

struct ProximityResult: Codable {
    var proximityStatus: Int
    var value: String
    init(proximityStatus: ProximityState, value: String) {
            self.proximityStatus = proximityStatus.rawValue
            self.value = value
        }
}
