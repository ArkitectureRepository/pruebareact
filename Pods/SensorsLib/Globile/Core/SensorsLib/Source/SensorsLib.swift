//
//  SensorsLib.swift
//

import Foundation

/**
Sensors codes
*/
public enum SensorType: Int {
    case PROXIMITY_SENSOR = 1
}

/**
 Main class of component SensorsLib
 */
public class SensorsLib: NSObject {

    internal static var sharedProximity = ProximitySensor()

    /// Function to perform hybrid actions
    ///
    /// - Parameter json: values to perform the hybrid action
    /// - Returns: output of the hybrid action in completion handler
    public class func hybridInterface(_ json: String, completionHandler: @escaping (String) -> Void) {

        do {

            let request = try SensorsLib.decode(json, type: SensorsRequest.self)
            let sensorCode = request.sensorCode
            let operation = request.operation

            switch sensorCode {
            case SensorType.PROXIMITY_SENSOR.rawValue:
                if operation == SensorsOperation.STOP.rawValue {
                    sharedProximity.enableHybridProximityMonitor(enabled: false) { _ in }
                    let result = try SensorsLib.encode(SensorsResult(sensorCode: sensorCode, sensorData: nil, active: false))
                    completionHandler(result)
                } else if operation == SensorsOperation.START.rawValue {
                    sharedProximity.enableHybridProximityMonitor(enabled: true) { result in
                        completionHandler(result)
                    }
                }

            default:
                let result = try SensorsLib.encode(SensorsResult(sensorCode: sensorCode, sensorData: "Incorrect sensor code", active: nil))
                completionHandler(result)
            }
        } catch let error {
            print(error.localizedDescription)
            completionHandler((try? SensorsLib.encode(SensorsError())) ?? "")
        }

    }

    /// Function to perform native actions
    ///
    /// - Parameters
    ///      sensorType: sensor selected
    ///      operation: operation to perform
    /// - Returns: output of the hybrid action
    public func sensorOperation(sensorType: SensorType, operation: SensorsOperation, completionHandler: @escaping (SensorsStatus) -> Void) {

        switch sensorType {
        case .PROXIMITY_SENSOR:
            if operation == .STOP {
                SensorsLib.sharedProximity.enableProximityMonitor(enabled: false) { _ in }
                let result = SensorsStatus(sensorType: sensorType, sensorData: nil, active: false)
                completionHandler(result)
            } else if operation == .START {
                SensorsLib.sharedProximity.enableProximityMonitor(enabled: true) { result in
                    completionHandler(result)
                }
            }

        }

    }
}
