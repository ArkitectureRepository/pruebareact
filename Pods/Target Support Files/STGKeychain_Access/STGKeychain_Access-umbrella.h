#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "STGKeychain_Access.h"

FOUNDATION_EXPORT double STGKeychain_AccessVersionNumber;
FOUNDATION_EXPORT const unsigned char STGKeychain_AccessVersionString[];

