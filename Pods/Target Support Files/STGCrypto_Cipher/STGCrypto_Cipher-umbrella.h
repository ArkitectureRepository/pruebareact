#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "GMEllipticCurveCrypto+hash.h"
#import "GMEllipticCurveCrypto.h"
#import "STGCrypto_Cipher.h"

FOUNDATION_EXPORT double STGCrypto_CipherVersionNumber;
FOUNDATION_EXPORT const unsigned char STGCrypto_CipherVersionString[];

