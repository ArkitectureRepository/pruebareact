#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "STGSecure_Storage.h"

FOUNDATION_EXPORT double STGSecure_StorageVersionNumber;
FOUNDATION_EXPORT const unsigned char STGSecure_StorageVersionString[];

