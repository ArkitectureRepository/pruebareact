//
//  ViewControllerExtension.swift
//  BranchLocator
//
//  Created by Daniel Rincon on 10/06/2019.
//

import Foundation


func addNavBarImage(imageName: String, navigationItem: UINavigationItem) {
    
    
    let imageView = UIImageView.init(image: UIImage.init(named: imageName))
    imageView.contentMode = .scaleAspectFit
    imageView.translatesAutoresizingMaskIntoConstraints = false
    
    let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 16))
    view.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(imageView)
    
    navigationItem.titleView = view
    imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    imageView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: CGFloat(0.6)).isActive = true
    
    view.heightAnchor.constraint(equalTo: navigationItem.titleView!.heightAnchor).isActive = true
    view.centerXAnchor.constraint(equalTo: navigationItem.titleView!.centerXAnchor).isActive = true
    view.centerYAnchor.constraint(equalTo: navigationItem.titleView!.centerYAnchor).isActive = true
}
