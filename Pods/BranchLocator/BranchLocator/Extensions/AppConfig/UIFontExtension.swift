import UIKit
import SantanderUIKitLib

extension UIFont {
//    public static func santander_HeadlineBold(ofSize size: CGFloat) -> UIFont {
//        let font = UIFont(name: "SantanderHeadline-Bold", size: size)!
//        return font
//    }
//
//    public static func santander_HeadlineBoldIt(ofSize size: CGFloat) -> UIFont {
//        let font = UIFont(name: "SantanderHeadline-BoldIt", size: size)!
//        return font
//    }
//    
//    public static func santander_HeadlineIt(ofSize size: CGFloat) -> UIFont {
//        let font = UIFont(name: "SantanderHeadline-It", size: size)!
//        return font
//    }
//
//    public static func santander_HeadlingRegular(ofSize size: CGFloat) -> UIFont {
//        let font = UIFont(name: "SantanderHeadline-Regular", size: size)!
//        return font
//    }
//
//    public static func santander_TextBold(ofSize size: CGFloat) -> UIFont {
//        let font = UIFont(name: "SantanderText-Bold", size: size)!
//        return font
//    }
//
//    public static func santander_TextBoldItalic(ofSize size: CGFloat) -> UIFont {
//        let font = UIFont(name: "SantanderText-BoldItalic", size: size)!
//        return font
//    }
//
//    public static func santander_TextItalic(ofSize size: CGFloat) -> UIFont {
//        let font = UIFont(name: "SantanderText-Italic", size: size)!
//        return font
//    }
//
//    public static func santander_TextLight(ofSize size: CGFloat) -> UIFont {
//        let font = UIFont(name: "SantanderText-Light", size: size)!
//        return font
//    }
//
//
//    public static func santander_TextLightItalic(ofSize size: CGFloat) -> UIFont {
//        let font = UIFont(name: "SantanderText-LightItalic", size: size)!
//        return font
//    }
//
//    public static func
//        santander_TextRegular(ofSize size: CGFloat) -> UIFont {
//        let font = UIFont(name: "SantanderText-Regular", size: size)!
//        return font
//    }

    /*public static func openSans_Bold(ofSize size: CGFloat) -> UIFont {
        let font = UIFont(name: "OpenSans-Bold", size: size)!
        return font
    }
    
    public static func openSans_BoldItalic(ofSize size: CGFloat) -> UIFont {
        let font = UIFont(name: "OpenSans-BoldItalic", size: size)!
        return font
    }
    
    public static func openSans_ExtraBold(ofSize size: CGFloat) -> UIFont {
        let font = UIFont(name: "OpenSans-ExtraBold", size: size)!
        return font
    }
    
    public static func openSans_ExtraBoldItalic(ofSize size: CGFloat) -> UIFont {
        let font = UIFont(name: "OpenSans-ExtraBoldItalic", size: size)!
        return font
    }
    
    public static func openSans_Italic(ofSize size: CGFloat) -> UIFont {
        let font = UIFont(name: "OpenSans-Italic", size: size)!
        return font
    }
    
    public static func openSans_Light(ofSize size: CGFloat) -> UIFont {
        let font = UIFont(name: "OpenSans-Light", size: size)!
        return font
    }
    
    public static func openSans_LightItalic(ofSize size: CGFloat) -> UIFont {
        let font = UIFont(name: "OpenSans-LightItalic", size: size)!
        return font
    }
    
    public static func openSans_Regular(ofSize size: CGFloat) -> UIFont {
        let font = UIFont(name: "OpenSans-Regular", size: size)!
        return font
    }
    
    public static func openSans_SemiBold(ofSize size: CGFloat) -> UIFont {
        let font = UIFont(name: "OpenSans-SemiBold", size: size)!
        return font
    }
    
    public static func openSans_SemiBoldItalic(ofSize size: CGFloat) -> UIFont {
        let font = UIFont(name: "OpenSans-SemiBoldItalic", size: size)!
        return font
    }
    */
}



enum DetailCardCellAndViewThemeFont {
    
    case headliners
    case bodyText
    case subHeadings
    case callButton
    case appointmentButton
    case blueButtons
    case closestBranchLabel
    case santanderLbl
    case branchInfoLbl
    case streetDataLbl
    case rightAndLeftStatusLabelTitle
    case rightAndLeftStatusLabelBody
    case daysOfWeek
    case openingTimesOfWeek
    case statusBranchBranchView
    case newAddtionalInfoForFirstRelatedPOITitle
    case newAddtionalInfoForFirstRelatedPOIBody
    case newAddtionalInfoForMainRelatedPOITitle
    case newAddtionalInfoForMainRelatedPOIBody
    case newAddtionalInfoForMainRelatedPOIBodyFormat2
    case closingInLabel
    
}

extension DetailCardCellAndViewThemeFont {
    var value: UIFont {
        var fonts: UIFont
        switch self {
        case .headliners:
            fonts = SantanderFont.regular(size: 14)
        case .bodyText:
            fonts = SantanderFont.regular(size: 14)
        case .subHeadings:
            fonts = SantanderFont.bold(size: 16)
        case .callButton:
            fonts = SantanderFont.regular(size: 14)
        case .appointmentButton:
            fonts = SantanderFont.regular(size: 14)
        case .blueButtons:
            fonts = SantanderFont.regular(size: 14)
        case .closestBranchLabel:
            fonts = SantanderFont.headlineBold(size: 14)
        case .santanderLbl:
            fonts = SantanderFont.bold(size: 16)
        case .branchInfoLbl:
            fonts = SantanderFont.light(size: 14)
        case .streetDataLbl:
            fonts = SantanderFont.regular(size: 14)
        case .rightAndLeftStatusLabelTitle:
            fonts = SantanderFont.bold(size: 15)
        case .rightAndLeftStatusLabelBody:
            fonts = SantanderFont.regular(size: 15)
        case .daysOfWeek:
            fonts = SantanderFont.bold(size: 15)
        case .openingTimesOfWeek:
            fonts = SantanderFont.regular(size: 15)
        case .statusBranchBranchView:
            fonts = SantanderFont.regular(size: 14)
        case .newAddtionalInfoForFirstRelatedPOITitle:
            fonts = SantanderFont.regular(size: 15)
        case .newAddtionalInfoForFirstRelatedPOIBody:
            fonts = SantanderFont.regular(size: 15)
        case .newAddtionalInfoForMainRelatedPOITitle:
            fonts = SantanderFont.regular(size: 15)
        case .newAddtionalInfoForMainRelatedPOIBody:
            fonts = SantanderFont.regular(size: 15)
        case .newAddtionalInfoForMainRelatedPOIBodyFormat2:
            fonts = SantanderFont.bold(size: 15)
        case .closingInLabel:
            fonts = SantanderFont.regular(size: 14)
        }
        return fonts
    }
}


enum MapViewControllerThemeFont {
    case searchAgainButton
    case buttonFilterAttrString
}

extension MapViewControllerThemeFont {
    var value: UIFont {
        var fonts: UIFont
        switch  self {
        case .searchAgainButton:
            fonts = SantanderFont.lightItalic(size: 14)
        case .buttonFilterAttrString:
            fonts = SantanderFont.regular(size: 16)
        }
        return fonts
    }
}



enum  MapBarContainerViewFont {
    case rightLeftLabelsSelected
    case rightLeftLabelsUnselected
}


extension MapBarContainerViewFont {
    var value: UIFont {
        var fonts: UIFont
        switch self {
        case .rightLeftLabelsSelected:
            fonts = SantanderFont.bold(size: 16)
        case .rightLeftLabelsUnselected:
            fonts = SantanderFont.regular(size: 16)
        }
        return fonts
    }
}


enum  DetailButtonsSectionTableViewFont {
    case rightLeftLabelsSelected
    case rightLeftLabelsUnselected
}



extension DetailButtonsSectionTableViewFont {
    var value: UIFont {
        var fonts: UIFont
        switch self {
        case .rightLeftLabelsSelected:
            fonts = SantanderFont.bold(size: 16)
        case .rightLeftLabelsUnselected:
            fonts = SantanderFont.regular(size: 16)
        }
        return fonts
    }
}


enum  FilterTableViewCellFont {
    case titleUnselected
    case titleSelected
}

extension FilterTableViewCellFont {
    var value: UIFont {
        var fonts: UIFont
        switch self {
        case .titleUnselected:
            fonts = SantanderFont.regular(size: 16)
        case .titleSelected:
            fonts = SantanderFont.bold(size: 18)
        }
        return fonts
    }
}


enum FilterTableViewFont {
    case filterTitle
    case clearButton
    case headerText
    case applyButtonTitle
}
extension FilterTableViewFont {
    var value: UIFont {
        var fonts: UIFont
        switch self {
        case .filterTitle:
            fonts = SantanderFont.bold(size: 16)
        case .clearButton:
            fonts = SantanderFont.regular(size: 12)
        case .headerText:
            fonts = SantanderFont.bold(size: 16)
        case .applyButtonTitle:
            // there is no value listed in sketch or in xcode for this?
            fonts = SantanderFont.bold(size: 16)
        }
        return fonts
    }
}


enum ListViewFont {
    case title
    case subtitle
    case distanceLabel
}

extension ListViewFont {
    var value: UIFont{
        var fonts: UIFont
        switch self {
        case .title:
           fonts = SantanderFont.bold(size: 16)
        case .subtitle:
            fonts = SantanderFont.regular(size: 15)
        case .distanceLabel:
            fonts = SantanderFont.regular(size: 14)
        }
        return fonts
    }
}
