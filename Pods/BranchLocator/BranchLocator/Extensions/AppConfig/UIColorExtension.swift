import UIKit
import SantanderUIKitLib

extension CGColor {
    func toUIColor() -> UIColor {
        return UIColor(cgColor: self)
    }
}

extension UIColor {
    
    @nonobjc class var santanderRed: UIColor {
        return .globileSantanderRed
    }
    
    @nonobjc class var bostonRed: UIColor {
        return .globileBostonRed
    }
    
    @nonobjc class var rubyRed: UIColor {
        return .globileRubyRed
    }
    
    @nonobjc class var boringGreen: UIColor {
        return .globileSuccess
    }
    
    @nonobjc class var mediumGrey: UIColor {
        return .globileMediumGrey
    }
    
    @nonobjc class var lightGrey: UIColor {
        return .globileLightGrey
    }
    
    @nonobjc class var darkGrey: UIColor {
        return .globileDarkGrey
    }
    
    @nonobjc class var paleGrey: UIColor {
        return .globileBackground
    }
    
    @nonobjc class var warningYellow: UIColor {
        return .globileYellow
    }
    
    @nonobjc class var limeGreen: UIColor {
        return .globileLimeGreen
    }
    
    @nonobjc class var turquoise: UIColor {
        return .globileTurquoise
    }
    
    @nonobjc class var purple: UIColor {
        return .globilePurple
    }
    
    @nonobjc class var blue: UIColor {
        return .globileBlue
    }
    
    @nonobjc class var sky: UIColor {
        return .globileLightSky
    }
    @nonobjc class var mediumSky: UIColor {
        return .globileMediumSky
    }
    @nonobjc class var darkSky: UIColor {
        return .globileDarkSky
    }
    
    @nonobjc class var popularMagenta: UIColor {
        return UIColor(red: 229 / 255, green: 0, blue: 83 / 255, alpha: 1.0)
    }
    
    static func fromHex(_ hexColor: String) -> UIColor {
        var rgbValue: UInt32 = 0
        Scanner(string: hexColor.replacingOccurrences(of: "#", with: "")).scanHexInt32(&rgbValue)
        
        let components = (
            R: CGFloat((rgbValue >> 16) & 0xff) / 255,
            G: CGFloat((rgbValue >> 08) & 0xff) / 255,
            B: CGFloat((rgbValue >> 00) & 0xff) / 255
        )
        
        let cgColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [components.R, components.G, components.B, 1])!
        
        return cgColor.toUIColor()
    }
    
    
    
}


enum DetailCardCellAndViewThemeColor {
    
    case headliners
    case bodyText
    case subHeadings
    case iconTints
    case iconBackgrounds
    case tableViewSeperators
    case callButton
    case appointmentButton
    case buttonBackground
    case blueButtons
    case closestBranchLabel
    case santanderLbl
    case branchInfoLbl
    case streetDataLbl
    case rightAndLeftStatusLabelTitle
    case rightAndLeftStatusLabelBody
    case daysOfWeek
    case openingTimesOfWeek
    case statusBranchBranchView
    case newAddtionalInfoForFirstRelatedPOITitle
    case newAddtionalInfoForFirstRelatedPOIBody
    case newAddtionalInfoForMainRelatedPOITitle
    case newAddtionalInfoForMainRelatedPOIBody
    case newAddtionalInfoForMainRelatedPOIBodyFormat1
    case dragButton
    case closingInLabel
    case closingLabelCircleViewOrange
    case closingLabelCircleViewRed
    case closingLabelCircleViewGreen

}

extension DetailCardCellAndViewThemeColor {
    var value: UIColor {
        var colors = UIColor.clear
        switch self {
        case .headliners:
            colors = UIColor.mediumGrey
        case .bodyText:
            colors = UIColor.darkGrey
        case .subHeadings:
            colors = UIColor.darkGrey
        case .iconTints:
            colors = UIColor.santanderRed
        case .iconBackgrounds:
            colors = UIColor.globileWhite
        case .tableViewSeperators:
            colors = UIColor.mediumSky
        case .callButton:
            colors = UIColor.santanderRed
        case .appointmentButton:
            colors = UIColor.santanderRed
        case .buttonBackground:
            colors = UIColor.globileWhite
        case .blueButtons:
            colors = UIColor.blue
        case .closestBranchLabel:
            colors = UIColor.santanderRed
        case .santanderLbl:
            colors = UIColor.mediumGrey
        case .branchInfoLbl:
            colors = UIColor.mediumGrey
        case .streetDataLbl:
            colors = UIColor.mediumGrey
        case .rightAndLeftStatusLabelTitle:
            colors = UIColor.darkGrey
        case .rightAndLeftStatusLabelBody:
            colors = UIColor.mediumGrey
        case .daysOfWeek:
            colors = UIColor.darkGrey
        case .openingTimesOfWeek:
            colors = UIColor.darkGrey
        case .statusBranchBranchView:
            colors = UIColor.darkGrey
        case .newAddtionalInfoForFirstRelatedPOITitle:
            colors = UIColor.darkGrey
        case .newAddtionalInfoForFirstRelatedPOIBody:
            colors = UIColor.darkGrey
        case .newAddtionalInfoForMainRelatedPOITitle:
            colors = UIColor.darkGrey
        case .newAddtionalInfoForMainRelatedPOIBody:
            colors = UIColor.darkGrey
        case .newAddtionalInfoForMainRelatedPOIBodyFormat1:
            colors = UIColor.santanderRed
        case .dragButton:
            colors = UIColor.darkGrey
        case .closingInLabel:
            colors = UIColor.darkGrey
        case .closingLabelCircleViewOrange:
            colors = UIColor.warningYellow
        case .closingLabelCircleViewRed:
            colors = UIColor.santanderRed
        case .closingLabelCircleViewGreen:
            colors = UIColor.boringGreen
        }
        return colors
    }
}


enum MapViewControllerThemeColor {
    case searchAgainButtonSelected
    case searchAgainButtonUnselected
    case buttonFilterAttrString
    case locationMapButtonTintColor
    case locationMapButtonWhenInUse
    case locationMapButtonBackgroundColor
    case locationMapButtonNotDetermined
    case iconColors
    case filterIcon
    case clusterViewBackground
    case clusterViewTextColor
}

extension MapViewControllerThemeColor {
    var value: UIColor {
        var colors = UIColor.clear
        switch  self {
            
        case .searchAgainButtonSelected:
            colors = UIColor.mediumGrey
        case .searchAgainButtonUnselected:
            colors = UIColor.lightGrey
        case .buttonFilterAttrString:
            colors = UIColor.mediumGrey
        case .locationMapButtonTintColor:
            colors = UIColor.darkGrey
        case .locationMapButtonWhenInUse:
            colors = UIColor.darkGrey
        case .locationMapButtonBackgroundColor:
            colors = UIColor.white
        case .locationMapButtonNotDetermined:
            colors = UIColor.lightGrey
        case .iconColors:
            colors = UIColor.santanderRed
        case .filterIcon:
            colors = UIColor.mediumGrey
        case .clusterViewBackground:
            colors = UIColor.white
        case .clusterViewTextColor:
            colors = UIColor.santanderRed
        }
        return colors
    }
}

enum  MapBarContainerViewColor {
    case bottomSeperatorView
    case bottomSeperatorViewSelected
    case bottomSeperatorViewUnselected
    case centerView
    case rightLeftLabelsSelected
    case rightLeftLabelsUnselected
}

extension MapBarContainerViewColor {
    var value: UIColor {
        var colors = UIColor.clear
        
        switch self {
            
        case .bottomSeperatorView:
            colors = UIColor.mediumSky
        case .bottomSeperatorViewSelected:
            colors = UIColor.santanderRed
        case .bottomSeperatorViewUnselected:
            colors = UIColor.mediumSky
        case .centerView:
            colors = UIColor.mediumSky
        case .rightLeftLabelsSelected:
            colors = UIColor.darkGrey
        case .rightLeftLabelsUnselected:
            colors = UIColor.mediumGrey
            
        }
        return colors
    }
}

enum  DetailButtonsSectionTableViewColor {
    case bottomSeperatorView
    case bottomSeperatorViewSelected
    case bottomSeperatorViewUnselected
    case centerView
    case rightLeftLabelsSelected
    case rightLeftLabelsUnselected
}

extension DetailButtonsSectionTableViewColor {
    var value: UIColor {
        var colors = UIColor.clear
        
        switch self {
            
        case .bottomSeperatorView:
            colors = UIColor.mediumSky
        case .bottomSeperatorViewSelected:
            colors = UIColor.santanderRed
        case .bottomSeperatorViewUnselected:
            colors = UIColor.mediumSky
        case .centerView:
            colors = UIColor.mediumSky
        case .rightLeftLabelsSelected:
            colors = UIColor.darkGrey
        case .rightLeftLabelsUnselected:
            colors = UIColor.mediumGrey
            
        }
        return colors
    }
}

enum  FilterTableViewCellColor {
    case titleUnselected
    case titleSelected
    case iconBorderSelected
    case iconSelectedBackground
    case iconBorderUnselected
    case tableViewCellBackground
}

extension FilterTableViewCellColor {
    var value: UIColor {
        var colors = UIColor.clear
        switch self {
        case .titleUnselected:
            colors = UIColor.mediumGrey
        case .titleSelected:
            colors = UIColor.darkGrey
        case .iconBorderSelected:
            colors = UIColor.santanderRed
        case .iconSelectedBackground:
            colors = UIColor.globileWhite
        case .iconBorderUnselected:
            colors = UIColor.mediumGrey
        case .tableViewCellBackground:
            colors = UIColor.paleGrey
        }
        return colors
    }
}

enum FilterTableViewColor {
    case filterTitle
    case applyButton
    case clearButton
    case filterTableViewBackgroundColor
    case headerText
    case customBorder
    case viewBackground
    case applyButtonTitle
}
extension FilterTableViewColor {
    var value: UIColor {
        var colors = UIColor.clear
        switch self {
        case .filterTitle:
            colors = UIColor.darkGrey
        case .applyButton:
            colors = UIColor.santanderRed
        case .clearButton:
            colors = UIColor.santanderRed
        case .filterTableViewBackgroundColor:
            colors = UIColor.paleGrey
        case .headerText:
            colors = UIColor.darkGrey
        case .customBorder:
            colors = UIColor.mediumSky
        case .viewBackground:
            colors = UIColor.paleGrey
        case .applyButtonTitle:
            colors = UIColor.globileWhite
        }
        return colors
    }
}

enum ListViewColor {
    case title
    case subtitle
    case arrow
    case distanceLabel
}

extension ListViewColor {
    var value: UIColor{
        var colors = UIColor.clear
        switch self {
        case .title:
            colors = UIColor.darkGrey
        case .subtitle:
            colors = UIColor.mediumGrey
        case .arrow:
            colors = UIColor.mediumGrey
        case .distanceLabel:
            colors = UIColor.mediumGrey
        }
        return colors
    }
}
