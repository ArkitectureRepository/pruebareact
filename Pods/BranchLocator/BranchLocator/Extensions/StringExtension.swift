//
//  StringExtension.swift
//  LocatorApp
//
//  Created by Ivan Cabezon on 23/8/18.
//  Copyright © 2018 Globile. All rights reserved.
//

import Foundation
import UIKit

extension String {
	
	var firstUppercased: String {
		guard let first = first else { return "" }
		return String(first).uppercased() + dropFirst()
	}
	var firstCapitalized: String {
		guard let first = first else { return "" }
		return String(first).capitalized + dropFirst()
	}
	
// you can use this if your string comes in a big block and is seperated in /n
    func toUppercaseAtSentenceBoundary() -> String {
        var result = ""
        if self.contains("\n") {
            self.uppercased().enumerateSubstrings(in: self.startIndex..<self.endIndex, options: .bySentences) { (sub, _, _, _)  in
                result += String(sub!.prefix(1))
                result += String(sub!.dropFirst(1)).lowercased()
            }
        } else {
            result += String(self.prefix(1))
            result += String(self.dropFirst(1)).lowercased()
        }
        return result as String
    }
    
	func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
		let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
		let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [kCTFontAttributeName as NSAttributedString.Key: font], context: nil)
		
		return ceil(boundingBox.width)
	}
	
	func height(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
		let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
		let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [kCTFontAttributeName as NSAttributedString.Key: font], context: nil)
		
		return ceil(boundingBox.height)
	}
    
    func cutString() -> String {
        let index = self.index(self.startIndex, offsetBy: 3)
        let mySubString = self.prefix(upTo: index)
        let myString = String(mySubString)
        return myString
    }
    
    func convertHtml() -> NSAttributedString{
        guard let data = data(using: .utf16) else { return NSAttributedString() }
        do{
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
    }
	
    func replaceStringWithHTMLURLToURL() -> String {
        return self.replacingOccurrences(of:"<[^>]+>" , with: "", options: .regularExpression, range: nil)
    }
    
    func matchesWithURL() -> String {
        let types: NSTextCheckingResult.CheckingType = .link
        let detector = try? NSDataDetector(types: types.rawValue)
        
        guard let detect = detector else {
            return ""
        }
        
        let range = NSRange(self.startIndex..<self.endIndex, in: self)
        let matches = detect.matches(in: self, options: .reportCompletion, range: range)
        
        if matches.count > 0 {
            return "\(matches[0])"
        } else {
            return ""
        }
    }
}
