import Foundation
import MapKit
import ObjectMapper
import CoreLocation

public class BranchesAPIRepository {
    
    let branchesService = ServiceRequest()
    
    let regionsServer: [RegionServer] = [
        RegionServer(country: "United States", lat: 29.4247, long: -98.4935),
        RegionServer(country: "Netherlands", lat: 52.35, long: 4.9167)]

    public init() {
        
    }
    
    //	func getFees(forCountry country: String, completionHandler: @escaping (_: [Fee], _: Int) -> Void) {
    //		let url = AppConfiguration.baseURL.appendingPathComponent("atm/getSurchargefromCountry").absoluteString
    //
    //		branchesService.GET(with: url, params: ["country": country]) { data, statusCode, _ in
    //			guard let data = data,
    //				let dataString = String(data: data, encoding: String.Encoding.utf8) else {
    //					completionHandler([], statusCode)
    //					return
    //			}
    //
    //			DispatchQueue.main.async {
    //				if let feesResponse = Mapper<FeeResponse>().map(JSONString: dataString) {
    //					completionHandler(feesResponse.surcharges, statusCode)
    //				} else {
    //					completionHandler([], 0)
    //				}
    //			}
    //		}
    //	}
    
    public func findBranches(with location: CLLocationCoordinate2D,
                             filters: [Filter] = [],
                             numResults: Int? = nil,
                             country: String? = "ES", isCustomer: Bool? = false,
                             completionHandler: @escaping (_: [POI], _: Int) -> Void) {
        
        let lat = location.latitude
        let lon = location.longitude
        
        let country = country ?? "ES"
        let isCustomer = isCustomer ?? false
        
        // swiftlint:disable colon
        var params: [String : String] = ["country" : country, "customer" : isCustomer ? "true" : "false"]
        // swiftlint:enable colon
        
        if let numResults = numResults {
            params["config"] = "{\"resultsConfig\":{\"maxResults\":\(numResults),\"minRadius\":0.5,\"numElemPag\":10,\"maxRadius\":1000}, \"coords\":[\(lat), \(lon)]}"
        } else {
            params["config"] = "{\"coords\":[\(lat), \(lon)]}"
        }
        
        params = fill(params, with: filters)

        let countryName = closestServer(regions: regionsServer, closestToLocation: CLLocation(latitude: lat, longitude: lon))
        let url = AppConfiguration(fromCountry: countryName).getBaseURL().appendingPathComponent("find/defaultView").absoluteString
        
        branchesService.GET(with: url, params: params) { data, statusCode, _ in
            guard let data = data,
                let dataString = String(data: data, encoding: String.Encoding.utf8) else {
                    DispatchQueue.main.async {
                        completionHandler([], statusCode)
                    }
                    return
            }
            
            let pois = Mapper<POI>().mapArray(JSONString: dataString)
            let cleanPOIs = self.cleanDuplicates(poiArray: pois ?? [])
            let cleanSuperClosePOIs = self.refactorSuperClosePOIs(poiArray: cleanPOIs)
            
            DispatchQueue.main.async {
                completionHandler(cleanSuperClosePOIs, statusCode)
            }
        }
    }
    
    func closestServer(regions: [RegionServer], closestToLocation location: CLLocation) -> CountryServer? {
        if regions.isEmpty {
            return nil
        }
        var locations = [CLLocation]()
        for location in regions {
            locations.append(CLLocation(latitude: location.lat, longitude: location.long))
        }

        var closestLocation: CLLocation?
        var smallestDistance: CLLocationDistance?
        for loc in locations {
            let distance = loc.distance(from: location)
            if smallestDistance == nil || distance < smallestDistance! {
                closestLocation = loc
                smallestDistance = distance
            }
        }
        var countryName: String?
        let closestLat: Double = Double(closestLocation?.coordinate.latitude ?? 0)
        let closestLong: Double = Double(closestLocation?.coordinate.longitude ?? 0)
        for loc in regions {
            if closestLat == loc.lat && closestLong == loc.long {
                countryName = loc.country
            }
        }

        switch countryName {
        case CountryServer.unitedStates.rawValue:
            return CountryServer.unitedStates

        case CountryServer.netherlands.rawValue:
            return CountryServer.netherlands
            
        default:
            return CountryServer.netherlands
        }

    }
    
    func fill(_ params: [String: String], with filters: [Filter]) -> [String: String] {
        if filters.isEmpty {
            return params
        }
        
        var params = params
        
        var filtersStr = [String]()
        var typeStr = [String]()
        var subtypeStr = [String]()
        
        for filter in filters {
            switch filter {
            case .withdrawMoney: filtersStr.append("WITHDRAW")
            case .lowDenominationBill: filtersStr.append("LOW_DENOMINATION_BILL")
            case .withdrawWithoutCard: filtersStr.append("RETIRO_CON_CODIGO")
            case .coworkingSpaces: filtersStr.append("COWORKING_SPACES")
            case .wifi: filtersStr.append("WIFI")
            case .securityBox: filtersStr.append("SAFE_BOX")
            case .driveThru: filtersStr.append("DRIVE_THRU")
            case .wheelchairAccess: filtersStr.append("ACCESIBILITY")
            case .audioGuidance: filtersStr.append("AUDIO_GUIDANCE")
            case .contactLess: filtersStr.append("CONTACTLESS")
            case .opensEvenings: filtersStr.append("OPEN_EVENINGS")
            case .opensSaturdays: filtersStr.append("OPEN_SATURDAY")
            case .parking: filtersStr.append("PARKING")
            case .depositMoney: filtersStr.append("MULTICAJERO")
            case .partners:
                typeStr.append(Type.corresponsales.rawValue)
            case .individual:
                subtypeStr.append(SubTypeCode.universities.rawValue)
                subtypeStr.append(SubTypeCode.individuals.rawValue)    // No filter defined for individuals
            case .workcafe:
                subtypeStr.append(SubTypeCode.workCafe.rawValue)
            case .santanderSelect:
                subtypeStr.append(SubTypeCode.select.rawValue)
            case .privateBank:
                subtypeStr.append(SubTypeCode.privateBanking.rawValue)
            case .pymesEmpresas:
                subtypeStr.append(SubTypeCode.pyme.rawValue)
                subtypeStr.append(SubTypeCode.companies.rawValue)
            case .popularPastor:
                subtypeStr.append(SubTypeCode.popular.rawValue)
                subtypeStr.append(SubTypeCode.pastor.rawValue)
            case .santanderATM:
                subtypeStr.append(SubTypeCode.santanderATM.rawValue)
            case .otherATMs:
                subtypeStr.append(SubTypeCode.nonSantanderATM.rawValue)
            default:
                break
            }
        }
        
        if !filtersStr.isEmpty {
            params["filterAttributeList"] = filtersStr.joined(separator: ",")
        }
        
        if !typeStr.isEmpty {
            params["filterType"] = typeStr.joined(separator: ",")
        }
        
        if !subtypeStr.isEmpty {
            params["filterSubtype"] = subtypeStr.joined(separator: ",")
        }
        
        return params
    }
    
    // if it contains the poi it will find the index of the duplicate poi in the branches array and add to its relatedPOI property.
    
    func cleanDuplicates(poiArray: [POI]) -> [POI] {
        var branchesArray = poiArray.filter { poi -> Bool in
            return poi.objectType.code == .branch
        }

        let atmsArray = poiArray.filter { poi -> Bool in
            return poi.objectType.code == .atm
        }

        let corresponsalArray = poiArray.filter { poi -> Bool in
            return poi.objectType.code == .corresponsales
        }
        
        // this index is giving me back that the poi object is located as the 0 element in the array when its actually the first
        // its working with calle bravo murrilo 278-280 and the one ending in popular
        // the issue seems to be when we get the index.. its getting the wrong index for exisitng elements in array
        
        var poisWithoutDuplicates: [POI] = []
        
        for poi in atmsArray {
            if !branchesArray.contains(poi) {
                poisWithoutDuplicates.append(poi)
            } else if let idx = branchesArray.firstIndex(of: poi) {
                branchesArray[idx].relatedPOIs.append(poi)
            }
        }
        
        poisWithoutDuplicates.append(contentsOf: branchesArray)
        
        for poi in corresponsalArray {
            poisWithoutDuplicates.append(poi)
        }
    
        poisWithoutDuplicates = poisWithoutDuplicates.sorted(by: { poi1, poi2 -> Bool in
            return poi1.distanceInKM < poi2.distanceInKM
        })

        return poisWithoutDuplicates
    }
    
    func refactorSuperClosePOIs(poiArray: [POI]) -> [POI] {
        
        /*
            This method is because some pois are so close that mapkit wont display it
            and with clustering enabled it will make the cluster unclickable.
        
            logic: after POIs have been cleaned of any duplicates we run through that list to find any POIs
            that have a coordinate closer than 15.0 meters if thats the case we will create a new POI object
            and change its coordinate to a modified new distance of 0.0001 at both lat and lon values.
            In turn seperating the POIs so that they can both be seen and interacted with.
         */
        
        // contains a list of already modified poi
        var modfiedArray = [POI]()
       
        // we will return our new array. with our modified pois included
        var branchesArray = [POI]()
        
        let superCloseDistanceInMeters = 15.0
        var count = 0
        var mainAdded = false
       
        for comparisonPOI in poiArray {
            var closePOIS = [POI]()
            
            for poi in poiArray {
                if !modfiedArray.contains(where: {$0.code == comparisonPOI.code}) &&
                    !modfiedArray.contains(where: {$0.code == poi.code}) {
                    
                    if let lat = poi.location?.latitude ,
                        let lon = poi.location?.longitude,
                        let comparisonLat = comparisonPOI.location?.latitude,
                        let comparisonLon = comparisonPOI.location?.longitude {
                        
                        let coordinate0 = CLLocation(latitude: lat, longitude: lon)
                        let coordinate1 = CLLocation(latitude: comparisonLat, longitude: comparisonLon)
                        
                        // checking distance between the pois if its super close then we will add it to a new array
                        if coordinate0.distance(from: coordinate1) < superCloseDistanceInMeters && poi.code != comparisonPOI.code {
                            count += 1
                            closePOIS.append(poi)
                            if !mainAdded {
                                count += 1
                                mainAdded = true
                                closePOIS.append(comparisonPOI)
                            }
                        }
                    }
                }
            }
            mainAdded = false
            
            /* once added to array we we make a new poi object and pass new coordinates,
                    then append it to our modified array which contains a list of already modified pois so that they arent modified twice */
            
            if closePOIS.count > 1 {
                let modifiedPois = modifyClosePoisLocation(closePOIS: closePOIS)
                modfiedArray.append(contentsOf: modifiedPois)
                branchesArray.append(contentsOf: modifiedPois)

            } else {
                if !branchesArray.contains(where: {$0.code == comparisonPOI.code}) {
                    branchesArray.append(comparisonPOI)
                }
            }
        }
        return branchesArray
    }
    
    func modifyClosePoisLocation(closePOIS: [POI]) -> [POI] {
        var modifiedPois: [POI] = []
        let numClosePois = closePOIS.count
        var angle = 0.0
        var latitudeIncreaser = 0.0
        var longitudeIncreaser = 0.0
        var circleRadius = 0.000085
        let step = (2 * Double.pi) / Double(numClosePois-1)
        var firstLoc: CLLocationCoordinate2D?
        var modifiedLocation: CLLocationCoordinate2D?
        
        if numClosePois > 6 {
            circleRadius = 0.00015
        }
        
        for close in closePOIS {
            if firstLoc == nil {
                firstLoc = CLLocationCoordinate2D(latitude: close.location!.latitude, longitude: close.location!.longitude)
                modifiedLocation = firstLoc
            } else {
                latitudeIncreaser = Double(ceil( circleRadius * cos(angle) * 100000) / 100000)
                longitudeIncreaser = Double(ceil( circleRadius * sin(angle) * 100000) / 100000)
                angle += step
                modifiedLocation = CLLocationCoordinate2D(latitude: firstLoc!.latitude + latitudeIncreaser, longitude: firstLoc!.longitude + longitudeIncreaser)
            }
            let newPOI: POI = POI(poi: close, locationParam: modifiedLocation!)!
            
            modifiedPois.append(newPOI)
        }
        return modifiedPois
    }
}
