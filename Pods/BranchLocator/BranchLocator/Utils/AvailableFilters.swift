
import Foundation

public class AvailableFilters: Codable {
    
    public static func defaultAvailableFilters() -> [FilterType] {
        return [.mostPopular,
                .service,
                .facilities,
                .accessibility,
                .pointsOfInterest]
    }
    
    public static func defaultWithOutPopularAndOthers() -> [FilterType] {
        return [.mostPopular,
                .service,
                .facilities,
                .accessibility,
                .pointsOfInterestWithOutPopularAndOthers]
    }

}
