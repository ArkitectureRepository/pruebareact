
extension UIApplication {
    public class func urlSchemeLauncher(url: String) {
        if let url = URL(string: url) {
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.canOpenURL(url)
                }
            }
        }
    }

    public class func urlSchemeLauncher(urls: [String]) {
        urls.compactMap {URL(string: $0)}
            .filter { UIApplication.shared.canOpenURL($0)}
            .forEach {
                urlSchemeLauncher(url: $0.absoluteString)
        }
    }
}
