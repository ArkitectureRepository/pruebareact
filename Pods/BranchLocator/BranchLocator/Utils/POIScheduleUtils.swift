
import UIKit

class POIScheduleUtils {
    
    static func calculateRemainingHours(times: String?, isATM: Bool, simplified: Bool, completion: @escaping (_ showColor: UIColor, _ timeLeft: String)->()) {
        // some businesses only have one close time some two
        guard let time = times else {return}
        //para evitar calcular minutos de cierre en los cajeros
        if time == "00:00-23:59" || time == "00:00-24:00"  {
            let properties = colorLabelCoordinater(timeUntil: 62, simplified: simplified)
            completion(properties.0,properties.1)
            return
        }
        var timeStringsArray = [String]()
        var timesAsDatesArray = [Date]()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        formatter.timeZone = TimeZone.current
        // device time
        let stringDate = formatter.string(from: NSDate() as Date)
        let deviceTime = formatter.date(from: stringDate)!
        let seperateTimes = time.components(separatedBy: "\n")
        // seperate strings
        for timeSlots in seperateTimes {
            for timeSection in timeSlots.components(separatedBy: "-") {
                // now with the seperate times add to an array of time strings
                timeStringsArray.append(timeSection)
            }
        }
        // put all into dates
        for date in timeStringsArray {
            // turn strings into a date
            //comprobaciones para evitar crashes
            if let timesAsDate = formatter.date(from: date) {
                timesAsDatesArray.append(timesAsDate)
            } else if date == "24h" {
                let properties = colorLabelCoordinater(timeUntil: 62, simplified: simplified)
                completion(properties.0,properties.1)
                return
            } else {
                let properties = colorLabelCoordinater(timeUntil: -1, simplified: simplified)
                completion(properties.0,properties.1)
                return
            }
        }
        // array can contain a max of 4 values
        if timesAsDatesArray.count == 4 {
            // business has two opening and closing times
            if deviceTime < timesAsDatesArray[1] {
                // if user time is less than first closing time
                // then check if if user time is before first opening time
                if deviceTime < timesAsDatesArray[0] {
                    // is before opening time
                    let properties = colorLabelCoordinater(timeUntil: 0, simplified: false)
                    completion(properties.0,properties.1)
                } else {
                    let timeUntil = timesAsDatesArray[1].minutes(from: deviceTime)
                    let properties = colorLabelCoordinater(timeUntil: timeUntil, simplified: simplified)
                    completion(properties.0,properties.1)
                }
            } else if deviceTime < timesAsDatesArray[2] {
                // if now is before second opening time
                // it still hasnt opened from the first closing time
                let properties = colorLabelCoordinater(timeUntil: 0, simplified: false)
                completion(properties.0,properties.1)
            }else {
                if deviceTime < timesAsDatesArray[3] {
                    let timeUntil = timesAsDatesArray[3].minutes(from: deviceTime)
                    let properties = colorLabelCoordinater(timeUntil: timeUntil, simplified: simplified)
                    completion(properties.0,properties.1)
                } else {
                    // place is closed
                    let properties = colorLabelCoordinater(timeUntil: 0, simplified: false)
                    completion(properties.0,properties.1)
                }
            }
        } else {
            // business only has one opening and closing time
            if deviceTime < timesAsDatesArray[0] {
                // place hasnt opened yet
                let properties = colorLabelCoordinater(timeUntil: 0, simplified: false)
                completion(properties.0,properties.1)
            } else {
                // check if device time is before the closing date
                if deviceTime < timesAsDatesArray[1] {
                    let timeUntil = timesAsDatesArray[1].minutes(from: deviceTime)
                    let properties = colorLabelCoordinater(timeUntil: timeUntil, simplified: simplified)
                    completion(properties.0,properties.1)
                } else {
                    let properties = colorLabelCoordinater(timeUntil: 0, simplified: false)
                    completion(properties.0,properties.1)
                }
            }
        }
    }
    
    // TODO: localize these values closing in.
    static func colorLabelCoordinater(timeUntil: Int, simplified: Bool) -> (UIColor, String) {
        var circleColor = UIColor()
        var timeLbl = ""
        
        switch timeUntil {
        case 0:
            circleColor = DetailCardCellAndViewThemeColor.closingLabelCircleViewRed.value
            timeLbl = localizedString("bl_closed")
        case 1...60:
            circleColor = DetailCardCellAndViewThemeColor.closingLabelCircleViewOrange.value
            timeLbl = String(format: localizedString("bl_closing_in"), timeUntil)
            break
        case 61...:
            circleColor = DetailCardCellAndViewThemeColor.closingLabelCircleViewGreen.value
            timeLbl = simplified ? localizedString("bl_open_simply") : localizedString("bl_open_now")
            break
        case -1:
            circleColor = DetailCardCellAndViewThemeColor.closingLabelCircleViewRed.value
            timeLbl = localizedString("bl_closed")
            break
        default:
            circleColor = DetailCardCellAndViewThemeColor.closingLabelCircleViewGreen.value
            timeLbl = localizedString("bl_open_now")
            break
        }
        return (circleColor, timeLbl)
    }
    
}
