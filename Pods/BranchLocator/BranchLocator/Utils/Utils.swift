//
//  Utils.swift
//  LocatorApp
//
//  Created by vectoradmin on 28/8/18.
//  Copyright © 2018 Globile. All rights reserved.
//

import UIKit
import MapKit
import GlobileUtilsLib

func getLanguageOption(from dict: [String: Any]) -> String {
    if let region = Locale.current.languageCode?.lowercased(),
        let str = dict[region] as? String {
        return str
    } else if let str = dict["default"] as? String {
        return str
    } else {
        return ""
    }
}
public func navigateToDestination(latitude: Double, longitude: Double, navigationMaps: [Maps]) {
    
    let latitudePattern = "@LATITUDE"
    let longitudePattern = "@LONGITUDE"
   
    var urlSchemes: [String] = []
    
    for navigationMap in navigationMaps{
        let urlScheme = navigationMap.rawValue
        var urlSchemeFormated = urlScheme.replacingOccurrences(of: latitudePattern, with: "\(latitude)", options: .regularExpression)
        urlSchemeFormated = urlSchemeFormated.replacingOccurrences(of: longitudePattern, with: "\(longitude)", options: .regularExpression)
        urlSchemes.append(urlSchemeFormated)
    }
    urlSchemes.append("http://maps.apple.com/?daddr=\(latitude),\(longitude)")
    openWithNavigationApp(urlSchemes: urlSchemes)
    
}

private func openWithNavigationApp(urlSchemes: [String]){
    UIApplication.urlSchemeLauncher(urls: urlSchemes)
}

// https://developers.google.com/maps/documentation/urls/ios-urlscheme
// https://developers.google.com/waze/deeplinks/
// https://developer.apple.com/library/archive/featuredarticles/iPhoneURLScheme_Reference/MapLinks/MapLinks.html#//apple_ref/doc/uid/TP40007899-CH5-SW1
public enum Maps: String {
    case googleMaps = "comgooglemaps://?daddr=@LATITUDE,@LONGITUDE"
    case waze = "waze://?ll=@LATITUDE,@LONGITUDE&navigate=yes"
    case appleMaps = "http://maps.apple.com/?daddr=@LATITUDE,@LONGITUDE"
}

extension Maps: Codable {
    
    enum Key: CodingKey {
        case rawValue
    }
    enum CodingError: Error {
        case unknownValue
    }
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)
        let rawValue = try container.decode(Int.self, forKey: .rawValue)
        switch rawValue {
        case 0:
            self = .googleMaps
        case 1:
            self = .waze
        case 2:
            self = .appleMaps
        default:
            throw CodingError.unknownValue
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        switch self {
        case .googleMaps:
            try container.encode(0, forKey: .rawValue)
        case .waze:
            try container.encode(1, forKey: .rawValue)
        case .appleMaps:
            try container.encode(2, forKey: .rawValue)
        }
    }
}


