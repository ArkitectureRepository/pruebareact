import Foundation

public protocol FilterTypeProtocol {
	var title: String { get }
	var filters: [Filter] { get }
}

public enum FilterType: FilterTypeProtocol {
    case mostPopular
    case service
    case facilities
    case accessibility
    case pointsOfInterest
    case pointsOfInterestWithOutPopularAndOthers
	
    static let defaultFilters: [Filter] = []
	
    public var filters: [Filter] {
        switch self {
        case .mostPopular:
            return [
//				.available,
//				.noFee,
				.withdrawMoney,
				.depositMoney,
				.individual,
				.santanderATM,
				.workcafe
            ]
        case .service:
            return [.withdrawWithoutCard,
                    .lowDenominationBill,
                    .contactLess,
                    .opensEvenings,
                    .opensSaturdays
            ]
        case .facilities:
            return [.parking,
                    .coworkingSpaces,
                    .wifi,
                    .securityBox,
                    .driveThru
            ]
        case .accessibility:
            return [.wheelchairAccess,
                    .audioGuidance
            ]
        case .pointsOfInterest:
            return [.santanderSelect,
                    .privateBank,
                    .pymesEmpresas,
                    .popularPastor,
                    .partners,
                    .otherATMs
            ]
        case .pointsOfInterestWithOutPopularAndOthers:
        return [.santanderSelect,
                .privateBank,
                .pymesEmpresas,
                .partners]
        }
    }
    
    public var title: String {
        switch self {
        case .mostPopular:
            return localizedString("bl_most_popular")
        case .service:
            return localizedString("bl_services")
        case .facilities:
            return localizedString("bl_facilities")
        case .accessibility:
            return localizedString("bl_accessibility")
        case .pointsOfInterest:
            return localizedString("bl_points_of_interest")
        case .pointsOfInterestWithOutPopularAndOthers:
            return localizedString("bl_points_of_interest")
        }
    }
    
}

extension FilterType: Codable {

    enum Key: CodingKey {
        case rawValue
    }

    enum CodingError: Error {
        case unknownValue
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)
        let rawValue = try container.decode(Int.self, forKey: .rawValue)
        switch rawValue {
        case 0:
            self = .mostPopular
        case 1:
            self = .service
        case 2:
            self = .facilities
        case 3:
            self = .accessibility
        case 4:
            self = .pointsOfInterest
        case 5:
            self = .pointsOfInterestWithOutPopularAndOthers
        default:
            throw CodingError.unknownValue
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        switch self {
        case .mostPopular:
            try container.encode(0, forKey: .rawValue)
        case .service:
            try container.encode(1, forKey: .rawValue)
        case .facilities:
            try container.encode(2, forKey: .rawValue)
        case .accessibility:
            try container.encode(3, forKey: .rawValue)
        case .pointsOfInterest:
            try container.encode(4, forKey: .rawValue)
        case .pointsOfInterestWithOutPopularAndOthers:
            try container.encode(5, forKey: .rawValue)
        }
    }
}
