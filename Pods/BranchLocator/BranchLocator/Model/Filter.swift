import Foundation
import ObjectMapper

public enum Filter {
    case available				// Not available
    case noFee					// Not available
    case withdrawMoney
    case lowDenominationBill
    case individual
    case santanderATM  			// We can't filter by only santander ATMs since it doesn't exist a subtype santander atm and the ATM type englobes both
    case workcafe
    case withdrawWithoutCard
    case coworkingSpaces
    case wifi
    case securityBox
    case driveThru
    case wheelchairAccess
    case audioGuidance
    case santanderSelect
    case privateBank
    case pymesEmpresas
    case popularPastor
    case partners
    case otherATMs
	case depositMoney      		// Multicajero
    case contactLess
    case opensEvenings
    case opensSaturdays
    case parking
	// "PAY" filter not implemented YET
    func icon(selected: Bool) -> UIImage {
        switch self {
        case .otherATMs:
            if selected {
                return UIImage(resourceName: "Other_ATM_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "Other_ATM_GREY") ?? UIImage()
            }
        case .santanderATM:
            if selected {
                return UIImage(resourceName: "Santander_ATM_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "Santander_ATM_GREY") ?? UIImage()
            }
        case .partners:
            if selected {
                return UIImage(resourceName: "Partners_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "Partners_GREY") ?? UIImage()
            }
        case .popularPastor:
            return UIImage(resourceName: "popular") ?? UIImage()
        case .workcafe:
			if selected {
                return UIImage(resourceName: "Work_Cafe_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "Work_Cafe_GREY") ?? UIImage()
            }
        case .lowDenominationBill:
            if selected {
                return UIImage(resourceName: "Low_Banknotes_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "Low_Banknotes_GREY") ?? UIImage()
            }
        case .withdrawWithoutCard:
            if selected {
                return UIImage(resourceName: "Withdraw_Without_Card_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "Withdraw_Without_Card_GREY") ?? UIImage()
            }
        case .depositMoney:
            if selected {
                return UIImage(resourceName: "Deposit_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "Deposit_GREY") ?? UIImage()
            }
        case .contactLess:
            if selected {
                return UIImage(resourceName: "Card_Issuance_Instantly_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "Card_Issuance_Instantly_GREY") ?? UIImage()
            }
        case .opensEvenings:
            if selected {
                return UIImage(resourceName: "Open_Afternoons_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "Open_Afternoons_GREY") ?? UIImage()
            }
        case .opensSaturdays:
            if selected {
                return UIImage(resourceName: "Open_Saturdays_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "Open_Saturdays_GREY") ?? UIImage()
            }
        case .parking:
            if selected {
                return UIImage(resourceName: "Own_Parking_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "Own_Parking_GREY") ?? UIImage()
            }
        case .coworkingSpaces:
            if selected {
                return UIImage(resourceName: "Co-working_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "Co-working_GREY") ?? UIImage()
            }
        case .wifi:
            if selected {
                return UIImage(resourceName: "WiFi_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "WiFi_GREY") ?? UIImage()
            }
        case .securityBox:
            if selected {
                return UIImage(resourceName: "Security_Boxes_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "Security_Boxes_GREY") ?? UIImage()
            }
        case .driveThru:
            if selected {
                return UIImage(resourceName: "Drive_Thru_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "Drive_Thru_GREY") ?? UIImage()
            }
        case .wheelchairAccess:
            if selected {
                return UIImage(resourceName: "Wheelchair_Accessibility_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "Wheelchair_Accessibility_GREY") ?? UIImage()
            }
        case .audioGuidance:
            if selected {
                return UIImage(resourceName: "Audio_Guidance_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "Audio_Guidance_GREY") ?? UIImage()
            }
        case .noFee:
            if selected {
                return UIImage(resourceName: "No_Commissions_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "No_Commissions_GREY") ?? UIImage()
            }
        case .withdrawMoney:
            if selected {
                return UIImage(resourceName: "Withdrawal_RED") ?? UIImage()
            } else {
                return UIImage(resourceName: "Withdrawal_GREY") ?? UIImage()
            }
        case .available:
            if selected {
				return UIImage(resourceName: "active") ?? UIImage()
            }
        default:
			return UIImage(resourceName: "sanRed") ?? UIImage()
        }
		return UIImage()
    }
    var title: String {
        switch self {
        case .available:
            return localizedString("bl_now_available")
        case .noFee:
            return localizedString("bl_no_fee")
        case .withdrawMoney:
            return localizedString("bl_withdraw_money")
        case .lowDenominationBill:
            return localizedString("bl_low_denomination_bills")
        case .individual:
            return localizedString("bl_individuals")
        case .santanderATM:
            return localizedString("bl_santander_atms_filters")
        case .workcafe:
            return localizedString("bl_workcafe")
        case .withdrawWithoutCard:
            return localizedString("bl_withdraw_without_card")
        case .coworkingSpaces:
            return localizedString("bl_coworking_spaces_filters")
        case .wifi:
            return localizedString("bl_wifi_filters")
        case .securityBox:
            return localizedString("bl_security_box_filters")
        case .driveThru:
            return localizedString("bl_drive_thru")
        case .wheelchairAccess:
            return localizedString("bl_wheelchair_access")
        case .audioGuidance:
            return localizedString("bl_audio_guidance")
        case .santanderSelect:
            return localizedString("bl_select")
        case .privateBank:
            return localizedString("bl_private_banking")
        case .pymesEmpresas:
            return "\(localizedString("bl_pymes"))/\(localizedString("bl_companies"))"
        case .popularPastor:
            return "\(localizedString("bl_popular"))/\(localizedString("bl_pastor"))"
        case .partners:
            return localizedString("bl_partners")
        case .otherATMs:
            return localizedString("bl_other_atms")
        case .depositMoney:
            return localizedString("bl_deposit_money")
        case .contactLess:
            return localizedString("bl_contactless")
        case .opensEvenings:
            return localizedString("bl_opens_evenings")
        case .opensSaturdays:
            return localizedString("bl_opens_saturdays")
        case .parking:
            return localizedString("bl_parking_filters")
        }
    }
}
extension Filter: RawRepresentable {
	public init?(rawValue: String) {
        switch rawValue {
        case "available": self = .available
        case "noFee": self = .noFee
        case "withdrawMoney": self = .withdrawMoney
        case "lowDenominationBill": self = .lowDenominationBill
        case "individual": self = .individual
        case "santanderATM": self = .santanderATM
        case "workcafe": self = .workcafe
        case "withdrawWithoutCard": self = .withdrawWithoutCard
        case "coworkingSpaces": self = .coworkingSpaces
        case "wifi": self = .wifi
        case "securityBox": self = .securityBox
        case "driveThru": self = .driveThru
        case "wheelchairAccess": self = .wheelchairAccess
        case "audioGuidance": self = .audioGuidance
        case "santanderSelect": self = .santanderSelect
        case "privateBank": self = .privateBank
        case "pymesEmpresas": self = .pymesEmpresas
        case "popularPastor": self = .popularPastor
        case "partners": self = .partners
        case "otherATMs": self = .otherATMs
        case "depositMoney": self = .depositMoney
        case "contactLess": self = .contactLess
        case "opensEvenings": self = .opensEvenings
        case "opensSaturdays": self = .opensSaturdays
        case "parking": self = .parking
        default:
            self = .available
        }
    }
	public var rawValue: String {
        switch self {
        case .available:return "available"
        case .noFee: return "noFee"
        case .withdrawMoney: return "withdrawMoney"
        case .lowDenominationBill: return "lowDenominationBill"
        case .individual: return "individual"
        case .santanderATM: return "santanderATM"
        case .workcafe: return "workcafe"
        case .withdrawWithoutCard: return "withdrawWithoutCard"
        case .coworkingSpaces: return "coworkingSpaces"
        case .wifi: return "wifi"
        case .securityBox: return "securityBox"
        case .driveThru: return "driveThru"
        case .wheelchairAccess: return "wheelChairAccess"
        case .audioGuidance: return "audioGuidance"
        case .santanderSelect: return "santanderSelect"
        case .privateBank: return "privateBank"
        case .pymesEmpresas: return "pymesEmpresas"
        case .popularPastor: return "popularPastor"
        case .partners: return "partners"
        case .otherATMs: return "otherATMs"
        case .depositMoney: return "depositMoney"
        case .contactLess: return "contactLess"
        case .opensEvenings: return "opensEvenings"
        case .opensSaturdays: return "opensSaturdays"
        case .parking: return "ownParking"
        }
    }
}

extension Filter: Codable {

    enum Key: CodingKey {
        case rawValue
    }
    
    enum CodingError: Error {
        case unknownValue
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)
        let rawValue = try container.decode(Int.self, forKey: .rawValue)
        switch rawValue {
        case 0:
            self = .available
        case 1:
            self = .noFee
        case 2:
            self = .withdrawMoney
        case 3:
            self = .lowDenominationBill
        case 4:
            self = .individual
        case 5:
            self = .santanderATM
        case 6:
            self = .workcafe
        case 7:
            self = .withdrawWithoutCard
        case 8:
            self = .coworkingSpaces
        case 9:
            self = .wifi
        case 10:
            self = .securityBox
        case 11:
            self = .driveThru
        case 12:
            self = .wheelchairAccess
        case 13:
            self = .audioGuidance
        case 14:
            self = .santanderSelect
        case 15:
            self = .privateBank
        case 16:
            self = .pymesEmpresas
        case 17:
            self = .popularPastor
        case 18:
            self = .partners
        case 19:
            self = .otherATMs
        case 20:
            self = .depositMoney
        case 21:
            self = .contactLess
        case 22:
            self = .opensEvenings
        case 23:
            self = .opensEvenings
        case 24:
            self = .parking
        
        default:
            throw CodingError.unknownValue
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        switch self {
        case .available:
            try container.encode(0, forKey: .rawValue)
        case .noFee:
            try container.encode(1, forKey: .rawValue)
        case .withdrawMoney:
            try container.encode(2, forKey: .rawValue)
        case .lowDenominationBill:
            try container.encode(3, forKey: .rawValue)
        case .individual:
            try container.encode(4, forKey: .rawValue)
        case .santanderATM:
            try container.encode(5, forKey: .rawValue)
        case .workcafe:
            try container.encode(6, forKey: .rawValue)
        case .withdrawWithoutCard:
            try container.encode(7, forKey: .rawValue)
        case .coworkingSpaces:
            try container.encode(8, forKey: .rawValue)
        case .wifi:
            try container.encode(9, forKey: .rawValue)
        case .securityBox:
            try container.encode(10, forKey: .rawValue)
        case .driveThru:
            try container.encode(11, forKey: .rawValue)
        case .wheelchairAccess:
            try container.encode(12, forKey: .rawValue)
        case .audioGuidance:
            try container.encode(13, forKey: .rawValue)
        case .santanderSelect:
            try container.encode(14, forKey: .rawValue)
        case .privateBank:
            try container.encode(15, forKey: .rawValue)
        case .pymesEmpresas:
            try container.encode(16, forKey: .rawValue)
        case .popularPastor:
            try container.encode(17, forKey: .rawValue)
        case .partners:
            try container.encode(18, forKey: .rawValue)
        case .otherATMs:
            try container.encode(19, forKey: .rawValue)
        case .depositMoney:
            try container.encode(20, forKey: .rawValue)
        case .contactLess:
            try container.encode(21, forKey: .rawValue)
        case .opensEvenings:
            try container.encode(22, forKey: .rawValue)
        case .opensSaturdays:
            try container.encode(23, forKey: .rawValue)
        case .parking:
            try container.encode(24, forKey: .rawValue)
        }
    }
    
}
