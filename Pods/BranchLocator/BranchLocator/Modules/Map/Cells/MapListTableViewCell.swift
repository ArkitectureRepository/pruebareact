//
//  MapListTableViewCell.swift
//  BranchLocator
//
//  Created by Tarsha De Souza on 18/06/2019.
//

import UIKit
import SantanderUIKitLib

class MapListTableViewCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = ListViewColor.title.value
            titleLabel.font = ListViewFont.title.value
        }
    }
    @IBOutlet var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.textColor = ListViewColor.subtitle.value
            descriptionLabel.font = ListViewFont.subtitle.value
        }
    }
    @IBOutlet var distanceLabel: UILabel! {
        didSet {
            distanceLabel.textColor = ListViewColor.distanceLabel.value
            distanceLabel.font = ListViewFont.distanceLabel.value
        }
    }
    @IBOutlet var disclosureImageView: UIImageView!
    @IBOutlet weak var openingHoursLbl: UILabel! {
        didSet {
            openingHoursLbl.textColor = ListViewColor.subtitle.value
            openingHoursLbl.font = ListViewFont.subtitle.value
        }
    }
    @IBOutlet weak var circleArrowView: UIView! {
        didSet {
            circleArrowView.layer.cornerRadius = circleArrowView.frame.size.width/2
            circleArrowView.clipsToBounds = true

            circleArrowView.layer.borderColor = UIColor.globileSantanderRed.cgColor
            circleArrowView.layer.borderWidth = 1.0
        }
    }
    @IBOutlet weak var scheduleColorView: UIView! {
        didSet {
            scheduleColorView.backgroundColor = .santanderRed
        }
    }
    @IBOutlet weak var poiTypeIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    func configureCell(with result: POIAnnotation, openingHours: String, circleColor: UIColor) {
        configureIcon(poiAnnotation: result)
        openingHoursLbl.text = openingHours
        scheduleColorView.backgroundColor = circleColor
        self.backgroundColor = .globileWhite
        descriptionLabel.text = result.mapPin.location?.fullAddress.lowercased().capitalized
        let locale = Locale.current
        if locale.usesMetricSystem {
            distanceLabel.text = String(format: "%.2f km", result.mapPin.distanceInKM)
        } else {
            distanceLabel.text = String(format: "%.2f mi", result.mapPin.distanceInMiles)
        }
        var initTitleText = ""
        switch result.mapPin.objectType.code {
        case .corresponsales?:
            initTitleText = localizedString("bl_branchInfoCorresponsales").capitalized
        default:
            initTitleText = localizedString("bl_santander").capitalized
        }
        guard let city = result.mapPin.location?.city else {
            titleLabel.text = initTitleText
            return
        }
        titleLabel.text = "\(initTitleText) - \(city)".capitalized
    }
    func configureIcon(poiAnnotation: POIAnnotation) {
        let objectType = poiAnnotation.mapPin.objectType.code
        poiTypeIcon.image = nil
        if poiAnnotation.mapPin.subType?.code == SubTypeCode.workCafe {
            poiTypeIcon.image = UIImage(resourceName: "Work_Cafe_RED")
        } else {

            switch objectType {
            case .atm:
                poiTypeIcon.image = UIImage(resourceName: "blAtm")
                poiTypeIcon.tintColor = .santanderRed
            case .branch:
                poiTypeIcon.image = UIImage(resourceName: "sanRed")
                poiTypeIcon.tintColor = .santanderRed
            case .corresponsales:
                self.poiTypeIcon.image = UIImage(resourceName: "Partners_RED")
                poiTypeIcon.tintColor = .santanderRed
            default:
                poiTypeIcon.image = UIImage(resourceName: "sanRed")
                poiTypeIcon.tintColor = .santanderRed
            }
        }
    }
}
