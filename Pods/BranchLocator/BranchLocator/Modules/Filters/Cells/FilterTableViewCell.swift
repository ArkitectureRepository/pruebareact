//
//  FilterTableViewCell.swift
//  LocatorApp
//
//  Created by Santander on 17/08/18.
//  Copyright © 2018 Globile. All rights reserved.
//

import UIKit
import SantanderUIKitLib

class FilterTableViewCell: UITableViewCell {
    @IBOutlet weak var icon: FilterSelection!
	@IBOutlet weak var title: UILabel! {
		didSet {
			title.textColor = UIColor.mediumGrey
		}
	}
    public var selectedFont = SantanderFont.bold(size: 18)
    public var unselectedFont = SantanderFont.regular(size: 18)
    public var selectedIcon = UIImage()
    public var unselectedIcon = UIImage()
    public var currentFilter: Filter?
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    func configureCell(with result: Filter) {
        title.text = result.title
        selectedIcon = result.icon(selected: true)
        unselectedIcon = result.icon(selected: false)
        currentFilter = result
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            icon.icon.image = selectedIcon
            icon.tintColor = .santanderRed
            title.font = selectedFont
            icon.changeColors(bgView: FilterTableViewCellColor.iconSelectedBackground.value,
                                  border: FilterTableViewCellColor.iconBorderSelected.value.cgColor)
            self.backgroundColor = FilterTableViewCellColor.tableViewCellBackground.value
        } else {
            icon.icon.image = unselectedIcon
            icon.tintColor = UIColor.mediumGrey
            title.font = unselectedFont
            icon.changeColors(bgView: FilterTableViewCellColor.tableViewCellBackground.value,
                              border: FilterTableViewCellColor.iconBorderUnselected.value.cgColor)
            self.backgroundColor = FilterTableViewCellColor.tableViewCellBackground.value
        }
    }
}
