# Branch Locator  
[![MKT](https://img.shields.io/badge/Branch%20Locator-1.0.0-blue.svg)](https://img.shields.io/badge/Branch%20Locator-1.0.0-blue.svg)
[![MKT](https://img.shields.io/badge/language-Swift-orange.svg)](https://img.shields.io/badge/language-Swift-orange.svg)
[![MKT](https://img.shields.io/badge/platform-iOS-lightgrey.svg)](https://img.shields.io/badge/platform-iOS-lightgrey.svg)
[![MKT](https://img.shields.io/badge/license-Santander-red.svg)](./LICENSE)


![icon.png](Images/icon.png)


---
# Description
This document describes the Globile ATM-Branch Locator.

# Requirements
-   Xcode 10.0+
-   iOS 9.0+
-   Swift 5+

# Installation  
### CocoaPods 
CocoaPods is a dependencies administrator for Cocoa projects. 
```console
$gem install cocopoapods
```

### Podfile 
To integrate BranchLocation you should Create a Podfile, and add your dependencies:
```ruby
platform :ios, ‘9.0’
source "https://github.com/CocoaPods/Specs.git"
source 'git@github.com:globile-software/IS-Core-iOS.git'
source "git@github.com:globile-software/Arch-Components-iOS.git"
source "git@github.com:globile-software/Globile-Spec.git"

target 'Globile' do
    use_frameworks!
    pod 'BranchLocator', '2.2.3'
end
```
Run ```$ pod install ``` in your project directory.
Open ```YourProject.xcworkspace``` and build.

# Usage
### swift 
```swift
import BranchLocator
```
```ruby

let viewController = MapRouter.createModule(shouldShowTitle: true, filtersToApply: [],
                                                    availableFilters: AvailableFilters.defaultWithOutPopularAndOthers(),
                                                    navigationMapsList: [.appleMaps],
                                                    urlLauncher: UIApplication.shared,
                                                    analyticsDelegate: nil) // or AnalyticsHandler.shared
self.navigationController?.pushViewController(viewController, animated: true)

```

### Objective-c 
```ruby
@import BranchLocator
```
```ruby
MapViewController * viewController = [MapRouter createModuleWithShouldShowTitle:NO filters:[] availableFilters:null navigationMapsList:mapList urlLauncher:urlLauncher analyticsDelegate:AnalyticsHandler.shared filterEnabled:YES];    
[self.navigationController pushViewController:viewController animated:YES];
```

* The parameter ```shouldShowTitle``` is optional and you could use to hide or show the navigation view title. Default value is ``` true ```.

* The parameter ```filtersToApply``` does not have a specific usage at the moment.

* The parameter ```availableFilters``` is optional and you could use it to enable the filters that will be available, there is further information in the section below.

* The parameter ```navigationMapsList``` is an array of  ```Maps```  where you can choose which navigation apps can be opened and in which order of priority.

* The parameter ```urlLauncher``` needs a singleton app instance, the recommended value is  ``` UIApplication.shared ```.

* For the parameter ```filterEnabled```, you can show and enable the options related to filter by passing true, or false if you don't want them.

## Choosing your custom filters

* If you set the ```availableFilters``` parameter to nil, this means the defaultAvailableFilters will be applied. Therefore, apply nil if you DO NOT wish to customize filters. 

* To customize filters open ```AvailableFilters.swift``` and there are some functions that you can invoke to set the ```availableFilters``` parameter. 

* (For example: if you want to exclude the popular branches and others Atm filter from showing, set the ```availableFilters``` parameter to ```AvailableFilters.defaultWithOutPopularAndOthers()``` this function will return a FilterTypeProtocol array with the filters that are going to be shown.
```
[
    FilterType.mostPopular,
    FilterType.service,
    FilterType.facilities,
    FilterType.accessibility,
    FilterType.pointsOfInterestWithOutPopularAndOthers   <--- See that this category excludes '.popularPastor' and '.otherATMs' filter.
]
```
## Choosing the navigation apps and its priority
* In order to choose which navigation apps can be opened, you need to set the ```navigationMapsList``` parameter with following elements ```.googleMaps```, ```.waze```  ,```.appleMaps```  inside an array in order of priority ```[.first,.second,.third]```
* ( For example:  ```[ .googleMaps, .waze, .appleMaps]```, in this case, GoogleMaps has the priority to be opened but if the user does not have it installed, then it will try to open it with Waze and so on.
* Notes:  Apple maps application is always set as a default application and as a last choice (If none of the other applications were installed).
* Important note: If the ```.appleMaps``` is set as a first priority it will ignore the others because apple maps application is always available.


## Metrics
You have to implement an AnalyticsHandler. An example of the implementation can be found [here](https://github.com/globile-software/BranchLocator-iOS_App).

## HTML customization
In the branch details, the field Notices and News now support different types of formatting.

## Localizable strings
You must import the file  ``` Localizable.strings ```  of your current country to your main project directory where you can change the values to your suitable localization (This file is located inside the ``` BranchLocator/Assets ``` folder)


# Jira
https://gitlab.alm.gsnetcloud.corp/branchlocator/bl-ios-lib


