//
//  CacheLib.swift
//  Alamofire
//
//  Created by adrian.a.fernandez on 15/01/2019.
//

import Foundation
import Cache

public protocol CacheLibProtocol {
    func saveCacheData<T: Codable>(object: T, forKey key: String, expiry: Expiry) throws
    func saveCacheImage(image: UIImage, forKey key: String, expiry: Expiry) throws
    func clearCache() throws
    func removeCacheData(forKey key: String) throws
    func getCacheData<T: Codable>(forKey key: String, withType type: T.Type) throws -> T?
    func getCacheImage(forKey key: String) throws -> UIImage?
    func setupConfiguration(name: String, expiryTime: TimeInterval, maxSize: UInt)
}

/**
 Main class of component CahceLib
 */
@objc(CacheLib)
public class CacheLib: NSObject, CacheLibProtocol {

    /**
     Singleton static instance
     */
    public static var shared = CacheLib()

    public var configuration: Configuration = Configuration()

    public struct Configuration: Codable {
        var name: String = "SantanderGlobile"
        var expiryTime: TimeInterval = 2*3600
        var maxSize: UInt = 10000
    }

    struct JsonObject: Codable {
        var key: String
        var jsonObject: [String: String]
        var expiryTime: TimeInterval
    }

    private lazy var diskConfig: DiskConfig = {
        var directory: URL?
        do {
            directory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true).appendingPathComponent("MyPreferences")
        } catch {
            print("Error getting URL")
        }
        return DiskConfig(name: configuration.name,
                          expiry: .date(Date().addingTimeInterval(configuration.expiryTime)),
                          maxSize: configuration.maxSize,
                          directory: directory,
                          protectionType: .complete)
    }()

    private lazy var memoryConfig: MemoryConfig = {
        return MemoryConfig(expiry: .date(Date().addingTimeInterval(2*60)), countLimit: 50, totalCostLimit: 0)
    }()

    public lazy var storage: Storage<String>? = {
        return try? Storage(diskConfig: diskConfig, memoryConfig: memoryConfig, transformer: TransformerFactory.forCodable(ofType: String.self))
    }()

    /**
     Function for persist objects thats conform Codable protocol (aka JSON).
     
     - Parameters:
     - object<T>: type of object for persist.
     - key: id of object to persist in String format.
     - expiry: enum thats indicate expire time.
     */
    public func saveCacheData<T: Codable>(object: T, forKey key: String, expiry: Expiry = .never) throws {
        let codableStorage = storage?.transformCodable(ofType: T.self)
        try codableStorage?.setObject(object, forKey: key, expiry: expiry)
    }

    /**
     Function for persist objects thats conform Codable protocol (aka JSON).
     
     - Parameters:
     - image: image object (UIImage) for persist.
     - key: id of object to persist in String format.
     - expiry: enum thats indicate expire time.
     */
    public func saveCacheImage(image: UIImage, forKey key: String, expiry: Expiry = .never) throws {
        let imageStorage = storage?.transformImage()
        try imageStorage?.setObject(image, forKey: key, expiry: expiry)
    }

    /**
     Function to clear cache persistance.
     - Throws: Error
     */
    public func clearCache() throws {
        try storage?.removeAll()
    }

    /**
     Function to clear object persisted.
     
     - Parameters:
     - key: id of object to persist in String format.
     
     - Throws: Error
     */
    public func removeCacheData(forKey key: String) throws {
        try storage?.removeObject(forKey: key)
    }

    /**
     Function for get object persisted thats conform Codable protocol (aka JSON).
     
     - Parameters:
     - key: id of object to persist in String format.
     - type: object type for decode object
     */
    public func getCacheData<T: Codable>(forKey key: String, withType type: T.Type) throws -> T? {
        if let codableStorage = storage?.transformCodable(ofType: T.self) {
            if (try? codableStorage.isExpiredObject(forKey: key)) ?? false {
                try? codableStorage.removeObject(forKey: key)
            }
            return try codableStorage.object(forKey: key)
        } else { return nil }

    }

    /**
     Function for get persisted image objects.
     
     - Parameters:
     - key: id of object to persist in String format.
     */
    public func getCacheImage(forKey key: String) throws -> UIImage? {
        if let imageStorage = storage?.transformImage() {
            if (try? imageStorage.isExpiredObject(forKey: key)) ?? false {
                try? imageStorage.removeObject(forKey: key)
            }
            return try imageStorage.object(forKey: key)

        } else { return nil }
    }

    /**
     Function for setup configuration.
     
     - Parameters:
     - name: Name of disk configuration.
     - expiryTime: Default expiry time(seconds).
     - maxSize: Max size.
     */
    public func setupConfiguration(name: String, expiryTime: TimeInterval, maxSize: UInt) {
        configuration.name = name
        configuration.expiryTime = expiryTime
        configuration.maxSize = maxSize
    }

    /// Function to perform hybrid actions
    ///
    /// - Parameter json: values to perform the hybrid action
    /// - Returns: output of the hybrid action
    @objc public class func hybridInterface(_ json: String) -> String {
        var result: String?

        do {
            let request = try decode(json, type: CacheLibRequest.self)
            let operation = request.operation

            switch operation {
            case .cache:
                var expiryTime = Expiry.never
                if request.expiryTime != nil {
                    expiryTime = Expiry.seconds(Double(request.expiryTime!))
                }
                if let key = request.key {
                    try shared.saveCacheData(object: request.jsonObject, forKey: key, expiry: expiryTime)
                    result = try encode(CacheLibResult(operation: operation))
                } else {
                    result = try encode(CacheLibResult(false, operation: operation))
                }
            case .load:
                if let key = request.key {
                    let plainData = try shared.getCacheData(forKey: key, withType: String.self)
                    result = try encode(CacheLibResult(operation: operation, plainData: plainData))
                } else {
                    result = try encode(CacheLibResult(false, operation: operation))
                }
            case .remove:
                if let key = request.key {
                    try shared.removeCacheData(forKey: key)
                    result = try encode(CacheLibResult(operation: operation))
                } else {
                    result = try encode(CacheLibResult(false, operation: operation))
                }
            case .remove_all:
                try shared.clearCache()
                result = try encode(CacheLibResult(operation: operation))
            }
        } catch {
            do {
                let configuration = try decode(json, type: CacheLibConfiguration.self)
                let name = configuration.name
                let expiryTime = configuration.expireTime
                let maxSize = configuration.maxSize
                shared.setupConfiguration(name: name, expiryTime: TimeInterval(expiryTime), maxSize: UInt(maxSize))
                result = try encode(CacheLibResult(operation: nil))
            } catch let error {
                print(error.localizedDescription)
                result = try? encode(CacheLibResult(false, operation: nil))
            }
        }


        guard let resultValue = result else { return (try? encode(CacheLibResult(false, operation: nil))) ?? "" }
        return resultValue
    }
}

private extension CacheLib {

    static func decode<T: Codable>(_ json: String, type: T.Type) throws -> T {

        guard let data = json.data(using: .utf8) else {
            throw NSError(domain: "Unable to parse action", code: 1, userInfo: nil)
        }

        return try JSONDecoder().decode(T.self, from: data)
    }

    static func encode<T: Codable>(_ result: T) throws -> String {
        let data = try JSONEncoder().encode(result)
        if let result = String(data: data, encoding: .utf8) {
            return result
        }

        throw NSError(domain: "JSON could't be encoded into a string.", code: 1, userInfo: nil)
    }
}
