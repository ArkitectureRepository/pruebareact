import Foundation


public enum CacheLibOperation: String, Codable {
    case cache, load, remove, remove_all
}

public struct CacheLibRequest: Codable {
    var operation: CacheLibOperation // Operations ("cache", "load", "remove", "remove_all")
    var key: String? // alias to store
    var jsonObject: String? // data to cache
    var expiryTime: Double? // time to expire cache data in seconds
}

public struct CacheLibConfiguration: Codable {
    var name: String
    var expireTime: Int
    var maxSize: Int
}

public struct CacheLibResult: Codable {
    var success: Bool // request status
    var operation: CacheLibOperation? // performed operation
    var plainData: String? // data to return

    init(_ success: Bool = true, operation: CacheLibOperation?, plainData: String? = nil) {
        self.success = success
        self.operation = operation
        self.plainData = plainData
    }
}
