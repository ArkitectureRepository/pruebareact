//
import Foundation
import MapKit

public struct GeolocationLibResponse: Codable {
    var latitude: Double = 0
    var longitude: Double = 0
    var altitude: Double = 0
    var accuracy: Double = 0
    var altitude_accuracy: Double = 0
    var heading: String = ""
    var speed: Double = 0
    var timestamp: Date = Date()

    public func toCLLocation() -> CLLocation {
        return CLLocation(latitude: latitude, longitude: longitude)
    }
}
