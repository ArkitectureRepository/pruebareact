//
import Foundation
import CoreLocation
import GlobileUtilsLib

public protocol GeolocationLibDelegate {
    func didReceiveLocation(_ location: GeolocationLibResponse)
    func didReceiveLocation(_ json: String)
}

extension GeolocationLibDelegate {
    public func didReceiveLocation(_ location: GeolocationLibResponse) {}
    public func didReceiveLocation(_ json: String) {}
}

public enum GeolocationLibMode {
    case hybrid
    case native
}

@objc(GeolocationLib)
public class GeolocationLib: NSObject, CLLocationManagerDelegate {

    public let locationManager = CLLocationManager()
    public var delegate: GeolocationLibDelegate?
    public var hybridUsage: Bool = false
    var locationModel = GeolocationLibResponse()

    public init(mode: GeolocationLibMode = .native) {
        super.init()
        locationManager.delegate = self
        switch mode {
        case .hybrid:
            hybridUsage = true
        case .native:
            hybridUsage = false
        }

        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }

    }

    public func fetchCountry(from location: CLLocation, completion: @escaping (_ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion( placemarks?.first?.country,
                       error)
        }
    }

    /// Function to perform hybrid actions
    ///
    /// - Parameter json: values to perform the hybrid action
    /// - Returns: output of the hybrid action

    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            if let location = manager.location {
                getLocationData(location)
                manager.stopUpdatingLocation()
        }
    }
    
    internal func getLocationData(_ location: CLLocation) {
        locationModel.latitude = location.coordinate.latitude
        locationModel.longitude = location.coordinate.longitude
        locationModel.altitude = location.altitude
        locationModel.accuracy = location.horizontalAccuracy
        locationModel.altitude_accuracy = location.verticalAccuracy
        locationModel.speed = location.speed
        if let heading = locationManager.heading {
            locationModel.heading = heading.description
        }
        locationModel.timestamp = location.timestamp
        if hybridUsage {
            delegate?.didReceiveLocation(utils_json_encodeResponse(result: locationModel))
        } else {
            delegate?.didReceiveLocation(locationModel)
        }
    }
}
